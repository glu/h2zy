# H2Zy
## Updated by glu on 18/10/2019
1. PRW.config.root has been updated, I download and merge new samples into old config.root, including all of the samples in the list below 
    https://gitlab.cern.ch/ATLAS-EGamma/Software/PhotonID/RadiativeZ/blob/master/ZllgAnalysis/data/mc16_13TeV.DAOD_EGAMx.25ns.p39xx.txt
    
    the new PRW config files lie in /radZAna/source/H2Zy/data/mc16a_13TeV.Zgam.PRW.config.NEW.root (mc16d and mc16e are also in the directory)
    NOTE: I replace the old path of config files in H2ZyAnalysisMC16_Rel21.cfg with new absolute path, in order to avoid re-compilation. 
          So you have to replace the path with yours, before you run the package.
          （For example: EventHandler.PRW.ConfigFilesMC16aFastSim:  /afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16a_13TeV.Zgam.PRW.config.NEW.root
                         EventHandler.PRW.ConfigFilesMC16aFullSim:  /afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16a_13TeV.Zgam.PRW.config.NEW.root
                         ....
                        Please replace those paths with yours）

2. The CrossSection.xxxxxx in H2ZyAnalysisMC16_Rel21.cfg are not used in the analysis, so I set many of them to 1. Don't mind it.

3. MC16_lumi.txt is the source from which we read Cross-section and other information. I have updated it with information of newly-added samples.
   If there are any new samples to be included, please update this file.