################################################################################
# Package: VertexPositionReweighting
################################################################################

# Declare the package name:
atlas_subdir( VertexPositionReweighting )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthToolSupport/AsgTools
                          Control/xAODRootAccess
                          Event/xAOD/xAODEventInfo
                          Event/xAOD/xAODTruth
                          PhysicsAnalysis/AnalysisCommon/PATInterfaces
)

# Component(s) in the package:
atlas_add_library( VertexPositionReweighting
                   VertexPositionReweighting/*.h Root/*.cxx
                   PUBLIC_HEADERS VertexPositionReweighting
                   LINK_LIBRARIES AsgTools
                                  xAODRootAccess
                                  xAODEventInfo
                                  xAODTruth
                                  PATInterfaces
)
