// $Id$
// vim: ts=3:sw=3:sts=3:expandtab
// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):#include <TFile.h>
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TCanvas.h>
#include <TH1F.h>
// #include <TTreeFormula.h>
// #include <TTree.h>
#include <TLatex.h>
#include <TF1.h>
#include <TStyle.h>
#include <TROOT.h>
// #include <TSystem.h>
#include <TPaveText.h>
// Infrastructure include(s):
#ifdef ROOTCORE
#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#   include "xAODRootAccess/TStore.h"
#endif // ROOTCORE

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
// #include "xAODMuon/MuonContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include "boost/filesystem.hpp"
//#include "boost/filesystem/path.hpp"

// Local include(s):
#include "VertexPositionReweighting/errorcheck.h"
// #include "VertexPositionReweighting/MuonEfficiencyToolExample.h"
// #include "VertexPositionReweighting/MuonSelectionToolExample.h"
// #include "VertexPositionReweighting/MuonSmearingToolExample.h"
#include "VertexPositionReweighting/VertexPositionReweightingTool.h"

// Other include(s):
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicRegistry.h"

void setStyle(float scale);
void afterDraw(TCanvas& can);

int main( int argc, char* argv[] ) {

   // The application's name:
   const char* APP_NAME = argv[ 0 ];

   // Check if we received a file name:
   if( argc < 2 ) {
      Error( APP_NAME, "No file name received!" );
      Error( APP_NAME, "  Usage: %s [xAOD file name]", APP_NAME );
      return 1;
   }

//    int c = (ROOT_VERSION_CODE & 0xFF);
//    int b = (ROOT_VERSION_CODE & 0xFF00) >> 8;
//    int a = (ROOT_VERSION_CODE & 0xFF0000) >> 16;
//    Info( APP_NAME, "ROOT_VERSION_CODE: ROOT_VERSION(%d,%d,%d)", a, b, c);
//    Info( APP_NAME, "ROOT_VERSION_CODE: %d.%02d/%02d", a, b, c);
//    Info( APP_NAME, "ROOT_RELEASE: %s", ROOT_RELEASE);
// #ifdef ROOTCORE_RELEASE_SERIES
//    Info( APP_NAME, "ROOTCORE_RELEASE_SERIES: %d", ROOTCORE_RELEASE_SERIES);
// #else
//    Info( APP_NAME, "ROOTCORE_RELEASE_SERIES: (undefined)");
// #endif
//    return 0;

   // Initialise the application:
   CHECK( xAOD::Init( APP_NAME ) );

   // Open the input file:
   const TString fileName = argv[ 1 ];
   Info( APP_NAME, "Opening file: %s", fileName.Data() );
   std::auto_ptr< TFile > ifile( TFile::Open( fileName, "READ" ) );
   CHECK( ifile.get() );

   // Create a TEvent object:
   xAOD::TEvent event( xAOD::TEvent::kClassAccess );
   CHECK( event.readFrom( ifile.get() ) );
   Info( APP_NAME, "Number of events in the file: %i",
         static_cast< int >( event.getEntries() ) );
   

   // const std::string release;
   // event.retrieveMetaInput(&release, "/TagInfo/AtlasRelease");
   // Info( APP_NAME, "Release: %s", release.c_str());
 
   // TTree* metaData = (TTree*) ifile->Get("MetaData");V
   // //TTreeFormula tf("tf","EventBookkeepers.m_nWeightedAcceptedEvents",metaData);
   // TTreeFormula tf("tf","TagInfo/AtlasRelease",metaData);
   // metaData->LoadTree(0);
   // tf.UpdateFormulaLeaves();
   // tf.GetNdata();
   // const char* tmp = tf.EvalStringInstance(1);
   // std::cout << "tmp " << tmp << std::endl;

   // TFile* outFile = TFile::Open("myoutput.root", "RECREATE");
   // event.writeTo(outFile);
   // Create a transient object store. Needed for the tools.
   xAOD::TStore store;
       
   // Decide how many events to run over:
   Long64_t entries = event.getEntries();
   if( argc > 2 ) {
      const Long64_t e = atoll( argv[ 2 ] );
      if( e < entries ) {
         entries = e;
      }
   }

   /* // Create the tool(s) to test: */
   // CP::MuonSelectionToolExample selTool( "MuonSelector" );
   // CP::MuonSmearingToolExample corrTool( "MuonCorrector" );
   // CP::MuonEfficiencyToolExample effTool( "MuonEfficiency" );
   CP::VertexPositionReweightingTool vtxTool( "VertexPosition" );

   vtxTool.msg().setLevel(MSG::DEBUG);
   //vtxTool.setProperty("WeightsFileName", "lala.root");
   //CHECK(vtxTool.setProperty("MCProductionTag", "s1504_s1482_s1299"));
   //
   // auto canonical = boost::filesystem::canonical(fileName.Data());
   // auto dirAndFile = canonical.parent_path().filename() / canonical.filename();
   // Info( APP_NAME, "Canonical path: %s", canonical.filename().string().c_str());
   // Info( APP_NAME, "Canonical path: %s", temp.c_str());
   // Info( APP_NAME, "Canonical path: %s", blah.c_str());
   //CHECK(vtxTool.setProperty("MCProductionTag", "s1504_s1482"));
   // CHECK(vtxTool.setProperty("MCProductionTag", dirAndFile.c_str()));
   
   // CHECK(vtxTool.setProperty("DataTag", "IndetBeampos-14TeV-SigmaXY12um-SigmaZ50mm-001"));
   //CHECK(vtxTool.setProperty("DataMean", 10.));
   //CHECK(vtxTool.setProperty("DataSigma", 20.));
   CHECK(vtxTool.setProperty("DataMean", 30.));
   CHECK(vtxTool.setProperty("DataSigma", 80.));
   // CHECK(vtxTool.setProperty("MCMean", -12.27));
   // CHECK(vtxTool.setProperty("MCSigma", 57.22));

   CHECK(vtxTool.initialize());

   TH1F hFirstVertexZ("hFirstVertexZ", "hFirstVertexZ", 50, -300, 300);
   hFirstVertexZ.SetTitle("All Events, first vertex");
   hFirstVertexZ.SetXTitle("TruthVertex z position [mm]");
   hFirstVertexZ.SetYTitle("Entries");
   
   TH1F hFirstVertexZRew("hFirstVertexZRew", "hFirstVertexZRew", 50, -300, 300);
   hFirstVertexZRew.SetTitle("All Events, first vertex (reweighted)");
   hFirstVertexZRew.SetXTitle("TruthVertex z position [mm]");
   hFirstVertexZRew.SetYTitle("Entries");

   // Loop over the events:
   for( Long64_t entry = 0; entry < entries; ++entry ) {

      // Tell the object which entry to look at:
      event.getEntry( entry );

      // Print some event information for fun:
      //event.copy("EventInfo");
      const xAOD::EventInfo* ei = 0;
      CHECK( event.retrieve( ei, "EventInfo" ) );

      // Info( APP_NAME, "%f +- %f", ei->beamPosZ(), ei->beamPosSigmaZ());

      // Info( APP_NAME,
            // "===>>>  start processing event #%i, "
            // "run #%i %i events processed so far  <<<===",
            // static_cast< int >( ei->eventNumber() ),
            // static_cast< int >( ei->runNumber() ),
            // static_cast< int >( entry ) );
            //
      // const xAOD::EventInfo* ei2 = ei;
      if (entry % 50 == 0) {
         Info( APP_NAME,
            "===>>>  start processing event #%i, "
            "run #%i %i events processed so far  <<<===",
            static_cast< int >( ei->eventNumber() ),
            static_cast< int >( ei->runNumber() ),
            static_cast< int >( entry ) );
      }
      //const xAOD::
      const xAOD::TruthVertexContainer* vertices = 0;
      //CHECK( event.retrieve(vertices, "TruthVertex") );
   #if defined(ROOTCORE_RELEASE_SERIES) && ROOTCORE_RELEASE_SERIES >= 23
      const std::string truthVertexContainer = "TruthVertices";
   #else
      const std::string truthVertexContainer = "TruthVertex";
   #endif
      auto hasTruthVertex = event.retrieve(vertices, truthVertexContainer);
      Info (APP_NAME, "hasTruthVertex: %s", hasTruthVertex ? "true" : "false");


      // Info( APP_NAME, "Number of vertices: %i",
            // static_cast< int >(vertices->size()) );
 
      // if (entry == 0) {
         // hVertexZ.SetTitle(TString::Format("Run %i, Event %i (Entry %i)",
                  // ei->runNumber(),
                  // (int)ei->eventNumber(), (int)entry));
      // }

      // const std::vector<float>& weights = ei->mcEventWeights();
      // for (unsigned long i = 0; i < weights.size(); i++) {
      //    Info(APP_NAME, "weight %lu: %f", i, weights[i]);
      // }



      // double weight = ei->mcEventWeight(0);
      double vtxWeight = 1.0;
      switch (vtxTool.getWeight(vtxWeight)) {
         case CP::CorrectionCode::Error:
            Error(APP_NAME, "Error getting vtx correction");
            break;
         case CP::CorrectionCode::OutOfValidityRange:
            Info(APP_NAME, "Warning, out of validity range");
            break;
         case CP::CorrectionCode::Ok: break;
      }
      if (entry % 100 == 0)
         Info(APP_NAME, "vtxWeight: %f", vtxWeight);

      if (hasTruthVertex) {
         for (auto vtx: *vertices) {
            double vtx_z = vtx->z();
            if (vtx_z != 0) {
               Info (APP_NAME, "Filling vtx_z = %f", vtx_z);
               hFirstVertexZ.Fill(vtx_z);
               hFirstVertexZRew.Fill(vtx_z, vtxWeight);
               break;
            }
         }
      }

   }


   float scale = 1.3f;
   //float scalex = scale;
   setStyle(scale);
   float fitmax = 300.f;

   //double integral = hFirstVertexZ.GetSumOfWeights()*12;//Integral("width");
   double integral = hFirstVertexZ.Integral("width");
   std::cout << "integral " << integral << std::endl;
   std::cout << "events " << hFirstVertexZ.GetEntries() << std::endl;
   double norm = integral / (vtxTool.getSourceSigma() * sqrt(2*TMath::Pi()));
   TF1 sourceFun("sourceFun", "gaus(0)", -300, 300);
   sourceFun.SetParameters(norm, vtxTool.getSourceMean(), vtxTool.getSourceSigma());
   sourceFun.SetLineColor(kBlue);
   sourceFun.SetLineWidth(2);
   sourceFun.SetNpx(1000);
   
   //TF1 sourceFun2("sourceFun2", "gaus(0)", -300, 300);
   TF1 sourceFun2("sourceFun2", "[0]*0.3989422804/[2] * exp(-0.5*((x-[1])/[2])^2)", -300, 300);
   sourceFun2.SetParNames("Integral", "Mean", "Sigma");
   //sourceFun2.SetParameters(norm, vtxTool.getSourceMean(), vtxTool.getSourceSigma());
   sourceFun2.SetParameters(integral, vtxTool.getSourceMean(), vtxTool.getSourceSigma());
   sourceFun2.FixParameter(0, integral);
   sourceFun2.SetLineColor(kBlue);
   sourceFun2.SetLineStyle(kDashed);
   sourceFun2.SetLineWidth(1);
   sourceFun2.SetNpx(1000);
   hFirstVertexZ.Fit(&sourceFun2, "r", "", -fitmax, fitmax);

   TF1 targetFun("targetFun", "gaus(0)", -300, 300);
   integral = hFirstVertexZRew.Integral("width");
   norm = integral / (vtxTool.getTargetSigma() * sqrt(2*TMath::Pi()));
   targetFun.SetParameters(norm, vtxTool.getTargetMean(), vtxTool.getTargetSigma());
   targetFun.SetLineColor(kRed);
   targetFun.SetLineWidth(2);
   targetFun.SetNpx(1000);
   
   // TF1 targetFun2("targetFun2", "gaus(0)", -300, 300);
   TF1 targetFun2("targetFun2", "[0]*0.3989422804/[2] * exp(-0.5*((x-[1])/[2])^2)", -300, 300);
   //TF1 targetFun2("targetFun2", "0.3989422804/[1] * exp(-0.5*((x-[0])/[1])^2)", -300, 300);
   targetFun2.SetParNames("Integral", "Mean", "Sigma");
   //targetFun2.SetParameters(norm, vtxTool.getTargetMean(), vtxTool.getTargetSigma());
   targetFun2.SetParameters(integral, vtxTool.getTargetMean(), vtxTool.getTargetSigma());
   targetFun2.FixParameter(0, integral);
   targetFun2.SetLineColor(kRed);
   targetFun2.SetLineStyle(kDashed);
   targetFun2.SetLineWidth(1);
   targetFun2.SetNpx(1000);
   hFirstVertexZRew.Fit(&targetFun2, "r", "", -fitmax, fitmax);


   gStyle->SetOptStat("niemr");

   // double maximum = 1.10 * std::max({
   double maximum = 1.30 * std::max({
      hFirstVertexZ.GetMaximum(),
      hFirstVertexZRew.GetMaximum(),
      sourceFun.GetMaximum(),
      targetFun.GetMaximum()
   });

   hFirstVertexZ.SetMaximum(maximum);
   hFirstVertexZRew.SetMaximum(maximum);

   // gROOT->LoadMacro("~/atlasstyle-00-03-05/AtlasStyle.C");
   // gROOT->ProcessLine("SetAtlasStyle();");

   TLatex l;
   // hVertexZ.Draw();
   // can.SaveAs("hVertexZ.png");

   l.SetTextFont(42);
   // l.SetTextAlign(11); // left, bottom
   l.SetTextAlign(13); // left, top
   l.SetTextSize(gStyle->GetTitleFontSize());
         //-)0.035f*scale);
   // l.DrawLatexNDC(0.12, 0.83, TString::Format("\\text{Source:}\\, %.2f \\pm %.2f", vtxTool.getSourceMean(), vtxTool.getSourceSigma()));
   // l.DrawLatexNDC(0.12, 0.76, TString::Format("\\text{Target:}\\, %.2f \\pm %.2f", vtxTool.getTargetMean(), vtxTool.getTargetSigma()));
   //
   
   auto dataTag = *(vtxTool.getProperty<std::string>("DataTag"));
   if (dataTag == "")
      dataTag = "set manually";

   for (int i = 0; i < 3; i++) {
      TCanvas can;
      TH1* histo;
      TF1* fit;
      std::string filename;

      switch (i) {
      case 0: histo = &hFirstVertexZ;
              fit = &sourceFun2;
              filename = "hFirstVertexZ.png";
              break;
      case 1: histo = &hFirstVertexZRew;
              fit = &targetFun2;
              filename = "hFirstVertexZRew.png";
              break;
      case 2: histo = &hFirstVertexZ;
              fit = &sourceFun2;
              filename = "hFirstVertexZSourceOnly.png";
              break;
      }
   
      histo->SetMinimum(0);
      histo->UseCurrentStyle();
      histo->SetLineWidth(2);
      histo->SetMarkerStyle(kFullCircle);

      histo->Draw("E");
      afterDraw(can);

      float tx, ty;
      tx = gStyle->GetTitleX();
      ty = gStyle->GetTitleY();
      l.SetTextColor(kBlue);
      ty-=0.07; l.DrawLatexNDC(tx, ty, TString::Format("Source: %.2f #pm %.2f", vtxTool.getSourceMean(), vtxTool.getSourceSigma()));
      if (i < 2) {
         l.SetTextColor(kRed);
         ty-=0.07; l.DrawLatexNDC(tx, ty, TString::Format("Target: %.2f #pm %.2f (%s)", vtxTool.getTargetMean(), vtxTool.getTargetSigma(), dataTag.c_str()));
      }
      l.SetTextColor(kBlack);
      ty-=0.07; l.DrawLatexNDC(tx, ty, TString::Format("Fit: %.2f #pm %.2f", fit->GetParameter("Mean"), fit->GetParameter("Sigma")));

      sourceFun.Draw("same");
      if (i < 2) {
         targetFun.Draw("same");
      }
      can.SaveAs(filename.c_str());
   }

   return 0;
}
void setStyle(float scale) {
   float scalex = scale;
   gStyle->SetTitleSize(0.035f*scale, "xyz");
   gStyle->SetTitleSize(0.035f*scale, "t");
   gStyle->SetTitleXOffset((float) sqrt(scalex));
   gStyle->SetTitleYOffset((float) sqrt(scale));
   gStyle->SetTitleFont(42, "xyz");
   gStyle->SetTitleFont(42, "t");
   gStyle->SetLabelSize(0.035f*scale, "xyz");
   gStyle->SetPadLeftMargin(0.1f*scalex);
   gStyle->SetPadTopMargin(0.02f*scale);
   gStyle->SetPadRightMargin(0.02f*scalex);
   gStyle->SetPadBottomMargin(0.1f*scale);
   gStyle->SetLineWidth(static_cast<short>(std::round(1.2*scale)));
   gStyle->SetTitleX(0.1f*scalex+0.03f*scalex);
   gStyle->SetTitleY(1.0f-0.04f*scale);
   gStyle->SetTitleFontSize(0.035f*scale);
   gStyle->SetTitleW(1.0f - gStyle->GetPadLeftMargin()-gStyle->GetPadRightMargin()-0.05f*scalex);
   gStyle->SetTitleH(gStyle->GetTitleSize("t"));
   gStyle->SetOptDate(1);

   // gStyle->SetTitleBorderSize(1);
   gStyle->SetTitleAlign(12);
}

void afterDraw(TCanvas& can) {
   can.Update();
   auto ttt = dynamic_cast<TPaveText*>(can.FindObject("title"));
   ttt->SetMargin(0);
   ttt->SetTextAlign(13);
   ttt->SetCornerRadius(0);
   ttt->SetTextSize(gStyle->GetTitleFontSize());
   can.Modified();
}

