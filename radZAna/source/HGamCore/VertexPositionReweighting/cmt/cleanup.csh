# echo "cleanup VertexPositionReweighting VertexPositionReweighting-00-00-00 in /afs/cern.ch/user/j/jmansour/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/2.3.10/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtVertexPositionReweightingtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtVertexPositionReweightingtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=VertexPositionReweighting -version=VertexPositionReweighting-00-00-00 -path=/afs/cern.ch/user/j/jmansour/workarea  $* >${cmtVertexPositionReweightingtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt cleanup -csh -pack=VertexPositionReweighting -version=VertexPositionReweighting-00-00-00 -path=/afs/cern.ch/user/j/jmansour/workarea  $* >${cmtVertexPositionReweightingtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtVertexPositionReweightingtempfile}
  unset cmtVertexPositionReweightingtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtVertexPositionReweightingtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtVertexPositionReweightingtempfile}
unset cmtVertexPositionReweightingtempfile
exit $cmtcleanupstatus

