// $Id$
// vim: ts=3:sw=3:sts=3:expandtab

#include "VertexPositionReweighting/VertexPositionReweightingTool.h"

// xAOD EDM
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

// Root headers
#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"
#include "TF1.h"

// JSON parser
#include "picojson.h"

// System headers
#include <fstream>

namespace CP {

   #if defined(ROOTCORE_RELEASE_SERIES) && ROOTCORE_RELEASE_SERIES >= 23
      const std::string VertexPositionReweightingTool::s_truthVertexContainer = "TruthVertices";
      const std::string VertexPositionReweightingTool::s_truthEventContainer = "TruthEvents";
   #else
      const std::string VertexPositionReweightingTool::s_truthVertexContainer = "TruthVertex";
      const std::string VertexPositionReweightingTool::s_truthEventContainer = "TruthEvent";
   #endif

   VertexPositionReweightingTool::VertexPositionReweightingTool(const std::string& name)
      : asg::AsgTool(name)
   {
      const double NaN = std::numeric_limits<double>::quiet_NaN(); 
      declareProperty("DataMean",  m_dataMean=NaN,  "Mean of beamspot for data (target)");
      declareProperty("DataSigma", m_dataSigma=NaN, "Sigma of beamspot for data (target)");
      declareProperty("McMean",    m_mcMean=NaN,    "Mean of beamspot for MC (source)");
      declareProperty("McSigma",   m_mcSigma=NaN,   "Sigma of beamspot for MC (source)");
      declareProperty("DataTag",   m_dataTag,       "Conditions tag for data (target) beamspot");
      declareProperty("BeamspotFile", m_beamspotFile="data/beamspot.json", "File containing beamspot information from conditions database");
   }
   
   StatusCode VertexPositionReweightingTool::initialize() {
      ATH_MSG_INFO("Initializing VertexPositionReweightingTool");

      if (!std::isnan(m_mcMean))     ATH_MSG_DEBUG("Manually set McMean=" << m_mcMean << ", this should be only used for testing.");
      if (!std::isnan(m_mcSigma))    ATH_MSG_DEBUG("Manually set McSigma=" << m_mcSigma << ", this should be only used for testing.");

      if (m_dataTag.empty()) {
         if (std::isnan(m_dataMean) || std::isnan(m_dataSigma)) {
            ATH_MSG_ERROR("Must set either DataMean and DataSigma, or DataTag.");
            return StatusCode::FAILURE;
         }
         ATH_MSG_DEBUG("Manually set DataMean=" << m_dataMean);
         ATH_MSG_DEBUG("Manually set DataSigma=" << m_dataSigma);
      } else {
         std::ifstream input(m_beamspotFile);
         if (!input.is_open()) {
            ATH_MSG_ERROR("Could not open beamspot info file '" << m_beamspotFile << "'.");
            return StatusCode::FAILURE;
         }
         
         picojson::value v;
         input >> v;

         auto dict = v.get<picojson::object>();
         for (auto it: dict) {
            ATH_MSG_INFO(it.first << ": " << it.second.to_str());
         }

         auto vBeamspots = v.get(m_dataTag);
         auto vBeamspot = vBeamspots.get(0).get<picojson::object>();
         m_dataMean = vBeamspot["posZ"].get<double>();
         m_dataSigma = vBeamspot["sigmaZ"].get<double>();
         ATH_MSG_DEBUG("Successfully loaded beamspot info from '" << m_beamspotFile << "'.");
      }
      
      ATH_MSG_DEBUG("Requested data beamspot: " << m_dataMean << " ± " << m_dataSigma);

      return StatusCode::SUCCESS;
   }

   void VertexPositionReweightingTool::loadMCBeamspot() {
      static bool mcBeamspotLoaded = false;
      if (mcBeamspotLoaded)
         return;
      
      const xAOD::EventInfo* ei = 0;
      if (evtStore()->retrieve( ei, "EventInfo" ).isFailure()) {
         throw std::runtime_error("Could not retrieve EventInfo");
      }
      if (std::isnan(m_mcMean))
         m_mcMean = ei->beamPosZ();
      if (std::isnan(m_mcSigma))
      m_mcSigma = ei->beamPosSigmaZ();
      ATH_MSG_DEBUG("Generated MC beamspot: " << m_mcMean << " ± " << m_mcSigma);

      mcBeamspotLoaded = true;
   }

   CorrectionCode VertexPositionReweightingTool::getWeight(const double vxp_z, double& weight) {
      loadMCBeamspot();

      double pData = TMath::Gaus(vxp_z, m_dataMean, m_dataSigma, true);
      double pMC = TMath::Gaus(vxp_z, m_mcMean, m_mcSigma, true);
      weight = pData/pMC;
      
      const double maxWeight = 100.;
      weight = std::max(1./maxWeight, std::min(maxWeight, weight));
      return CorrectionCode::Ok;
   }
   
   double VertexPositionReweightingTool::getVertexPosition() {
      // ATH_MSG_DEBUG ("VertexPositionReweightingTool::getVertexPosition()");
      // try directly
      const xAOD::TruthEventContainer* tec;
      StatusCode rc = evtStore()->retrieve<xAOD::TruthEventContainer>(tec, s_truthEventContainer);
      if (rc.isSuccess()) {
         ATH_MSG_DEBUG ("TruthEventContainer: number of truth events: " << tec->size());
         for (auto te: *tec) {

            auto tvl = te->signalProcessVertexLink();
            ATH_MSG_DEBUG ("For this TruthEvent:");
            ATH_MSG_DEBUG ("  signalProcessVertexLink isValid(): " << (tvl.isValid()?"true":"false"));
            ATH_MSG_DEBUG ("  signalProcessVertex: " << (long)te->signalProcessVertex());

            ATH_MSG_DEBUG ("  number of truth vertices " << te->nTruthVertices());

            // this doesnt seem to work:
            auto tv = te->signalProcessVertex();
            // auto tv = te->truthVertex(0);
            if (!tv) {
               ATH_MSG_DEBUG ("tv == 0");
               continue;
            }
            return tv->z();
         }
         // int i = 0;
         // std::cout << "len " << tec->size() << std::endl;
         // for (auto te: *tec) {
            // auto tv = te->signalProcessVertex();
            // if (!tv) continue;
            // std::cout << std::endl;
            // std::cout << "tv " << (void*)(tv) << std::endl;
            // std::cout << i++ << ": " << tv->z() << std::endl;
         // }
      // } else {
         // std::cout << "Couldn't retrieve TruthEventContainer" << std::endl;
      }

      // try old method
      ATH_MSG_INFO ("Couldn't retrieve vertex via TruthEventContainer, trying old method via TruthVertexContainer.");

      const xAOD::TruthVertexContainer* vertices = 0;
      rc = evtStore()->retrieve<xAOD::TruthVertexContainer>(vertices, s_truthVertexContainer);
      if (rc.isSuccess()) {
         xAOD::TruthVertexContainer::const_iterator vtx_it;
         xAOD::TruthVertexContainer::const_iterator vtx_begin = vertices->begin();
         for (vtx_it = vtx_begin; vtx_it != vertices->end(); ++vtx_it) {
            // Info (APP_NAME, "  Vertex Z position: %g", (*vtx_it)->z());
            double vtx_z = (*vtx_it)->z();
            if (vtx_z != 0) return vtx_z;
         }
      // } else {
         // std::cout << "Couldn't retrieve TruthVertexContainer" << std::endl;
      }
      
      throw std::runtime_error( "Could not retrieve primary vertex position" );
   }


   /// Gets the weight for the current event.
   CorrectionCode VertexPositionReweightingTool::getWeight(double& weight)
   {
      try {
         double vtx_z = getVertexPosition();
         // ATH_MSG_DEBUG("vertex position: " << vtx_z);
         return getWeight(vtx_z, weight);
      }
      catch (const std::runtime_error&) {
         return CorrectionCode::Error;
      }
   }

} // namespace CP

