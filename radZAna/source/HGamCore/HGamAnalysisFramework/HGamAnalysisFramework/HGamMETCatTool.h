#pragma once

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"

namespace HG {

  class HGamMETCatTool {
  private:

    virtual HG::EventHandler *eventHandler();

    float m_metCutHigh       = 100.0;
    float m_metCutLow        = 50.0;
    float m_yy_ptCut         = 100.0;
    float m_pthardCut        = 40.0;
    float m_yy_MoriondptRest = 15.0;

    TString mv2_WP_Name = "MV2c10_FixedCutBEff_70";


    double m_metCatWeight = 1.0;


    bool PassesHMHP(xAOD::PhotonContainer    photons, xAOD::MissingETContainer met);
    bool PassesHMLP(xAOD::PhotonContainer    photons, xAOD::MissingETContainer met);
    bool PassesIntMET(xAOD::PhotonContainer    photons, xAOD::JetContainer       jets, xAOD::MissingETContainer met);
    bool Passes1Lep(xAOD::ElectronContainer  electrons, xAOD::MuonContainer      muons);
    bool Passes2LepOrMore(xAOD::ElectronContainer  electrons, xAOD::MuonContainer      muons);
    bool Passes3JetOrMore(xAOD::JetContainer       jets);
    bool PassesMoriondRest(xAOD::PhotonContainer    photons);
    bool Passes2JetOrMore1bJetOrMore(xAOD::JetContainer   jets);

    double getLeptonSFs(xAOD::ElectronContainer electrons, xAOD::MuonContainer muons);

  public:
    HGamMETCatTool(HG::EventHandler *eventHandler);
    virtual ~HGamMETCatTool();

    virtual EL::StatusCode initialize(Config &config);

    const std::vector<TString> m_METCatNames = {"NoCat", "HMHP", "HMLP", "IntMET", "Rest", "RR"};
    enum m_METCatEnum {Moriond_NOCAT  = 0, Moriond_HMHP   = 1, Moriond_HMLP  = 2, Moriond_IntMET = 3, Moriond_Rest = 4, Moriond_RR  = 5};

    std::pair<int, float> getEventInfoCategoryAndWeight();

    std::pair<int, float> getCategoryAndWeight(xAOD::PhotonContainer    photons,
                                               xAOD::ElectronContainer  electrons,
                                               xAOD::MuonContainer      muons,
                                               xAOD::JetContainer       jets,
                                               xAOD::MissingETContainer met);

    void saveCategoryAndWeight(xAOD::PhotonContainer    photons,
                               xAOD::ElectronContainer  electrons,
                               xAOD::MuonContainer      muons,
                               xAOD::JetContainer       jets,
                               xAOD::MissingETContainer met);

    HG::EventHandler          *m_eventHandler; //!


  };

}
