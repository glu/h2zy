#pragma once

// ASG framework includes
#include "AsgTools/AsgTool.h"

// Local includes
#include "HGamAnalysisFramework/IHGamHelperTool.h"

namespace HG {

  /// Implementation for the cross-section tool
  ///
  /// Takes a list of objects and saves those necessary
  /// for the cross-section analysis.
  ///
  /// @author Christopher Meyer <chris.meyer@cern.ch>
  ///
  class HGamXSecTool : public virtual IHGamHelperTool,
    public asg::AsgTool {

    /// Create a proper constructor for Athena
    ASG_TOOL_CLASS(HGamXSecTool, HG::IHGamHelperTool)

  private:
    /// @name Function(s) doing the heavy lifting
    /// @{

    /// Save extra variables necessary for possible detailed studies
    StatusCode saveBaselineVariables(bool isTruth = false);

    /// @}

  public:
    HGamXSecTool(const std::string &name);
    virtual ~HGamXSecTool();

    /// @name Function(s) implementing the asg::IAsgTool interface
    /// @{

    /// Function initialising the tool
    virtual StatusCode initialize();

    /// @}

    /// @name Function(s) implementing the IHGamHelperTool interface
    /// @{

    /// Save all variables only needed for systematic shifts
    StatusCode saveSystematicVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                       xAOD::ElectronContainer  *electrons = nullptr,
                                       xAOD::MuonContainer      *muons     = nullptr,
                                       xAOD::JetContainer       *jets      = nullptr,
                                       xAOD::MissingETContainer *mets      = nullptr);

    /// Save all standard variables not included in systematic subset
    StatusCode saveStandardVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                     xAOD::ElectronContainer  *electrons = nullptr,
                                     xAOD::MuonContainer      *muons     = nullptr,
                                     xAOD::JetContainer       *jets      = nullptr,
                                     xAOD::MissingETContainer *mets      = nullptr);

    /// Save extra variables necessary for possible detailed studies
    StatusCode saveDetailedVariables(xAOD::PhotonContainer    *photons   = nullptr,
                                     xAOD::ElectronContainer  *electrons = nullptr,
                                     xAOD::MuonContainer      *muons     = nullptr,
                                     xAOD::JetContainer       *jets      = nullptr,
                                     xAOD::MissingETContainer *mets      = nullptr);

    /// Save extra variables necessary for possible detailed studies
    StatusCode saveTruthVariables(xAOD::TruthParticleContainer *photons   = nullptr,
                                  xAOD::TruthParticleContainer *electrons = nullptr,
                                  xAOD::TruthParticleContainer *muons     = nullptr,
                                  xAOD::JetContainer           *jets      = nullptr,
                                  xAOD::MissingETContainer     *mets      = nullptr);

    /// @}

  }; // class HGamXSecTool

} // namespace HG
