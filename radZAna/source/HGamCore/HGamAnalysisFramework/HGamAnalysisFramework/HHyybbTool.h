#pragma once

// STL include(s):
#include <vector>

// EDM include(s):
#include "AsgTools/AsgMessaging.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"

// ROOT include(s):
#include "TLorentzVector.h"
#include "TString.h"

// Forward declarations
namespace TMVA { class Reader; }
namespace HG { class EventHandler; class TruthHandler; }
namespace xAOD { class hhWeightTool; class TEvent; }
namespace lwt { class LightweightNeuralNetwork; }


namespace HG {
  /*!
     @class HHyybbTool
     @brief The main class for the HH -> yybb analysis.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the HH -> yybb analysis.
   */
  class HHyybbTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler);

    /**
     * @brief Destructor
     */
    virtual ~HHyybbTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config Set of options
     */
    virtual EL::StatusCode initialize(Config &config);

    /**
     * @brief Function to save all the yybb information (Nominal by default)
     * @details
     *
     * @param photons Container with all photons
     * @param electrons Container with all electrons (before overlap removal)
     * @param muons Container with all muons (before overlap removal)
     * @param jets Container with all jets
     * @param jets_no_JVT Container with all jets (before JVT cut)
     */
    void saveHHyybbInfo(const xAOD::PhotonContainer &photons,
                        const xAOD::ElectronContainer &electrons,
                        const xAOD::MuonContainer &muons,
                        xAOD::JetContainer &jets,
                        const xAOD::JetContainer &jets_no_JVT);

    /*!
       @fn EL::StatusCode writeContainers()
       @brief Write jet containers to the output files
     */
    EL::StatusCode writeContainers();

    /*!
       @enum jetPreselectionEnum
       @brief Number of jets in the event that pass the b-tagging working point cut
       @var discrete
       @brief 0  = use the MV2c10 bin obtained through checking which WPs pass and fail
       @var continuous
       @brief 1  = used the continuous MV2c10 score
     */
    enum jetPreselectionEnum {discrete = 0, continuous = 1, discretePT = 2};

    /*!
       @enum leptonCorrectionEnum
       @brief Muon correction category enumeration
       @var allMu
       @brief 0 = correct the jet 4v using all muons in it (i.e. with dR < 0.4)
       @var allLep
       @brief 1 = correct the jet 4v using all muons and electrons in it (i.e. with dR < 0.4)
       @var noLepCor
       @brief 2 = do not correct the jet 4v
     */
    enum leptonCorrectionEnum {allMu = 0, allLep = 1, noLepCor = 2};

    /*!
       @enum analysisSelectionEnum
       @brief analysis selection category enumeration
       @var cutBased
       @brief 0 = simple cut based analysis
     */

    enum analysisSelectionEnum { cutBased = 0 };


    /*!
       @enum yybbCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum yybbCutflowEnum {CENJETS = 0, TAGGING = 1, BPT = 2, BBMASS = 3, YYMASS = 4, PASSYYBB = 5};


  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!
    std::unique_ptr<xAOD::hhWeightTool> m_hhWeightTool; //!
    std::unique_ptr<lwt::LightweightNeuralNetwork> m_preselectionNN; //!

    // Algorithm setup
    bool m_detailedHHyybb;       //! Are we running a fully detailed HHyybb analysis?

    // User-configurable: jet selection and muon corrections
    float m_jet_eta_max;         //! Maximum eta of jets
    float m_muon_pTmin;          //! Minimum pT of muons
    float m_muon_DRmax;          //! Maximum deltaR between muons and the jet
    float m_electron_pTmin;      //! Minimum pT of muons
    float m_electron_DRmax;      //! Maximum deltaR between muons and the jet

    // Jet pT cuts
    float m_j1_pTMin; //! (in GeV)
    float m_j2_pTMin; //! (in GeV)

    // Mass cuts
    float m_massHiggs; //! what we consider the exact Higgs mass to be (in GeV)
    float m_mjj_min;   //! lower bound of mbb window (in GeV)
    float m_mjj_max;   //! upper bound of mbb window (in GeV)
    float m_myy_min;   //! lower bound of tight myy window (in GeV)
    float m_myy_max;   //! upper bound of tight myy window (in GeV)

    // Jet related quantities required for perform selections
    int m_nBJets85; //! number of b-jets at the 85% wp
    int m_nBJets70; //! number of b-jets at the 70% wp
    int m_nCentralJets; //! number of central jets
    float m_weightBTagging; //! event weight from b-tagging

    // Maps for per-event outputs
    std::map<TString, float> m_eventFloats;
    std::map<TString, int>   m_eventInts;
    // only written out in the case of yybb-Detailed
    std::map<TString, float> m_eventFloatsDetailed;

    // Vectors of aux data to save as jet decorations (some slimming wrt the original number of decorations is needed)
    std::vector<std::string> m_auxIntsToSave;
    std::vector<std::string> m_auxFloatsToSave;
    std::vector<std::string> m_auxDoublesToSave;

    // Which selections are we running (determined by the m_detailedHHyybb flag)
    std::vector<jetPreselectionEnum> m_jetPreselections;
    std::vector<leptonCorrectionEnum> m_leptonCorrections;
    std::vector<analysisSelectionEnum> m_analysisSelections;

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /**
     * @brief Function which performs full yybb selection and saves info to info maps (nominal by default)
     * @param photons Container with all photons
     * @param jetPreSelCat Which jet preselection to use
     */
    void performSelection(const xAOD::PhotonContainer &photons, const jetPreselectionEnum jetPreSelCat);

    /*!
       \fn void recordCandidateJetCollections(xAOD::JetContainer &jets)
       \brief Write out jet collections for each of the jet preselection possibilities
       \param electrons Container with all electrons (before overlap removal)
       \param muons Container with all muons (before overlap removal)
     */
    void applyLeptonPtCuts(const xAOD::ElectronContainer &electrons, const xAOD::MuonContainer &muons);

    /*!
       \fn void calcJetQuantities(xAOD::JetContainer &jets)
       \brief Calculate jet related quantities used later in performSelection
       \param jets Container before any corrections
     */
    void calcJetQuantities(const xAOD::JetContainer &jets);

    /*!
       \fn void decorateJets(xAOD::JetContainer &jets);
       \brief Decorate jets with various useful derived quantities
     */
    void decorateJets(xAOD::JetContainer &jets);

    /*!
       \fn void recordCandidateJetCollections(xAOD::JetContainer &jets)
       \brief Write out jet collections for each of the jet preselection possibilities
       \param jets Container with all jets
       \param jets_no_JVT Container with all jets before applying JVT cut
     */
    void recordCandidateJetCollections(xAOD::JetContainer &jets, const xAOD::JetContainer &jets_no_JVT);

    /*!
       \fn void recordCandidateJetCollection(xAOD::JetContainer &jets_central, const jetPreselectionEnum &jetPreSelCat);
       \brief Write out jet collection for a given jet preselection
       \param jets_central The jet container with all central jets in this event
       \param jetPreSelCat Which jet preselection to use
     */
    void recordSingleJetCollection(xAOD::JetContainer &jets_central, const jetPreselectionEnum jetPreSelCat);

    /*!
       \fn void sortJetsByContinuousMV2c10(xAOD::JetContainer &jets_central)
       \brief Return a view of the input collection, sorted using the continuous MV2c10 discriminant
       \param jets_central The jet container with all central jets in this event
     */
    xAOD::JetContainer sortJetsByContinuousMV2c10(xAOD::JetContainer &jets_central);

    /*!
       \fn void sortJetsByDiscreteMV2c10(xAOD::JetContainer &jets_central)
       \brief Return a view of the input collection, sorted using discrete MV2c10 bins (plus other variables)
       \param jets_central The jet container with all central jets in this event
     */
    xAOD::JetContainer sortJetsByDiscreteMV2c10(xAOD::JetContainer &jets_central);

    /*!
      \fn void sortJetsByDiscretePT(xAOD::JetContainer &jets_central)
      \brief Return a view of the input collection, sorted using discrete MV2c10 bin and then by pT in the event of a tie
      \param jets_central The jet container with all central jets in this event
    */

    xAOD::JetContainer sortJetsByDiscretePT(xAOD::JetContainer &jets_central);

    /*!
       \fn const std::string shallowCopyLeptonCorrectedJets(const jetPreselectionEnum &jetPreSelCat, const leptonCorrectionEnum &lepCorrType);
       \brief Write jets after lepton correction to the output event
       \param jetPreSelCat Which jet preselection to use
       \param lepCorrType The lepton correction strategy to be applied (options: allMu, allLep, noLepCor)
     */
    const std::string shallowCopyLeptonCorrectedJets(const jetPreselectionEnum &jetPreSelCat, const leptonCorrectionEnum &lepCorrType);

    /*!
       \fn xAOD::JetFourMom_t getLeptonCorrectedFourVector(xAOD::Jet *jet, const leptonCorrectionEnum &lepCorrType)
       \brief Calls the selected lepton correction strategy and returns a corrected 4-vector
       \param jet The Jet object under consideration.
       \param lepCorrType The muon correction strategy to be applied (options: allMu, clMu, pTMu)
     */
    xAOD::JetFourMom_t getLeptonCorrectedFourVector(xAOD::Jet *jet, const leptonCorrectionEnum &lepCorrType);

    /*!
       \fn void calculateMassesAndCutflow(const xAOD::PhotonContainer &selPhotons, const xAOD::JetContainer *candidate_jets, const TString &jetPreSelLabel, const leptonCorrectionEnum &lepCorrType);
       \brief Set four object information to maps
       \param selPhotons Container with all photons
       \param candidate_jets Container with candidate jets
       \param jetPreSelLabel Label for the jet preselection being used
       \param lepCorrType The lepton correction strategy to be applied (options: allMu, allLep, noLepCor)
     */
    void calculateMassesAndCutflow(const xAOD::PhotonContainer &selPhotons,
                                   const xAOD::JetContainer *candidate_jets,
                                   const TString &jetPreSelLabel,
                                   const leptonCorrectionEnum &lepCorrType);

    void performCutBased(const xAOD::PhotonContainer &selPhotons,
                         const xAOD::JetContainer *candidate_jets,
                         const TString &jetPreSelLabel,
                         const leptonCorrectionEnum &lepCorrType);

    /*!
       \fn double weightJVT()
       \brief Fetch JVT (in)efficiency SF
     */
    double weightJVT();

    /*!
       \fn double weightBTagging(const xAOD::JetContainer &jets)
       \brief Fetch flavour tagging SF
       \param jets Container with all jets
     */
    double calculateBTaggingWeight(const xAOD::JetContainer &jets);

    /*!
       \fn HHyybbTool::weightNLO()
       \brief do truth hh reweighting to NLO where appropriate
     */
    double weightNLO();

    /*!
       \fn void recordPerEventQuantities()
       \brief Write maps to eventInfo
     */
    void recordPerEventQuantities();

    /*!
       \fn TString getJetPreselLabel(const jetPreselectionEnum &jetPreSelCat)
       \brief Fetch label for jet preselection type
     */
    TString getJetPreselLabel(const jetPreselectionEnum &jetPreSelCat);

    /*!
       \fn TString getLeptonCorrLabel(const leptonCorrectionEnum &corrType)
       \brief Fetch label for lepton correction type
     */
    TString getLeptonCorrLabel(const leptonCorrectionEnum &lepCorrType);


    /*!
       \fn TString getJetCollectionName(const jetPreselectionEnum &jetPreSelCat, const leptonCorrectionEnum &lepCorrType)
       \brief Construct appropriate name for this jet collection
     */
    std::string getJetCollectionName(const jetPreselectionEnum &jetPreSelCat, const leptonCorrectionEnum &lepCorrType = HHyybbTool::noLepCor);

  };
}
