#pragma once

// EDM include(s):
#ifndef __CINT__
  #include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
  #include "PathResolver/PathResolver.h"
  #include "xAODBase/IParticle.h"
  #include "xAODRootAccess/tools/Message.h"
#endif

// ROOT include(s):
#include "TH1.h"
#include "TLorentzVector.h"
#include "TObject.h"
#include "TPRegexp.h"

//! \name   A few general helper methods and definitions
//! \author Dag Gillberg
//@{

// Copied from ROOT, to protect against them changing the function
// since it's also defined in our HGamCore/CMakeLists.txt
#define HGAM_VERSION(a, b, c)  (((a) << 16) + ((b) << 8) + (c))

//! \brief Hgamma namespace
namespace HG {

  // constants and typedefs

  /// Helper macro to check xAOD::TReturnCode return values
  /// See Attila's slide 8-9 at: https://indico.cern.ch/event/362819/
  /// TODO: replace with ANA_CHECK (JR)
#define EL_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( ! EXP.isSuccess() ) {                          \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),   \
             #EXP );                                     \
      return EL::StatusCode::FAILURE;                   \
    }                                                   \
  } while( false );

#define CP_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( EXP != CP::SystematicCode::Ok ) {              \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),   \
             #EXP );                                     \
      return CP::SystematicCode::Unsupported;           \
    }                                                   \
  } while( false );

#define CC_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( EXP != CP::CorrectionCode::Ok ) {              \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),   \
             #EXP );                                     \
    }                                                   \
  } while( false );

#define HG_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( ! EXP.isSuccess() ) {                          \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),   \
             #EXP );                                     \
    }                                                   \
  } while( false );

  //! \brief method to abort program with error message
  void fatal(TString msg);

  //! \brief typedef for a vector of doubles (to save some typing)
  typedef std::vector<double>  NumV;

  //! \brief typedef for a vector of ints (to save some typing)
  typedef std::vector<int>     IntV;

  //! \brief typedef for a vector of TStrings (to save some typing)
  typedef std::vector<TString> StrV;

  //! \brief typedef for a vector of std::strings (to save some typing)
  typedef std::vector<std::string> StdStrV;

  //! \brief Converts a text line to a vector of words
  //  \param str input string with words
  //  \param sep separator to define where a word ends or starts
  StrV vectorize(TString str, TString sep = " ");

  //! \brief Converts string of separated numbers to vector<double>
  //  \param str input string with numbers
  //  \param sep separator to define where a number ends or starts
  NumV vectorizeNum(TString str, TString sep = " ");

  template <typename T> // what is this used for? JR
  struct Identity {
    typedef T type;
  };

  // 1*GeV = 1000*MeV
  static const double GeV(1000), invGeV(1.0 / GeV);

  namespace Iso {
    enum IsolationType {
      LooseTrackOnly,
      Loose,
      Gradient,
      GradientLoose,
      FixedCutTight,
      FixedCutTightTrackOnly,
      FixedCutLoose,
      FCLoose,
      FCLoose_FixedRad,
      FCTight,
      FCTight_FixedRad,
      FCTightTrackOnly_FixedRad,
      FixedCutHighPtTrackOnly,
      FCHighPtCaloOnly,
      FCTightTrackOnly,
      FixedCutTightCaloOnly,
      FixedCutLooseCaloOnly,
      FixedCutPflowTight,
      FixedCutPflowLoose,
      UserDefined,
      Undefined
    }; // enum IsolationType
  } // namespace Iso

  // new enums for looseprime definitions
  // note that 4a = 6 for consistency with old code
  enum PhotonLoosePrime {PhotonLP2 = 2, PhotonLP3 = 3, PhotonLP4 = 4, PhotonLP5 = 5, PhotonLP4a = 6};

  // all add requirements to get slightly tighter than Loose.
  // Prime5 used to be loosest; Note that this is not valid for 2017+ triggers. And LP4 is also no longer valid for 2017+ triggers, but LoosePrime4a below is

  const unsigned int PhotonLoosePrime5 = egammaPID::PhotonLoose |
                                         0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon | // never failed in isEMTight anymore, but added since it's included in tight
                                         0x1 << egammaPID::ClusterStripsEratio_Photon; // not the normal "Eratio" quoted

  const unsigned int PhotonLoosePrime4 = PhotonLoosePrime5 | // also require "wstot"
                                         0x1 << egammaPID::ClusterStripsWtot_Photon;

  // this is the new good one for 2017 data
  const unsigned int PhotonLoosePrime4a = PhotonLoosePrime5 | // also require "Eratio"
                                          0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;

  const unsigned int PhotonLoosePrime3 = PhotonLoosePrime4 | // "wstot" + "Eratio"
                                         0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;

  const unsigned int PhotonLoosePrime2 = PhotonLoosePrime3 | // also require "DeltaE" ("w3" and "Fside" are only remaining requirements for tight)
                                         0x1 << egammaPID::ClusterStripsDeltaE_Photon;

  // The above can be reversed, and thought of as tight minus:
  // * Prime2: w3, Fside // good for 2017 data
  // * Prime3: w3, Fside, DeltaE // good for 2017 data
  // * Prime4a: w3, Fside, DeltaE, wstot // good for 2017 data
  // * Prime4: w3, Fside, DeltaE, Eratio // bad for 2017 data
  // * Prime5: w3, Fside, DeltaE, Eratio, wstot // bad for 2017 data


  //! \brief calculates DeltaR in (y,phi)-space instead of (eta,phi) given by p4().DeltaR()
  inline double DRrap(const TLorentzVector &p1, const TLorentzVector &p2)
  {
    double dy(p1.Rapidity() - p2.Rapidity()), dphi(p1.DeltaPhi(p2));
    return sqrt(dy * dy + dphi * dphi);
  }

  //! \brief returns true if a given file or directory exist
  bool fileExist(TString fn);

  TH1 *getHistogramFromFile(TString fname, TString hname);

  template<typename T>
  std::unique_ptr<T> getHistogramPtrFromFile(TString fname, TString hname)
  {
    std::unique_ptr<T> ptr(dynamic_cast<T *>(getHistogramFromFile(fname, hname)));
    return ptr;
  }

  //! \brief A class for storing global variables: isAOD, isMAOD, isDAOD, ...
  class GlobalVariables {

  private:

    static GlobalVariables *m_ptr;

    enum GlobalFlag { FlagNotSet = -1, FlagFalse = 0, FlagTrue = 1 };
    enum InputType { InputTypeNotSet = 0, InputTypeAOD = 1, InputTypeDAOD = 2, InputTypeMAOD = 3 };

    InputType  the_inputtype;
    GlobalFlag m_isAFII;
    GlobalFlag m_isMC;
    TString    m_mcType;
    TString error_message;

    GlobalVariables()
    {
      the_inputtype = InputTypeNotSet;
      m_isAFII = FlagNotSet;
      m_isMC   = FlagNotSet;
      m_mcType = "";
      error_message = "Error! Asked for %s but it has not yet been set. Contact a developer. Exiting.";
    };

    ~GlobalVariables();

  public:

    static GlobalVariables *getInstance();
    void set_InputType(bool isAOD, bool isMAOD);
    void set_isAFII(bool b);
    void set_mcType(TString b);
    void set_isMC(bool b);
    TString get_mcTypeUsingAmiTag(TString amiTag);
    bool isAFII();
    TString mcType();
    bool isMC();
    bool isData();
    bool isMAOD();
    bool isAOD();
    bool isDAOD();

    // static GlobalVariables *getInstance()
    // {
    //   if (m_ptr == nullptr) { m_ptr = new GlobalVariables(); }

    //   return m_ptr;
    // }

    // void set_InputType(bool isAOD, bool isMAOD)
    // {
    //   if (isAOD) { the_inputtype = InputTypeAOD; }
    //   else if (isMAOD) { the_inputtype = InputTypeMAOD; }
    //   else { the_inputtype = InputTypeDAOD; }
    // }

    // void set_isAFII(bool b)    { m_isAFII = (b ? FlagTrue : FlagFalse); }
    // void set_mcType(TString b) { m_mcType = b; }
    // void set_isMC(bool b)    { m_isMC   = (b ? FlagTrue : FlagFalse); }

    // TString get_mcTypeUsingAmiTag(TString amiTag)
    // {
    //   if (TPRegexp("r7326").MatchB(amiTag)) { return "MC15b"; }
    //   else if (TPRegexp("r7267").MatchB(amiTag)) { return "MC15b"; }
    //   else if (TPRegexp("r7725").MatchB(amiTag)) { return "MC15c"; }
    //   else if (TPRegexp("a818").MatchB(amiTag)) { return "MC15c"; }
    //   else if (TPRegexp("r9364").MatchB(amiTag)) { return "MC16a"; }
    //   else if (TPRegexp("r9781").MatchB(amiTag)) { return "MC16c"; }
    //   else if (TPRegexp("r10201").MatchB(amiTag)) { return "MC16d"; }
    //   else {
    //     fatal(Form("Failed to determine mcType from amiTag %s.", amiTag.Data()));
    //   }

    //   return "MC16a";
    // }

    // bool isAFII()
    // {
    //   if (m_isAFII == FlagNotSet) { HG::fatal(Form(error_message, "isAFII")); }

    //   return (bool)m_isAFII;
    // }

    // TString mcType()
    // {
    //   if (m_mcType == "") { HG::fatal(Form(error_message, "mcType")); }

    //   return m_mcType;
    // }

    // bool isMC()
    // {
    //   if (m_isMC == FlagNotSet) { HG::fatal(Form(error_message, "isMC")); }

    //   return (bool)m_isMC;
    // }

    // bool isData()
    // {
    //   if (m_isMC == FlagNotSet) { HG::fatal(Form(error_message, "isData")); }

    //   return !isMC();
    // }

    // bool isMAOD()
    // {
    //   if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isMAOD")); }

    //   return the_inputtype == InputTypeMAOD;
    // }

    // bool isAOD()
    // {
    //   if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isAOD")); }

    //   return the_inputtype == InputTypeAOD;
    // }

    // bool isDAOD()
    // {
    //   if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isDAOD")); }

    //   return !isAOD() && !isMAOD();
    // }

  }; // GlobalVariables

  void setAndLock_InputType(bool isAOD, bool isMAOD);
  void setAndLock_isAFII(bool b);
  void setAndLock_mcType(TString b);
  TString get_mcTypeUsingAmiTag(TString b);
  void setAndLock_isMC(bool b);

  // The functions for general use:
  bool isMAOD();
  bool isAOD();
  bool isDAOD();
  bool isAFII();
  TString mcType();
  bool isData();
  bool isMC();

#ifndef __MAKECINT__

  //! \brief print 4-vector as string
  TString fourVecAsText(const TLorentzVector &p4);
  TString fourVecAsText(const xAOD::IParticle *p);

  //! \brief calculates DeltaR in (y,phi)-space instead of (eta,phi) given by p4().DeltaR()
  inline double DRrap(const xAOD::IParticle *p1, const xAOD::IParticle *p2)
  {
    return DRrap(p1->p4(), p2->p4());
  }

  //! \brief calculates DeltaR in (eta,phi)-space
  inline double DR(const xAOD::IParticle *p1, const xAOD::IParticle *p2)
  {
    return p1->p4().DeltaR(p2->p4());
  }

  //! \brief returns smallest DR between ptcl and any of the objects in ptcls
  //!        if ptcl occurs in the list of particles, it is ignored
  template <class T> double minDR(const xAOD::IParticle *ptcl, T ptcls)
  {
    double mindr = 99;

    for (auto p : ptcls) if (p != ptcl && DR(ptcl, p) < mindr) { mindr = DR(ptcl, p); }

    return mindr;
  }

  //! \brief returns smallest DR between ptcl and any of the objects in ptcls in (y,phi)-space
  //!        if ptcl occurs in the list of particles, it is ignored
  template <class T> double minDRrap(const xAOD::IParticle *ptcl, T ptcls)
  {
    double mindr = 99;

    for (auto p : ptcls) if (p != ptcl && DRrap(ptcl, p) < mindr) { mindr = DRrap(ptcl, p); }

    return mindr;
  }

  //! \brief check if the given object is inside a given container. Returns a bool.
  template <class T1, class T2> bool isObjInCont(T1 &ptclSearch, T2 &ptcls)
  {
    bool found = false;

    for (auto ptcl : ptcls)
      if (ptcl == ptclSearch)
      { found = true; }

    return found;
  }

#endif

  //@}
}
