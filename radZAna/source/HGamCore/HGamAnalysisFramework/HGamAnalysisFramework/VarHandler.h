#pragma once

// STL include(s):
#include <string>

// EDM include(s):
#include "EventLoop/StatusCode.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMuon/MuonContainer.h"


namespace xAOD {
  class TEvent;
  class TStore;
  class AuxInfoBase;
}

namespace HG {

  class VarHandler {
  private:
    static VarHandler        *m_ptr;

    std::string               m_sysName;
    std::string               m_MxAODName;

    xAOD::TEvent             *m_event;
    xAOD::TStore             *m_store;

    xAOD::IParticleContainer  m_photons;
    xAOD::IParticleContainer  m_electrons;
    xAOD::IParticleContainer  m_muons;
    xAOD::IParticleContainer  m_jets;
    xAOD::MissingETContainer  m_mets;

    bool                      m_recoContAvail;
    bool                      m_recoPhotonsAvail;
    bool                      m_recoElectronsAvail;
    bool                      m_recoMuonsAvail;
    bool                      m_recoJetsAvail;
    bool                      m_recoMetsAvail;

    xAOD::IParticleContainer  m_truthPhotons;
    xAOD::IParticleContainer  m_truthElectrons;
    xAOD::IParticleContainer  m_truthMuons;
    xAOD::IParticleContainer  m_truthJets;
    xAOD::MissingETContainer  m_truthMets;
    xAOD::IParticleContainer  m_higgsBosons;

    bool                      m_truthContAvail;
    bool                      m_truthPhotonsAvail;
    bool                      m_truthElectronsAvail;
    bool                      m_truthMuonsAvail;
    bool                      m_truthJetsAvail;
    bool                      m_truthMetsAvail;
    bool                      m_higgsBosonsAvail;


  public:
    /// Get instance of singleton class
    static VarHandler *getInstance();

    /// Get MxAOD EventInfo name for event
    std::string        getEventInfoName() const;

    /// Get pointer to collection
    const xAOD::IParticleContainer *getPhotons(bool truth = false) const;
    const xAOD::IParticleContainer *getJets(bool truth = false) const;
    const xAOD::IParticleContainer *getElectrons(bool truth = false) const;
    const xAOD::IParticleContainer *getMuons(bool truth = false) const;
    const xAOD::MissingETContainer *getMissingETs(bool truth = false) const;
    const xAOD::IParticleContainer *getHiggsBosons() const;

    /// Get MxAOD EventInfo for event
    xAOD::EventInfo       *getEventInfoFromStore(bool createInfo = true);
    const xAOD::EventInfo *getEventInfoFromEvent();

    /// Other useful information
    std::string getSysName() const;

    /// Get MxAOD TruthEventInfo for event
    xAOD::EventInfo       *getTruthEventInfoFromStore(bool createInfo = true);
    const xAOD::EventInfo *getTruthEventInfoFromEvent();

    /// Set TEvent and TStore
    void               setEventAndStore(xAOD::TEvent *event, xAOD::TStore *store);

    /// Set current systematic variation
    CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys);

    /// Set object containers
    void setContainers(const xAOD::IParticleContainer *photons   = nullptr,
                       const xAOD::IParticleContainer *electrons = nullptr,
                       const xAOD::IParticleContainer *muons     = nullptr,
                       const xAOD::IParticleContainer *jets      = nullptr,
                       const xAOD::MissingETContainer *mets      = nullptr);
    bool checkContainers() const;

    /// Set truth object containers
    void setTruthContainers(const xAOD::IParticleContainer *photons   = nullptr,
                            const xAOD::IParticleContainer *electrons = nullptr,
                            const xAOD::IParticleContainer *muons     = nullptr,
                            const xAOD::IParticleContainer *jets      = nullptr,
                            const xAOD::MissingETContainer *mets      = nullptr);
    void setHiggsBosons(const xAOD::IParticleContainer *higgs);
    bool checkTruthContainers() const;

    /// Reset containers to null pointers to avoid carry-over from previous event
    void               clearContainers();

    /// Write MxAOD EventInfo to output
    EL::StatusCode     write();

    /// Write MxAOD TruthEventInfo to output
    EL::StatusCode     writeTruth();



  private:
    /// Default constructor
    VarHandler();

    /// Default detructor
    ~VarHandler();



  }; // class VarHandler


  template <class T>
  class VarBase {
  protected:
    T                                m_default;
    std::string                      m_name;
    bool                             m_truthOnly;
    bool                             m_recoOnly;
    SG::AuxElement::Accessor<T>      m_decVal;
    SG::AuxElement::ConstAccessor<T> m_accVal;



  protected:



  protected:
    /// Calculate variable of interest, should be defined by inherited class
    virtual T calculateValue(bool truth = false);

    /// Get variable of interest, first by checking TEvent, then calculateVarlue()
    T getValue(bool truth = false);

    /// Check if variable exists in TEvent. If so return true, and set value by reference
    bool checkInEvent(T &value, bool truth = false);
    bool checkInStore(T &value, bool truth = false);



  public:
    /// Default constructor
    VarBase(const std::string &name);

    /// Default constructor
    virtual ~VarBase();

    /// Add variable to the current EventInfo
    /// @truth: if true, calculates value using truth containers
    void addToStore(bool truth);

    /// Set value manually
    void setValue(const T &value);
    void setTruthValue(const T &value);

    /// Check if value exists in TEvent or TStore
    bool exists();
    bool truthExists();

    /// Get variable of interest
    T operator()();

    /// Get truth variable of interest
    T truth();



  }; // class VarBase

}

#include "HGamAnalysisFramework/VarHandler.hpp"
