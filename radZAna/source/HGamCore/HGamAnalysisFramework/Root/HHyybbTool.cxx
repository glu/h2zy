///////////////////////////////////////////
// HHyybb Tool
// Tool performs all yybb selections
// All info decorated to MxAODs
// @author: james.robinson@cern.ch
// @author: alan.james.taylor@cern.ch
///////////////////////////////////////////

// STL include(s):
#include <algorithm>
#include <cmath>
#include <fstream>

// EDM include(s):
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include "hhTruthWeightTools/hhWeightTool.h"
#include "xAODCore/ShallowCopy.h"

// ROOT include(s):
#include "TLorentzVector.h"

// lwtnn include(s):
#include "lwtnn/LightweightNeuralNetwork.hh"
#include "lwtnn/parse_json.hh"

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/HHyybbTool.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"

namespace HG {
  // _______________________________________________________________________________________
  HHyybbTool::HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_j1_pTMin(25.0)
    , m_j2_pTMin(25.0)
    , m_mjj_min(60.0)
    , m_mjj_max(190.0)
    , m_myy_min(120.0)
    , m_myy_max(130.0)
    , m_auxIntsToSave( {"HadronConeExclTruthLabelID", "MV2c10bin", "DL1bin"})
  // , m_auxFloatsToSave({"yybb_preselectionNN_score"})
  , m_auxDoublesToSave({"MV2c10_discriminant"})
  // default jets where the variables computed are saved to systematics
  , m_jetPreselections({HHyybbTool::discretePT})
  , m_leptonCorrections({HHyybbTool::noLepCor})
  , m_analysisSelections({HHyybbTool::cutBased})
  {}


  // _______________________________________________________________________________________
  // DO NOT MOVE THIS TO THE HEADER - the destructor must be able to see the destructors for all classes which use std::unique_ptr
  HHyybbTool::~HHyybbTool() {}


  // _______________________________________________________________________________________
  HG::EventHandler *HHyybbTool::eventHandler()
  {
    return m_eventHandler;
  }


  // _______________________________________________________________________________________
  HG::TruthHandler *HHyybbTool::truthHandler()
  {
    return m_truthHandler;
  }

  // _______________________________________________________________________________________
  EL::StatusCode HHyybbTool::initialize(Config &config)
  {
    // Set default cut values etc. here...
    // For MxAOD production, the defaults will be used almost everywhere

    // Check if we are doing a detailed run or not
    m_detailedHHyybb = config.getBool("HHyybb.DetailedInfo", false);
    ATH_MSG_INFO("Write out detailed information?............... " << (m_detailedHHyybb ? "YES" : "NO"));

    // Muon and jet configuration
    m_muon_pTmin = config.getNum("HHyybbTool.MuonPtMin", 4.0); //! all muons to be considered for muon correction must have pT > m_muon_pTmin (in GeV)
    ATH_MSG_INFO("MuonPtMin..................................... " << m_muon_pTmin << " GeV");
    m_muon_DRmax = config.getNum("HHyybbTool.MuonJetDRMax", 0.4); //! all muons to be considered for muon correction must have dR(muon, jet) < m_muon_DRmax
    ATH_MSG_INFO("MuonJetDRMax.................................. " << m_muon_DRmax);
    m_electron_pTmin = config.getNum("HHyybbTool.ElectronPtMin", 4.0); //! all electrons to be considered for electron correction must have pT > m_electron_pTmin (in GeV)
    ATH_MSG_INFO("ElectronPtMin................................. " << m_electron_pTmin << " GeV");
    m_electron_DRmax = config.getNum("HHyybbTool.ElectronJetDRMax", 0.4); //! all electrons to be considered for electron correction must have dR(electron, jet) < m_electron_DRmax
    ATH_MSG_INFO("ElectronJetDRMax.............................. " << m_electron_DRmax);
    m_jet_eta_max = config.getNum("HHyybbTool.JetEtaMax", 2.5); //! only central jets are b-tagged
    ATH_MSG_INFO("JetEtaMax..................................... " << m_jet_eta_max);
    m_massHiggs = config.getNum("HHyybbTool.HiggsMass", 125.0); //! what we consider the Higgs mass to be
    ATH_MSG_INFO("HiggsMass..................................... " << m_massHiggs);

    // Initialise the dihiggs NLO(ish) to NLO tool
    m_hhWeightTool.reset(new xAOD::hhWeightTool("hhWeights"));
    m_hhWeightTool->initialize();

    // Load preselection neural net
    std::ifstream input_stream(PathResolverFindCalibFile("HGamAnalysisFramework/lwtnn/network-yybb-preselection.json"));
    lwt::JSONConfig lwtnn_config = lwt::parse_json(input_stream);
    m_preselectionNN.reset(new lwt::LightweightNeuralNetwork(lwtnn_config.inputs, lwtnn_config.layers, lwtnn_config.outputs));

    // If we're in detailed mode then run all combinations of selections
    //if (m_detailedHHyybb) {
    //m_jetPreselections.push_back(HHyybbTool::continuous);
    //m_jetPreselections.push_back(HHyybbTool::discretePT);
    //m_leptonCorrections.push_back(HHyybbTool::allMu);
    //m_leptonCorrections.push_back(HHyybbTool::allLep);
    // }

    return EL::StatusCode::SUCCESS;
  }


  // _______________________________________________________________________________________
  // Perform HHyybb selection and write relevant outputs to the output MxAOD
  void HHyybbTool::saveHHyybbInfo(const xAOD::PhotonContainer   &photons,
                                  const xAOD::ElectronContainer &electrons,
                                  const xAOD::MuonContainer     &muons,
                                  xAOD::JetContainer            &jets,
                                  const xAOD::JetContainer      &jets_no_JVT)
  {
    // --------
    // Clear maps containing all event info to decorate to MxAOD
    m_eventFloats.clear();
    m_eventInts.clear();
    m_eventFloatsDetailed.clear();

    // --------
    // Reset the per-event jet quantities to 0 at the start of each event
    m_nBJets85 = 0;
    m_nBJets70 = 0;
    m_nCentralJets = 0;
    m_weightBTagging = 0.0;

    // --------
    // Apply minimum pT cuts to electrons and muons
    ATH_MSG_DEBUG("About to apply kinematic preselection cuts to leptons");
    applyLeptonPtCuts(electrons, muons);

    // --------
    // Add MV2c10 bin and other decorations to jets
    ATH_MSG_DEBUG("About to decorate jets with tagging and kinematic information");
    decorateJets(jets);

    // --------
    // Construct one output jet collection for each jet preselection possibility
    ATH_MSG_DEBUG("About to record candidate jet collections to the output");
    recordCandidateJetCollections(jets, jets_no_JVT);

    // --------
    // Calculate jet related quantities used later in analysis selection
    calcJetQuantities(jets);

    // --------
    // Perform selection using continuous MV2c10 score and/or discrete MV2c10 WPs
    ATH_MSG_DEBUG("About to apply HHyybb analysis cuts for each jet preselection choice");

    for (auto jetPreselection : m_jetPreselections) {
      performSelection(photons, jetPreselection);
    }

    // --------
    // Save all the per-event quantities to the eventHandler()
    // Should always be called last, once all of these have been filled
    recordPerEventQuantities();
  }


  // _______________________________________________________________________________________
  // Function which performs full yybb selection for a given jet preselection and for all possible lepton corrections
  void HHyybbTool::performSelection(const xAOD::PhotonContainer &photons, const HHyybbTool::jetPreselectionEnum jetPreSelCat)
  {
    // -------
    // Get jet pre-selection label
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelCat);

    // --------
    // Get the candidate jets: best two jets from (un)tagged jets
    // This uses jets from *before* the lepton-correction
    const xAOD::JetContainer *candidateJets = nullptr;
    eventHandler()->evtStore()->retrieve(candidateJets, getJetCollectionName(jetPreSelCat));

    // --------
    // Get HHyybb weight from JVT, b-tagging and NLO scale factors
    m_eventFloats["yybb_" + jetPreSelLabel + "weight"] = weightJVT() * m_weightBTagging * weightNLO();

    // --------
    // Set the four object information
    for (auto leptonCorrection : m_leptonCorrections) {
      // Create, store and retrieve lepton-corrected shallow copies of the candidate jets
      const xAOD::JetContainer *correctedCandidateJets = nullptr;

      if (leptonCorrection == HHyybbTool::noLepCor) {
        correctedCandidateJets = candidateJets;
      } else {
        eventHandler()->evtStore()->retrieve(correctedCandidateJets, shallowCopyLeptonCorrectedJets(jetPreSelCat, leptonCorrection));
      }

      // Calculate masses and perform cutflow
      calculateMassesAndCutflow(photons, correctedCandidateJets, jetPreSelLabel, leptonCorrection);

      // Perform the cut based analysis
      performCutBased(photons, correctedCandidateJets, jetPreSelLabel, leptonCorrection);

    }
  }


  // _______________________________________________________________________________________
  // Write containers for leptons passing the pT cuts to the event store (transient)
  void HHyybbTool::applyLeptonPtCuts(const xAOD::ElectronContainer &electrons, const xAOD::MuonContainer &muons)
  {
    // --------
    // Apply pT cuts to electrons
    auto selectedElectrons = std::make_unique< ConstDataVector<xAOD::ElectronContainer> >(SG::VIEW_ELEMENTS);

    for (const auto electron : electrons) {
      if (electron && (electron->pt() >= m_electron_pTmin * HG::GeV)) { selectedElectrons->push_back(electron); }
    }

    if (eventHandler()->evtStore()->record(selectedElectrons.release(), "yybb_electrons_passing_pT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of electron collection to TStore, throwing exception"); }

    // ... and to muons
    auto selectedMuons = std::make_unique< ConstDataVector<xAOD::MuonContainer> >(SG::VIEW_ELEMENTS);

    for (const auto muon : muons) {
      if (muon && (muon->pt() >= m_muon_pTmin * HG::GeV)) { selectedMuons->push_back(muon); }
    }

    if (eventHandler()->evtStore()->record(selectedMuons.release(), "yybb_muons_passing_pT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of muon collection to TStore, throwing exception"); }


  }


  // _______________________________________________________________________________________
  // Write deep-copies of the candidate jets to the output file for the various pre-selection choices
  // Also write central-jets-before-JVT to the event store (transient)
  void HHyybbTool::recordCandidateJetCollections(xAOD::JetContainer &jets, const xAOD::JetContainer &jets_no_JVT)
  {
    // --------
    // Apply central jet requirement (for b-tagging reasons)
    xAOD::JetContainer jets_central(SG::VIEW_ELEMENTS);
    std::copy_if(jets.begin(), jets.end(), std::back_inserter(jets_central), [this](const xAOD::Jet * j) {
      return fabs(j->eta()) <= m_jet_eta_max;
    });

    // ... and do the same for the jets before JVT
    auto jets_no_JVT_passing_cuts = std::make_unique< ConstDataVector<xAOD::JetContainer> >(SG::VIEW_ELEMENTS);
    std::copy_if(jets_no_JVT.begin(), jets_no_JVT.end(), std::back_inserter(*jets_no_JVT_passing_cuts), [this](const xAOD::Jet * j) {
      return fabs(j->eta()) <= m_jet_eta_max;
    });

    if (eventHandler()->evtStore()->record(jets_no_JVT_passing_cuts.release(), "yybb_jets_central_no_JVT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }


    // --------
    // Choose jets using the continuous MV2c10 score and/or discrete MV2c10 bins
    for (auto jetPreselection : m_jetPreselections) {
      recordSingleJetCollection(jets_central, jetPreselection);
    }
  }


  // _______________________________________________________________________________________
  // Record a new deep copied jet collection to the output file
  void HHyybbTool::recordSingleJetCollection(xAOD::JetContainer &jets_central, const HHyybbTool::jetPreselectionEnum jetPreSelCat)
  {
    // Create the new container and its auxiliary store.
    auto selectedJets = std::make_unique<xAOD::JetContainer>();
    auto selectedJetsAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedJets->setStore(selectedJetsAux.get());

    // Get sorted jets
    xAOD::JetContainer sortedJets = (jetPreSelCat == HHyybbTool::discrete) ? sortJetsByDiscreteMV2c10(jets_central) :
                                    (jetPreSelCat == HHyybbTool::continuous) ? sortJetsByContinuousMV2c10(jets_central) :
                                    (jetPreSelCat == HHyybbTool::discretePT) ? sortJetsByDiscretePT(jets_central) :
                                    xAOD::JetContainer(SG::VIEW_ELEMENTS);

    if (sortedJets.size() >= 2) {
      // Make a new jet which copies the auxdata from the two highest scoring jets
      for (size_t idx = 0; idx < 2; ++idx) {
        xAOD::Jet *selectedJet = new xAOD::Jet();
        selectedJets->push_back(selectedJet);
        // Only copy specific auxdata to reduce the size (otherwise we could use "*selectedJet = *sortedJets.at(idx);" instead of the following lines)
        selectedJet->setJetP4(sortedJets.at(idx)->jetP4());

        for (auto name : m_auxIntsToSave) { selectedJet->auxdata<int>(name) = sortedJets.at(idx)->auxdata<int>(name); }

        for (auto name : m_auxFloatsToSave) { selectedJet->auxdata<float>(name) = sortedJets.at(idx)->auxdata<float>(name); }

        for (auto name : m_auxDoublesToSave) { selectedJet->auxdata<double>(name) = sortedJets.at(idx)->auxdata<double>(name); }
      }

      // Sort by pT before returning
      std::sort(selectedJets->begin(), selectedJets->end(), [](const auto & i, const auto & j) { return i->pt() > j->pt(); });
    }

    // Write jet collections to output
    if (eventHandler()->evtStore()->record(selectedJets.release(), getJetCollectionName(jetPreSelCat) + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection (1) to TStore, throwing exception"); }

    if (eventHandler()->evtStore()->record(selectedJetsAux.release(), getJetCollectionName(jetPreSelCat) + HG::VarHandler::getInstance()->getSysName() + "Aux.").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection (2) to TStore, throwing exception"); }






  }


  // _______________________________________________________________________________________
  // Preselection using a neural net trained on discrete MV2c10 bins
  xAOD::JetContainer HHyybbTool::sortJetsByDiscreteMV2c10(xAOD::JetContainer &jets_central)
  {
    xAOD::JetContainer sortedJets(SG::VIEW_ELEMENTS);

    // Sort the jets by their neural net score
    for (auto jet : jets_central) { sortedJets.push_back(jet); }

    std::sort(sortedJets.begin(), sortedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->auxdata<float>("yybb_preselectionNN_score") > j->auxdata<float>("yybb_preselectionNN_score");
    });

    return sortedJets;
  }


  // _______________________________________________________________________________________
  // Pre-selection using discrete MV2c10 bins with pT used as a tie-breaker
  xAOD::JetContainer HHyybbTool::sortJetsByDiscretePT(xAOD::JetContainer &jets_central)
  {
    xAOD::JetContainer sortedJets(SG::VIEW_ELEMENTS);

    // Sort the jets by the discrete mv2c10 bins then by pT
    for (auto jet : jets_central) { sortedJets.push_back(jet); }

    // first sort jets by pT then do a stable sort by MV2c10bin

    std::sort(sortedJets.begin(), sortedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->pt() > j->pt();
    });

    std::stable_sort(sortedJets.begin(), sortedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->auxdata<int>("MV2c10bin") > j->auxdata<int>("MV2c10bin");
    });

    return sortedJets;
  }


  // _______________________________________________________________________________________
  // Preselection using the continuous MV2c10 score directly
  xAOD::JetContainer HHyybbTool::sortJetsByContinuousMV2c10(xAOD::JetContainer &jets_central)
  {
    xAOD::JetContainer sortedJets(SG::VIEW_ELEMENTS);

    // Sort the jets by their MV2c10 discriminant
    for (auto jet : jets_central) { sortedJets.push_back(jet); }

    std::sort(sortedJets.begin(), sortedJets.end(), [](const xAOD::Jet * i, const xAOD::Jet * j) {
      return i->auxdata<double>("MV2c10_discriminant") > j->auxdata<double>("MV2c10_discriminant");
    });

    return sortedJets;
  }


  // _______________________________________________________________________________________
  // Decorate jets with lepton corrections, MV2c10 bin and other quantities
  void HHyybbTool::decorateJets(xAOD::JetContainer &jets)
  {
    for (auto jet : jets) {
      // --------
      // Decorate each jet with its discrete MV2c10 bin
      int jet_MV2c10bin = jet->auxdata<char>("MV2c10_FixedCutBEff_85") ?
                          jet->auxdata<char>("MV2c10_FixedCutBEff_77") ?
                          jet->auxdata<char>("MV2c10_FixedCutBEff_70") ?
                          jet->auxdata<char>("MV2c10_FixedCutBEff_60") ? 4 : 3 : 2 : 1 : 0;
      jet->auxdata<int>("MV2c10bin") = jet_MV2c10bin;

      // --------
      // Decorate each jet with its discrete DL1 bin
      int jet_DL1bin = jet->auxdata<char>("DL1_FixedCutBEff_85") ?
                       jet->auxdata<char>("DL1_FixedCutBEff_77") ?
                       jet->auxdata<char>("DL1_FixedCutBEff_70") ?
                       jet->auxdata<char>("DL1_FixedCutBEff_60") ? 4 : 3 : 2 : 1 : 0;
      jet->auxdata<int>("DL1bin") = jet_DL1bin;

      // --------
      // Decorate with |Delta phi| wrt other jets
      TLorentzVector otherjets;

      for (auto otherjet : jets) {
        if (jet != otherjet) { otherjets += otherjet->p4(); }
      }

      jet->auxdata<float>("yybb_abs_delta_phi_in_rad") = jets.size() > 1 ? fabs(otherjets.DeltaPhi(jet->p4()) / M_PI) : -1;

      // --------
      // Decorate with preselection neural net
      std::map<std::string, double> inputs = {
        {"jet_log_E", log(jet->e() / 1000.)},
        {"jet_log_pT", log(jet->pt() / 1000.)},
        {"jet_abs_eta", fabs(jet->eta())},
        {"jet_abs_delta_phi_in_rad", jet->auxdata<float>("yybb_abs_delta_phi_in_rad")},
        {"jet_MV2c10bin", jet->auxdata<int>("MV2c10bin")}
      };
      jet->auxdata<float>("yybb_preselectionNN_score") = m_preselectionNN->compute(inputs).at("BJetiness");
    }
  }


  // _______________________________________________________________________________________
  // Calculate a number of quantities from jets used later in the analysis selections
  void HHyybbTool::calcJetQuantities(const xAOD::JetContainer &jets)
  {
    for (auto jet : jets) {
      if (jet->auxdata<char>("MV2c10_FixedCutBEff_85")) { m_nBJets85++; }

      if (jet->auxdata<char>("MV2c10_FixedCutBEff_70")) { m_nBJets70++; }

      if (fabs(jet->eta()) <= m_jet_eta_max) { m_nCentralJets++; }
    }

    m_weightBTagging = calculateBTaggingWeight(jets);
  }


  // _______________________________________________________________________________________
  // Make a shallow copy of the input collection with lepton-corrected 4-vectors
  const std::string HHyybbTool::shallowCopyLeptonCorrectedJets(const HHyybbTool::jetPreselectionEnum &jetPreSelCat,
      const HHyybbTool::leptonCorrectionEnum &lepCorrType)
  {
    // Get input jet collection
    const xAOD::JetContainer *jets = nullptr;
    eventHandler()->evtStore()->retrieve(jets, getJetCollectionName(jetPreSelCat));

    // Make a shallow copy of the candidate jets
    auto correctedJetsShallowCopy = xAOD::shallowCopyContainer(*jets);
    std::unique_ptr<xAOD::JetContainer> correctedJets(correctedJetsShallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> correctedJetsAux(correctedJetsShallowCopy.second);

    if (jets->size() > 0) {
      // Set the 4-vector for each jet
      for (auto jet : *correctedJets) {
        jet->setJetP4(getLeptonCorrectedFourVector(jet, lepCorrType));
      }

      // Sort by pT
      std::sort(correctedJets->begin(), correctedJets->end(), [](const auto & i, const auto & j) { return i->pt() > j->pt(); });
    }

    // Write jet collection to output
    const std::string outputName(getJetCollectionName(jetPreSelCat, lepCorrType));


    if (eventHandler()->evtStore()->record(correctedJets.release(), outputName + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }

    if (eventHandler()->evtStore()->record(correctedJetsAux.release(), outputName + HG::VarHandler::getInstance()->getSysName() + "Aux.").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }




    return outputName;
  }


  // _______________________________________________________________________________________
  // Calls the selected lepton correction strategy
  xAOD::JetFourMom_t HHyybbTool::getLeptonCorrectedFourVector(xAOD::Jet *jet, const HHyybbTool::leptonCorrectionEnum &lepCorrType)
  {
    // Filter only electrons which pass the dR cut
    const xAOD::MuonContainer *muons = nullptr;
    eventHandler()->evtStore()->retrieve(muons, "yybb_muons_passing_pT");

    ConstDataVector<xAOD::MuonContainer> muons_passing_dR_cut(SG::VIEW_ELEMENTS);

    for (const auto muon : *muons) {
      if (jet->p4().DeltaR(muon->p4()) <= m_muon_DRmax) { muons_passing_dR_cut.push_back(muon); }
    }

    // ... and similarly for electrons
    const xAOD::ElectronContainer *electrons = nullptr;
    eventHandler()->evtStore()->retrieve(electrons, "yybb_electrons_passing_pT");

    ConstDataVector<xAOD::ElectronContainer> electrons_passing_dR_cut(SG::VIEW_ELEMENTS);

    for (const auto electron : *electrons) {
      if (jet->p4().DeltaR(electron->p4()) <= m_electron_DRmax) { electrons_passing_dR_cut.push_back(electron); }
    }

    // Decorate jet with corrected values using all nearby muons
    TLorentzVector muonsInJet(jet->p4());

    for (auto muon : muons_passing_dR_cut) { muonsInJet += muon->p4(); }

    // Decorate jet with corrected values using all nearby muons and electrons
    TLorentzVector leptonsInJet(muonsInJet);

    for (auto electron : electrons_passing_dR_cut) { leptonsInJet += electron->p4(); }

    // Return corrected 4-vector
    if (lepCorrType == HHyybbTool::allMu) {
      jet->auxdata<int>("yybb_nLeptonsAdded") = muons_passing_dR_cut.size();
      return xAOD::JetFourMom_t(muonsInJet.Pt(), muonsInJet.Eta(), muonsInJet.Phi(), muonsInJet.M());
    } else if (lepCorrType == HHyybbTool::allLep) {
      jet->auxdata<int>("yybb_nLeptonsAdded") = muons_passing_dR_cut.size() + electrons_passing_dR_cut.size();
      return xAOD::JetFourMom_t(leptonsInJet.Pt(), leptonsInJet.Eta(), leptonsInJet.Phi(), leptonsInJet.M());
    }

    return jet->jetP4();
  }


  // _______________________________________________________________________________________
  // Set additional event info
  void HHyybbTool::calculateMassesAndCutflow(const xAOD::PhotonContainer &selPhotons, const xAOD::JetContainer *candidate_jets, const TString &jetPreSelLabel, const HHyybbTool::leptonCorrectionEnum &lepCorrType)
  {
    // Construct diphoton, jet and dijet 4-vectors

    TLorentzVector y1 = (selPhotons.size() >= 2) ? selPhotons.at(0)->p4() * HG::invGeV : TLorentzVector(0, 0, 0, 0);
    TLorentzVector y2 = (selPhotons.size() >= 2) ? selPhotons.at(1)->p4() * HG::invGeV : TLorentzVector(0, 0, 0, 0);
    TLorentzVector yy = y1 + y2;

    TLorentzVector j1 = (candidate_jets->size() >= 2) ? candidate_jets->at(0)->p4() * HG::invGeV : TLorentzVector(0, 0, 0, 0);
    TLorentzVector j2 = (candidate_jets->size() >= 2) ? candidate_jets->at(1)->p4() * HG::invGeV : TLorentzVector(0, 0, 0, 0);
    TLorentzVector jj = j1 + j2;

    // ... and the 4-body vector with and without applying the mH constraint
    TLorentzVector jj_cnstrnd = jj * (HHyybbTool::m_massHiggs / jj.M());
    TLorentzVector yyjj = yy + jj;
    TLorentzVector yyjj_cnstrnd = yy + jj_cnstrnd;

    // Fill output values if there are two photons and two jets
    TString lepCorrLabel = getLeptonCorrLabel(lepCorrType);
    m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_jj"] = (candidate_jets->size() >= 2) ? jj.M() : -99;
    m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_yyjj"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj.M() : -99;
    m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_yyjj_cnstrnd"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj_cnstrnd.M() : -99;
    m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_yyjj_tilde"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj.M() - yy.M() - jj.M() + 2 * HHyybbTool::m_massHiggs : -99;

    // Calculate variables that can later be used in performSelection
    // only saved in the case of yybb-Detailed
    m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_yy"] = (selPhotons.size() >= 2) ? y1.DeltaR(y2) : -99;
    m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_jj"] = (candidate_jets->size() >= 2) ? j1.DeltaR(j2) : -99;
    m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_yyjj"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yy.DeltaR(jj) : -99;


    // Now perform the cutflow
    // =======================

    // Cut 0 - CENJETS Cut
    // :: at least two jets with |eta| < 2.5
    if (candidate_jets->size() < 2) {
      m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = CENJETS;
      return;
    }

    // Cut 1 - TAGGING Cut
    // :: we don't have a tagging cut at the moment
    if (false) {
      m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = TAGGING;
      return;
    }

    // Cut 2 - BPT Cut
    // :: pT cut for jets
    if ((std::max(j1.Pt(), j2.Pt()) <= m_j1_pTMin) || (std::min(j1.Pt(), j2.Pt()) <= m_j2_pTMin)) {
      m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = BPT;
      return;
    }

    // Cut 3 - BBMASS Cut
    // :: dijet mass cut
    if ((jj.M() < m_mjj_min) || (jj.M() > m_mjj_max)) {
      m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = BBMASS;
      return;
    }

    // Cut 4 - YYMASS Cut
    // :: diphoton mass cut
    if (selPhotons.size() < 2 || (yy.M() < m_myy_min) || (yy.M() > m_myy_max)) {
      m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = YYMASS;
      return;
    }

    // If we pass everything then we have two jets that pass the pT and Mass cuts.
    m_eventInts["yybb_" + jetPreSelLabel + lepCorrLabel + "cutFlow"] = PASSYYBB;
  }


  // _______________________________________________________________________________________
  // Perform simple cut-based selection
  void HHyybbTool::performCutBased(const xAOD::PhotonContainer &selPhotons, const xAOD::JetContainer *candidate_jets, const TString &jetPreSelLabel, const HHyybbTool::leptonCorrectionEnum &lepCorrType)
  {
    TString lepCorrLabel = getLeptonCorrLabel(lepCorrType);
    TString fullLabel = "_cutBased_" + jetPreSelLabel + lepCorrLabel + "Cat";

    m_eventInts["yybb_nonResLooseMjj" + fullLabel ] = -99;
    m_eventInts["yybb_nonRes" + fullLabel ] = -99;

    // >= 2 central jets, 0 leptons, less than 6 central jets, >= 2 b-tags at 85% working point, reject 3 b-tags at 70% working point.
    if (var::N_lep() == 0 && m_nCentralJets < 6 && m_nBJets85 >= 2 && m_nBJets70 < 3 && candidate_jets->size() >= 2 && selPhotons.size() >= 2) {

      // use variables already calculated by the CalculateMasses function
      float deltaR_yy = m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_yy"];
      float deltaR_jj = m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_jj"];
      float deltaR_yyjj = m_eventFloatsDetailed["yybb_" + jetPreSelLabel + lepCorrLabel + "deltaR_yyjj"];
      float m_yyjj_tilde = m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_yyjj_tilde"];
      float m_jj = m_eventFloats["yybb_" + jetPreSelLabel + lepCorrLabel + "m_jj"];

      // Require tight m_bb selection and sort into categories
      if (m_jj >= 90.0 && m_jj <= 140.0) {
        // First selection is based on low/high mass
        if (m_yyjj_tilde >= 350.0) {
          if (deltaR_yy < 2.0 && deltaR_jj < 2.0 && deltaR_yyjj < 3.4) {
            if (m_nBJets70 == 2)   {
              m_eventInts["yybb_nonRes" + fullLabel  ] = 4;
            } else {
              m_eventInts["yybb_nonRes" + fullLabel ] = 3;
            }
          }
        } else  {
          if (m_nBJets70 == 2)   {
            m_eventInts["yybb_nonRes" + fullLabel ] = 2;
          } else {
            m_eventInts["yybb_nonRes" + fullLabel ] = 1;
          }
        }
      }

      // Repeat the selection above with the m_bb selection loosened to [70,180] GeV to enable the study of 2D fit
      if (m_jj >= 70.0 && m_jj <= 180.0) {
        // First selection is based on low/high mass
        if (m_yyjj_tilde >= 350.0) {
          if (deltaR_yy < 2.0 && deltaR_jj < 2.0 && deltaR_yyjj < 3.4) {
            if (m_nBJets70 == 2)   {
              m_eventInts["yybb_nonResLooseMjj" + fullLabel] = 4;
            } else {
              m_eventInts["yybb_nonResLooseMjj" + fullLabel] = 3;
            }
          }
        } else  {
          if (m_nBJets70 == 2)   {
            m_eventInts["yybb_nonResLooseMjj" + fullLabel ] = 2;
          } else {
            m_eventInts["yybb_nonResLooseMjj" + fullLabel] = 1;
          }
        }
      }
    }
  }


  // _______________________________________________________________________________________
  // Write out jet collections to MxAOD
  EL::StatusCode HHyybbTool::writeContainers()
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(EL::StatusCode);

    // only write out the jets if we are in yybb detailed mode
    if (m_detailedHHyybb) {

      // Read jet collections from the event store and write them to the output file
      for (auto jetPreselection : m_jetPreselections) {
        for (auto leptonCorrection : m_leptonCorrections) {
          const std::string collectionName = getJetCollectionName(jetPreselection, leptonCorrection) + HG::VarHandler::getInstance()->getSysName();

          // Retrieve the jet collection and write it to the output file
          xAOD::JetContainer *selectedJets = nullptr;
          ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJets, collectionName));
          ANA_CHECK(eventHandler()->outputEvt()->record(selectedJets, collectionName));

          // Now do the same for the aux collection (different type depending on whether this is a deep or shallow copy)
          if (leptonCorrection == HHyybbTool::noLepCor) {
            xAOD::AuxContainerBase *selectedJetsAux = nullptr;
            ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJetsAux, collectionName + "Aux."));
            ANA_CHECK(eventHandler()->outputEvt()->record(selectedJetsAux, collectionName + "Aux."));
          } else {
            xAOD::ShallowAuxContainer *selectedJetsAux = nullptr;
            ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJetsAux, collectionName  + "Aux."));
            ANA_CHECK(eventHandler()->outputEvt()->record(selectedJetsAux, collectionName + "Aux."));
          }
        }
      }

    }

    return EL::StatusCode::SUCCESS;
  }


  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HHyybbTool::recordPerEventQuantities()
  {
    // Floats
    for (auto element : m_eventFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : m_eventInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }

    // In the case of yybbDetailed, record additional quantities
    if (m_detailedHHyybb) {
      // Floats
      for (auto element : m_eventFloatsDetailed) {
        eventHandler()->storeVar<float>(element.first.Data(), element.second);
      }
    }
  }


  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  // Use uncorrected, no-JVT jets
  double HHyybbTool::weightJVT()
  {
    // Retrieve jets passing the |eta| cut before JVT
    const xAOD::JetContainer *jets_no_JVT_passing_cuts = nullptr;
    eventHandler()->evtStore()->retrieve(jets_no_JVT_passing_cuts, "yybb_jets_central_no_JVT");

    // Return multiplicative combination of JVT efficiencies
    if (jets_no_JVT_passing_cuts->size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(jets_no_JVT_passing_cuts);
    }

    return 1.0;
  }


  // _______________________________________________________________________________________
  // Calculate the b-tagging scale-factor weight from the b-tagging SFs for *all* jets where we look at the MV2c10 result
  double HHyybbTool::calculateBTaggingWeight(const xAOD::JetContainer &jets)
  {
    // Our default is to use the scale-factor from continuous tagging
    TString SFName("SF_MV2c10_Continuous");

    // Retrieve appropriate b-tagging scale-factors
    double jetSF  = 1.0;

    for (auto jet : jets) {
      jetSF *= jet->isAvailable<float>(SFName.Data()) ? jet->auxdata<float>(SFName.Data()) : 0.0;
    }

    return jetSF;
  }


  // _______________________________________________________________________________________
  // Get hh truth weight to reweight signal hh MC samples to full NLO prediction
  double HHyybbTool::weightNLO()
  {
    if (HG::isData()) { return 1.0; }

    // Get truth Higgses and require that there are exactly two
    xAOD::TruthParticleContainer truthHiggses = truthHandler()->getHiggsBosons();

    if (truthHiggses.size() != 2) {
      if (truthHiggses.size() > 2) { ATH_MSG_ERROR("More than two final state Higgses in the event record."); }

      return 1.0;
    }

    ATH_MSG_DEBUG("Found " << truthHiggses.size() << " Higgses which may need an NLO weight.");

    // Hard-code the DSID for SM hh
    if (m_eventHandler->mcChannelNumber() != 342620) { return 1.0; }

    // Pass mHH in MeV
    float mHH = (truthHiggses.at(0)->p4() + truthHiggses.at(1)->p4()).M();
    double weight = m_hhWeightTool->getWeight(mHH);
    ATH_MSG_DEBUG("Found a di-Higgs system with mass " << mHH * HG::invGeV << " GeV => NLO weight: " <<  weight);
    return weight;
  }


  // _______________________________________________________________________________________
  // Get the label for a given jet pre-selection
  TString HHyybbTool::getJetPreselLabel(const HHyybbTool::jetPreselectionEnum &jetPreSelCat)
  {
    if (jetPreSelCat == HHyybbTool::discrete) { return TString("discreteMV2c10_"); }

    if (jetPreSelCat == HHyybbTool::continuous) { return TString("continuousMV2c10_"); }

    if (jetPreSelCat == HHyybbTool::discretePT) { return TString("discreteMV2c10pT_"); }

    return TString("");
  }


  // _______________________________________________________________________________________
  // Get the label for a given lepton correction
  TString HHyybbTool::getLeptonCorrLabel(const HHyybbTool::leptonCorrectionEnum &lepCorrType)
  {
    if (lepCorrType == HHyybbTool::allMu) { return TString("AllMu_"); }

    if (lepCorrType == HHyybbTool::allLep) { return TString("AllLep_"); }

    return TString("");
  }


  // _______________________________________________________________________________________
  // Get the name for this jet collection
  std::string HHyybbTool::getJetCollectionName(const jetPreselectionEnum &jetPreSelCat, const leptonCorrectionEnum &lepCorrType)
  {
    TString collectionName = "yybbAntiKt4EMTopoJets_" + getJetPreselLabel(jetPreSelCat) + (lepCorrType == HHyybbTool::noLepCor ? "" : "lepCorr" + getLeptonCorrLabel(lepCorrType));
    return std::string(TString(collectionName.Strip(TString::kTrailing, '_')).Data());
  }

}

