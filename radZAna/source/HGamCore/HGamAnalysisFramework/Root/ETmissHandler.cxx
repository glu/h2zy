// EDM include(s):
#include "METUtilities/METMaker.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"

// Local include(s):
#include "HGamAnalysisFramework/ETmissHandler.h"


namespace HG {

  /*! \brief Class that
   *  \author Luis March
   */

  //______________________________________________________________________________
  ETmissHandler::ETmissHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store)
    : HgammaHandler(name, event, store)
    , m_metSysTool(nullptr)
  { }

  //______________________________________________________________________________
  ETmissHandler::~ETmissHandler()
  { }

  //______________________________________________________________________________
  EL::StatusCode ETmissHandler::initialize(Config &config)
  {
    ANA_CHECK_SET_TYPE(EL::StatusCode);

    ANA_CHECK(HgammaHandler::initialize(config));

    // Read in configuration information
    m_containerName = config.getStr(m_name + ".ContainerName");
    m_truthName     = config.getStr(m_name + ".TruthContainerName", "MET_Truth");

    m_assocMapName  = config.getStr(m_name + ".METAssociactionMapName", "METAssoc_AntiKt4EMTopoHgg");
    m_coreName      = config.getStr(m_name + ".METCoreName", "METAssoc_AntiKt4EMTopoHgg");

    m_assocMapHardestPVName  = config.getStr(m_name + ".METAssociactionMapHardPVName", "METAssoc_AntiKt4EMTopo");
    m_coreHardestPVName      = config.getStr(m_name + ".METCoreHardPVName", "METAssoc_AntiKt4EMTopo");

    m_metCst        = config.getStrV(m_name + ".METCST");
    m_metTypes      = config.getStrV(m_name + ".METTypes");

    m_uncalibElecs  = config.getStr("ElectronHandler.ContainerName", "Electrons");
    m_uncalibMuons  = config.getStr("MuonHandler.ContainerName", "Muons");


    // Configuration file following the recommendations of the MET CP group.

    m_configPrefix = config.getStr(m_name + ".ConfigPrefix", "METUtilities/data17_13TeV/prerec_Jan16");

    // Configuration option to reconstruct MET ONLY with regard to
    // the hardest vertex. Need to be used in derivations different
    // than HIGG1D1. Set to true by default.

    m_useHardestVertex = config.getBool("HgammaAnalysis.UseHardestVertex", true);
    m_doPFlow = config.getBool(m_name + ".DoPFlow", false);

    if (m_doPFlow)
    {HG::fatal("DoPFlow option is set to true. PFlow MET is not correctly reconstructed by the ETmissHandler yet. Please, remove this message in Root/ETmissHandler.cxx only for performance studies.");}

    // METMaker tool for diphoton vertex etmiss reconstruction.

    m_metMakerTool.setTypeAndName("met::METMaker/METMaker");
    ANA_CHECK(m_metMakerTool.setProperty("JetSelection", config.getStr("ETmissHandler.JetSelection", "Tight").Data()));
    ANA_CHECK(m_metMakerTool.setProperty("DoPFlow", m_doPFlow));
    ANA_CHECK(m_metMakerTool.retrieve());

    // METMaker tool for hardest vertex etmiss reconstruction.

    m_metMakerHardTool.setTypeAndName("met::METMaker/METMakerHardVertex");
    ANA_CHECK(m_metMakerHardTool.setProperty("JetSelection", config.getStr("ETmissHandler.JetSelection", "Tight").Data()));
    ANA_CHECK(m_metMakerHardTool.setProperty("JetJvtMomentName", config.getStr("ETmissHandler.hardJVTName", "hardVertexJvt").Data()));
    ANA_CHECK(m_metMakerHardTool.setProperty("DoPFlow", m_doPFlow));
    ANA_CHECK(m_metMakerHardTool.retrieve());

    metSignifTool.setTypeAndName("met::METSignificance/metSignif");
    ANA_CHECK(metSignifTool.setProperty("SoftTermParam", met::Random));
    ANA_CHECK(metSignifTool.setProperty("TreatPUJets",   true));
    ANA_CHECK(metSignifTool.setProperty("DoPhiReso",     true));
    ANA_CHECK(metSignifTool.setProperty("JetCollection", config.getStr("ETmissHandler.JetType", "AntiKt4EMTopo").Data()));
    // ANA_CHECK( metSignifTool.setProperty("OutputLevel", MSG::VERBOSE) );
    ANA_CHECK(metSignifTool.retrieve());

    // METSystematics tool

    m_metSysTool = new met::METSystematicsTool("METSystematicsTool");
    CP_CHECK(m_name, m_metSysTool->setProperty("ConfigPrefix", m_configPrefix));

    //      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigPrefix", "METUtilities/data15_13TeV/Dec15v1"                                       ));
    //      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigPrefix", "METUtilities/data16_13TeV/prerec_May16v1"                                       ));
    //      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigPrefix", "METUtilities/data16_13TeV/rec_Dec16v1"                                       ));

    if (m_doPFlow) {  // PFlow MET
      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigSoftTrkFile", "TrackSoftTerms-pflow.config"));
    } else if (HG::isAFII()) { // AtlFast-II (Fast Simulation)
      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigSoftTrkFile", "TrackSoftTerms_AFII.config"));
    } else { // Full Simulation : Standard TST MET
      CP_CHECK(m_name, m_metSysTool->setProperty("ConfigSoftTrkFile", "TrackSoftTerms.config"));
    }

    //    CP_CHECK(m_name, m_metSysTool->setProperty("JetColl"         , config.getStr("JetHandler.ContainerName", "AntiKt4EMTopoJets").Data()));
    //    if (m_configPrefix.find("data17") == std::string::npos) {
    //  CP_CHECK(m_name, m_metSysTool->setProperty("ConfigJetTrkFile", "JetTrackSyst.config"));
    //}

    if (m_metSysTool->initialize().isFailure())
    { fatal("Failed to initialize METSystematicTool"); }

    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  void ETmissHandler::addGhostsToJets(xAOD::JetContainer &calibJets)
  {


    const xAOD::MuonContainer *uncalibMuons = nullptr;
    const xAOD::ElectronContainer *uncalibElecs = nullptr;

    if (m_event->retrieve(uncalibMuons, m_uncalibMuons).isFailure())
    { fatal("ETmiss Handler : Failed to retrieve muon container for ghost association. Exiting"); }

    if (m_event->retrieve(uncalibElecs, m_uncalibElecs).isFailure())
    { fatal("ETmiss Handler : Failed to retrieve muon container for ghost association. Exiting"); }

    met::addGhostMuonsToJets(*uncalibMuons, calibJets);
    //    met::addGhostElecsToJets( *uncalibElecs, calibJets );

  }
  //______________________________________________________________________________

  bool ETmissHandler::calculateRefGamma(const xAOD::PhotonContainer *photons,
                                        const xAOD::MissingETAssociationMap *metMap,
                                        const xAOD::MissingETContainer * /*coreMet*/,
                                        xAOD::MissingETContainer *outputMet)
  {

    // Calculate photons contribution to MET.
    if (!m_doHardVertex) {
      HG_CHECK(m_name, m_metMakerTool->rebuildMET("RefGamma", xAOD::Type::Photon, outputMet, photons, metMap));
    } else {
      HG_CHECK(m_name, m_metMakerHardTool->rebuildMET("RefGamma", xAOD::Type::Photon, outputMet, photons, metMap));
    }

    return true;

  }
  //______________________________________________________________________________

  bool ETmissHandler::calculateRefElectron(const xAOD::ElectronContainer *electrons,
                                           const xAOD::MissingETAssociationMap *metMap,
                                           const xAOD::MissingETContainer * /*coreMet*/,
                                           xAOD::MissingETContainer *outputMet)
  {

    // Calculate electrons contribution to MET.

    if (!m_doHardVertex) {
      HG_CHECK(m_name, m_metMakerTool->rebuildMET("RefEle", xAOD::Type::Electron, outputMet, electrons, metMap));
    } else {
      HG_CHECK(m_name, m_metMakerHardTool->rebuildMET("RefEle", xAOD::Type::Electron, outputMet, electrons, metMap));
    }

    return true;

  }
  //______________________________________________________________________________

  bool ETmissHandler::calculateRefMuon(const xAOD::MuonContainer *muons,
                                       const xAOD::MissingETAssociationMap *metMap,
                                       const xAOD::MissingETContainer * /*coreMet*/,
                                       xAOD::MissingETContainer *outputMet)
  {

    // Calculate muons contribution to MET.
    if (!m_doHardVertex) {
      HG_CHECK(m_name, m_metMakerTool->rebuildMET("Muons", xAOD::Type::Muon, outputMet, muons, metMap));
    } else {
      HG_CHECK(m_name, m_metMakerHardTool->rebuildMET("Muons", xAOD::Type::Muon, outputMet, muons, metMap));
    }

    return true;

  }
  //______________________________________________________________________________
  bool ETmissHandler::calculateRefJetandSoftTerms(const xAOD::JetContainer *jets,
                                                  const xAOD::MissingETAssociationMap *metMap,
                                                  const xAOD::MissingETContainer *coreMet,
                                                  xAOD::MissingETContainer *outputMet, bool doJvt)
  {

    // Calculate Jet and soft-term contribution to MET. These contributions are calculated together, considering
    // if we want to use Jvt or not (Jvt cut applied depending how metMaker is configured : setProperty("CustomJetJvtCut")
    // or setProperty("JetSelection"). Correct SoftClus must be calculated without applying a Jvt cut (doJvt = false)

    if (!m_doHardVertex) {
      HG_CHECK(m_name, m_metMakerTool->rebuildJetMET("RefJet", "SoftClus", "PVSoftTrk", outputMet, jets, coreMet, metMap, doJvt));


    } else {
      HG_CHECK(m_name, m_metMakerHardTool->rebuildJetMET("RefJet", "SoftClus", "PVSoftTrk", outputMet, jets, coreMet, metMap, doJvt));
    }


    return true;
  }
  //______________________________________________________________________________

  StatusCode ETmissHandler::doHardVertex(const xAOD::PhotonContainer   *photons,
                                         const xAOD::ElectronContainer *electrons,
                                         const xAOD::MuonContainer     *muons,
                                         const xAOD::JetContainer      *jets,
                                         xAOD::MissingETContainer      *outMet)
  {

    m_doHardVertex = true;

    // Retrieve the MET association map: Needed for METMaker tool
    const xAOD::MissingETAssociationMap *metMapHardVertex = nullptr;

    if (m_event->retrieve(metMapHardVertex, m_assocMapHardestPVName).isFailure())
    { fatal("Unable to retrieve " + m_assocMapHardestPVName + " from TEvent"); }

    // Retrieve the MET core container: Needed for METMaker tool
    const xAOD::MissingETContainer *coreMetHardVertex  = nullptr;

    if (m_event->retrieve(coreMetHardVertex, m_coreHardestPVName).isFailure())
    { fatal("Unable to retrieve " + m_coreHardestPVName + " from TEvent"); }

    // Reinitialize MET association map flags to ensure a correct overlap removal.

    metMapHardVertex->resetObjSelectionFlags();

    // Calculate different Ref terms. Calculate SoftClus properly => Calculate CST.

    calculateRefGamma(photons, metMapHardVertex, coreMetHardVertex, outMet);
    calculateRefElectron(electrons, metMapHardVertex, coreMetHardVertex, outMet);
    calculateRefMuon(muons, metMapHardVertex, coreMetHardVertex, outMet);

    /* Removed since new tool can't store two different soft terms in same container
    calculateRefJetandSoftTerms(jets,metMapHardVertex,coreMetHardVertex,outMet,false);

    // Apply possible systematic uncertainty shifts: CST
    if (HG::isMC()) {
      xAOD::MissingET *softClusMet = (*outMet)["SoftClus"];
      if (softClusMet == nullptr)
        fatal("Couldn't retrieve SoftClus from outMet, exiting!");
      CC_CHECK(m_name, m_metSysTool->applyCorrection(*softClusMet));
    }

    HG_CHECK(m_name, m_metMakerHardTool->buildMETSum("CST", outMet, MissingETBase::Source::LCTopo ));

    // We clear RefJet, SoftClus and PVSoftTrk from outMet => Calculate TST

    for (auto metCST: *outMet) {
      if (std::find(m_metCst.begin(), m_metCst.end(), metCST->name().c_str()) != m_metCst.end())
        metCST->clear();
    }

    // Calculate Jet and Soft-terms. Calculate PVSoftTrk properly. */

    calculateRefJetandSoftTerms(jets, metMapHardVertex, coreMetHardVertex, outMet, true);

    // Apply possible systematic uncertainty shifts: TST
    if (HG::isMC()) {
      xAOD::MissingET *softTrkMet = (*outMet)["PVSoftTrk"];

      if (softTrkMet == nullptr)
      { fatal("Couldn't retrieve PVSoftTrk from outMet, exiting!"); }

      CC_CHECK(m_name, m_metSysTool->applyCorrection(*softTrkMet));
    }

    HG_CHECK(m_name, m_metMakerHardTool->buildMETSum("TST", outMet, MissingETBase::Source::Track));

    return StatusCode::SUCCESS;

  }
  //______________________________________________________________________________

  StatusCode ETmissHandler::doDiphotonVertex(const xAOD::PhotonContainer   *photons,
                                             const xAOD::ElectronContainer *electrons,
                                             const xAOD::MuonContainer     *muons,
                                             const xAOD::JetContainer      *jets,
                                             xAOD::MissingETContainer      *outMet)
  {

    m_doHardVertex = false;

    // Retrieve the MET association map: Needed for METMaker tool
    const xAOD::MissingETAssociationMap *metMap = nullptr;

    if (m_event->retrieve(metMap, m_assocMapName).isFailure())
    { fatal("Unable to retrieve " + m_assocMapName + " from TEvent"); }

    // Retrieve the MET core container: Needed for METMaker tool
    const xAOD::MissingETContainer *coreMet  = nullptr;

    if (m_event->retrieve(coreMet, m_coreName).isFailure())
    { fatal("Unable to retrieve " + m_coreName + " from TEvent"); }

    // Reinitialize MET association map flags to ensure a correct overlap removal.

    metMap->resetObjSelectionFlags();

    // Calculate different Ref terms. Calculate SoftClus properly => Calculate CST.

    calculateRefGamma(photons, metMap, coreMet, outMet);
    calculateRefElectron(electrons, metMap, coreMet, outMet);
    calculateRefMuon(muons, metMap, coreMet, outMet);

    /* Removed since new tool can't store two different soft terms in same container
    calculateRefJetandSoftTerms(jets,metMap,coreMet,outMet,false);

    // Apply possible systematic uncertainty shifts: CST
    if (HG::isMC()) {
      xAOD::MissingET *softClusMet = (*outMet)["SoftClus"];
      if (softClusMet == nullptr)
        fatal("Couldn't retrieve SoftClus from outMet, exiting!");
      CC_CHECK(m_name, m_metSysTool->applyCorrection(*softClusMet));
    }

    HG_CHECK(m_name, m_metMakerTool->buildMETSum("CST", outMet, MissingETBase::Source::LCTopo ));

    // We clear RefJet, SoftClus and PVSoftTrk from outMet => Calculate TST
    for (auto metCST: *outMet) {
      if (std::find(m_metCst.begin(), m_metCst.end(), metCST->name().c_str()) != m_metCst.end())
        metCST->clear();
    }

    // Calculate Jet and Soft-terms. Calculate PVSoftTrk properly. */

    calculateRefJetandSoftTerms(jets, metMap, coreMet, outMet, true);

    // Apply possible systematic uncertainty shifts: TST
    if (HG::isMC()) {
      xAOD::MissingET *softTrkMet = (*outMet)["PVSoftTrk"];

      if (softTrkMet == nullptr)
      { fatal("Couldn't retrieve PVSoftTrk from outMet, exiting!"); }

      CC_CHECK(m_name, m_metSysTool->applyCorrection(*softTrkMet));
    }

    HG_CHECK(m_name, m_metMakerTool->buildMETSum("TST", outMet, MissingETBase::Source::Track));

    return StatusCode::SUCCESS;

  }

  //______________________________________________________________________________
  xAOD::MissingETContainer ETmissHandler::getCorrectedContainer()
  {

    // Get Shallow copy from TEvent/TStore
    // second argument false --> make empty raw contianer (not shallow copy of xAOD), since it's rebuilt below
    bool calib = false;
    xAOD::MissingETContainer shallowContainer = getShallowContainer(calib, false);

    if (calib) {
      xAOD::MissingETContainer corrected(SG::VIEW_ELEMENTS);

      for (auto type : m_metTypes) {
        corrected.push_back(shallowContainer[type.Data()]);
      }

      return corrected;
    }

    xAOD::MissingETContainer dummyContainer;
    return dummyContainer;
  }
  //______________________________________________________________________________
  xAOD::MissingETContainer ETmissHandler::getCorrectedContainer(xAOD::PhotonContainer   *photons,
      xAOD::JetContainer      *jets,
      xAOD::ElectronContainer *electrons,
      xAOD::MuonContainer     *muons)
  {
    bool calib = false;


    static SG::AuxElement::Accessor<float> ph_pt("pt");
    static SG::AuxElement::Accessor<float> ph_px("px");
    static SG::AuxElement::Accessor<float> ph_py("py");
    static SG::AuxElement::Accessor<float> ph_E("E");
    static SG::AuxElement::Accessor<float> ph_pz("pz");
    static SG::AuxElement::Accessor<float> ph_eta("eta");
    static SG::AuxElement::Accessor<float> ph_phi("phi");

    static SG::AuxElement::Accessor<float> ph_hard_pt("hardVertexPt");
    static SG::AuxElement::Accessor<float> ph_hard_eta("hardVertexEta");
    static SG::AuxElement::Accessor<float> ph_hard_phi("hardVertexPhi");

    static SG::AuxElement::Accessor<float> ph_diphot_pt("diphotVertexPt");
    static SG::AuxElement::Accessor<float> ph_diphot_eta("diphotVertexEta");
    static SG::AuxElement::Accessor<float> ph_diphot_phi("diphotVertexPhi");


    const xAOD::EventInfo *eventInfo = nullptr;

    if (m_event->retrieve(eventInfo, "EventInfo").isFailure()) {
      HG::fatal("Cannot access EventInfo");
    }

    // Get Shallow copy from TEvent/TStore
    // second argument false --> make empty raw contianer (not shallow copy of xAOD), since it's rebuilt below

    xAOD::MissingETContainer shallowContainer = getShallowContainer(calib, false);

    if (calib) {
      xAOD::MissingETContainer corrected(SG::VIEW_ELEMENTS);

      for (auto type : m_metTypes) {
        corrected.push_back(shallowContainer[type.Data()]);
      }

      return corrected;
    }


    xAOD::JetContainer *corr_jets = const_cast<xAOD::JetContainer *>(jets);
    addGhostsToJets(*corr_jets);

    // If requested to recalculate MET w.r.t diphoton vertex, ensure that needed
    // containers are present in the input file.

    if (!m_useHardestVertex && (!m_event->contains<xAOD::MissingETAssociationMap>(m_assocMapName) || !m_event->contains<xAOD::MissingETContainer>(m_coreName))) {
      fatal("UseHardestVertex flag set to false but no association map named" + m_assocMapName + " or Core container " + m_coreName + "found in input file."
            "Please change " + m_name + ".METAssociactionMapName or " + m_name + ".METCoreName to a valid name or set " + m_name + ".UseHardestVertex option to YES.");
    }

    // If there are no primary vertices, return default container

    if (m_event->contains<xAOD::VertexContainer>("PrimaryVertices")) { // For DxAODs
      const xAOD::VertexContainer *vertices = nullptr;

      if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
      { HG::fatal("Couldn't retrieve PrimaryVertices inside ETmissHandler"); }

      int NPV = 0;

      for (auto vertex : *vertices) {
        if (vertex->vertexType() == xAOD::VxType::PriVtx) {
          NPV++;
          break;
        }
      }

      if (NPV == 0) {
        Warning("ETmissHandler::getCorrectedContainer()", "No primary vertices found, returning MET container with empty components");

        for (auto type : m_metTypes) {
          xAOD::MissingET *noPVmet = new xAOD::MissingET(0, 0, 0, type.Data());
          shallowContainer.push_back(noPVmet);
        }

        /* Removed since new tool can't store two different soft terms in same container
        for(auto type : m_metCst) {
          xAOD::MissingET* noPVmet = new xAOD::MissingET(0,0,0,type.Data());
          shallowContainer.push_back(noPVmet);
        }
        */

        xAOD::MissingETContainer noPVcont(SG::VIEW_ELEMENTS);

        for (auto type : m_metTypes)
        { noPVcont.push_back(shallowContainer[type.Data()]); }

        return noPVcont;
      }
    }

    // For MET rebuilding, need access to the actual container linked with the auxdata
    // which is already in TStore (HgammaHandler magic)

    TString shallowName = "Shallow" + m_containerName + m_sysName;
    xAOD::MissingETContainer *shallowMet  = nullptr;

    if (m_store->retrieve(shallowMet, shallowName.Data()).isFailure())
    { fatal("Unable to retrieve ShallowMET from TEvent"); }

    // Create photon container with pT and eta w.r.t hardest vertex.

    xAOD::MissingETContainer *diphotonMet = new xAOD::MissingETContainer();
    xAOD::AuxContainerBase *diphotonMetAux = new xAOD::AuxContainerBase();
    diphotonMet->setStore(diphotonMetAux);

    xAOD::MissingETContainer *hardMet = new xAOD::MissingETContainer();
    xAOD::AuxContainerBase *hardMetAux = new xAOD::AuxContainerBase();
    hardMet->setStore(hardMetAux);

    if (!m_useHardestVertex)
    { HG_CHECK(m_name, doDiphotonVertex(photons, electrons, muons, corr_jets, diphotonMet)); }

    // Create photon container with pT and eta w.r.t hardest vertex.

    for (xAOD::Photon *photon : *photons) {
      ph_diphot_pt(*photon) = ph_pt(*photon);
      ph_diphot_eta(*photon) = ph_eta(*photon);
      ph_diphot_phi(*photon) = ph_phi(*photon);
      photon->setP4(ph_hard_pt(*photon), ph_hard_eta(*photon), ph_hard_phi(*photon), 0);
    }

    HG_CHECK(m_name, doHardVertex(photons, electrons, muons, corr_jets, hardMet));

    for (xAOD::Photon *photon : *photons) {
      photon->setP4(ph_diphot_pt(*photon), ph_diphot_eta(*photon), ph_diphot_phi(*photon), 0);
    }


    xAOD::MissingETContainer corrected(SG::VIEW_ELEMENTS);

    // If there is no MissingETAssociationMap calculated w.r.t the diphoton selected vertex, and
    // the tool is configured to use this, stop and complain
    if (!m_useHardestVertex) {
      if (!m_event->contains<xAOD::MissingETAssociationMap>(m_assocMapName) || !m_event->contains<xAOD::MissingETContainer>(m_coreName))
      { fatal("ETmissHandler configured for diphoton pointing MET, but association map doesn't exist?"); }
    }

    // Rename all Ref Terms from hardest vertex MET and store them in the output container.
    for (auto metTerm : *hardMet) {
      xAOD::MissingET *newMet = new xAOD::MissingET();
      shallowMet->push_back(newMet);
      *newMet = *metTerm;

      if (!m_useHardestVertex) { newMet->setName("hardVertex" + metTerm->name()); }

      corrected.push_back(newMet);
    }

    //save METSignificance as an MET object (ex=metsig, ey=0, et=metsig).

    //bugs, comment it for the time being

    //    metSignifTool->varianceMET(hardMet, eventInfo->averageInteractionsPerCrossing(), "RefJet", "PVSoftTrk", "TST");
    //    double m_metSigHard = metSignifTool->GetSignificance();

    //double m_metSigHard = 0;

    //xAOD::MissingET *newMetSigHard = new xAOD::MissingET(m_metSigHard, 0, m_metSigHard, "metSig");

    //if (!m_useHardestVertex) { newMetSigHard->setName("hardVertexmetSig"); }

    //shallowMet->push_back(newMetSigHard);

    //corrected.push_back(newMetSigHard);

    if (!m_useHardestVertex) {
      for (auto metTerm : *diphotonMet) {
        xAOD::MissingET *newMet = new xAOD::MissingET();
        shallowMet->push_back(newMet);
        *newMet = *metTerm;
        corrected.push_back(newMet);
      }

      //bugs, comment it for the time being

      //      metSignifTool->varianceMET(diphotonMet, eventInfo->averageInteractionsPerCrossing(), "RefJet", "PVSoftTrk", "TST");
      //      double m_metSig = metSignifTool->GetSignificance();

      //double m_metSig = 0;

      //xAOD::MissingET *newMetSig = new xAOD::MissingET(m_metSig, 0, m_metSig, "metSig");
      //shallowMet->push_back(newMetSig);

      //corrected.push_back(newMetSig);
    }

    delete diphotonMet;
    delete diphotonMetAux;
    delete hardMet;
    delete hardMetAux;

    // Return MET container: RefGamma, RefEle, Muons               => Common to CST and TST
    //                       CST                                   => Only CST MET saved (needed an extra shallowMet container for RefJet and SoftClus)
    //                       RefJet, PVSoftTrk, SoftClus and TST   => From TST
    //                                                             => and all previous MET terms with prefix "hardVertex"
    //                                                               (MET terms calculated consistently with respect to the hardest vertex of the event)

    return corrected;
  }



  //______________________________________________________________________________
  xAOD::MissingETContainer ETmissHandler::applySelection(xAOD::MissingETContainer &container)
  {
    xAOD::MissingETContainer selected(SG::VIEW_ELEMENTS);

    for (auto met : container) {
      // Limit MET to the types specified in config (TST, CST, ...)
      if (std::find(m_metTypes.begin(), m_metTypes.end(), met->name().c_str()) == m_metTypes.end())
      { continue; }

      selected.push_back(met);
    }

    return selected;
  }

  //______________________________________________________________________________
  CP::SystematicCode ETmissHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    bool isAffected = false;

    for (auto var : sys) {
      if (m_metSysTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }
    }

    if (isAffected) {
      if (HG::isMC()) { CP_CHECK(m_name, m_metSysTool->applySystematicVariation(sys)); }
    } else {
      if (HG::isMC()) { CP_CHECK(m_name, m_metSysTool->applySystematicVariation(CP::SystematicSet())); }
    }

    // For MET, always make new container since shifting hard objects indirectly affects result
    m_sysName = sys.name() == "" ? "" : "_" + sys.name();

    return CP::SystematicCode::Ok;
  }

}

