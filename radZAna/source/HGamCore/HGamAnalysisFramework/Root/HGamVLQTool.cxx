///////////////////////////////////////////
// VLQ yyb Tool
// Tool performs all the yyb selections
// All info decorated to MxAODs
// @author: jahred.adelman@cern.ch
// @author: elizabeth.brost@cern.ch
///////////////////////////////////////////

// STL includes
#include <algorithm>

// ATLAS/ROOT includes
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"

// Local includes
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HGamVLQTool.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"

namespace HG {
  // _______________________________________________________________________________________
  HGamVLQTool::HGamVLQTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler, const bool & /*isMC*/)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_BTagWP("MV2c10_HybBEff_77")
    , m_minMuonPT(10)
    , m_minElectronPT(10)
    , m_centraljet_eta_max(2.5)
    , m_centraljet_pt_min(25)
    , m_forwardjet_eta_max(4.4)
    , m_forwardjet_eta_min(1.8)
    , m_forwardjet_pt_min(25)
    , m_yyb_min(300)
    , m_ystar_max(1.1)
    , m_pt_priority_bjet(true)
    , btagWPMin(WP85)
  {}

  // _______________________________________________________________________________________
  HGamVLQTool::~HGamVLQTool()
  {
  }

  // _______________________________________________________________________________________
  HG::EventHandler *HGamVLQTool::eventHandler()
  {
    return m_eventHandler;
  }

  // _______________________________________________________________________________________
  HG::TruthHandler *HGamVLQTool::truthHandler()
  {
    return m_truthHandler;
  }

  // _______________________________________________________________________________________
  EL::StatusCode HGamVLQTool::initialize(Config &config)
  {
    // Set default cut values etc here...
    // Almost all of these are set hard-coded because this will be used by the nominal MxAOD production
    // Also safer :)

    m_minMuonPT = config.getNum("HGamVLQTool.MinMuonPt", 10.0); //! minimum muon pt (in GeV) for lepton veto
    ATH_MSG_INFO("MinMuonPt.................................. " << m_minMuonPT);

    m_minElectronPT = config.getNum("HGamVLQTool.MinElectronPt", 10.0); //! minimum electron pt (in GeV) for lepton veto
    ATH_MSG_INFO("MinElectronPt.............................. " << m_minElectronPT);

    m_centraljet_eta_max = config.getNum("HGamVLQTool.MaxCentralJetEta", 2.5); //! Max central jet abs eta
    ATH_MSG_INFO("MaxCentralJetEta........................... " << m_centraljet_eta_max);

    m_centraljet_pt_min = config.getNum("HGamVLQTool.MinCentralJetPt", 25); //! Min central jet abs pt
    ATH_MSG_INFO("MinCentralJetPt........................... " << m_centraljet_pt_min);

    m_forwardjet_eta_max = config.getNum("HGamVLQTool.MaxForwardJetEta", 4.4); //! Max forward jet abs eta
    ATH_MSG_INFO("MaxForwardJetEta........................... " << m_forwardjet_eta_max);

    m_forwardjet_eta_min = config.getNum("HGamVLQTool.MinForwardJetEta", 1.8); //! Min forward jet abs eta
    ATH_MSG_INFO("MinForwardJetEta........................... " << m_forwardjet_eta_min);

    m_forwardjet_pt_min = config.getNum("HGamVLQTool.MinForwardJetPt", 25); //! Min forward jet abs pt
    ATH_MSG_INFO("MinForwardJetPt........................... " << m_forwardjet_pt_min);

    m_yyb_min = config.getNum("HGamVLQTool.MinMYYB", 300); //! Mininum yy+b max to consider
    ATH_MSG_INFO("MinMYYB.................................... " << m_yyb_min);

    m_pt_priority_bjet = config.getBool("HGamVLQTool.PriorityToPtForBJet", true); //! use pt first instead of first btag WP for selecting jet
    ATH_MSG_INFO("Prioritize Pt for selecting bjet..........? " << (m_pt_priority_bjet ? "YES" : "NO"));

    btagWPMin = WP85;
    ATH_MSG_INFO("minBtagWP................................. " << btagWPMin);

    m_ystar_max = config.getNum("HGamVLQTool.MaxYStar", 1.1); //! maximum y*
    ATH_MSG_INFO("MaxYStar................................. " << m_ystar_max);

    if (m_BTagWP == "MV2c10_HybBEff_77") { btagWPMin = WP77; }
    else if (m_BTagWP == "MV2c10_HybBEff_70") { btagWPMin = WP70; }
    else if (m_BTagWP == "MV2c10_HybBEff_60") { btagWPMin = WP60; }

    return EL::StatusCode::SUCCESS;
  }

  // _______________________________________________________________________________________
  void HGamVLQTool::saveHGamVLQInfo(xAOD::PhotonContainer photons,
                                    xAOD::JetContainer   &jets,
                                    xAOD::JetContainer   &jetsJVT,
                                    xAOD::MuonContainer &muons,
                                    xAOD::ElectronContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
    std::map<TString, float> eventInfoFloats;
    std::map<TString, int>   eventInfoInts;


    // Perform selection
    performSelection(photons, jets, jetsJVT, muons, electrons,
                     eventInfoFloats, eventInfoInts);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToEventInfo(eventInfoFloats, eventInfoInts);
  }

  // _______________________________________________________________________________________
  // Function which performs full yybb selection and saves info to info maps (nominal by default)
  void HGamVLQTool::performSelection(xAOD::PhotonContainer photons,
                                     xAOD::JetContainer &jets,
                                     xAOD::JetContainer &jets_noJVT,
                                     xAOD::MuonContainer &muons,
                                     xAOD::ElectronContainer &electrons,
                                     std::map<TString, float> &eventInfoFloats,
                                     std::map<TString, int> &eventInfoInts)
  {


    // First apply photon selection
    if (photons.size() < 2) {
      eventInfoInts["yyb_cutFlow"] = PHOTONS;
      return;
    }


    // Next apply lepton veto
    xAOD::MuonContainer muonsPassing(SG::VIEW_ELEMENTS);

    // --------
    // Apply muon requirements
    std::copy_if(muons.begin(), muons.end(), std::back_inserter(muonsPassing), [this](const xAOD::Muon * muon) {
      return (muon->pt() >= m_minMuonPT * HG::GeV);
    });


    xAOD::ElectronContainer electronsPassing(SG::VIEW_ELEMENTS);

    // --------
    // Apply electron requirements
    std::copy_if(electrons.begin(), electrons.end(), std::back_inserter(electronsPassing), [this](const xAOD::Electron * electron) {
      return (electron->pt() >= m_minElectronPT * HG::GeV);
    });

    if (muonsPassing.size() > 0) { eventInfoInts["yyb_cutFlow"] = LEPVETO; return;}

    if (electronsPassing.size() > 0) { eventInfoInts["yyb_cutFlow"] = LEPVETO; return;}


    xAOD::JetContainer jets_central(SG::VIEW_ELEMENTS);

    // --------
    // Find the jets

    // note that "central jets" and "forward jets" at this stage do not have to be orthogonal
    std::vector<int> centraljets;
    std::vector<int> forwardjets;
    int counter = 0; // counter. This is very inelegant, but works

    for (auto jet : jets) {
      if (fabs(jet->eta()) <= m_centraljet_eta_max && jet->pt() >= m_centraljet_pt_min * HG::GeV) {
        centraljets.push_back(counter);
      }

      if (fabs(jet->eta()) >= m_forwardjet_eta_min && fabs(jet->eta()) <= m_forwardjet_eta_max &&
          jet->pt() >= m_forwardjet_pt_min * HG::GeV) {
        forwardjets.push_back(counter);
      }

      counter++;
    }

    // Check for central jets
    if (centraljets.size() == 0) { eventInfoInts["yyb_cutFlow"] = CENJET; return;}

    // Apply btagging requirement on the above central jets
    TLorentzVector TaggedJetToUse;
    TaggedJetToUse.SetPtEtaPhiM(0, 0, 0, 0);
    btagWP WPJetToUse = WP100;
    Int_t TaggedJetIndex(-1); // If this is -1 we didn't find a tagged jet

    for (auto jetIndex : centraljets) {
      const xAOD::Jet *jet = jets[jetIndex];

      if (jet->auxdata<char>(m_BTagWP.Data())) {
        if (m_pt_priority_bjet) { // don't use the btagging priority, only care if it passes the baseline cut
          if (TaggedJetToUse.Pt() < jet->pt()) {
            TaggedJetToUse.SetPtEtaPhiM(jet->pt(), jet->eta(), jet->phi(), jet->m());
            TaggedJetIndex = jetIndex;
          }
        } else { // pick tightest WP first
          btagWP btagWPThisJet = WP85; // it was tagged, so minimum of 85%

          if (jet->isAvailable<char>("MV2c10_HybBEff_60") && jet->auxdata<char>("MV2c10_HybBEff_60")) {
            btagWPThisJet = WP60;
          } else if (jet->auxdata<char>("MV2c10_HybBEff_70")) {
            btagWPThisJet = WP70;
          } else if (jet->auxdata<char>("MV2c10_HybBEff_77")) {
            btagWPThisJet = WP77;
          }

          if (btagWPThisJet == WPJetToUse) { // the same WP, resort to pt
            if (TaggedJetToUse.Pt() < jet->pt()) {
              TaggedJetToUse.SetPtEtaPhiM(jet->pt(), jet->eta(), jet->phi(), jet->m());
              TaggedJetIndex = jetIndex;
            }
          } else if (btagWPThisJet > WPJetToUse) { // this jet wins
            TaggedJetToUse.SetPtEtaPhiM(jet->pt(), jet->eta(), jet->phi(), jet->m());
            WPJetToUse = btagWPThisJet;
            TaggedJetIndex = jetIndex;
          }
        }
      }
    }

    if (TaggedJetIndex == -1) { //kludge to check that we found a jet
      eventInfoInts["yyb_cutFlow"] = BTAG;
      return;
    }

    // Apply forward jet requirement, too. If no forward jets, return
    if (forwardjets.size() == 0) {eventInfoInts["yyb_cutFlow"] = FORWARDJET; return;}

    // Or if there is only one forward jet and it is the tagged jet, we also return
    if (forwardjets.size() == 1 && forwardjets[0] == TaggedJetIndex) {eventInfoInts["yyb_cutFlow"] = FORWARDJET; return;}

    // We succeeded, so now find the highest pt forward jet, since we record it. But ignore the btagged jet
    Int_t ForwardJetIndex(-1);
    Double_t maxForwardJetPt(0);

    for (auto jetIndex : forwardjets) {
      if (jetIndex == TaggedJetIndex) { continue; }

      const xAOD::Jet *jet = jets[jetIndex];

      if (jet->pt() > maxForwardJetPt) {
        ForwardJetIndex = jetIndex;
        maxForwardJetPt = jet->pt();
      }
    }


    TLorentzVector yybVec = TaggedJetToUse + photons[0]->p4() + photons[1]->p4();
    TLorentzVector yyVec = photons[0]->p4() + photons[1]->p4();

    if (yyVec.M() >= 122.5 * HG::GeV && yyVec.M() <= 127.5 * HG::GeV) {
      eventInfoInts["yyb_m_yy_cat"] = SR;
    } else if (yyVec.M() <= 120 * HG::GeV || yyVec.M() >= 130 * HG::GeV) {
      eventInfoInts["yyb_m_yy_cat"] = SIDEBANDS;
    } else {
      eventInfoInts["yyb_m_yy_cat"] = VR;
    }

    if (yybVec.M() < m_yyb_min * HG::GeV) {
      eventInfoInts["yyb_cutFlow"] = YYBMASS;
      return;
    }

    if (0.5 * fabs(yyVec.Rapidity() - TaggedJetToUse.Rapidity()) > m_ystar_max) {
      eventInfoInts["yyb_cutFlow"] = YSTAR;
      return;
    }

    eventInfoInts["yyb_cutFlow"] = PASSYYB;

    eventInfoFloats["yyb_VLQmass"] = yybVec.M() * HG::invGeV;
    eventInfoInts["yyb_SelectedTaggedJet"] = TaggedJetIndex;
    eventInfoInts["yyb_SelectedForwardJet"] = ForwardJetIndex;

    const xAOD::Jet *TaggedJet = jets[TaggedJetIndex];
    const xAOD::Jet *ForwardJet = jets[ForwardJetIndex];

    // --------
    // Save truth flavor info for selected jets
    eventInfoInts["yyb_truth_label_taggedjet"] = int((HG::isMC())) ? TaggedJet->auxdata<int>("HadronConeExclTruthLabelID") : -99 ;
    eventInfoInts["yyb_truth_label_forwardjet"] = int((HG::isMC())) ? ForwardJet->auxdata<int>("HadronConeExclTruthLabelID") : -99 ;

    // --------
    // Calculate the JVT scale-factor from uncorrected, no-JVT jets
    double weight_JVT = weightJVT(jets_noJVT);

    // --------
    // Calculate the b-tagging scale-factor from the candidate jet
    double weight_b_tagging = weightBTagging(TaggedJet);

    // --------
    // Get HGamVLQ weight from JVT, b-tagging and NLO scale factors
    eventInfoFloats["yyb_weight"] = weight_JVT * weight_b_tagging;
  }




  // _______________________________________________________________________________________
  // Fetch the btagging SF weight
  double HGamVLQTool::weightBTagging(const xAOD::Jet *jet)
  {

    TString SFName = TString("SF_") + m_BTagWP;
    return jet->isAvailable<float>(SFName.Data()) ? jet->auxdata<float>(SFName.Data()) : -1;
  }


  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  double HGamVLQTool::weightJVT(xAOD::JetContainer &jets_noJVT)
  {

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > m_centraljet_eta_max) { continue; }

      if (jet->pt() < m_centraljet_pt_min * HG::invGeV) { continue; }

      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }



  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HGamVLQTool::saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts)
  {
    // Floats
    for (auto element : eventInfoFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : eventInfoInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }
  }

}
