// Local include(s):
#include "HGamAnalysisFramework/FCNCTool.h"

#include "TMVA/Reader.h"

namespace HG {

  //_______________________________________________________________________________________
  FCNCTool::FCNCTool(HG::EventHandler *eventHandler)
    : m_eventHandler(eventHandler)
  { }

  //_______________________________________________________________________________________
  HG::EventHandler *FCNCTool::eventHandler()
  {
    return m_eventHandler;
  }

  //_______________________________________________________________________________________
  EL::StatusCode FCNCTool::initialize(Config &config)
  {
    m_eventCounter = 0;

    m_debug = config.getInt("HGamFCNC.Debug", 0);

    // Up to Njets
    m_maxNjet    = config.getInt("HGamFCNC.maxNjet", 5);
    m_maxNjetLep = config.getInt("HGamFCNC.maxNjetLep", 5);

    // All the following could go to the config.
    // jet pT cuts
    m_jptCC = 30e3;
    m_jptCF = 30e3;
    m_doLooseJ = config.getBool("HGamFCNC.doLooseJ", false);

    if (m_doLooseJ) {
      m_jptCC = 25e3;
      Info("", "Applying loose central jet pt cut");
    }

    m_dofJVT = config.getBool("HGamFCNC.dofJVT", true);

    if (m_dofJVT) { Info("", "Applying forward JVT cut"); }

    m_FTagUseDL1 = config.getBool("HGamFCNC.FTagUseDL1", true);

    if (m_FTagUseDL1) { Info("", "Using DL1 for b- and c-tagging"); }

    // Top1, 2 "central" values (used to find the best combination for BDTs)
    m_mTop1Central = config.getNum("HGamFCNC.mT1Central", 171.);
    m_mTop2Central = config.getNum("HGamFCNC.mT2Central", 170.);

    // top mass windows
    m_mTop1HadLow  = config.getNum("HGamFCNC.mT1tightHadLo", 152.);
    m_mTop1HadHigh = config.getNum("HGamFCNC.mT1tightHadHi", 190.);
    m_mTop2HadLow  = config.getNum("HGamFCNC.mT2tightHadLo", 120.);
    m_mTop2HadHigh = config.getNum("HGamFCNC.mT2tightHadHi", 220.);
    m_mTop1LepLow  = config.getNum("HGamFCNC.mT1tightLepLo", 152.);
    m_mTop1LepHigh = config.getNum("HGamFCNC.mT1tightLepHi", 190.);
    m_mTop2LepLow  = config.getNum("HGamFCNC.mT2tightLepLo", 130.);
    m_mTop2LepHigh = config.getNum("HGamFCNC.mT2tightLepHi", 210.);

    // pT cut on electron (tighter than default in HGam a priori) and muon (default HGam a priori)
    m_ptelC = config.getNum("HGamFCNC.elpTCut", 15e3);
    m_ptmuC = config.getNum("HGamFCNC.mupTCut", 10e3);

    // m(electron-photon) mZ veto : not by default
    m_meyCutLow  = config.getNum("HGamFCNC.meyCutLow", 1e9);   // official 36 ifb analysis : veto if [84,94] GeV
    m_meyCutHigh = config.getNum("HGamFCNC.meyCutHigh", -1.);

    // mT cut
    m_mTC = config.getNum("HGamFCNC.mTCut", 30e3);

    // write detailed info (should be put to false for systematics)
    m_WriteDetails = config.getBool("HGamFCNC.doWriteDetails", false);

    if (InitBDT(config) == EL::StatusCode::FAILURE)
    { HG::fatal("Cannot initialize BDT"); }

    // truthhandler is null by default
    m_truthHandler = nullptr;
    std::vector<double> tqHlist{410804, 410805, 410806, 410807, 410824, 410825, 410826, 410827,
                                410764, 410765, 410766, 410767, 410768, 410769, 410770, 410771,
                                410752, 410753, 410754, 410755, 410756, 410757, 410758, 410759,
                                412051, 412052, 412053, 412054, 412055, 412056, 412057, 412058};
    m_mcidForMatch = config.getNumV("HGamFCNC.tqHMCList", tqHlist);

    m_writeTruthInfo = false;
    int mcid = getChannelNumber();

    // should be false for systematics...
    if (std::find(tqHlist.begin(), tqHlist.end(), mcid) != tqHlist.end()) {
      m_writeTruthInfo = true;
      m_WriteDetails   = true;
    }

    return EL::StatusCode::SUCCESS;
  }

  void FCNCTool::InitMap()
  {

    if (!m_declared) {
      m_declared = true;

      // Integer
      m_eventInfoInts.insert(std::make_pair("fcnc_cutFlow", 0));
      m_eventInfoInts.insert(std::make_pair("fcnc_njet", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_nbjet", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_njet30", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_idLep", 0));
      m_eventInfoInts.insert(std::make_pair("fcnc_ntopc", -1));
      m_eventInfoInts.insert(std::make_pair("fcnc_cat", -1));

      // Floats
      m_eventInfoFloats.insert(std::make_pair("fcnc_mT", -1.));
      m_eventInfoFloats.insert(std::make_pair("fcnc_etm", -1.));
      m_eventInfoFloats.insert(std::make_pair("fcnc_phim", 999.));

      if (HG::isMC()) { m_eventInfoFloats.insert(std::make_pair("fcnc_weight", 0.)); }

      if (m_WriteDetails) {
        m_eventInfoInts.insert(std::make_pair("fcnc_igoodComb", -1));
        m_eventInfoFloats.insert(std::make_pair("fcnc_MefftopSM", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mW", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_dRbW", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mindRbj", 9e9));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mtt", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pTb", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_maxdRcy", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_HT", -1.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_HTcjy0y1bjj0j1", -1.));
      }

      // Will save dummy T1 info for tH production
      if (m_truthHandler != nullptr && m_writeTruthInfo) {

        m_eventInfoInts.insert(std::make_pair("fcnc_T1qpid", 0));
        m_eventInfoInts.insert(std::make_pair("fcnc_T2qpid", 0));

        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pxTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pyTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pzTrueT1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT1q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT1qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT1qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT1qv", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pxTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pyTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_pzTrueT2", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2q", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2qv", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2qv", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueGam0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueGam0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueGam0;", -99.));

        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueGam1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueGam1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueGam1;", -99.));

        m_eventInfoInts.insert(std::make_pair("fcnc_qpidWc0", -99));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2Wc0", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2Wc0", -99.));

        m_eventInfoInts.insert(std::make_pair("fcnc_qpidWc1", -99));
        m_eventInfoFloats.insert(std::make_pair("fcnc_ptTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_phiTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_etaTrueT2Wc1", -99.));
        m_eventInfoFloats.insert(std::make_pair("fcnc_mTrueT2Wc1", -99.));

      }

      // Vector of integer or char
      m_eventInfoVInts.insert(std::make_pair("fcnc_nbTop2", vint()));
      m_eventInfoVInts.insert(std::make_pair("fcnc_ncTop1", vint()));
      m_eventInfoVInts.insert(std::make_pair("fcnc_jetInd", vint()));
      m_eventInfoVInts.insert(std::make_pair("fcnc_icat",   vint()));

      // Vector of floats
      m_eventInfoVFloats.insert(std::make_pair("fcnc_mTop1", vfloat()));
      m_eventInfoVFloats.insert(std::make_pair("fcnc_mTop2", vfloat()));
      m_eventInfoVFloats.insert(std::make_pair("fcnc_Score", vfloat()));

    } else {

      m_eventInfoInts["fcnc_cutFlow"] = 0;
      m_eventInfoInts["fcnc_njet"]    = -1;
      m_eventInfoInts["fcnc_nbjet"]   = -1;
      m_eventInfoInts["fcnc_njet30"]  = -1;
      m_eventInfoInts["fcnc_idLep"]   = 0;
      m_eventInfoInts["fcnc_ntopc"]   = -1;
      m_eventInfoInts["fcnc_cat"]     = -1;

      // Floats
      m_eventInfoFloats["fcnc_mT"]     = -1.;
      m_eventInfoFloats["fcnc_etm"]    = -1.;
      m_eventInfoFloats["fcnc_phim"]   = 999.;

      if (HG::isMC()) { m_eventInfoFloats["fcnc_weight"] = 0.; }

      if (m_WriteDetails) {
        m_eventInfoInts["fcnc_igoodComb"]   = -1;
        m_eventInfoFloats["fcnc_MefftopSM"] = -1.;
        m_eventInfoFloats["fcnc_mW"]        = -1.;
        m_eventInfoFloats["fcnc_dRbW"]      = -1.;
        m_eventInfoFloats["fcnc_mindRbj"]   = 9e9;
        m_eventInfoFloats["fcnc_mtt"]       = -1.;
        m_eventInfoFloats["fcnc_pTb"]       = -1.;
        m_eventInfoFloats["fcnc_maxdRcy"]   = -1.;
        m_eventInfoFloats["fcnc_HT"]        = -1.;
        m_eventInfoFloats["fcnc_HTcjy0y1bjj0j1"] = -1.;
      }

      if (m_truthHandler != nullptr && m_writeTruthInfo) {

        m_eventInfoInts["fcnc_T1qpid"]        = 0;
        m_eventInfoInts["fcnc_T2qpid"]        = 0;

        m_eventInfoFloats["fcnc_mTrueT1"]     = -1.;
        m_eventInfoFloats["fcnc_pxTrueT1"]    = -9e9;
        m_eventInfoFloats["fcnc_pyTrueT1"]    = -9e9;
        m_eventInfoFloats["fcnc_pzTrueT1"]    = -9e9;
        m_eventInfoFloats["fcnc_ptTrueT1q"]   = -1.;
        m_eventInfoFloats["fcnc_etaTrueT1q"]  = -9e9;
        m_eventInfoFloats["fcnc_phiTrueT1q"]  = -9e9;
        m_eventInfoFloats["fcnc_mTrueT1q"]    = -1.;
        m_eventInfoFloats["fcnc_ptTrueT1qv"]  = -1.;
        m_eventInfoFloats["fcnc_etaTrueT1qv"] = -9e9;
        m_eventInfoFloats["fcnc_phiTrueT1qv"] = -9e9;

        m_eventInfoFloats["fcnc_mTrueT2"]     = -1.;
        m_eventInfoFloats["fcnc_pxTrueT2"]    = -9e9;
        m_eventInfoFloats["fcnc_pyTrueT2"]    = -9e9;
        m_eventInfoFloats["fcnc_pzTrueT2"]    = -9e9;
        m_eventInfoFloats["fcnc_ptTrueT2q"]   = -1.;
        m_eventInfoFloats["fcnc_etaTrueT2q"]  = -9e9;
        m_eventInfoFloats["fcnc_phiTrueT2q"]  = -9e9;
        m_eventInfoFloats["fcnc_mTrueT2q"]    = -1.;
        m_eventInfoFloats["fcnc_ptTrueT2qv"]  = -1.;
        m_eventInfoFloats["fcnc_etaTrueT2qv"] = -9e9;
        m_eventInfoFloats["fcnc_phiTrueT2qv"] = -9e9;

        m_eventInfoFloats["fcnc_ptTrueGam0"]  = -1.;
        m_eventInfoFloats["fcnc_phiTrueGam0"] = -9e9;
        m_eventInfoFloats["fcnc_etaTrueGam0"] = -9e9;

        m_eventInfoFloats["fcnc_ptTrueGam1"]  = -1.;
        m_eventInfoFloats["fcnc_phiTrueGam1"] = -9e9;
        m_eventInfoFloats["fcnc_etaTrueGam1"] = -9e9;

        m_eventInfoInts["fcnc_qpidWc0"]        = 0;
        m_eventInfoFloats["fcnc_ptTrueT2Wc0"]  = -1.;
        m_eventInfoFloats["fcnc_phiTrueT2Wc0"] = -9e9;
        m_eventInfoFloats["fcnc_etaTrueT2Wc0"] = -9e9;
        m_eventInfoFloats["fcnc_mTrueT2Wc0"]   = -1.;

        m_eventInfoInts["fcnc_qpidWc1"]        = 0;
        m_eventInfoFloats["fcnc_ptTrueT2Wc1"]  = -1.;
        m_eventInfoFloats["fcnc_phiTrueT2Wc1"] = -9e9;
        m_eventInfoFloats["fcnc_etaTrueT2Wc1"] = -9e9;
        m_eventInfoFloats["fcnc_mTrueT2Wc1"]   = -1.;

      }

      // Vector of integer or char
      m_eventInfoVInts["fcnc_nbTop2"].clear();
      m_eventInfoVInts["fcnc_ncTop1"].clear();
      m_eventInfoVInts["fcnc_jetInd"].clear();
      m_eventInfoVInts["fcnc_icat"].clear();

      // Vector of floats
      m_eventInfoVFloats["fcnc_mTop1"].clear();
      m_eventInfoVFloats["fcnc_mTop2"].clear();
      m_eventInfoVFloats["fcnc_Score"].clear();

    }

  }

  //_______________________________________________________________________________________
  void FCNCTool::saveFCNCInfo(xAOD::PhotonContainer    &photons,
                              xAOD::MuonContainer      &muons,
                              xAOD::ElectronContainer  &electrons,
                              xAOD::MissingETContainer &met,
                              xAOD::JetContainer       &jets,
                              const xAOD::EventInfo   *ei)
  {
    m_eventCounter++;

    m_evt = ei->eventNumber();

    if (m_debug > 0)
    { Info("", "Reading event %lu", m_evt); }

    // Initialisation
    InitMap();

    // Perform the analysis (and compute BDT score)
    m_eventInfoInts["fcnc_cutFlow"] = performSelection(photons, muons, electrons, met, jets);

    // Save some truth information for the signal
    if (m_truthHandler && m_writeTruthInfo) {

      int mcid = getChannelNumber();
      bool searchEx = (mcid >= 410804 && mcid <= 410827) || (mcid >= 410674 && mcid <= 410771);

      if (searchEx && !findTrueTopEx()) { Info("", "did not find the true top decaying to Higgs ?"); }

      if (!findTrueTopSM()) { Info("", "did not find the true top decaying to SM ?"); }

    }

    //Save all the eventInfoMaps to the eventHandler()
    //Should always be called last
    saveMapsToEventInfo();

  }

  unsigned int FCNCTool::performSelection(xAOD::PhotonContainer    &photons,
                                          xAOD::MuonContainer      &mus,
                                          xAOD::ElectronContainer  &els,
                                          xAOD::MissingETContainer &met,
                                          xAOD::JetContainer       &jets)
  {
    static SG::AuxElement::ConstAccessor<float> emSF("scaleFactor");
    static SG::AuxElement::ConstAccessor<char> isoAcc("isIsoFixedCutLoose");
    static SG::AuxElement::ConstAccessor<char> idAcc("isTight");
    static SG::AuxElement::Decorator<char> maybeC("maybeC");
    static SG::AuxElement::Decorator<int>  jindex("jindex");

    // Two good photons
    if ((photons.size() < 2) || (photons.size() >= 2 && (photons[0]->pt() < 40 * HG::GeV || photons[1]->pt() < 30 * HG::GeV)))
    { return 0; }

    //
    if (!idAcc.isAvailable(*photons[0]) || !idAcc.isAvailable(*photons[1]))
    { fatal("Cannot access the id information (Tight)"); }

    if (!(idAcc(*photons[0]) && idAcc(*photons[1])))
    { return 0; }

    if (!isoAcc.isAvailable(*photons[0]) || !isoAcc.isAvailable(*photons[1]))
    { fatal("Cannot access the isolation information (FixedCutLoose)"); }

    if (!(isoAcc(*photons[0]) && isoAcc(*photons[1])))
    { return 0; }

    // with an invariant mass within [100,160] GeV/c2
    m_p4g1   = photons[0]->p4();
    m_p4g2   = photons[1]->p4();
    m_p4H    = photons[0]->p4() + photons[1]->p4();
    m_mgg    = m_p4H.M() / HG::GeV;

    if (m_mgg < 100 || m_mgg > 160)
    { return 1; }

    // at least 4 jets or 2 jets and a lepton
    m_lep.p4 = TLorentzVector(0, 0, 0, 0);
    m_lep.sf = 0;
    m_lep.id = 0;
    unsigned int nMu = 0, nEl = 0;

    for (auto el : els) {
      if (el->pt() > m_ptelC) {
        if (nEl == 0) {
          m_lep.p4 = el->p4();
          m_lep.sf = HG::isMC() ? emSF(*el) : 1.;
          m_lep.id = -11 * int(el->charge());
        }

        nEl++;
      }
    }

    for (auto mu : mus) {
      if (mu->pt() > m_ptmuC) {
        if (nMu == 0) { // I can eventually overwrite, anyway I do not care since I want at most one lepton
          m_lep.p4 = mu->p4();
          m_lep.sf = HG::isMC() ? emSF(*mu) : 1.;
          m_lep.id = -13 * int(mu->charge());
        }

        nMu++;
      }
    }

    unsigned int nLep = nMu + nEl;

    int Nbjet  = 0;
    int Njetxx = 0;
    int Njet30 = 0;

    double sumpTj = 0;
    int ij = 0;

    for (auto jet : jets) {

      // The jet->index() corresponds to the position in the original container...
      jindex(*jet) = ij++;

      if (isGoodJ(jet)) {
        Njetxx++;

        bool isB77 = isBt(*jet);

        if (isB77) { Nbjet++; }

        double jpt = jet->pt() / HG::GeV;
        sumpTj += jpt;

        if (jpt > 30.) { Njet30++; }

        maybeC(*jet) = 0;
      }
    }

    // For events that might go into ttbar categories, look at c-tagging eligible jets :
    // use recycling strategy to deal with the c- and b-tag SF
    if ((nLep >= 1 && Njetxx >= 2) || (nLep == 0 && Njetxx >= 4)) {
      for (auto jet : jets) {
        if (isGoodJ(jet)) {
          // Is the jet eligible for c-tagging
          double amTop1 = (jet->p4() + m_p4H).M() / HG::GeV;

          if ((nLep == 0 && amTop1 >= m_mTop1HadLow && amTop1 <= m_mTop1HadHigh) ||
              (nLep >= 1 && amTop1 >= m_mTop1LepLow && amTop1 <= m_mTop1LepHigh)) { maybeC(*jet) = 1; }
        }
      }
    }

    m_sumpTj                       = sumpTj;
    m_eventInfoInts["fcnc_njet"]   = Njetxx;
    m_eventInfoInts["fcnc_nbjet"]  = Nbjet;
    m_eventInfoInts["fcnc_njet30"] = Njet30;

    if (!((Njetxx >= 3) || (Njetxx >= 1 && nLep >= 1)))
    { return 2; }

    if (Njetxx >= 1 && nLep == 1)
    { return LeptonAnaSel(met, jets); }
    else if (Njetxx >= 3 && nLep == 0)
    { return HadronAnaSel(jets); }
    else
    { return 3; }

    return 3;
  }


  //_______________________________________________________________________________________
  //Save event info from maps to eventInfo
  void FCNCTool::saveMapsToEventInfo()
  {
    //Floats
    for (auto element : m_eventInfoFloats)
    { eventHandler()->storeVar<float>(element.first.Data(), element.second); }

    //Ints
    for (auto element : m_eventInfoInts)
    { eventHandler()->storeVar<int>(element.first.Data(), element.second); }

    //Vectors
    for (auto element : m_eventInfoVInts)
    { eventHandler()->storeVar<std::vector<int>>(element.first.Data(), element.second); }

    for (auto element : m_eventInfoVFloats)
    { eventHandler()->storeVar<std::vector<float>>(element.first.Data(), element.second); }

  }

  std::vector<double> FCNCTool::getNuPz(TLorentzVector &lep, const xAOD::MissingET *met, double &mT)
  {

    mT = sqrt(2.*lep.Pt() * met->met() * (1 - cos(lep.Phi() - met->phi())));

    //
    bool useDaniel = true;
    double amW = 80.4e3;

    if (useDaniel && mT > amW)
    { amW = mT + 100; }

    double ci = amW * amW / 2. + lep.Px() * met->mpx() + lep.Py() * met->mpy();
    //
    double a = lep.Perp2();
    double b = -2.*lep.Pz() * ci;
    double c = lep.E() * lep.E() * met->met() * met->met() - ci * ci;
    //
    double delta = b * b - 4.*a * c;
    std::vector<double> pz; // if useDaniel, should always be two solutions

    if (delta > 0) {
      pz.resize(2);
      pz[0] = (-b - sqrt(delta)) / 2. / a;
      pz[1] = (-b + sqrt(delta)) / 2. / a;
    } else {
      pz.resize(1);
      pz[0] = -b / 2. / a;
    }

    if (m_debug > 0) {
      TLorentzVector p4nu1(met->mpx(), met->mpy(), pz[0], 0.);
      p4nu1.SetE(p4nu1.P());
      TLorentzVector p4nu2(met->mpx(), met->mpy(), pz[1], 0.);
      p4nu2.SetE(p4nu2.P());
      Info("", "the event, mgg = %f, ETm = %f, pTLep = %f, phiMET = %f, phiLep = %f, mT = %f, pznu1 = %f, pznu2 = %f, mW1 = %f, mW2 = %f",
           m_mgg, met->met() * 1e-3, lep.Pt() * 1e-3, met->phi(), lep.Phi(), mT * 1e-3, pz[0] * 1e-3, pz[1] * 1e-3, (lep + p4nu1).M() * 1e-3, (lep + p4nu2).M() * 1e-3);
    }

    return pz;
  }

  unsigned int FCNCTool::LeptonAnaSel(xAOD::MissingETContainer &met,
                                      xAOD::JetContainer       &jets)
  {

    static SG::AuxElement::Accessor<float> BtSF("SF_MV2c10_FixedCutBEff_77");
    static SG::AuxElement::Accessor<float> DL1BtSF("SF_DL1_FixedCutBEff_77");
    static SG::AuxElement::Accessor<float> DL1CtSF("SF_DL1_CTag_Loose");
    static SG::AuxElement::Accessor<char> maybeC("maybeC");
    static SG::AuxElement::Accessor<int> jindex("jindex");

    xAOD::MissingET *metFinal = *(met.find("TST"));
    double jetSF = 1;

    double mT = -1;
    std::vector<double> pznu = this->getNuPz(m_lep.p4, metFinal, mT);

    m_eventInfoFloats["fcnc_mT"]   = mT / HG::GeV;
    m_eventInfoFloats["fcnc_etm"]  = metFinal->met() / HG::GeV;
    m_eventInfoFloats["fcnc_phim"] = metFinal->phi();
    m_eventInfoInts["fcnc_idLep"]  = m_lep.id;

    // mey veto
    if (abs(m_lep.id) == 11) {
      double mey1 = (m_lep.p4 + m_p4g1).M();
      double mey2 = (m_lep.p4 + m_p4g2).M();

      if ((mey1 > m_meyCutLow && mey1 < m_meyCutHigh) || (mey2 > m_meyCutLow && mey2 < m_meyCutHigh))
      {return 10; };
    }

    // mT > 30 GeV
    if (mT < m_mTC)
    { return 11; }

    // Jets
    int NGoodJ = m_eventInfoInts["fcnc_njet"];
    int iMax   = m_maxNjetLep > NGoodJ ? NGoodJ : m_maxNjetLep;
    int nMaxComb = iMax > 1 ? 2 * iMax * (iMax - 1) : 2;
    m_comb.clear();
    m_comb.resize(nMaxComb);
    int theCat = 0;
    int nc = 0;

    for (unsigned int isol = 0; isol < pznu.size(); isol++) {

      TLorentzVector nup4(metFinal->mpx(), metFinal->mpy(), pznu[isol], 0.);
      nup4.SetE(nup4.P());
      TLorentzVector wp4 = m_lep.p4 + nup4;

      // The loop for SM top decay
      int kc = 0;

      for (auto kjet : jets) {

        if (!isGoodJ(kjet)) { continue; }

        if (kc > m_maxNjetLep - 1) { break; }

        bool isB    = isBt(*kjet) && !maybeC(*kjet);

        if (HG::isMC() && isol == 0) {
          if (!maybeC(*kjet)) { jetSF *= (m_FTagUseDL1 ? DL1BtSF(*kjet) : BtSF(*kjet)); }
          else { jetSF *= (m_FTagUseDL1 ? DL1CtSF(*kjet) : 1.); } // no c-tag SF for MV2-based WP
        }

        TLorentzVector kp4 = kjet->p4();

        TLorentzVector tStand = kp4 + wp4;
        double mTop2 = tStand.M() / HG::GeV;
        bool Top2OK  = mTop2 >= m_mTop2LepLow && mTop2 <= m_mTop2LepHigh;

        // The loop for Exot top decay
        int jc = 0;

        for (auto jjet : jets) {

          if (!isGoodJ(jjet))        { continue; }

          if (jc > m_maxNjetLep - 1) { break; }

          if (kc == jc)              { jc++; continue; }

          bool isC = isCt(*jjet);
          TLorentzVector tExot = jjet->p4() + m_p4H;
          double mTop1 = tExot.M() / HG::GeV;
          bool Top1OK = mTop1 >= m_mTop1LepLow && mTop1 <= m_mTop1LepHigh;

          if (isB) {
            if (Top1OK) {
              if (isC && Top2OK) {
                theCat = 1;
                m_comb[nc].cat = 1;
              } else if (!isC && Top2OK) {
                m_comb[nc].cat = 2;

                if (theCat != 1)               { theCat = 2; }
              } else if (isC && !Top2OK) {
                m_comb[nc].cat = 3;

                if (theCat == 0 || theCat > 3) { theCat = 3; }
              } else if (!isC && !Top2OK) {
                m_comb[nc].cat = 4;

                if (theCat == 0 || theCat > 4) { theCat = 4; }
              }
            } else if (Top2OK) {
              m_comb[nc].cat = 5;

              if (theCat == 0) { theCat = 5; }
            }
          }

          // Here, these are all the combinations, even if the tagging / masses are not fine
          if (nc >= nMaxComb)
          { Error("", "Should never happen, nc = %i, max nc = %i", nc, nMaxComb); }

          m_comb[nc].mTop1  = mTop1;
          m_comb[nc].mTop2  = mTop2;
          m_comb[nc].nbTop2 = isB ? 1 : 0;
          m_comb[nc].ncTop1 = isC ? 1 : 0;
          m_comb[nc].jetInd = jindex(*jjet) + 10 * jindex(*kjet);

          jc++;
          nc++;

        } // jet for exot top decay

        // there is only one jet
        if (NGoodJ == 1) {
          if (isB && Top2OK && theCat == 0)
          { theCat = 5; m_comb[nc].cat = 5; }

          if (nc >= nMaxComb)
          { Error("", "Should never happen, nc = %i, max nc = %i", nc, nMaxComb); }

          m_comb[nc].mTop1  = -1;
          m_comb[nc].mTop2  = mTop2;
          m_comb[nc].nbTop2 = isB ? 1 : 0;
          m_comb[nc].ncTop1 = -1;
          m_comb[nc].jetInd = 10 * jindex(*kjet);
          nc++;
        }

        kc++;
      }   // jet for sm top decay
    }     // neutrino pz sol

    m_eventInfoInts["fcnc_ntopc"]    = nc;

    for (auto comb : m_comb) {
      m_eventInfoVFloats["fcnc_mTop1"].push_back(comb.mTop1);
      m_eventInfoVFloats["fcnc_mTop2"].push_back(comb.mTop2);
      m_eventInfoVInts["fcnc_nbTop2"] .push_back(comb.nbTop2);
      m_eventInfoVInts["fcnc_ncTop1"] .push_back(comb.ncTop1);
      m_eventInfoVInts["fcnc_jetInd"] .push_back(comb.jetInd);
      m_eventInfoVInts["fcnc_icat"]   .push_back(comb.cat);
    }

    if (theCat == 0)
    { return 12; }

    if (HG::isMC()) { m_eventInfoFloats["fcnc_weight"] = m_lep.sf * jetSF; }

    m_eventInfoInts["fcnc_cat"] = theCat;

    if (theCat == 5) {
      m_eventInfoVFloats["fcnc_Score"] = LeptHBDT();
    }

    return 13;

  }

  unsigned int FCNCTool::HadronAnaSel(xAOD::JetContainer &jets)
  {

    static SG::AuxElement::Accessor<float> BtSF("SF_MV2c10_FixedCutBEff_77");
    static SG::AuxElement::Accessor<float> DL1BtSF("SF_DL1_FixedCutBEff_77");
    static SG::AuxElement::Accessor<float> DL1CtSF("SF_DL1_CTag_Loose");
    static SG::AuxElement::Accessor<char> maybeC("maybeC");
    static SG::AuxElement::Accessor<int> jindex("jindex");

    int theCat = 0;

    // nComb = n! / 3! (n-4)! (= n * C(n-1,3))
    int NGoodJ = m_eventInfoInts["fcnc_njet"];
    int iMax   = m_maxNjet > NGoodJ ? NGoodJ : m_maxNjet;
    int nMaxComb = iMax > 3 ? TMath::Factorial(iMax) / 6 / TMath::Factorial(iMax - 4) : 1; // 24 / 6 / 1 = 4
    m_comb.clear();
    m_comb.resize(nMaxComb);
    //
    int nc = 0;
    int ic = 0;
    double jetSF = 1;

    // Top2 building : jjj
    for (auto ijet : jets) {
      if (!isGoodJ(ijet)) { continue; }

      if (ic > m_maxNjet - 1) { break; } // use only the maxNjet leading

      bool isBi = isBt(*ijet) && !maybeC(*ijet);

      if (HG::isMC()) {
        if (!maybeC(*ijet)) { jetSF *= (m_FTagUseDL1 ? DL1BtSF(*ijet) : BtSF(*ijet)); }
        else { jetSF *= (m_FTagUseDL1 ? DL1CtSF(*ijet) : 1.); } // no c-tag SF for MV2-base WP
      }

      TLorentzVector ip4 = ijet->p4();

      int jc = 0;

      for (auto jjet : jets) {
        if (!isGoodJ(jjet))     { continue; }

        if (jc > m_maxNjet - 1) { break; }          // use only the maxNjet leading

        if (jc <= ic)           { jc++; continue; } // do not care about the order

        bool isBj = isBt(*jjet) && !maybeC(*jjet);

        TLorentzVector jp4 = jjet->p4();

        int kc = 0;

        for (auto kjet : jets) {
          if (!isGoodJ(kjet))     { continue; }

          if (kc > m_maxNjet - 1) { break; }          // use only the maxNjet leading

          if (kc <= jc)           { kc++; continue; } // do not care about the order

          bool isBk = isBt(*kjet) && !maybeC(*kjet);

          TLorentzVector kp4 = kjet->p4();

          TLorentzVector tStan = ip4 + jp4 + kp4;
          double mTop2 = tStan.M() * 1e-3;
          bool Top2OK  = mTop2 >= m_mTop2HadLow && mTop2 <= m_mTop2HadHigh;

          // Now Top1 building : yyh
          int lc = 0;

          for (auto ljet : jets) {
            if (!isGoodJ(ljet))                   { continue; }

            if (lc > m_maxNjet - 1)               { break; } // use only the maxNjet leading

            if (lc == kc || lc == jc || lc == ic) { lc++; continue; }

            bool isC = isCt(*ljet);

            TLorentzVector tExot = ljet->p4() + m_p4H;
            double mTop1 = tExot.M() * 1e-3;
            bool Top1OK  = mTop1 >= m_mTop1HadLow && mTop1 <= m_mTop1HadHigh;

            if (isBi || isBj || isBk) {
              if (Top1OK) {
                if (isC && Top2OK) {
                  theCat = 1;
                  m_comb[nc].cat = 1;
                } else if (!isC && Top2OK) {
                  m_comb[nc].cat = 2;

                  if (theCat != 1)               { theCat = 2; }
                } else if (isC && !Top2OK) {
                  m_comb[nc].cat = 3;

                  if (theCat == 0 || theCat > 3) { theCat = 3; }
                } else if (!isC && !Top2OK) {
                  m_comb[nc].cat = 4;

                  if (theCat == 0 || theCat > 4) { theCat = 4; }
                }
              } else if (Top2OK) {
                m_comb[nc].cat = 5;

                if (theCat == 0) { theCat = 5; }
              }
            }

            m_comb[nc].mTop1  = mTop1;
            m_comb[nc].mTop2  = mTop2;

            if (isBi) { m_comb[nc].nbTop2++; }

            if (isBj) { m_comb[nc].nbTop2++; }

            if (isBk) { m_comb[nc].nbTop2++; }

            m_comb[nc].ncTop1 = isC ? 1 : 0;
            m_comb[nc].jetInd = jindex(*ljet) + 10 * jindex(*ijet) + 100 * jindex(*jjet) + 1000 * jindex(*kjet);

            nc++;
            lc++;
          } //      jet de Top1

          // there are only 3 jets (so ntopc should be 1)
          if (NGoodJ == 3) {
            if ((isBi || isBj || isBk) && Top2OK) {
              m_comb[nc].cat = 5;

              if (theCat == 0) { theCat = 5; }
            }

            m_comb[nc].mTop1 = -1;
            m_comb[nc].mTop2 = mTop2;

            if (isBi) { m_comb[nc].nbTop2++; }

            if (isBj) { m_comb[nc].nbTop2++; }

            if (isBk) { m_comb[nc].nbTop2++; }

            m_comb[nc].ncTop1 = -1;
            m_comb[nc].jetInd = 10 * jindex(*ijet) + 100 * jindex(*jjet) + 1000 * jindex(*kjet);
            nc++;
          }

          kc++;
        }   // 3eme jet de Top2

        jc++;
      }     // 2eme jet de Top2

      ic++;
    }       // 1er  jet de Top2

    m_eventInfoInts["fcnc_ntopc"] = nc;

    for (auto comb : m_comb) {
      m_eventInfoVFloats["fcnc_mTop1"].push_back(comb.mTop1);
      m_eventInfoVFloats["fcnc_mTop2"].push_back(comb.mTop2);
      m_eventInfoVInts["fcnc_nbTop2"] .push_back(comb.nbTop2);
      m_eventInfoVInts["fcnc_ncTop1"] .push_back(comb.ncTop1);
      m_eventInfoVInts["fcnc_jetInd"] .push_back(comb.jetInd);
      m_eventInfoVInts["fcnc_icat"]   .push_back(comb.cat);
    }

    if (theCat == 0)
    { return 22; }

    if (HG::isMC()) { m_eventInfoFloats["fcnc_weight"] = jetSF; }

    m_eventInfoInts["fcnc_cat"] = theCat;

    m_eventInfoVFloats["fcnc_Score"] = (theCat == 5) ? HadtHBDT(jets) : HadttBDT(jets);

    return 23;
  }

  std::vector<float> FCNCTool::LeptHBDT()
  {
    // Prepare input variables (all can be retrieved from MxAOD in nominal case)
    m_bdtIn["pTyy"]      = m_p4H.Perp() / HG::GeV;
    m_bdtIn["etayy"]     = fabs(m_p4H.Rapidity());
    m_bdtIn["mT"]        = m_eventInfoFloats["fcnc_mT"];
    m_bdtIn["Qlep"]      = m_lep.id > 0 ? -1. : 1.;
    m_bdtIn["Njetpro"]   = m_eventInfoInts["fcnc_njet"];
    m_bdtIn["HTjets"]    = m_sumpTj + m_lep.p4.Perp() / HG::GeV + m_eventInfoFloats["fcnc_etm"];
    //m_bdtIn["MefftopEx"] = (m_p4g1.Perp() + m_p4g2.Perp()) / HG::GeV;
    m_bdtIn["MefftopEx"] = (m_p4g1.Perp() + m_p4g2.Perp()) / m_p4H.M();
    // For cross-validation
    m_bdtInEvt2  = float(m_evt % 2);

    if (m_debug > 100) {
      for (auto s : m_bdtIn)
      { Info("", "%s = %4.3f", s.first.Data(), s.second); }

      Info("", "Score = %1.4f", m_reader["LeptH"]->EvaluateMVA("BDT"));
    }

    if (m_WriteDetails) {
      m_eventInfoFloats["fcnc_HT"] = m_sumpTj;
    }

    std::vector<float> res;

    for (auto reader : m_reader) {
      if (reader.first.Contains("LeptH")) {
        res.push_back(reader.second->EvaluateMVA("BDT"));
      }
    }

    return res;
  }

  std::vector<float> FCNCTool::HadtHBDT(xAOD::JetContainer &jets)
  {
    static SG::AuxElement::Accessor<char> maybeC("maybeC");

    // Prepare input variables (all can be retrieved from MxAOD in nominal case)
    // Easy ones
    m_bdtIn["pTyy"]      = m_p4H.Perp() / HG::GeV;
    //m_bdtIn["MefftopEx"] = (m_p4g1.Perp() + m_p4g2.Perp()) / HG::GeV;
    m_bdtIn["MefftopEx"] = (m_p4g1.Perp() + m_p4g2.Perp()) / m_p4H.M();
    m_bdtIn["HTjets"]    = m_sumpTj;

    // For the others... From jetInd, I can a priori reproduce easily those var, not need to store
    // first, need to find the best comb : mjjj closest to 170
    int icomb = 0, igcomb = -1;
    double dmTop2 = 9e9;

    for (auto comb : m_comb) {
      if (comb.cat != 5)
      { icomb++; continue; }

      double dm = fabs(comb.mTop2 - m_mTop2Central);

      if (dm < dmTop2) {
        dmTop2 = dm;
        igcomb = icomb;
      }

      icomb++;
    }

    // then in this combination, choose the b-jet if several (hight pT)
    // and W-jets are the rest.
    m_bdtIn["MefftopSM"] = 0;
    int ijk = m_comb[igcomb].jetInd / 10;
    int j[3] = { (ijk % 10), (ijk % 100) / 10, ijk / 100 };

    TLorentzVector whlv(0, 0, 0, 0);
    double maxpt = -1;
    int indexBj  = -1;
    const xAOD::Jet *bjet(nullptr);

    for (int i = 0; i < 3; i++) {
      const xAOD::Jet *jet = jets.at(j[i]);
      m_bdtIn["MefftopSM"] += (jet->pt() / HG::GeV);

      if (!maybeC(*jet) && isBt(*jet) && jet->pt() > maxpt) {
        indexBj = j[i];
        maxpt   = jet->pt();
        bjet    = jet;
      }
    }

    m_bdtIn["mindRbj"]   = 9e9;

    for (int i = 0; i < 3; i++) {
      if (j[i] == indexBj) {
        continue;
      }

      const xAOD::Jet *jet = jets.at(j[i]);
      whlv += jet->p4();
      double dR = bjet->p4().DeltaR(jet->p4());

      if (dR < m_bdtIn["mindRbj"]) {
        m_bdtIn["mindRbj"] = dR;
      }
    }

    m_bdtIn["mW"]   = whlv.M() / HG::GeV;
    m_bdtIn["dRbW"] = bjet->p4().DeltaR(whlv);

    if (m_WriteDetails) {
      m_eventInfoInts["fcnc_igoodComb"]   = igcomb;
      m_eventInfoFloats["fcnc_MefftopSM"] = m_bdtIn["MefftopSM"];
      m_eventInfoFloats["fcnc_mW"]        = m_bdtIn["mW"];
      m_eventInfoFloats["fcnc_dRbW"]      = m_bdtIn["dRbW"];
      m_eventInfoFloats["fcnc_mindRbj"]   = m_bdtIn["mindRbj"];
      m_eventInfoFloats["fcnc_HT"]        = m_sumpTj;
    }

    // For cross-validation
    m_bdtInEvt2  = float(m_evt % 2);

    std::vector<float> res;

    for (auto reader : m_reader) {
      if (reader.first.Contains("HadtH")) {
        res.push_back(reader.second->EvaluateMVA("BDT"));
      }
    }

    if (m_debug > 100) {
      for (auto s : m_bdtIn)
      { Info("", "%s = %f4.3", s.first.Data(), s.second); }

      for (auto r : res)
      { Info("", "Score = %f1.4", r); }
    }

    return res;
  }

  std::vector<float> FCNCTool::HadttBDT(xAOD::JetContainer &jets)
  {
    static SG::AuxElement::Accessor<char> maybeC("maybeC");

    // Prepare input variables
    // Easy one
    m_bdtIn["pTyy"] = m_p4H.Perp() / HG::GeV;

    // Other
    m_bdtIn["pTb"]     = -1.;
    m_bdtIn["mindRbj"] = 9e9;

    int icomb = 0, igcomb = -1;
    int indcmin  = -1;
    double dmTop1 = 9e9, dmTop2 = 9e9, dRbj = 9e9;

    // Loop on combinations
    for (auto comb : m_comb) {
      if (comb.cat != m_eventInfoInts["fcnc_cat"]) { icomb++; continue; }

      double dm1 = fabs(comb.mTop1 - m_mTop1Central);
      int ic     = comb.jetInd % 10;

      if (comb.cat == 1 || comb.cat == 2) {
        // For cat 1 and cat 2, Top2 is OK ==> minimize dm1 and then dm2
        if (dm1 < dmTop1 || ic == indcmin) {
          double dm2 = fabs(comb.mTop2 - m_mTop2Central);

          // if same jet for mTop1, will test dmTop2 otherwise, take this new comb.
          // If two different jets give exactly the same mTop1, keep the first
          if (dm1 < dmTop1) {
            dmTop1  = dm1;
            dmTop2  = dm2;
            indcmin = ic;
            igcomb  = icomb;
          } else if (dm2 < dmTop2) {
            dmTop2 = dm2;
            igcomb = icomb;
          }
        }
      } else {
        // For cat 3 and 4 : more tricky...
        if (dm1 < dmTop1 || ic == indcmin) {
          if (dm1 < dmTop1) {
            // each time I build a new smaller dmTop1, I re-initialize dRbj to large value
            dRbj = 9e9;
          }

          indcmin = ic;
          dmTop1  = dm1;
          const xAOD::Jet *bjet(nullptr);
          int j[3] = { (comb.jetInd % 100) / 10, (comb.jetInd % 1000) / 100, comb.jetInd / 1000 };

          // In fact, when there are multiple b, apparently Daniel only used the leading one
          //bool used[3] = { false, false, false };
          //for (int ac = 0; ac < comb.nbTop2; ac++) {
          double ptbmax = -1;

          for (int ij = 0; ij < 3; ij++) {
            const xAOD::Jet *jet = jets.at(j[ij]);
            int isb  = isBt(*jet) && !maybeC(*jet) /*&& !used[ij]*/;

            if (isb && jet->pt() > ptbmax) { bjet = jet; ptbmax = jet->pt(); /* used[ij] = true; break; */ }
          }

          // DeltaR between this jet and the other jets from the Top2
          double drjb = -1, drminbj = 9e9;
          TLorentzVector pw;

          for (int ij = 0; ij < 3; ij++) {
            const xAOD::Jet *ajet = jets.at(j[ij]);

            if (ajet == bjet) { continue; }

            double dr = ajet->p4().DeltaR(bjet->p4());

            if (dr > drjb)    { drjb = dr; }

            if (dr < drminbj) { drminbj = dr; }

            pw += ajet->p4();
          }

          if (drjb <= dRbj) {
            dRbj     = drjb;
            igcomb   = icomb;
            m_bdtIn["pTb"]     = bjet->pt() / HG::GeV;
            m_bdtIn["mindRbj"] = drminbj;
            m_bdtIn["mW"]      = pw.M() / HG::GeV;
          }
        }
      } // cat 2 case

      icomb++;
    }   // loop on comb

    if (igcomb < 0) { HG::fatal("no good comb ?? This shoud never happen. Find your bug."); }


    // Compute the variables (I need to find a better way to do all this)
    // The jets
    selComb comb = m_comb[igcomb];
    TLorentzVector cjet = jets.at(comb.jetInd % 10)->p4();
    const xAOD::Jet *bjet(nullptr);
    int jj[4] = { comb.jetInd % 10, (comb.jetInd % 100) / 10, (comb.jetInd % 1000) / 100, comb.jetInd / 1000 };

    TLorentzVector t2 = jets.at(jj[1])->p4() + jets.at(jj[2])->p4() + jets.at(jj[3])->p4();
    TLorentzVector t1 = jets.at(jj[0])->p4() + m_p4H;
    m_bdtIn["mtt"] = (t1 + t2).M() / HG::GeV;
    double sumpt = 0;

    for (int i = 0; i < 4; i++) { sumpt += jets.at(jj[i])->pt() / HG::GeV; }

    m_bdtIn["HTcjy0y1bjj0j1"] = sumpt + (m_p4g1.Perp() + m_p4g2.Perp()) / HG::GeV;

    double drc0 = cjet.DeltaR(m_p4g1);
    double drc1 = cjet.DeltaR(m_p4g2);
    m_bdtIn["maxdRcy"] = TMath::Max(drc0, drc1);

    // pTb, mindRbj and mW are already computed in the loop to find the good combination for categories 2A,2B
    if (m_eventInfoInts["fcnc_cat"] == 1 || m_eventInfoInts["fcnc_cat"] == 2) {

      for (int i = 1; i < 4; i++) {
        const xAOD::Jet *jet = jets.at(jj[i]);
        int isb  = isBt(*jet) && !maybeC(*jet);

        if (isb && jet->pt() > m_bdtIn["pTb"]) {
          bjet    = jet;
          m_bdtIn["pTb"] = jet->pt();
        }
      }

      m_bdtIn["pTb"] /= HG::GeV;

      TLorentzVector w;

      for (int i = 1; i < 4; i++) {
        const xAOD::Jet *jet = jets.at(jj[i]);

        if (jet != bjet) {
          w += jet->p4();
          double dr = bjet->p4().DeltaR(jet->p4());

          if (dr < m_bdtIn["mindRbj"]) { m_bdtIn["mindRbj"] = dr; }
        }
      }

      m_bdtIn["mW"] = w.M() / HG::GeV;
    }

    if (m_WriteDetails) {
      m_eventInfoInts["fcnc_igoodComb"]   = igcomb;
      m_eventInfoFloats["fcnc_mW"]        = m_bdtIn["mW"];
      m_eventInfoFloats["fcnc_mtt"]       = m_bdtIn["mtt"];
      m_eventInfoFloats["fcnc_pTb"]       = m_bdtIn["pTb"];
      m_eventInfoFloats["fcnc_maxdRcy"]   = m_bdtIn["maxdRcy"];
      m_eventInfoFloats["fcnc_mindRbj"]   = m_bdtIn["mindRbj"];
      m_eventInfoFloats["fcnc_HTcjy0y1bjj0j1"] = m_bdtIn["HTcjy0y1bjj0j1"];
    }

    // For cross-validation
    m_bdtInEvt2  = float(m_evt % 2);

    // For the category choice
    m_bdtInCat   = comb.cat < 2 ? 1 : 2; // not used anymore as A and B are merged in training
    m_bdtInmTop2 = comb.mTop2;

    std::vector<float> res;

    for (auto reader : m_reader) {
      if (reader.first.Contains("Hadtt")) {
        res.push_back(reader.second->EvaluateMVA("BDT"));
      }
    }

    if (m_debug > 100) {
      for (auto s : m_bdtIn)
      { Info("", "%s = %f4.3", s.first.Data(), s.second); }

      for (auto r : res)
      { Info("", "Score = %f1.4", r); }
    }

    return res;
  }

  // It still needs to code the variables of course, but at least the initialisation is flexible...
  EL::StatusCode FCNCTool::InitBDT(Config &config)
  {
    StrV defBDT{ "LeptH", "HadtH", "Hadtt", "HadtHfullmyy", "Hadttfullmyy" };
    StrV allBDT = config.getStrV("FCNCTool.BDTTypes", defBDT);

    // Some var are int (Qlep, Njetpro). But they should be declared in TMVA as float
    // Be careful, should be declared in the order of the xml files (and also Spectator variables)
    StrV defhadttVar{ "pTyy", "pTb", "maxdRcy", "mindRbj", "HTcjy0y1bjj0j1", "mW", "mtt" };
    StrV defhadtHVar{ "pTyy", "mW", "MefftopSM", "HTjets", "mindRbj", "MefftopEx", "dRbW" };
    StrV defleptHVar{ "pTyy", "mT", "Qlep", "Njetpro", "HTjets", "etayy", "MefftopEx" };

    for (auto bdtT : allBDT) {

      m_reader[bdtT] = new TMVA::Reader("!Color:!Silent");

      TString varI = "FCNCTool." + bdtT + "BDTVar";
      StrV allVar  = config.getStrV(varI.Data(), {});

      if (allVar.size() == 0) {
        if (bdtT.Contains("LeptH")) {
          allVar = defleptHVar;
        } else if (bdtT.Contains("HadtH")) {
          allVar = defhadtHVar;
        } else if (bdtT.Contains("Hadtt")) {
          allVar = defhadttVar;
        } else {
          Info("No default set of variables for BDT %s", bdtT.Data());
          return EL::StatusCode::SUCCESS;
        }
      }

      for (auto s : allVar) {

        if (m_bdtIn.find(s) == m_bdtIn.end())
        { m_bdtIn.emplace(s, -1.); } // -1 is a default, but it is a possible default in some case (Qlep, etayy)

        if (s != "mT") {
          m_reader[bdtT]->AddVariable(s.Data(), &(m_bdtIn[s]));
        } else {
          m_reader[bdtT]->AddVariable("mW", &(m_bdtIn["mT"]));
        }
      }

      // For k-folding
      m_reader[bdtT]->AddSpectator("EventNumberMod2", &(m_bdtInEvt2));

      // For category determination
      if (bdtT.Contains("Hadtt")) {
        m_reader[bdtT]->AddSpectator("mTopSM", &(m_bdtInmTop2));
      }

      // Booking
      TString xmlI = "FCNCTool." + bdtT + "XMLFile";
      TString xmlF = "HGamAnalysisFramework/TMVA_BDT_" + bdtT + ".xml";
      std::string weiFileName = config.getStr(xmlI.Data(), xmlF.Data()).Data();
      m_reader[bdtT]->BookMVA("BDT", PathResolverFindCalibFile(weiFileName).c_str());
      Info("FCNCTool", "Booked %s BDT, with weight file %s", bdtT.Data(), weiFileName.c_str());

    }

    return EL::StatusCode::SUCCESS;
  }

  bool FCNCTool::isGoodJ(const xAOD::Jet *jet)
  {
    if (!((jet->pt() > m_jptCC && fabs(jet->eta()) <= 2.5) || (jet->pt() > m_jptCF && fabs(jet->eta()) > 2.5))) { return false; }

    static SG::AuxElement::Accessor<char> fjvt("passFJVT");

    if (m_dofJVT && !fjvt(*jet))
    { return false; }

    return true;
  }

  bool FCNCTool::isBt(const xAOD::Jet &jet)
  {
    static SG::AuxElement::Accessor<char> isMV2Bt77("MV2c10_FixedCutBEff_77");
    static SG::AuxElement::Accessor<char> isDL1Bt77("DL1_FixedCutBEff_77");

    if (m_FTagUseDL1) {
      return isDL1Bt77(jet);
    } else {
      return isMV2Bt77(jet);
    }

    return false;
  }

  bool FCNCTool::isCt(const xAOD::Jet &jet)
  {
    static SG::AuxElement::Accessor<double> mv2cl100("MV2cl100_discriminant");
    static SG::AuxElement::Accessor<double> mv2c100("MV2c100_discriminant");

    //DL1/AntiKt4EMTopoJets/CTag_Loose -> fb = 0.18, cut = 0.61456
    //DL1/AntiKt4EMTopoJets/CTag_Tight -> fb = 0.08, cut = 1.3
    static SG::AuxElement::Accessor<char> isDL1CtLoose("DL1_CTag_Loose");
    /* // A priori not needed if one use CTag_Loose
    static SG::AuxElement::Accessor<double> dl1pb("DL1_pb");
    static SG::AuxElement::Accessor<double> dl1pc("DL1_pc");
    static SG::AuxElement::Accessor<double> dl1pu("DL1_pu");
    static const double fb = 0.18;
    static const double dl1_ctag_Cut = 0.61456;
    */

    if (fabs(jet.eta()) > 2.5)
    { return false; }

    if (m_FTagUseDL1) {

      /* // A priori not needed if one use CTag_Loose
      if (!dl1pb.isAvailable(jet) || (!dl1pu.isAvailable(jet) && !dl1pc.isAvailable(jet))) {
      Info("","dl1pb or both dl1pc and dl1pu no available for jet of pT = %3.2f, eta = %1.2f",jet.pt(),jet.eta());
      return false;
      }
      double pb = dl1pb(jet);
      double pc = dl1pc.isAvailable(jet) ? dl1pc(jet) : 1.-pb-dl1pu(jet);
      double dl1_ctag_Discriminant = log(pc / ( (1.-fb)*(1.-pc) - (1.-2*fb)*pb) );
      return dl1_ctag_Discriminant > dl1_ctag_Cut;
      */
      if (!isDL1CtLoose.isAvailable(jet)) {
        Info("", "DL1_CTag_Loose wp output not available for jet of pT = %3.2f, eta = %1.2f", jet.pt(), jet.eta());
        return false;
      }

      return isDL1CtLoose(jet);

    } else {
      if (!mv2c100.isAvailable(jet) || !mv2cl100.isAvailable(jet)) {
        Info("", "MV2c100 or MV2cl100 no available for jet of pT = %3.2f, eta = %1.2f in run %i, evt = %lu", jet.pt(), jet.eta(), m_eventHandler->runNumber(), m_evt);
        return false;
      }

      double cbtag = mv2c100(jet);
      double cltag = mv2cl100(jet);

      //// Daniel's cut
      //if (cltag > 0.5)
      //  return true;
      // Official cut (Loose)
      if (cltag > 0.19 && cbtag < 0.58)
      { return true; }
    }

    return false;
  }

  bool FCNCTool::findTrueTopEx()
  {

    const xAOD::TruthParticleContainer *truthParticles = m_truthHandler->getTruthParticles();
    const xAOD::TruthParticle *trueT1(nullptr);

    for (auto tp : *truthParticles) {
      int pdg    = tp->pdgId();

      if (abs(pdg) != 6)
      { continue; }

      int status = tp->status();

      if (status != 62) // Only Py8 for the time being
      { continue; }

      unsigned int nC = tp->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        int cpdg = tp->child(ic)->pdgId();

        if (cpdg == 25) {
          trueT1  = tp;

          const xAOD::TruthParticle *trueHiggs = (xAOD::TruthParticle *)tp->child(ic);
          const xAOD::TruthParticle *lastHiggs = (xAOD::TruthParticle *)ThisParticleFinal(trueHiggs);

          if (lastHiggs->nChildren() == 2) {
            m_eventInfoFloats["fcnc_ptTrueGam0"]  = lastHiggs->child(0)->pt() / HG::GeV;
            m_eventInfoFloats["fcnc_phiTrueGam0"] = lastHiggs->child(0)->phi();
            m_eventInfoFloats["fcnc_etaTrueGam0"] = lastHiggs->child(0)->eta();

            m_eventInfoFloats["fcnc_ptTrueGam1"]  = lastHiggs->child(1)->pt() / HG::GeV;
            m_eventInfoFloats["fcnc_phiTrueGam1"] = lastHiggs->child(1)->phi();
            m_eventInfoFloats["fcnc_etaTrueGam1"] = lastHiggs->child(1)->eta();
          } else if (m_debug > 0) {
            Info("", "Number of Higgs boson children different from 2 in event %i", m_eventCounter);
          }

          break;
        }
      }

      if (trueT1)
      { break; }
    }

    if (trueT1) {
      m_eventInfoFloats["fcnc_mTrueT1"]  = trueT1->m() / HG::GeV;
      m_eventInfoFloats["fcnc_pxTrueT1"] = trueT1->px() / HG::GeV;
      m_eventInfoFloats["fcnc_pyTrueT1"] = trueT1->py() / HG::GeV;
      m_eventInfoFloats["fcnc_pzTrueT1"] = trueT1->pz() / HG::GeV;

      unsigned int nC = trueT1->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        const xAOD::TruthParticle *tpc = trueT1->child(ic);
        int cpdg = tpc->pdgId();

        if (abs(cpdg) < 5) { // up / charm quark
          m_eventInfoFloats["fcnc_ptTrueT1q"]  = tpc->pt() / HG::GeV;
          m_eventInfoFloats["fcnc_etaTrueT1q"] = tpc->eta();
          m_eventInfoFloats["fcnc_phiTrueT1q"] = tpc->phi();
          m_eventInfoFloats["fcnc_mTrueT1q"]   = tpc->m() / HG::GeV;
          m_eventInfoInts["fcnc_T1qpid"]       = cpdg;
          std::vector<const xAOD::TruthParticle *> dauV;
          GetDaughter(tpc, dauV);
          TLorentzVector hlvVis(0, 0, 0, 0);

          for (auto dau : dauV) {
            if (!(dau->absPdgId() == 13 || dau->isNeutrino())) // jet from top without muons and neutrinos
            { hlvVis += dau->p4(); }
          }

          m_eventInfoFloats["fcnc_ptTrueT1qv"]  = hlvVis.Pt() / HG::GeV;
          m_eventInfoFloats["fcnc_etaTrueT1qv"] = hlvVis.Eta();
          m_eventInfoFloats["fcnc_phiTrueT1qv"] = hlvVis.Phi();
        }
      }

      return true;
    }

    return false;
  }

  bool FCNCTool::findTrueTopSM()
  {

    const xAOD::TruthParticleContainer *truthParticles = m_truthHandler->getTruthParticles();
    const xAOD::TruthParticle *trueT2(nullptr);

    for (auto tp : *truthParticles) {
      int pdg    = tp->pdgId();

      if (abs(pdg) != 6)
      { continue; }

      int status = tp->status();

      if (status != 62) // Only Py8 for the time being
      { continue; }

      unsigned int nC = tp->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        int cpdg = tp->child(ic)->pdgId();

        if (abs(cpdg) == 5) {
          trueT2  = tp;
          break;
        }
      }

      if (trueT2)
      { break; }
    }

    if (trueT2) {
      m_eventInfoFloats["fcnc_mTrueT2"]  = trueT2->m() / HG::GeV;
      m_eventInfoFloats["fcnc_pxTrueT2"] = trueT2->px() / HG::GeV;
      m_eventInfoFloats["fcnc_pyTrueT2"] = trueT2->py() / HG::GeV;
      m_eventInfoFloats["fcnc_pzTrueT2"] = trueT2->pz() / HG::GeV;

      unsigned int nC = trueT2->nChildren();

      for (unsigned int ic = 0; ic < nC; ic++) {
        const xAOD::TruthParticle *tpc = trueT2->child(ic);
        int cpdg = tpc->pdgId();

        // retrieve the b-quark (or maybe d, s)
        if (abs(cpdg) == 1 or abs(cpdg) == 3 or abs(cpdg) == 5) {
          m_eventInfoFloats["fcnc_ptTrueT2q"]  = tpc->pt() / HG::GeV;
          m_eventInfoFloats["fcnc_etaTrueT2q"] = tpc->eta();
          m_eventInfoFloats["fcnc_phiTrueT2q"] = tpc->phi();
          m_eventInfoFloats["fcnc_mTrueT2q"]   = tpc->m() / HG::GeV;
          m_eventInfoInts["fcnc_T2qpid"]       = cpdg;
          std::vector<const xAOD::TruthParticle *> dauV;
          GetDaughter(tpc, dauV);
          TLorentzVector hlvVis(0, 0, 0, 0);

          for (auto dau : dauV) {
            if (!(dau->absPdgId() == 13 || dau->isNeutrino()))
            { hlvVis += dau->p4(); }
          }

          m_eventInfoFloats["fcnc_ptTrueT2qv"]  = hlvVis.Pt() / HG::GeV;
          m_eventInfoFloats["fcnc_etaTrueT2qv"] = hlvVis.Eta();
          m_eventInfoFloats["fcnc_phiTrueT2qv"] = hlvVis.Phi();
        }

        // retrieve the W boson and its decay products
        if (abs(cpdg) == 24) {

          const xAOD::TruthParticle *lastW = (xAOD::TruthParticle *)ThisParticleFinal(tpc);
          size_t nWC = lastW->nChildren();

          for (unsigned int jc = 0; jc < nWC; jc++) {
            const xAOD::TruthParticle *wc = lastW->child(jc);
            int Wc_pdg = wc->pdgId();

            if (abs(wc->pdgId()) < 20) {
              if (abs(wc->pdgId()) % 2 == 0) { // c-quark, or charged lepton
                m_eventInfoFloats["fcnc_ptTrueT2Wc0"]  = wc->pt() / HG::GeV;
                m_eventInfoFloats["fcnc_phiTrueT2Wc0"] = wc->phi();
                m_eventInfoFloats["fcnc_etaTrueT2Wc0"] = wc->eta();
                m_eventInfoFloats["fcnc_mTrueT2Wc0"]   = wc->m() / HG::GeV;
                m_eventInfoInts["fcnc_qpidWc0"]        = Wc_pdg;
              } else {                     // s-quark, or neutrino
                m_eventInfoFloats["fcnc_ptTrueT2Wc1"]  = wc->pt() / HG::GeV;
                m_eventInfoFloats["fcnc_phiTrueT2Wc1"] = wc->phi();
                m_eventInfoFloats["fcnc_etaTrueT2Wc1"] = wc->eta();
                m_eventInfoFloats["fcnc_mTrueT2Wc1"]   = wc->m() / HG::GeV;
                m_eventInfoInts["fcnc_qpidWc1"]        = Wc_pdg;
              }
            }
          }

        }
      }

      if (m_eventInfoInts["fcnc_qpidWc1"] == 0 || m_eventInfoInts["fcnc_qpidWc0"] == 0) {
        Info("", "Could not find W decay daughters in event %i", m_eventCounter);
      }

      return true;
    }

    return false;
  }


  void FCNCTool::GetDaughter(const xAOD::TruthParticle *tp, std::vector<const xAOD::TruthParticle *> &vecDaughter)
  {
    const xAOD::TruthVertex *decVtx = tp->decayVtx();

    if (decVtx) {
      int nP = decVtx->nOutgoingParticles();

      for (int id = 0; id < nP; id++) {
        const xAOD::TruthParticle *tpd = decVtx->outgoingParticle(id);

        if (tpd) {
          bool oksta = tpd->barcode() < 200000;
          bool okpdg = (tp->pdgId() != 21 && tp->absPdgId() > 6);

          if (oksta && okpdg && std::find(vecDaughter.begin(), vecDaughter.end(), tpd) == vecDaughter.end())
          { vecDaughter.push_back(tpd); }

          this->GetDaughter(tpd, vecDaughter);
        }
      }
    }
  }

  const xAOD::TruthParticle *FCNCTool::ThisParticleFinal(const xAOD::TruthParticle *p)
  {
    if (p->nChildren() == 1) {
      const xAOD::TruthParticle *c = (const xAOD::TruthParticle *)p->child(0);

      if (c->pdgId() != p->pdgId()) { return p; }
      else { return ThisParticleFinal(c); }
    } else { return p; }
  }

}
