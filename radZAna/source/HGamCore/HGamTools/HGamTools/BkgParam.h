//

#ifndef BkgParam_h
#define BkgParam_h

#include <vector>
#include <map>
#include <cmath>
#include "TString.h"

#include "TEnv.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TCanvas.h"

// RooFit headers:
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"

#include "HGamTools/BkgParamCore.h"

namespace BkgTool {
  //
  // Selector for spurious signal -- implements the usual S/Sref and S/deltaS selections
  //
  class SpuriousSignalSelector : public Selector {
  public:
    // Constructor and destructor:
    SpuriousSignalSelector(const TString &configFileName, const TString &outputDir)
      : Selector(configFileName, outputDir), m_nSignalRef("RefSignal") { }
    virtual ~SpuriousSignalSelector() {}

    bool setup();
    bool makeResults(std::vector<Result *> &results);
    bool performFits(Parameterization &pdf);

  private:
    SignalYield m_nSignalRef;
    double m_maxSigOverError, m_maxSigOverRef, m_oneSigmaSigOverError, m_oneSigmaSigOverRef;
    double m_twoSigmaSigOverError, m_twoSigmaSigOverRef;
    double m_minChi2Pvalue;
  };

  class SpuriousSignalResult : public Result {
  public:
    SpuriousSignalResult(const TString &name) : Result(name) { }
    virtual ~SpuriousSignalResult() {}
    bool operator<(const Result &other) const;
    std::string m_sortingOption;
    void SetSortingOption(std::string s) {m_sortingOption = s;};
  };

  //
  // Selector for mass bias -- implements a selection on deltaM/M
  //
  class MassBiasSelector : public Selector {
  public:
    // Constructor and destructor:
    MassBiasSelector(const TString &configFileName, const TString &outputDir)
      : Selector(configFileName, outputDir), m_nSignalRef("RefSignal") { }
    virtual ~MassBiasSelector() {}

    bool setup();
    bool makeResults(std::vector<Result *> &results);
    bool performFits(Parameterization &pdf);

  private:
    SignalYield m_nSignalRef;
    double m_maxDeltaMOverM;
  };

  class MassBiasResult : public Result {
  public:
    MassBiasResult(const TString &name) : Result(name) { }
    virtual ~MassBiasResult() {}
    bool operator<(const Result &other) const;
  };

  bool makeHistogram(const std::vector<TString> &inputFiles, const TString &treeName, const TString &obsName,
                     const TString &weightVarName, const TString &outputFile, const TString &outputHistName,
                     unsigned int histBins, double histMin, double histMax, bool convertToGeV = false);
  bool makeTree(const std::vector<TString> &inputFiles, const TString &inputTreeName, const TString &inputObsName,
                const TString &outputFile, const TString &outputTreeName, const TString &outputObsName, const TString &cutFlag = "",
                double obsMin = -DBL_MAX, double obsMax = -DBL_MAX, bool convertToGeV = false, const TString &catName = "", int cat = -1);
}
#endif
