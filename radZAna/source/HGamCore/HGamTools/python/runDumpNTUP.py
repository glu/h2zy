#!/usr/bin/env python

# 
#	Author: Simone Michele Mazza
#	Mail: simone.mazza@mi.infn.it
#
#	Dump NTUPLES from mxAOD
#

import ROOT
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False
# suppress ROOT command line, use python optparse
ROOT.PyConfig.IgnoreCommandLineOptions = True

import logging
logging.basicConfig(level = logging.DEBUG)
from optparse import OptionParser, OptionGroup, OptionValueError
import socket
import time
import fnmatch, re
import datetime
from itertools import izip
import shutil
import glob


parser = OptionParser(epilog=
"""Examples:

  ./GggTestrun.py -w
""")
og_eventloop = OptionGroup(parser, 'EventLoop options', 'Configure how to run')

og_eventloop.add_option('--submitDir', help='dir to store the output', default='DumpNTUP')
og_eventloop.add_option('--nevents', type=int, help='number of events to process for all the datasets', default=0)
og_eventloop.add_option('--files', help='files to run, it accepts wildcard', default='*.root')
og_eventloop.add_option('--driver', help='select where to run', choices=('direct', 'condor'), default='direct')

parser.add_option_group(og_eventloop)

(options, args) = parser.parse_args()

logging.info('loading packages')
shutil.copyfile(ROOT.gSystem.ExpandPathName('$ROOTCOREDIR/scripts/load_packages.C'), 'load_packages.C')
ROOT.gROOT.ProcessLine('.x load_packages.C')
logging.info('loaded packages')

logging.info('creating new job')
job = ROOT.EL.Job()

# add our algorithm to the job
logging.info('creating algorithms')
main_alg = ROOT.DumpNTUP()
main_alg.mem = [] # use this to prevent ownwership problems

# sample handler
sh = ROOT.SH.SampleHandler()
sh.setMetaString("nc_tree", "CollectionTree")

file_names = glob.glob(options.files)

for file_name in file_names:
  print file_name
  sample_name = file_name.split("/")[len(file_name.split("/"))-1].split(".root")[0]
  sample = ROOT.SH.SampleLocal(sample_name)
  sample.add(file_name)
  sh.add(sample)

# set the name of the tree in our files
sh.setMetaString("nc_tree", "CollectionTree")

sh.printContent()

job.sampleHandler(sh)

# setting job options
if options.nevents != 0:
	logging.info('processing only %d events', options.nevents)
	job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)

# Configuration file
conf_file = "HGamTools/DumpNTUP.cfg"
#conf_file = "HGamAnalysisFramework/HGamRel20p7.config"

conf = ROOT.HG.Config()
conf.addFile(conf_file)
# Set the configuration file
main_alg.setConfig(conf)

# using ntuple service (not working with xAOD)
output = ROOT.EL.OutputStream("NTUP_output")
job.outputAdd(output)
ntuple = ROOT.EL.NTupleSvc("NTUP_output")
job.algsAdd(ntuple)

job.algsAdd(main_alg)

# configure the driver
logging.info('creating driver')
driver = None
if (options.driver == 'direct'):
    logging.info('running on direct')
    driver = ROOT.EL.DirectDriver()
elif (options.driver == 'condor'):
    logging.info('running on Condor')
    driver = ROOT.EL.CondorDriver()
    driver.shellInit =  "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase; source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || exit $?; source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --skipConfirm || exit $?;"
    driver.nFilesPerJob = 20
    job.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, 20)
    job.options().setString(ROOT.EL.Job.optCondorConf, 'Requirements = (OpSysMajorVer == 6 && Machine != "proof-08.mi.infn.it")')

driver.outputSampleName = "data15_13TeV.00284285.physics_Main.MxAOD.p2425.h012pre.root"

submitDir = options.submitDir + "_" + datetime.datetime.now().strftime('%Y%m%d') + "_" + datetime.datetime.now().strftime('%H%M%S')
driver.submitOnly(job, submitDir)

print "Done"
