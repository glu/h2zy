import ConfigParser
from optparse import OptionParser
import json
import os
import subprocess
import sys
import datetime
import re

######################
# Call script  python MxAODCheckAndDownload.py -h for options
# Possible options for sysCategory defined in MxAODinformations.config
# No need for mcType if category is of type dataXX
######################

startTime = datetime.datetime.now()
def LargeFileCheck(fileName):
	flag_bool=False
	os.popen("rucio list-files " + fileName + "_MxAOD.root | grep \"Total size :\" >> filesize_temp")
	with open('filesize_temp') as f:
		first_line = f.readline()
		if "GB" in first_line:
		    size=float(re.findall("\d+\.\d+", first_line)[0])
		    if size > 100.0:
		        print "File larger than 100 GB, going to download directly to EOS"
		        flag_bool=True
	os.popen("rm filesize_temp")
	return flag_bool



parser = OptionParser()
parser.add_option("-t", "--htag", help="htag you want to download", dest="htag")
parser.add_option("-c", "--category", help="category of samples: Detailed or systematics category", dest="cat")
parser.add_option("-m", "--mcType", help="MC type: example mc16a", dest="mcType")
parser.add_option("-s", "--sampleList", help="List of expected samples to be downloaded (in text file)", default="", dest="samples")
parser.add_option("-p", "--printOnly", help="Only print list of missing files and jobs (without downloading)", action="store_false", default=True, dest="download")
parser.add_option("-f", "--HGamCoreFolderPath", help="Path for HGamCore folder (used to read files lists and MxAODinformations.config)", default="../source/HGamCore" ,dest="path")
parser.add_option("-u", "--userName", help="Only look at jobs from specific user (default: consider all)", default="" ,dest="user")
parser.add_option("-d", "--days", help="Only look at jobs since specific amount of days (default=90)", default="90" ,dest="days")
(options, args) = parser.parse_args()

# Basic information about samples to download
print "\n=============================================================================="
htag = options.htag
print "htag: ", htag
category = options.cat
print "category: ", category
mcType = ""
if category.count("data")==0:
	mcType = options.mcType
print "mcType: ", mcType
print "==============================================================================\n"

# File containing information about all current grid jobs from higgs group production
pandaFile = "pandaOutput_"+startTime.strftime("%d_%b_%y_%H_%M_%S")+".json"
os.system("cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt")
os.system("curl -b bigpanda.cookie.txt -H 'Accept: application/json' -H 'Content-Type: application/json' \"https://bigpanda.cern.ch/tasks/?taskname=group.phys-higgs."+mcType+"*"+htag+"_*"+category+"*"+options.user+"*&days="+options.days+"&json\" > " + pandaFile)

# Load grid information in map
pandaMap = json.load(open(pandaFile))

# Paths to input file lists and eos group folder
inputListPath = options.path+"/HGamTools/data/input/"
eosPath = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/"

# Parce configuration options
config = ConfigParser.ConfigParser()
config.readfp(open(options.path+"/HGamTools/scripts/MxAODinformations.config"))
dataType = config.get(category, "dataType").replace("<mcType>",mcType)
MxAODconfig = config.get(category, "configFile").replace("<mcType>",mcType)
listOfFiles = config.get(category, "inputList").replace("<mcType>",mcType)
eosFolderStage = config.get(category, "eosFolderStage").replace("<mcType>",mcType)
eosFolder = config.get(category, "eosFolder").replace("<mcType>",mcType)

# Read list of samples to download (if done)
if options.samples == "":
	inFilesList = open(inputListPath+listOfFiles, "r");
else:
	inFilesList = open(options.samples, "r");
expectedFiles = inFilesList.readlines()
inFilesList.close()

# List of all MC samples available for certaint mc type
allFilesName = inputListPath+mcType+"_HIGG1D1.txt"
if dataType == "data":
	allFilesName = inputListPath+"data_13TeV_HIGG1D1.txt"
allFiles = open(allFilesName, "r")
listAllFiles = allFiles.readlines()
allFiles.close()
# Path of eos directory in which to check if file already exists
eosFolderToCheck = [ eosPath+htag+"_stage/"+eosFolderStage, eosPath+htag+"/"+eosFolder]

#

# Loop over list of expected files and format input
#***************************************************************

i = 0
while i < len(expectedFiles):

	if len(expectedFiles[i])==1:
		del expectedFiles[i]
		continue #do not increase i by 1

	if expectedFiles[i].endswith("\n"):
		expectedFiles[i] = expectedFiles[i][:-1]

	if expectedFiles[i].startswith("#"):
		del expectedFiles[i]
		continue #do not increase i by 1

	if dataType == "data" and not expectedFiles[i].count(category)>0:
		del expectedFiles[i]
		continue #do not increase i by 1

	if expectedFiles[i].count(" ") > 0 and dataType == "MC":
		expectedFiles[i] = expectedFiles[i][:expectedFiles[i].index(" ")]
	elif dataType == "data":
		expectedFiles[i] = expectedFiles[i][expectedFiles[i].index(":")+1:expectedFiles[i].index(".physics")]
		expectedFiles[i] = expectedFiles[i][expectedFiles[i].index(".")+1:]

	i += 1



# Loop over expected files, search for panda jobs
#***************************************************************

filesInEos = list();
unfinishedFiles = list();
downloadedFiles = list();
missingFiles= list();

for fileName in expectedFiles:

#	while i < len(listAllFiles):
#		if listAllFiles[i].startswith(fileName+" "):
#			ptag = listAllFiles[i][len(listAllFiles[i]-6):]
#			print ptag
#			break

	# Check if file is already in eos; if yes, continue
	found = False
	for folder in eosFolderToCheck:
		mypath = folder
		if not os.path.isdir(mypath):
			print "Folder ",mypath," does not exist (yet)."
			continue # Folder does not exist (yet)
		filesInFolder = [f for f in os.listdir(mypath)]
		for f in filesInFolder:
			if f.count(htag)>0 and f.count(category)>0 and f.count(fileName+".")>0:
				if dataType == "MC" and not f.count(mcType)>0 :
					continue
				filesInEos.append(mypath+"/"+f)
				found = True
	if found:
		continue


	# If not yet downloaded, check status from panda and evtl. download file
	chosenJob = "";

	for job in pandaMap:

		if job["taskname"].count(fileName+".") > 0 and job["taskname"].count(category) > 0 and job["taskname"].count(mcType) > 0:

			if job["status"] != "done":
				unfinishedFiles.append(job["taskname"]+"  "+job["status"])

			else:
				job["taskname"] = job["taskname"].replace("/","");
				# If several successful jobs for same file, take last one submitted
				if len(chosenJob) == 0:
					chosenJob = job["taskname"]
				elif job["taskname"][-1:].isalnum() and chosenJob[-1:].isalnum() and job["taskname"][-1:]>chosenJob[-1:]:
					chosenJob = job["taskname"]
				elif job["taskname"][-1:].isalnum() and not chosenJob[-1:].isalnum():
					chosenJob = job["taksname"]

	if len(chosenJob) == 0:
		missingFiles.append(fileName)
		continue
	else:
		if options.download:
			print "Going to download: ", chosenJob
			jobEnding = chosenJob[chosenJob.find(htag+"_"):]
			if dataType == "MC":
				if LargeFileCheck(chosenJob):
				    os.system("getMxAOD.sh  -o "+eosPath+htag+"_stage/"+eosFolderStage+" -fdmxs -t "+htag+" "+fileName+" "+jobEnding+" "+MxAODconfig+" "+mcType+"_HIGG1D1.txt")
				else:
				    os.system("getMxAOD.sh  -o /tmp/${USER} -fdmexs -t "+htag+" "+fileName+" "+jobEnding+" "+MxAODconfig+" "+mcType+"_HIGG1D1.txt")
			elif dataType == "data":
				os.system("getMxAOD.sh  -o /tmp/${USER} -fdmex -t "+htag+" run"+fileName+" "+jobEnding+" "+MxAODconfig+" data_13TeV_HIGG1D1.txt")
		downloadedFiles.append(fileName)


print "\nDownloaded/finished files:\n"
print '\n'.join([ str(element) for element in downloadedFiles ])
print "\nMissing files:\n"
print '\n'.join([ str(element) for element in missingFiles ])
print "\nUnfinished jobs:\n"
print '\n'.join([ str(element) for element in unfinishedFiles ])
print '\n'

os.system("rm "+pandaFile);
