////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  createSignalParameterization.cxx                                          //
//                                                                            //
//  Author: Andrew Hard                                                       //
//  Date: 19/08/2015                                                          //
//  Email: ahard@cern.ch                                                      //
//                                                                            //
//  This main method provides a tool for performing individual fits to the    //
//  resonance Monte Carlo in bins of a specified variable. This is useful for //
//  the differential cross-section measurements. Settings for the utility are //
//  provided in diffXSExample.cfg.                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

// ROOT include(s):
#include "TCanvas.h"
#include "TChain.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TPad.h"
#include "TString.h"

// Local include(s):
#include "HGamAnalysisFramework/Config.h"
#include "HGamTools/AtlasStyle.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include "HGamTools/SigParam.h"

/**
   -----------------------------------------------------------------------------
   A simple method for choosing which category a value should fall into, based
   on a specified binning.
   @param value - The value of the parameter.
   @param bining - A list of bin edges into which the value will fall.
   @returns - An integer category index starting at zero.
*/
int getDiffXSCategory(double value, std::vector<double> binning)
{
  for (int i_b = 0; i_b < (int)binning.size() - 1; i_b++) {
    if ((value >= binning[i_b]) && (value < binning[i_b + 1])) { return i_b; }
  }

  return -1;
}

/**
   -----------------------------------------------------------------------------
   Prints a progress bar to screen to provide elapsed time and remaining time
   information to the user. This is useful when processing large datasets.
   @param index - The current event index.
   @param total - The total number of events.
*/
void PrintProgressBar(int index, int total)
{
  if (index % 1000 == 0) {
    TString print_bar = " [";

    for (int bar = 0; bar < 20; bar++) {
      double current_fraction = double(bar) / 20.0;

      if (double(index) / double(total) > current_fraction) { print_bar.Append("/"); }
      else { print_bar.Append("."); }
    }

    print_bar.Append("] ");
    double percent = 100.0 * (double(index) / double(total));
    TString text = Form("%s %2.2f ", print_bar.Data(), percent);
    std::cout << text << "%\r" << std::flush;
  }
}

/**
   -----------------------------------------------------------------------------
   The main method for this utility. Provide 1 argument - the location of the
   config (.cfg) file, which should be stored in the data/ directory. The main()
   method runs over the samples provided, performs the fits requests, and gives
   comparisons of parameterized and non-parameterized fits.
*/
int main(int argc, char *argv[])
{
  // Check that the config file location is provided.
  if (argc < 2) { HG::fatal("No arguemnts provided"); }

  //HG::Config settings(TString(argv[1]));
  HG::Config *settings = new HG::Config(TString(argv[1]));

  std::cout << "createDiffXSSignal: WARNING! THIS EXAMPLE IS DEPRECATED."
            << std::endl;

  // Print configuration for benefit of user:
  std::cout << "createSignalParameterization will run with parameters:"
            << std::endl;
  settings->printDB();

  // Set the function type:
  TString function = settings->getStr("SignalFunctionalForm");

  // Check that output directory exists:
  TString outputDir = settings->getStr("OutputDir");
  system(Form("mkdir -vp %s", outputDir.Data()));

  // Set the ATLAS Style for plots:
  SetAtlasStyle();

  // Instantiate SigParam class for individual & parameterized fits:
  SigParam *sps
    = new SigParam(settings->getStr("SampleName"), outputDir + "/Individual");

  // Prepare for loop over input MxAOD/TTree:
  std::vector<TString> fileNames = settings->getStrV("InputFile");
  TChain *chain = new TChain(settings->getStr("TreeName"));

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    chain->AddFile(fileNames[i_f]);
  }

  // Assign the MxAOD/TTree branches to variables:
  float massVar;
  float weightVar;
  int cutFlow;
  float cateVar;
  chain->SetBranchAddress(settings->getStr("MassBranchName"), &massVar);
  chain->SetBranchAddress(settings->getStr("WeightBranchName"), &weightVar);
  chain->SetBranchAddress(settings->getStr("CutFlowBranchName"), &cutFlow);
  chain->SetBranchAddress(settings->getStr("VariableForCategories"), &cateVar);
  int nEvents = chain->GetEntries();
  int nCategories = 0;

  double resMass = settings->getNum("ResonanceMass");

  //--------------------------------------//
  // Loop over the Trees:
  // Loop over events to get parameterization:
  std::cout << "There are " << nEvents << " events to process." << std::endl;

  for (int index = 0; index < nEvents; index++) {
    chain->GetEntry(index);
    PrintProgressBar(index, nEvents);

    // Only use events passing the selection:
    if (cutFlow < settings->getInt("CutFlowIndex")) { continue; }

    // The observed mass fed into the SigParam tool should always be in GeV.
    double massToUse = (double)massVar / 1000.0;

    // The category index fed into the SigParam tool should start at 0.
    int currCate = getDiffXSCategory(cateVar, settings->getNumV("VariableBins"));

    if (currCate < 0) { continue; }

    // Counter for total number of categories:
    if (currCate >= nCategories) { nCategories = currCate + 1; }

    // Add the mass and weight values to the datasets for fitting:
    sps->addMassPoint(resMass, currCate, massToUse, weightVar);
  }

  //--------------------------------------//
  // Now fit and plot the resonance shapes!
  std::cout << "createSignalParameterization: Start fitting and plotting!"
            << std::endl;

  // Loop over the analysis categories:
  for (int i_c = 0; i_c < nCategories; i_c++) {
    if (sps->makeSingleResonance(resMass, i_c, function)) {
      sps->plotSingleResonance(resMass, i_c);
    } else {
      std::cout << "createSignalParameterization: Fit at cate=" << i_c
                << " did not converge :(" << std::endl;
    }

    sps->saveAll();
  }

  //--------------------------------------//
  // Print some values for category 0 at the first defined mass point:
  std::cout << "\ncreateSingleSignal: Printing parameters for cat 0, mH = "
            << resMass << " GeV" << std::endl;
  std::vector<TString> names = sps->getVariableNames(resMass, 0);

  for (int i_n = 0; i_n < (int)names.size(); i_n++) {
    std::cout << "\t" << names[i_n] << " = "
              << sps->getParameterValue(names[i_n], resMass, 0) << std::endl;
  }

  std::cout << "createDiffXSSignal: WARNING! THIS EXAMPLE IS DEPRECATED."
            << std::endl;
  return 0;
}
