// Local include(s):
#include "HGamAnalysisFramework/RunUtils.h"
#include "HGamTools/SkimAndSlim.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  SkimAndSlim *alg = new SkimAndSlim("SkimAndSlim");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
