////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  massResolutionValidation.cxx                                              //
//                                                                            //
//  Author: Andrew Hard                                                       //
//  Date: 16/11/2015                                                          //
//  Email: ahard@cern.ch                                                      //
//                                                                            //
//  The main method provides a tool for comparing the fitted mass resolution  //
//  to the derived mass resolution variable in HGamma MxAODs.                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

// ROOT include(s):
#include "TFile.h"
#include "TChain.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TString.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLatex.h"
#include "TLegend.h"

// Local include(s):
#include "HGamAnalysisFramework/Config.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include "HGamTools/SigParam.h"
#include "HGamTools/AtlasStyle.h"


/**
   -----------------------------------------------------------------------------
   Divide the events into categories based on the expected resolution.
   @param resVal - The value of the resonance variable.
   @param binning - A list of the resolution bin divisions.
*/
int getResCategory(float resVal, std::vector<double> binning)
{
  for (int i_b = 0; i_b < (int)binning.size() - 1; i_b++) {
    if (resVal >= binning[i_b] && resVal < binning[i_b + 1]) { return i_b; }
  }

  return -1;
}

/**
   -----------------------------------------------------------------------------
   Retrieve the total number of events in the file for event normalization:
*/
double getNTotEvtFromHist(TFile *f)
{
  // Find the cutflow histograms from the file based on limited name info:
  TIter next(f->GetListOfKeys());
  TObject *currObj;

  while ((currObj = (TObject *)next())) {
    TString currName = currObj->GetName();

    if (currName.Contains("CutFlow") && currName.Contains("weighted")
        && !currName.Contains("noDalitz")) {
      return (((TH1F *)f->Get(currName))->GetBinContent(3));
    }
  }

  std::cout << "createSignalParameterization: ERROR! MxAOD doesn't have cutflow"
            << std::endl;
  exit(0);
}

/**
   -----------------------------------------------------------------------------
   Copy files from a slow resource (e.g. EOS) to the local disk for faster
   processing.
   @param fileNames - The original file names.
   @returns - An updated list of file names.
*/
std::vector<TString> makeLocalFileCopies(std::vector<TString> fileNames)
{
  std::cout << "createSignalParameterization: Making local copies of inputs."
            << std::endl;
  std::vector<TString> result;
  result.clear();

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    TString newName = Form("tempFile%d.root", i_f);

    if (fileNames[i_f].Contains("root://eosatlas/")) {
      system(Form("xrdcp %s %s", fileNames[i_f].Data(), newName.Data()));
    } else if (fileNames[i_f].Contains("/eos/atlas/")) {
      system(Form("eos cp %s %s", fileNames[i_f].Data(), newName.Data()));
    } else {
      system(Form("cp %s %s", fileNames[i_f].Data(), newName.Data()));
    }

    result.push_back(newName);
  }

  return result;
}

/**
   -----------------------------------------------------------------------------
   Remove any files that were copied over for speed.
   @param fileNames - The original file names.
*/
void removeLocalFileCopies(std::vector<TString> fileNames)
{
  std::cout << "createSignalParameterization: Removing local copies of inputs."
            << std::endl;

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    system(Form("rm %s", fileNames[i_f].Data()));
  }
}

/**
   -----------------------------------------------------------------------------
   Prints a progress bar to screen to provide elapsed time and remaining time
   information to the user. This is useful when processing large datasets.
   @param index - The current event index.
   @param total - The total number of events.
*/
void printProgressBar(int index, int total)
{
  if (index % 1000 == 0) {
    TString print_bar = " [";

    for (int bar = 0; bar < 20; bar++) {
      double current_fraction = double(bar) / 20.0;

      if (double(index) / double(total) > current_fraction) { print_bar.Append("/"); }
      else { print_bar.Append("."); }
    }

    print_bar.Append("] ");
    double percent = 100.0 * (double(index) / double(total));
    TString text = Form("%s %2.2f ", print_bar.Data(), percent);
    std::cout << text << "%\r" << std::flush;
  }
}

/**
   -----------------------------------------------------------------------------
   The main method for this utility. Provide 1 argument - the location of the
   config (.cfg) file, which should be stored in the data/ directory. The main()
   method runs over the samples provided, performs the fits requests, and gives
   comparisons of parameterized and non-parameterized fits.
*/
int main(int argc, char *argv[])
{
  // Check that the config file location is provided.
  if (argc < 2) { HG::fatal("No arguemnts provided"); }

  HG::Config *settings = new HG::Config(TString(argv[1]));

  // Print configuration for benefit of user:
  std::cout << "createSignalParameterization will run with parameters:"
            << std::endl;
  settings->printDB();

  // Check which fits failed or succeeded:
  std::vector<TString> fitFailures;
  fitFailures.clear();
  std::vector<TString> fitSuccesses;
  fitSuccesses.clear();

  // Set the function type:
  TString function = settings->getStr("SignalFunctionalForm");

  // Check that output directory exists:
  TString outputDir = settings->getStr("OutputDir");
  system(Form("mkdir -vp %s", outputDir.Data()));

  // Set the ATLAS Style for plots:
  SetAtlasStyle();

  // List the individual samples:
  std::vector<TString> sampleNames = settings->getStrV("SampleNames");

  // Create a histogram of the resolution parameter:
  std::map<TString, TH1F *> histResList;
  histResList.clear();

  // Load the mass resolution binning from the config file:
  std::vector<double> resVector = settings->getNumV("MassResBins");

  // Create arrays to store output:
  double cateIndices[15][50] = {0};
  double expRes[15][50] = {{0.0}};
  double expResErrHi[15][50] = {{0.0}};
  double expResErrLo[15][50] = {{0.0}};
  double fitMean[15][50] = {{0.0}};
  double fitMeanErr[15][50] = {{0.0}};
  double fitSigma[15][50] = {{0.0}};
  double fitSigmaErr[15][50] = {{0.0}};
  double fitRes[15][50] = {{0.0}};
  double fitResErr[15][50] = {{0.0}};

  SigParam *sps = NULL;
  TChain *chain = NULL;
  int nCategories = 0;

  double resMassToUse = 125.0;// default value.

  //----------------------------------------//
  // Loop over the samples:
  for (int i_s = 0; i_s < (int)sampleNames.size(); i_s++) {
    std::cout << "multiMassValidation: Processing sample " << sampleNames[i_s]
              << std::endl;

    // Instantiate output histograms:
    histResList[sampleNames[i_s]]
      = new TH1F(Form("histResList_%s", (sampleNames[i_s]).Data()),
                 Form("histResList_%s", (sampleNames[i_s]).Data()),
                 50, resVector[0], resVector[(int)resVector.size() - 1]);

    // Instantiate signal parameterization class:
    sps = new SigParam(sampleNames[i_s], outputDir + "/Individual");
    sps->verbosity(settings->getBool("Verbose"));

    // Prepare for loop over input MxAOD/TTree:
    std::vector<TString> fileNames
      = settings->getStrV(Form("InputFile%s", (sampleNames[i_s]).Data()));

    // Make local copies of files if requested, to improve speed:
    if (settings->getBool("MakeLocalCopies")) {
      fileNames = makeLocalFileCopies(fileNames);
    }

    // Create TChain of input files:
    chain = new TChain(settings->getStr("TreeName"));

    for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
      chain->AddFile(fileNames[i_f]);
    }

    // Get the luminosity and reserve a variable for total event norm:
    double luminosity = settings->getNum("Luminosity");
    double nTotEvt = 1000.0;
    TString currFileName = "";

    // Assign the MxAOD/TTree branches to variables:
    float v_mass;
    float v_weight;
    float v_xsbreff;
    std::vector<float> *v_mH = 0;
    int v_cutFlow;
    float v_res;
    float v_zSel;
    float v_zTruth;
    chain->SetBranchAddress(settings->getStr("MassBranchName"), &v_mass);
    chain->SetBranchAddress(settings->getStr("WeightBranchName"), &v_weight);
    chain->SetBranchAddress(settings->getStr("XSBREffBranchName"), &v_xsbreff);
    chain->SetBranchAddress(settings->getStr("TruthMHiggsBranchName"), &v_mH);
    chain->SetBranchAddress(settings->getStr("CutFlowBranchName"), &v_cutFlow);
    chain->SetBranchAddress(settings->getStr("ResolutionBranchName"), &v_res);
    chain->SetBranchAddress(settings->getStr("SelVtxBranchName"), &v_zSel);
    chain->SetBranchAddress(settings->getStr("TruthVtxBranchName"), &v_zTruth);
    int nEvents = chain->GetEntries();
    nCategories = 0;

    // Load the mass resolution binning from the config file:
    std::vector<double> resBinning = settings->getNumV("MassResBins");

    //--------------------------------------//
    // Loop over the Trees:
    // Loop over events to get parameterization:
    std::cout << "There are " << nEvents << " events to process." << std::endl;

    for (int index = 0; index < nEvents; index++) {

      chain->GetEntry(index);
      printProgressBar(index, nEvents);

      // Change the nTotEvt normalization factor for each new file:
      if (!currFileName.EqualTo(chain->GetFile()->GetName())) {
        nTotEvt = getNTotEvtFromHist(chain->GetFile());
      }

      // Only use events passing the selection:
      if (v_cutFlow < settings->getInt("CutFlowIndex")) { continue; }

      // Make sure vertexing is working:
      if (!((v_zTruth - v_zSel) < 1.0)) { continue; }

      // Create method for choosing exclusive resolution categories:
      int currCate = getResCategory(v_res, resBinning);

      if (currCate < 0) { continue; }

      // The observed mass fed into the SigParam tool should always be in GeV.
      double massToUse = (double)v_mass / 1000.0;

      // The resonance mass fed into the SigParam tool should always be in GeV.
      if ((int)(v_mH->size()) < 1) { continue; }

      resMassToUse = (double)(round((v_mH->at(0)) / 1000.0));

      // Calculate the weight to use:
      double weightToUse = luminosity * v_xsbreff * v_weight / nTotEvt;

      // Counter for categories:
      if (currCate >= nCategories) { nCategories = currCate + 1; }

      // Add the mass and weight values to the datasets for fitting:
      sps->addMassPoint(resMassToUse, currCate, massToUse, weightToUse);
      histResList[sampleNames[i_s]]->Fill(v_res, weightToUse);
    }

    // Loop over the analysis categories to store fit data:
    for (int i_c = 0; i_c < nCategories; i_c++) {
      if (sps->makeSingleResonance(resMassToUse, i_c, function)) {
        sps->plotSingleResonance(resMassToUse, i_c);
        fitSuccesses
        .push_back(Form("mass=%2.2f GeV in category %d", resMassToUse, i_c));

        // Also store fit information:
        cateIndices[i_s][i_c] = (double)(i_c + 1);
        expRes[i_s][i_c] = 0.5 * (resBinning[i_c] + resBinning[i_c + 1]);
        expResErrHi[i_s][i_c] = resBinning[i_c + 1] - expRes[i_s][i_c];
        expResErrLo[i_s][i_c] = expRes[i_s][i_c] - resBinning[i_c];

        fitMean[i_s][i_c]
          = (sps->getParameterValue("muCBNom", resMassToUse, i_c));
        fitMeanErr[i_s][i_c]
          = sps->getParameterError("muCBNom", resMassToUse, i_c);

        // Use the width from the standard deviation calculation:
        fitSigma[i_s][i_c] = sps->getMeanOrStdDev("StdDev", resMassToUse, i_c);
        fitSigmaErr[i_s][i_c] = 0.00001;
        fitRes[i_s][i_c] = (sps->getMeanOrStdDev("StdDev", resMassToUse, i_c)
                            / fitMean[i_s][i_c]);
        fitResErr[i_s][i_c] = fabs((fitRes[i_s][i_c] / fitMeanErr[i_s][i_c]) -
                                   (fitRes[i_s][i_c] / (fitMean[i_s][i_c] +
                                                        fitMeanErr[i_s][i_c])));
      } else {
        std::cout << "multiMassValidation: Fit in cate = " << i_c
                  << " did not converge :(" << std::endl;
        fitFailures
        .push_back(Form("mass=%2.2f GeV in category %d", resMassToUse, i_c));
      }

      sps->saveAll();
    }

    // Remove any files copied locally for speed:
    if (settings->getBool("MakeLocalCopies")) { removeLocalFileCopies(fileNames); }

  }// End of loop over samples

  //--------------------------------------//
  // Plot the results!
  std::cout << "multiMassValidation: Start fitting and plotting!" << std::endl;

  Color_t colorList[15] = {kOrange + 7, kBlue + 1, kMagenta, kGreen + 2, kCyan + 1,
                           kOrange + 8, kBlue + 2, kMagenta + 1, kGreen + 3, kCyan + 2,
                           kOrange + 9, kBlue + 3, kMagenta + 2, kGreen + 4, kCyan + 3
                          };
  TCanvas *can = new TCanvas("can", "can");
  can->cd();

  // Draw the expected resolution vs. fitted in each category:
  TGraphAsymmErrors *gExpected
    = new TGraphAsymmErrors(nCategories, cateIndices[0], expRes[0], 0, 0,
                            expResErrLo[0], expResErrHi[0]);
  gExpected->SetFillColor(kRed - 10);
  gExpected->SetLineColor(kRed + 1);
  gExpected->GetXaxis()->SetTitle("Resolution category index");
  gExpected->GetYaxis()->SetTitle("#sigma_{m_{yy}} / m_{yy}");
  gExpected->GetXaxis()->SetNdivisions(nCategories);
  gExpected->Draw("3A");

  TLegend leg(0.18, 0.65, 0.45, 0.9);
  leg.SetTextFont(42);
  leg.SetBorderSize(0);
  leg.SetFillColor(0);
  leg.AddEntry(gExpected, "Expected Resolution", "F");

  TLegend leg2(0.63, 0.65, 0.88, 0.9);
  leg2.SetTextFont(42);
  leg2.SetBorderSize(0);
  leg2.SetFillColor(0);
  //leg2.AddEntry(gExpected, "Expected Resolution", "F");

  std::map<TString, TGraphErrors *> gFit;
  gFit.clear();

  for (int i_s = 0; i_s < (int)sampleNames.size(); i_s++) {
    gFit[sampleNames[i_s]]
      = new TGraphErrors(nCategories, cateIndices[i_s], fitRes[i_s], 0,
                         fitResErr[i_s]);
    gFit[sampleNames[i_s]]->SetLineWidth(1);
    gFit[sampleNames[i_s]]->SetLineColor(colorList[i_s]);
    gFit[sampleNames[i_s]]->SetMarkerColor(colorList[i_s]);
    leg.AddEntry(gFit[sampleNames[i_s]],
                 Form("Fitted Resolution %s", (sampleNames[i_s]).Data()), "P");
    gFit[sampleNames[i_s]]->Draw("EPSAME");
  }

  leg.Draw("SAME");
  can->Print(Form("%s/res_vs_category.eps", outputDir.Data()));
  can->Clear();

  // Draw the resolution parameter distribution:
  for (int i_s = 0; i_s < (int)sampleNames.size(); i_s++) {
    histResList[sampleNames[i_s]]
    ->GetXaxis()->SetTitle("#sigma_{m_{yy}} / m_{yy}");
    histResList[sampleNames[i_s]]->GetYaxis()->SetTitle("Weighted entries");
    histResList[sampleNames[i_s]]->SetLineColor(colorList[i_s]);
    histResList[sampleNames[i_s]]->SetMarkerColor(colorList[i_s]);
    histResList[sampleNames[i_s]]->SetMarkerSize(0.5);
    histResList[sampleNames[i_s]]->SetMarkerStyle(20 + i_s);
    histResList[sampleNames[i_s]]
    ->Scale(1.0 / histResList[sampleNames[i_s]]->Integral());
    leg2.AddEntry(histResList[sampleNames[i_s]],
                  Form("Fitted Resolution %s", (sampleNames[i_s]).Data()), "EP");

    if (i_s == 0) { histResList[sampleNames[i_s]]->Draw("E1"); }
    else { histResList[sampleNames[i_s]]->Draw("E1SAME"); }
  }

  leg2.Draw("SAME");
  can->Print(Form("%s/resolution_distribution.eps", outputDir.Data()));
  can->Clear();

  // Draw the resolution vs. mass plot:
  std::map<TString, TGraphErrors *> gFitMassVsRes;
  gFitMassVsRes.clear();

  for (int i_s = 0; i_s < (int)sampleNames.size(); i_s++) {
    gFitMassVsRes[sampleNames[i_s]]
      = new TGraphErrors(nCategories, fitSigma[i_s], fitMean[i_s],
                         fitSigmaErr[i_s], fitMeanErr[i_s]);
    gFitMassVsRes[sampleNames[i_s]]->SetLineWidth(1);
    gFitMassVsRes[sampleNames[i_s]]
    ->GetXaxis()->SetTitle("Fitted #sigma_{m_{yy}} [GeV]");
    gFitMassVsRes[sampleNames[i_s]]
    ->GetYaxis()->SetTitle("Fitted m_{yy} [GeV]");
    gFitMassVsRes[sampleNames[i_s]]->SetLineColor(colorList[i_s]);
    gFitMassVsRes[sampleNames[i_s]]->SetMarkerColor(colorList[i_s]);

    if (i_s == 0) { gFitMassVsRes[sampleNames[i_s]]->Draw("AEP"); }
    else { gFitMassVsRes[sampleNames[i_s]]->Draw("EPSAME"); }
  }

  TLine *line = new TLine();
  line->SetLineStyle(2);
  line->SetLineWidth(3);
  line->SetLineColor(kRed);
  line->DrawLine(gFitMassVsRes[sampleNames[0]]->GetXaxis()->GetXmin(),
                 resMassToUse,
                 gFitMassVsRes[sampleNames[0]]->GetXaxis()->GetXmax(),
                 resMassToUse);

  can->Print(Form("%s/resolution_vs_mass.eps", outputDir.Data()));

  // Print out a list of good and bad fits:
  std::cout << "createSingleSignal: Printing Summary" << std::endl;
  std::cout << "\tFits that succeeded (" << (int)fitSuccesses.size()
            << " total)" << std::endl;
  //for (int i_s = 0; i_s < (int)fitSuccesses.size(); i_s++) {
  //std::cout << "\t\t" << fitSuccesses[i_s] << std::endl;
  //}
  std::cout << "\tFits that failed (" << (int)fitFailures.size() << " total)"
            << std::endl;
  //for (int i_f = 0; i_f < (int)fitFailures.size(); i_f++) {
  //std::cout << "\t\t" << fitFailures[i_f] << std::endl;
  //}

  return 0;
}

