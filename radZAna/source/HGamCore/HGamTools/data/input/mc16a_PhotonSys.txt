# H125
# ------------------------------------------------------------------------------

PowhegPy8_NNLOPS_ggH125
PowhegPy8_NNPDF30_VBFH125
PowhegPy8_WmH125J
PowhegPy8_WpH125J
PowhegPy8_ZH125J
PowhegPy8_ggZH125
PowhegPy8_ttH125
MGPy8_tHjb125_yt_plus1
aMCnloHwpp_tWH125_yt_plus1
PowhegPy8_bbH125
aMCnloPy8_bbH125_yb2
aMCnloPy8_bbH125_ybyt


aMCnloPy8_X0toyy_200_NW
aMCnloPy8_X0toyy_40_NW
aMCnloPy8_X0toyy_50_NW
aMCnloPy8_X0toyy_60_NW
aMCnloPy8_X0toyy_70_NW
aMCnloPy8_X0toyy_80_NW
aMCnloPy8_X0toyy_90_NW
aMCnloPy8_X0toyy_100_NW
aMCnloPy8_X0toyy_110_NW
aMCnloPy8_X0toyy_120_NW
aMCnloPy8_X0toyy_140_NW
aMCnloPy8_X0toyy_160_NW
aMCnloPy8_X0toyy_180_NW
aMCnloPy8_X0toyy_60_W5p

PowhegPy8_ggH40
PowhegPy8_ggH60
PowhegPy8_ggH80
PowhegPy8_ggH100
PowhegPy8_ggH120
PowhegPy8_ggH160
PowhegPy8_ggH250
PowhegPy8_ggH750

PowhegPy8_VBFH40
PowhegPy8_VBFH60
PowhegPy8_VBFH80
PowhegPy8_VBFH100
PowhegPy8_VBFH120
PowhegPy8_VBFH160
PowhegPy8_VBFH200
PowhegPy8_VBFH300

Pythia8_ZH40
Pythia8_ZH60
Pythia8_ZH80
Pythia8_ZH100
Pythia8_ZH120
Pythia8_ZH160
Pythia8_ZH200
Pythia8_ZH300

Pythia8_WH40
Pythia8_WH60
Pythia8_WH80
Pythia8_WH100
Pythia8_WH120
Pythia8_WH160
Pythia8_WH200
Pythia8_WH300

Pythia8_ttH40
Pythia8_ttH60
Pythia8_ttH80
Pythia8_ttH100
Pythia8_ttH120
Pythia8_ttH160

MGPy8_ZBhb800LH01K05
MGPy8_ZBhb900LH01K01
MGPy8_ZBhb900LH01K05
MGPy8_ZBhb900LH01K10
MGPy8_ZBhb1000LH01K0
MGPy8_ZBhb1200LH01K0
MGPy8_ZBhb1400LH01K0
MGPy8_ZBhb1600LH01K0
