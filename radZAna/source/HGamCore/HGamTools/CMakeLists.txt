################################################################################
# Package: HGamTools
################################################################################

# Declare the package name:
atlas_subdir( HGamTools )

hgam_version(${AnalysisBase_VERSION} HGamCore_VERSION)
add_definitions( -D__RELEASE__=${HGamCore_VERSION} )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          HGamAnalysisFramework
                          Control/AthToolSupport/AsgTools
                          DataQuality/GoodRunsLists
                          Event/xAOD/xAODCutFlow
                          PhysicsAnalysis/D3PDTools/EventLoopAlgs
                          PhysicsAnalysis/AnalysisCommon/PileupReweighting
)

# Find the needed external(s):
find_package( ROOT COMPONENTS Core RooFitCore RooFit RooStats )
find_package( Lhapdf )

# build a CINT dictionary for the library
atlas_add_root_dictionary ( HGamToolsLib HGamToolsCintDict
                            ROOT_HEADERS Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT
)

# build a shared library
atlas_add_library( HGamToolsLib
                   HGamTools/*.h Root/*.cxx ${HGamToolsCintDict}
                   PUBLIC_HEADERS HGamTools
                   INCLUDE_DIRS ${LHAPDF_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${LHAPDF_LIBRARIES} ${ROOT_LIBRARIES}
                   AsgTools
                   EventLoopAlgs
                   GoodRunsListsLib
                   HGamAnalysisFrameworkLib
                   PileupReweightingLib
                   xAODCutFlow
)

# Install files from the package:
atlas_install_data( data/* )

# Install scripts from the package:
atlas_install_scripts( scripts/*.sh scripts/*.py scripts/make_skimslim )

# Executable(s) in the package:
atlas_add_executable( checkBkgModel util/checkBkgModel.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( checkMassBias util/checkMassBias.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( createDiffXSSignal util/createDiffXSSignal.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( createSignalParameterization util/createSignalParameterization.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( createSingleSignal util/createSingleSignal.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( fitResonantDiHiggs util/fitResonantDiHiggs.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( massResolutionValidation util/massResolutionValidation.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( printWeights util/printWeights.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( createSumOfWeights util/createSumOfWeights.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runHGamCutflowAndMxAOD util/runHGamCutflowAndMxAOD.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runHGamEventDisplay util/runHGamEventDisplay.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runHGamExample util/runHGamExample.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runHGamSimpleExample util/runHGamSimpleExample.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runHGamYieldExample util/runHGamYieldExample.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runPDFTool util/runPDFTool.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runSkimAndSlim util/runSkimAndSlim.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( runXSecSkimAndSlim util/runXSecSkimAndSlim.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )

atlas_add_executable( yybb_CutFlowMaker util/yybb_CutFlowMaker.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} HGamToolsLib )
