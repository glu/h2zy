#! /bin/bash
astyle --options=.astylerc --exclude="hhTruthWeightTools" --exclude="TruthWeightTools" --exclude="VertexPositionReweighting" --recursive "*.h" "*.hpp" "*.cxx"
