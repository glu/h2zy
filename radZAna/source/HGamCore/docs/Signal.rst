Building the signal workspace
-------------------------------

The signal shape of the diphoton invariant mass distribution is parametrized with an analytical function (e.g. Crystall Ball, Double Sided Crystall Ball, ...). This parametrization comes from a fit on simulation signal samples.

Check the example config `singleSigFit <https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/tree/master/HGamTools/data/signalParam/exp_signalWorkspace_ptH_0.cfg>` in the HGamCore package.

The example config is set up to run over the H->yy input samples and produce the workspace for the first ptH bin. The lines specific for each variable, are the lowest one below **# here the variable specific changes will be added**. 

In order to run the code, you have to adjust the list of input files which should be used by adding files to **InputFile**. This should correspond to your signal MC samples.
Furthermore, you can specify the directory to store the results via **OutputDir**. Note that this is then used from the spurious signal code to read in the workspace information from the signal only fit.

The following lines illustrate again, what has to be changed for each variable with the example of the inclusive selection

.. code-block:: bash

  VariableForCategories: variable name as stored in the mXAOD, or for the inclusive selection HGamEventInfoAuxDyn.isPassed
  VariableBins: 0.0 1.0 (bin boundaries as they should be used)
  CategoryNames: Fiducial (the name as then used on the signal parametrisation plot)


The other lines in the config file are the general settings how the signal parametrization is defined by setting the specif parameters, e.g. in the example configuration the settings for the Double Sided Crystall Ball e.g. **Param_nCBLo: 			[9.0,0.1,10.0]**

To run the actual code, you call

.. code-block:: bash

  createSingleSignal <singleSigFitName>.cfg

This will creates a directory including the signal workspaces saved as root file and the fit parameters also stored in text files. In addition, a plot is created showing the signal parametrisation fit.
