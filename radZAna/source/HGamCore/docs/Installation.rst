.. _Install:

Installation
============

Create a working directory

.. code-block:: bash

  mkdir hgam-your_label && cd $_
  mkdir build source run

Find the recommended tag for analysis or MxAOD production and the corresponding AnalysisBase release with which it should be set up.
For h024, a good pair used in these instructions is the one below, but generally use the latest available in the recommendations.

.. code-block:: bash

  21.2.56 -- v1.8.33-h024

Setup the analysis release

.. code-block:: bash

  cd source
  setupATLAS
  lsetup git
  asetup AnalysisBase,21.2.56,here

Setup the code to compile with the release used above

.. code-block:: bash

  cd $TestArea # this should be the "source" directory from above
  git clone --recursive https://:@gitlab.cern.ch:8443/atlas-hgam-sw/HGamCore.git # Assumes Kerberos checkout
  cd HGamCore
  git checkout v1.8.33-h024 # Generally the most recently recommended version is best
  git submodule update --init --recursive # Get proper dependencies
  cd -
  source HGamCore/HGamAnalysisFramework/scripts/setupCMake

Compile everything

.. code-block:: bash

  cd $TestArea/../build
  cmake ../source
  make -j4 # Compile using 4 cores; tweak as appropriate for your system
  source $AnalysisBase_PLATFORM/setup.sh
