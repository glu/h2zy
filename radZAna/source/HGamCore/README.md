
# HGam framework

This is the code used by the [HGam subgroup](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsGamGam) to produce and analyze mxAOD used in the analysis.

You can visit the [documentation](https://hgamdocs.web.cern.ch/index.html)