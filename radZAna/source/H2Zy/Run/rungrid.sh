# solution 1  start from PACKAGE/run

## step 1 making tarball
cd ../
rm -rf jobcontents.tar.gz
tar -czf jobcontents.tar.gz build/ source/
cd run
mv ../jobcontents.tar.gz .
## step 2 submit by "prun"
voms-proxy-init -voms atlas
lsetup panda
sh rungrid_LOCAL.sh DAOD.list
# where in rungrid_LOCAL.sh
# prun --nFilesPerJob 20 --useAthenaPackages --exec="sed -i 's/,/\n/g' input.txt; cat input.txt; ls -ltr; source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh; asetup AnalysisBase,21.2.25,here;source build/x86_64-slc6-gcc62-opt/setup.sh;pwd;ls -ltr; ./build/x86_64-slc6-gcc62-opt/bin/runH2ZyAnalysis source/H2Zy/data/H2ZyAnalysisDATA_Rel21.cfg InputFileList: input.txt OutputDir: output;cp output/hist-sample.root ./; ls -ltr" --inDS=$str2 --outDS=user.username.outname --writeInputToTxt=IN:input.txt --outputs=hist-sample.root --match="*.root*" --inTarBall=jobcontents.tar.gz

# solution 2 start from anywhere e.g. PACKAGE/run/, this solution is very slow to submit many DATASETs

#runH2ZyAnalysis H2Zy/H2ZyAnalysisDATA_Rel21.cfg GridDS: DATASETNAME  OutputDS: user.username.OUTPUT NumEvents: 50
