#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;
void changeName()
{
TString filename = "mc16d_13TeV.Zgam.PRW.config.NEW.root";
TH1::AddDirectory(false);
vector<TString> vec_name_histo;
map<TString , TH1F*> map_histo;
TString temp_str;
cout<<"start"<<endl;
vec_name_histo.push_back("pileup_chan301535_run300000");
vec_name_histo.push_back("pileup_chan301536_run300000");
vec_name_histo.push_back("pileup_chan301899_run300000");
vec_name_histo.push_back("pileup_chan301900_run300000");
vec_name_histo.push_back("pileup_chan301901_run300000");
vec_name_histo.push_back("pileup_chan301902_run300000");
vec_name_histo.push_back("pileup_chan301903_run300000");
vec_name_histo.push_back("pileup_chan301904_run300000");
vec_name_histo.push_back("pileup_chan306201_run300000");
vec_name_histo.push_back("pileup_chan306203_run300000");
vec_name_histo.push_back("pileup_chan306205_run300000");
vec_name_histo.push_back("pileup_chan306207_run300000");
vec_name_histo.push_back("pileup_chan306209_run300000");
vec_name_histo.push_back("pileup_chan306210_run300000");
vec_name_histo.push_back("pileup_chan306211_run300000");
vec_name_histo.push_back("pileup_chan306213_run300000");
vec_name_histo.push_back("pileup_chan306215_run300000");
vec_name_histo.push_back("pileup_chan306217_run300000");
vec_name_histo.push_back("pileup_chan306220_run300000");
vec_name_histo.push_back("pileup_chan306221_run300000");
vec_name_histo.push_back("pileup_chan306222_run300000");
vec_name_histo.push_back("pileup_chan306226_run300000");
vec_name_histo.push_back("pileup_chan306231_run300000");
vec_name_histo.push_back("pileup_chan306234_run300000");
vec_name_histo.push_back("pileup_chan306235_run300000");
vec_name_histo.push_back("pileup_chan306236_run300000");
vec_name_histo.push_back("pileup_chan306237_run300000");
vec_name_histo.push_back("pileup_chan306238_run300000");
vec_name_histo.push_back("pileup_chan306239_run300000");
vec_name_histo.push_back("pileup_chan306240_run300000");
vec_name_histo.push_back("pileup_chan306241_run300000");
vec_name_histo.push_back("pileup_chan306242_run300000");
vec_name_histo.push_back("pileup_chan306243_run300000");
vec_name_histo.push_back("pileup_chan306245_run300000");
vec_name_histo.push_back("pileup_chan306247_run300000");
vec_name_histo.push_back("pileup_chan306249_run300000");
vec_name_histo.push_back("pileup_chan306250_run300000");
vec_name_histo.push_back("pileup_chan306251_run300000");
vec_name_histo.push_back("pileup_chan306252_run300000");
vec_name_histo.push_back("pileup_chan306253_run300000");
vec_name_histo.push_back("pileup_chan306255_run300000");
vec_name_histo.push_back("pileup_chan306257_run300000");
vec_name_histo.push_back("pileup_chan306259_run300000");
vec_name_histo.push_back("pileup_chan306262_run300000");
vec_name_histo.push_back("pileup_chan306263_run300000");
vec_name_histo.push_back("pileup_chan306264_run300000");
vec_name_histo.push_back("pileup_chan306268_run300000");
vec_name_histo.push_back("pileup_chan306273_run300000");
vec_name_histo.push_back("pileup_chan306276_run300000");
vec_name_histo.push_back("pileup_chan306277_run300000");
vec_name_histo.push_back("pileup_chan306278_run300000");
vec_name_histo.push_back("pileup_chan306279_run300000");
vec_name_histo.push_back("pileup_chan306280_run300000");
vec_name_histo.push_back("pileup_chan306281_run300000");
vec_name_histo.push_back("pileup_chan306282_run300000");
vec_name_histo.push_back("pileup_chan306283_run300000");
vec_name_histo.push_back("pileup_chan306284_run300000");
vec_name_histo.push_back("pileup_chan308556_run300000");
vec_name_histo.push_back("pileup_chan308558_run300000");
vec_name_histo.push_back("pileup_chan308560_run300000");
vec_name_histo.push_back("pileup_chan308562_run300000");
vec_name_histo.push_back("pileup_chan342200_run300000");
vec_name_histo.push_back("pileup_chan342202_run300000");
vec_name_histo.push_back("pileup_chan342203_run300000");
vec_name_histo.push_back("pileup_chan342205_run300000");
vec_name_histo.push_back("pileup_chan342208_run300000");
vec_name_histo.push_back("pileup_chan342209_run300000");
vec_name_histo.push_back("pileup_chan342210_run300000");
vec_name_histo.push_back("pileup_chan342211_run300000");
vec_name_histo.push_back("pileup_chan342214_run300000");
vec_name_histo.push_back("pileup_chan342216_run300000");
vec_name_histo.push_back("pileup_chan342217_run300000");
vec_name_histo.push_back("pileup_chan342219_run300000");
vec_name_histo.push_back("pileup_chan342221_run300000");
vec_name_histo.push_back("pileup_chan342222_run300000");
vec_name_histo.push_back("pileup_chan342223_run300000");
vec_name_histo.push_back("pileup_chan342224_run300000");
vec_name_histo.push_back("pileup_chan342225_run300000");
vec_name_histo.push_back("pileup_chan342226_run300000");
vec_name_histo.push_back("pileup_chan342228_run300000");
vec_name_histo.push_back("pileup_chan342230_run300000");
vec_name_histo.push_back("pileup_chan342231_run300000");
vec_name_histo.push_back("pileup_chan342233_run300000");
vec_name_histo.push_back("pileup_chan342235_run300000");
vec_name_histo.push_back("pileup_chan342236_run300000");
vec_name_histo.push_back("pileup_chan342238_run300000");
vec_name_histo.push_back("pileup_chan342240_run300000");
vec_name_histo.push_back("pileup_chan342241_run300000");
vec_name_histo.push_back("pileup_chan342243_run300000");
vec_name_histo.push_back("pileup_chan342245_run300000");
vec_name_histo.push_back("pileup_chan343095_run300000");
vec_name_histo.push_back("pileup_chan343106_run300000");
vec_name_histo.push_back("pileup_chan343580_run300000");
vec_name_histo.push_back("pileup_chan344303_run300000");
vec_name_histo.push_back("pileup_chan344744_run300000");
vec_name_histo.push_back("pileup_chan344745_run300000");
vec_name_histo.push_back("pileup_chan344746_run300000");
vec_name_histo.push_back("pileup_chan344747_run300000");
vec_name_histo.push_back("pileup_chan344748_run300000");
vec_name_histo.push_back("pileup_chan344749_run300000");
vec_name_histo.push_back("pileup_chan344750_run300000");
vec_name_histo.push_back("pileup_chan344751_run300000");
vec_name_histo.push_back("pileup_chan344752_run300000");
vec_name_histo.push_back("pileup_chan344753_run300000");
vec_name_histo.push_back("pileup_chan344754_run300000");
vec_name_histo.push_back("pileup_chan344755_run300000");
vec_name_histo.push_back("pileup_chan344756_run300000");
vec_name_histo.push_back("pileup_chan344757_run300000");
vec_name_histo.push_back("pileup_chan344878_run300000");
vec_name_histo.push_back("pileup_chan345042_run300000");
vec_name_histo.push_back("pileup_chan345065_run300000");
vec_name_histo.push_back("pileup_chan345160_run300000");
vec_name_histo.push_back("pileup_chan345161_run300000");
vec_name_histo.push_back("pileup_chan345162_run300000");
vec_name_histo.push_back("pileup_chan345316_run300000");
vec_name_histo.push_back("pileup_chan345320_run300000");
vec_name_histo.push_back("pileup_chan345321_run300000");
vec_name_histo.push_back("pileup_chan345322_run300000");
vec_name_histo.push_back("pileup_chan345346_run300000");
vec_name_histo.push_back("pileup_chan345350_run300000");
vec_name_histo.push_back("pileup_chan345354_run300000");
vec_name_histo.push_back("pileup_chan345775_run300000");
vec_name_histo.push_back("pileup_chan345776_run300000");
vec_name_histo.push_back("pileup_chan345777_run300000");
vec_name_histo.push_back("pileup_chan345778_run300000");
vec_name_histo.push_back("pileup_chan345779_run300000");
vec_name_histo.push_back("pileup_chan345780_run300000");
vec_name_histo.push_back("pileup_chan345781_run300000");
vec_name_histo.push_back("pileup_chan345782_run300000");
vec_name_histo.push_back("pileup_chan345833_run300000");
vec_name_histo.push_back("pileup_chan345887_run300000");
vec_name_histo.push_back("pileup_chan345888_run300000");
vec_name_histo.push_back("pileup_chan345889_run300000");
vec_name_histo.push_back("pileup_chan345890_run300000");
vec_name_histo.push_back("pileup_chan345891_run300000");
vec_name_histo.push_back("pileup_chan345892_run300000");
vec_name_histo.push_back("pileup_chan345893_run300000");
vec_name_histo.push_back("pileup_chan345894_run300000");
vec_name_histo.push_back("pileup_chan345895_run300000");
vec_name_histo.push_back("pileup_chan345896_run300000");
vec_name_histo.push_back("pileup_chan345897_run300000");
vec_name_histo.push_back("pileup_chan345898_run300000");
vec_name_histo.push_back("pileup_chan345899_run300000");
vec_name_histo.push_back("pileup_chan345900_run300000");
vec_name_histo.push_back("pileup_chan346196_run300000");
vec_name_histo.push_back("pileup_chan346197_run300000");
vec_name_histo.push_back("pileup_chan346198_run300000");
vec_name_histo.push_back("pileup_chan364500_run300000");
vec_name_histo.push_back("pileup_chan364501_run300000");
vec_name_histo.push_back("pileup_chan364502_run300000");
vec_name_histo.push_back("pileup_chan364503_run300000");
vec_name_histo.push_back("pileup_chan364504_run300000");
vec_name_histo.push_back("pileup_chan364505_run300000");
vec_name_histo.push_back("pileup_chan364506_run300000");
vec_name_histo.push_back("pileup_chan364507_run300000");
vec_name_histo.push_back("pileup_chan364508_run300000");
vec_name_histo.push_back("pileup_chan364509_run300000");
vec_name_histo.push_back("pileup_chan364550_run300000");
vec_name_histo.push_back("pileup_chan364552_run300000");
vec_name_histo.push_back("pileup_chan364553_run300000");
vec_name_histo.push_back("pileup_chan364554_run300000");
vec_name_histo.push_back("pileup_chan364555_run300000");
vec_name_histo.push_back("pileup_chan364557_run300000");
vec_name_histo.push_back("pileup_chan364558_run300000");
vec_name_histo.push_back("pileup_chan364559_run300000");
vec_name_histo.push_back("pileup_chan364860_run300000");
vec_name_histo.push_back("pileup_chan364861_run300000");
vec_name_histo.push_back("pileup_chan364862_run300000");
vec_name_histo.push_back("pileup_chan364863_run300000");
vec_name_histo.push_back("pileup_chan364864_run300000");
vec_name_histo.push_back("pileup_chan364865_run300000");
vec_name_histo.push_back("pileup_chan366140_run300000");
vec_name_histo.push_back("pileup_chan366141_run300000");
vec_name_histo.push_back("pileup_chan366142_run300000");
vec_name_histo.push_back("pileup_chan366143_run300000");
vec_name_histo.push_back("pileup_chan366144_run300000");
vec_name_histo.push_back("pileup_chan366145_run300000");
vec_name_histo.push_back("pileup_chan366146_run300000");
vec_name_histo.push_back("pileup_chan366147_run300000");
vec_name_histo.push_back("pileup_chan366148_run300000");
vec_name_histo.push_back("pileup_chan366149_run300000");
vec_name_histo.push_back("pileup_chan366160_run300000");
vec_name_histo.push_back("pileup_chan366162_run300000");
vec_name_histo.push_back("pileup_chan407025_run300000");
vec_name_histo.push_back("pileup_chan407026_run300000");
vec_name_histo.push_back("pileup_chan410082_run300000");
vec_name_histo.push_back("pileup_chan410083_run300000");
vec_name_histo.push_back("pileup_chan410084_run300000");
vec_name_histo.push_back("pileup_chan410087_run300000");
vec_name_histo.push_back("pileup_chan410088_run300000");
vec_name_histo.push_back("pileup_chan410089_run300000");
vec_name_histo.push_back("pileup_chan410389_run300000");
vec_name_histo.push_back("pileup_chan410395_run300000");
vec_name_histo.push_back("pileup_chan301905_run300000");
vec_name_histo.push_back("pileup_chan301906_run300000");
vec_name_histo.push_back("pileup_chan301907_run300000");
vec_name_histo.push_back("pileup_chan361106_run304000");
vec_name_histo.push_back("pileup_chan361107_run300000");
vec_name_histo.push_back("pileup_chan361108_run300000");
vec_name_histo.push_back("pileup_chan361601_run300000");
vec_name_histo.push_back("pileup_chan364100_run304000");
vec_name_histo.push_back("pileup_chan364101_run304000");
vec_name_histo.push_back("pileup_chan364102_run300000");
vec_name_histo.push_back("pileup_chan364103_run304000");
vec_name_histo.push_back("pileup_chan364104_run304000");
vec_name_histo.push_back("pileup_chan364105_run304000");
vec_name_histo.push_back("pileup_chan364106_run304000");
vec_name_histo.push_back("pileup_chan364107_run304000");
vec_name_histo.push_back("pileup_chan364108_run300000");
vec_name_histo.push_back("pileup_chan364109_run304000");
vec_name_histo.push_back("pileup_chan364110_run304000");
vec_name_histo.push_back("pileup_chan364111_run300000");
vec_name_histo.push_back("pileup_chan364112_run304000");
vec_name_histo.push_back("pileup_chan364113_run304000");
vec_name_histo.push_back("pileup_chan364114_run304000");
vec_name_histo.push_back("pileup_chan364115_run300000");
vec_name_histo.push_back("pileup_chan364116_run300000");
vec_name_histo.push_back("pileup_chan364117_run300000");
vec_name_histo.push_back("pileup_chan364118_run300000");
vec_name_histo.push_back("pileup_chan364119_run300000");
vec_name_histo.push_back("pileup_chan364120_run304000");
vec_name_histo.push_back("pileup_chan364121_run304000");
vec_name_histo.push_back("pileup_chan364122_run300000");
vec_name_histo.push_back("pileup_chan364123_run304000");
vec_name_histo.push_back("pileup_chan364124_run304000");
vec_name_histo.push_back("pileup_chan364125_run300000");
vec_name_histo.push_back("pileup_chan364126_run300000");
vec_name_histo.push_back("pileup_chan364127_run300000");
vec_name_histo.push_back("pileup_chan364128_run300000");
vec_name_histo.push_back("pileup_chan364129_run300000");
vec_name_histo.push_back("pileup_chan364130_run300000");
vec_name_histo.push_back("pileup_chan364131_run300000");
vec_name_histo.push_back("pileup_chan364132_run300000");
vec_name_histo.push_back("pileup_chan364133_run300000");
vec_name_histo.push_back("pileup_chan364134_run300000");
vec_name_histo.push_back("pileup_chan364135_run300000");
vec_name_histo.push_back("pileup_chan364136_run300000");
vec_name_histo.push_back("pileup_chan364137_run300000");
vec_name_histo.push_back("pileup_chan364138_run300000");
vec_name_histo.push_back("pileup_chan364139_run300000");
vec_name_histo.push_back("pileup_chan364140_run300000");
vec_name_histo.push_back("pileup_chan364141_run300000");



TFile* f= new TFile(filename.Data(),"read");
TTree* output_tree_base = (TTree*) f->Get("PileupReweighting/MCPileupReweighting");
if (output_tree_base != nullptr) cout<<"find the tree"<<endl;
//TTree* output_tree = new TTree("MCPileupReweighting","tree1");

//TFile ffile("tree_output.root","recreate");
TTree* output_tree = output_tree_base->CloneTree();
if ( output_tree != nullptr) cout<<"find the copied tree"<<endl;
//TTree* output_tree = (TTree*) output_tree_base->CloneTree();
//output_tree->SetName("MCPileupReweighting");
//TTree* output_tree = (TTree*)output_tree_base->Clone("MCPileupReweighting");
Int_t channel ; 
UInt_t runNumber;
//TString histName;
Char_t histName[28];
vector<unsigned int> *periodStarts = 0;
vector<unsigned int> *periodEnds = 0;


output_tree_base->SetBranchStatus("*",0);
output_tree_base->SetBranchStatus("Channel",1);
output_tree_base->SetBranchAddress("Channel",&channel);
output_tree_base->SetBranchStatus("RunNumber",1);
output_tree_base->SetBranchAddress("RunNumber",&runNumber);
output_tree_base->SetBranchStatus("HistName",1);
output_tree_base->SetBranchAddress("HistName",histName);
output_tree_base->SetBranchStatus("PeriodStarts",1);
output_tree_base->SetBranchAddress("PeriodStarts",&periodStarts);
output_tree_base->SetBranchStatus("PeriodEnds",1);
output_tree_base->SetBranchAddress("PeriodEnds",&periodEnds);


output_tree->Reset();

output_tree->SetBranchStatus("*",0);
output_tree->SetBranchStatus("Channel",1);
//output_tree->SetBranchAddress("Channel",&channel);
output_tree->SetBranchStatus("RunNumber",1);
output_tree->SetBranchStatus("HistName",1);
output_tree->SetBranchStatus("PeriodStarts",1);
output_tree->SetBranchStatus("PeriodEnds",1);


cout<<output_tree->GetEntries()<<endl;
cout<<output_tree_base->GetEntries()<<endl;
for(int i = 0 ; i < output_tree_base->GetEntries() ; i++)
{
    
    output_tree_base->GetEntry(i);
    runNumber = 300000;
    TString str_histName = Form("pileup_chan%d_run300000",channel);
    strcpy(histName , str_histName.Data());
    output_tree->Fill();
   
    //if(i%100 == 0)  cout<<"runNumber is"<<runNumber<<endl;
   // output_tree->Fill();
}
cout<<output_tree->GetEntries()<<endl;
for(int i = 0 ; i < output_tree->GetEntries() ; i++)
{
output_tree->GetEntry(i);
if (i%100 == 0) cout<<"runNumber is: "<<runNumber<<endl;
    
} 
TFile ffile(("output_"+filename).Data(),"recreate");
ffile.cd();
ffile.mkdir("PileupReweighting");
TDirectory* dir = (TDirectory*) ffile.GetDirectory("PileupReweighting");
//ffile.cd("PileupReweighting");
//output_tree->Write();

for (vector<TString>::iterator it = vec_name_histo.begin() ; it != vec_name_histo.end() ; it++)
{
f->cd();
map_histo[*it]= (TH1F*) f->Get(("PileupReweighting/"+*it).Data());

//temp_str = "PileupReweighting/"+*it;
temp_str = *it;
//if(temp_str.Contains("_run304000")) {cout<<temp_str<<endl;temp_str.ReplaceAll("_run304000","_run300000");}
//else {continue;}
if(temp_str.Contains("_run304000")) {cout<<temp_str<<endl;}
temp_str.ReplaceAll("_run304000","_run300000");

map_histo[temp_str] = (TH1F*) map_histo[*it]->Clone(temp_str.Data());
map_histo[temp_str]->SetTitle(temp_str.Data());
ffile.cd();
map_histo[temp_str]->SetDirectory(dir);


//map_histo[temp_str]->Write();
}
output_tree->SetDirectory(dir);
ffile.Write();
ffile.Close();
//f->Write();
f->Close();



}
