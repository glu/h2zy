#include "EventLoopGrid/PrunDriver.h"

#include <string>

int main(int argc, char *argv[]) {
  std::string submitDir = argv[1];
  EL::PrunDriver driver;
  driver.status(submitDir);
}

