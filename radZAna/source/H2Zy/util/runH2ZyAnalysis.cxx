#include "H2Zy/H2ZyAnalysis.h"
#include "HGamAnalysisFramework/RunUtils.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  H2ZyAnalysis *alg = new H2ZyAnalysis("H2ZyAnalysis");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
