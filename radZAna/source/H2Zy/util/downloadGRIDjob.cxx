#include "EventLoopGrid/PrunDriver.h"

#include <string>
#include <iostream>

int main(int argc, char *argv[]) {
  if (argc!=2) {
    std::cout << "usage: downloadGRIDjob <submit_directory>" << std::endl;
    return 0;
  }
  std::string submitDir = argv[1];
  EL::PrunDriver driver;
  driver.retrieve(submitDir);
}

