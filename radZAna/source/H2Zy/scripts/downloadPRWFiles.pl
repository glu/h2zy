#!/bin/perl
# use this script to retrieve the PRW files generated on the grid with createPRWFiles.pl

my $debug=1;
my $submit=1;
my $dodownload=1;
my $dohadd=1;
my $domove=1;
my $version=3;

$ARGC=@ARGV;
if($ARGC!=1) {
    die "usage: ./downloadPRWFiles.pl <filename>\n";
}

open FILE, $ARGV[0] or die $!;

while (<FILE>) {
    $dataset = $_;
    chop($dataset);
    if ($dataset =~ /^#/) {
	if ($debug>0) { print "Skipping commented line $dataset\n"; }
    } elsif ($dataset eq "") {
	if ($debug>0) { print "Skipping empty line $dataset\n"; }
    } else {
	$dataset =~ m/(\w+).(\w+).(\w+).(\w+).(\w+).(\w+)/;
	my $mctype=$1;
	my $dsid=$2;
	my $sample=$3;
	my $recoormerge=$4;
	my $aodordaod=$5;
	my $fulltag=$6;
	if ($aodordaod ne "AOD") {
	    print "Sample is not AOD: it is $aodordaod. Skipping..\n";
	}
	else {
	    $outds="user.gmarchio.$mctype.$dsid.$recoormerge.PRW.${fulltag}.${version}_METADATA";
	    if ($dodownload>0) {
		$cmd = "dq2-get $outds/";
		if ($debug>0) { print "Command = $cmd\n"; }
		if ($submit>0) { system($cmd); }
	    }
	    if ($dohadd>0) {
		$cmd = "hadd $mctype.$dsid.$recoormerge.PRW.$fulltag.root $outds*/*root*";
		if ($debug>0) { print "Command = $cmd\n"; }
		if ($submit>0) { system($cmd); }
	    }
	    if ($domove>0) {
		#$cmd = "mv $mctype.$dsid.$recoormerge.PRW.$fulltag.root ../ZllgAnalysis/data/";
		$cmd = "mv $mctype.$dsid.$recoormerge.PRW.$fulltag.root ../data/";
		if ($debug>0) { print "Command = $cmd\n"; }
		if ($submit>0) { system($cmd); }
	    }
	}
    }
}
