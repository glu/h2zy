#!/bin/perl

#
# getAMI5xsec.pl: from a list of AODs, print information about each one using getDSInfoFromAMI5.py
#
my $debug=0;

$ARGC=@ARGV;
if($ARGC!=1) {
    die "usage: ./getAMI5xsec.pl <filename>\n";
}

open FILE, $ARGV[0] or die $!;
print "# AOD                                                                                      n(AOD)                    xsec(AMI) [nb]    filt eff [%]   eq. lumi [nb-1]  ECM  PDF  TUNE\n";          
while (<FILE>) {
    @words = split(/ +/, $_);
    $AOD=$words[0];
    if (substr($AOD,0,1) ne "#") {
	chop($AOD);
	$cmd = "python getDSInfoFromAMI5.py $AOD | grep -v \"#\"";
	if ($debug==1) {
	    print "$cmd\n";
	}
	else {
	    system($cmd);
	}
    }
}
