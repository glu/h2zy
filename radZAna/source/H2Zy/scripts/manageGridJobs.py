#!/usr/bin/python

import os, math, sys, re

#############################
#
# Script for submitting jobs for a list of samples to the grid
#
# VERY IMPORTANT :
# Execute from the Run directory
#
#############################

#############################
# parameters
#############################

if (len(sys.argv)!=2):
  print 'manageGridJobs.py <action>'
  sys.exit(2)

action = sys.argv[1]
if (action!="submit" and action!="check" and action!="download"):
  print 'manageGridJobs.py <action>'
  sys.exit(2)

# only print commands instead of executing them
test_only = True

# directory that will contain all job output
output_dir = "output"

# grid user name
user = "nproklov"

# tag for grid submission
suffix = "mytest"

#############################
# sample definition
#############################

# read the list from file
#fname = "../H2Zy/data/mc15_13TeV.DAOD_HIGG1D2.p2421.txt"
fname = "list.txt"
#fname = "../H2Zy/data/data15_13TeV_DJ.DAOD_HIGG1D2.p2425.txt"

if (os.path.isfile(fname) is not True):
  print 'Error: file ', fname, ' does not exist'
  sys.exit(2)

our_samples = []
with open(fname) as f:
  for line in f:
    if line[0]=='#': continue
    sample = line.rstrip('\n')
    datamcCampaign=''
    channelNumber=''
    sampleType=''
    recoStep=''
    outputFormat=''
    tag=''
    fields = sample.split(".")
    if (len(fields)==6):
      datamcCampaign = fields[0]
      channelNumber = fields[1]
      sampleType = fields[2]
      recoStep = fields[3]
      outputFormat = fields[4]
      tag = fields[5].rstrip('/')
    else:
      print "line does not represent a valid sample: ", sample 
    fields = datamcCampaign.split(":")
    datamcCampaign = fields[0]
    outputSample = 'user.'+user+'.'+datamcCampaign+'.'+channelNumber+'.'+outputFormat+'.'+tag+'.'+suffix+'/'

    cmd=''
    if (action=="check") :
      cmd='checkGRIDjob '+output_dir+'/'+sample
    elif (action=="download") :
      cmd='downloadGRIDjob '+output_dir+'/'+sample
    else:
      if (datamcCampaign=="mc16_13TeV"):
        cmd='runH2ZyAnalysis H2Zy/H2ZyAnalysisMC16_Rel21.cfg GridDS: '+sample+' OutputDS: '+outputSample+' NumEvents: -1 OutputDir: '+output_dir+'/'+sample
      else:
        cmd='runH2ZyAnalysis H2Zy/H2ZyAnalysisDATA_Rel21.cfg GridDS: '+sample+' OutputDS: '+outputSample+' NumEvents: -1 OutputDir: '+output_dir+'/'+sample
    if (test_only):
      print cmd
    else:
      os.system(cmd)
