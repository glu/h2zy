setupATLAS
#git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
#git clone --recursive -b release/v1.8 ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
git clone --recursive -b HZy_190821 ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
#cd HGamCore 
#git checkout tags/v1.8.35-h024
#cd -
#asetup AnalysisBase,21.2.56,here
asetup AnalysisBase,21.2.72,here
cd $TestArea/../build
cmake $TestArea
cmake --build $TestArea/../build
source $TestArea/../build/$CMTCONFIG/setup.sh
