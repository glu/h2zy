#
# getDSInfoFromAMI5.py: obtain information from AMI about sample xsection, filter efficiency and #events
#
from time import asctime
import pyAMI.client
import sys

def datasetInfo ( *dataset ):
    argument=[]
    argument.append("GetDatasetInfo")
    mydataset = "-logicalDatasetName="+str(dataset[0]).strip('[]')
    argument.append(mydataset)
    
    #print "# sample     EVTS       xsec(AMI) [nb]    filt eff [%]   ECM  PDF  eq. lumi [nb-1]"
    try:
      amiclient = pyAMI.client.Client('atlas')
    
      result = amiclient.execute(argument,format='dict_object')
     
      arrayOfDico = result.get_rows()  
      #print arrayOfDico

      for dict in arrayOfDico:
          thekeys = dict.keys()
          #print "\n thekeys\n", thekeys
          #if 'nFiles' in thekeys : print 'nfiles ', dict[u'nFiles']
          #if 'totalEvents' in thekeys : print 'totalEvents ', dict[u'totalEvents']
          nevts = int(dict[u'totalEvents'])
	  DSID = int(dict[u'datasetNumber'])
          xsection = 0.
	  if 'crossSection' in thekeys : xsection = float(dict[u'crossSection'])
          if (xsection==0.) and ('crossSection_mean' in thekeys) : xsection = float(dict[u'crossSection_mean'])
          filteff = 0.;
          if 'approx_GenFiltEff' in thekeys : filteff = float(dict[u'approx_GenFiltEff'])
          if (filteff==0.) and ('GenFiltEff_mean' in thekeys) : filteff = float(dict[u'GenFiltEff_mean'])
	  #xsection*= 1.541E-3*0.101
          if (xsection!=0.) :
              #print dict[u'logicalDatasetName'],"*", nevts, "*", xsection,"*", filteff,"*", nevts/(xsection*filteff),"*", dict[u'ECMEnergy'],"*", dict[u'PDF'],"*", dict[u'generatorTune']
	      print dict[u'logicalDatasetName'],"*", DSID, "*", nevts, "*", xsection,"*", filteff,"*", nevts/(xsection*filteff)
          else :
              #print dict[u'logicalDatasetName'],"*", nevts, "*", xsection,"*", filteff,"*", "NAN","*", dict[u'ECMEnergy'],"*", dict[u'PDF'],"*", dict[u'generatorTune']
	      print dict[u'logicalDatasetName'],"*", DSID, "*", nevts, "*", xsection,"*", filteff,"*", "NAN"

    except Exception, msg:
       print msg


if __name__ == '__main__':

    datasetInfo(sys.argv[1:])
