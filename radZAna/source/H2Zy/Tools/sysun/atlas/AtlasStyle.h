//
//   @file    AtlasStyle.h         
//   
//            ATLAS Style, based on a style file from BaBar
//
//
//   @author M.Sutton
// 
//   Copyright (C) 2010 Atlas Collaboration
//
//   $Id: AtlasStyle.h,v 1.1 2010/12/01 17:52:45 marchiori Exp $

#ifndef  __ATLASSTYLE_H
#define __ATLASSTYLE_H

#include "TStyle.h"

void SetAtlasStyle();

TStyle* AtlasStyle(); 

#endif // __ATLASSTYLE_H
