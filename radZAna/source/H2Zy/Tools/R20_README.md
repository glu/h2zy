# The H2Zy package

Higgs or X decaying into Zgamma analysis package, working with [HGamCore](https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/).

General note: to run the code you need a valid grid account. Bash shell should be used.

## First time setup

checkout the H2Zy package (for development: master. for production: tagged version)

check what is the latest H2Zy tag at https://gitlab.cern.ch/atlas-hgam-sw/h-Zy/H2Zy/

### Example of checking out and using the master brach (Cmake, Release 21):

```
mkdir MyAnalysis
cd MyAnalysis
mkdir source run build
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/h-Zy/H2Zy.git
source H2Zy/scripts/setupRel21.sh
```
If necessary, after `git clone`, command `git checkout tags/H2Zy-TagName` will check out a tag version

if you have logged out, then you need to setup the environment again:

```
source H2Zy/scripts/continueR21.sh
```

For compiling the package: 

```
cd build
cmake ../source
cmake --build .
```

For processing DATA/MC events: locally, we need the file lists like `infile_Rel21.txt` mentioned below, which contains exact DxAOD or xAOD files.
e.g. `PATH/DAOD_HIGG1D2.12598956._000001.pool.root.1`

```
cd $TestArea/../run
cp -r ../source/H2Zy/Run .
cd Run
source runMC16.sh infile_Rel21.txt
```

### Running grid job

  solution1: see this [GridJob script](https://gitlab.cern.ch/atlas-hgam-sw/h-Zy/H2Zy/blob/master/Run/rungrid.sh)

  solution2: (slow with many datasets)

  ```
  runH2ZyAnalysis H2Zy/H2ZyAnalysisDATA_Rel21.cfg GridDS: DATASETNAME  OutputDS: user.username.OUTPUT NumEvents: 50
  ```

### Output information

- histo_cutflow, cutflow_xxxxxx     :  the cut flow (number of unweighted events after each selection step)
- histo_cutflow_wt, cutflow_xxxxxx_w  :  the weighted cut flow (number of weighted events after each selection step)
- HZG_Tree : output tree for the final selected kinematic variables (see below for a description of its content)
- truthTree: a tree with the true momenta of the Higgs, Z, photon and leptons (only filled if EventHandler.FillTruthTree is true)

### For developers

- 1.Making new Branch

```
git checkout -b my-feature origin/master
```

- 2.Making changes.

```
git commit -am "new feature"
git push origin my-feature
```

- 3.Submitting "Merge Requests" in GitLab.

### If you are still working on Rel20.7 (RootCore): 

Since tag H2Zy-00-00-72, The H2Zy package stopped working with Release 20,7 and SVN.

The latest tag on SVN is H2Zy-00-00-71 (rcSetup Base,2.4.31, working with [HGamAnalysisFramework-00-03-00](https://gitlab.cern.ch/atlas-hgam-sw/HGamAnalysisFramework/tree/HGamAnalysisFramework-00-03-00)), taking it as an example:

```
mkdir MyAnalysis
cd MyAnalysis
svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/HiggsPhys/Run2/HGamma/xAOD/H2Zy/tags/H2Zy-00-00-71 H2Zy
source H2Zy/scripts/startMC15.sh
source H2Zy/scripts/bootstrapMC15.sh
```

if you have logged out, then you need to setup the environment again:

```
source H2Zy/scripts/startMC15.sh
```

Compiling:

```
rc find_packages
rc compile
```

Processing events:

```
cd H2Zy/Run/
source runMC15.sh infile_Rel20.txt
```

### Correction implementation  

- Zmass constraint: (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsZZllllPreparationRunII2014#Z_mass_constraint)
- FSR Correction: (https://twiki.cern.ch/twiki/bin/view/Atlas/FSRToolForxAOD) 

---

### Running on the grid on multiple datasets, listed in a txt file

The H2Zy/data directory contains list of DAODs which we should run on.
To submit all jobs in one shot (or check, or retrieve the output) one could do the following.
From the Run directory, type `python ../H2Zy/scripts/manageGridJobs.py <action>`
where action is either submit, check or download
Before doing this, change in  ../H2Zy/scripts/manageGridJobs.py the following flags:

- test_only: set it to False (it's True by default to avoid submitting jobs by mistake)
- output_dir: directory that will contain all job output
- user: your GRID user name
- suffix: a suffix that will be appended to the output grid dataset
- fname: set it to the name of the txt file containing the list of samples


### Signal model parametrisation

A common signal parameterization tool has been developed for the HGamma group, in HGamTools
See: https://indico.cern.ch/event/435660/contribution/2/attachments/1133076/1620259/HyySigParam_AHard.pdf

It can be used for parameterized fit as a function of the resonance mass or individual fits to individual mass points.
Both the signal yield and the shape can be parameterized.

SigParam provides an interface for both creating a new signal model and accessing a saved model. 
The output is a RooWorkspace object that can be handled by macros or loaded by other instances of SigParam.
It includes accessor methods for adding signal models to the user's independent workspaces
An example configuration file is available in HGamCore/HGamTools/data/signalParam/signalParamExample.cfg

### OUTPUT BRANCHES (NTUPLE FORMAT)
  
  Name | Meaning
  --- | ---
  mc_weight_xs                            | cross section weight (the xs in fb)
  mc_weight_final                         | final event weight (mc weight, vertex weight, pileup weight, scale factors)
  mc_weight_scalefactor                   | the product of all scale factors (including the trigger)
  mc_weight_pileup                        | pileup weight
  mc_weight_vertex                        | vertex weight  
  mc_weight_mc                        | mc weight
  mc_type                                 | 0=ggF, 1=VBF, 2=ttH, 3=WH, 4=ZH, 11=Z(ee)gamma Sherpa, 12=Z(mumu)gamma Sherpa, 13=Z(ee) Sherpa, 14=Z(mumu)Sherpa (see HZgammaHelper.cxx)
  m_hasPromptphoton                       | true if there is a true photon (pdgId==22), status=22, barcode < 200000 (this should be revisited because this number is different in different MC  samples), not from hadrons
  mc_ChannelNumber                        | mc channel number
  EventInfo.runNumber                     | run number
  EventInfo.eventNumber                   | event number
  EventInfo.averageIntPerXing             | average interactions per bunch crossing
  EventInfo.actualIntPerCrossing          | actual interactions per bunch crossing
  EventInfo.channel                       | 1 = eegamma, 2 = mumugamma
  EventInfo.cutflow                       | which cuts are passed by this event
  lX_charge                               | lepton charge (X=1,2)
  lX_pt, lX_eta, lX_phi                   | lepton reconstructed momentum
  lX_pt_Truth, lX_eta_Truth, lX_phi_Truth | lepton generated momentum
  lX_pt_Zmassconstraint, lX_eta_Zmassconst|aint, lX_phi_Zmassconstraint : lepton reconstructed momentum after Z mass constraint
  lX_passiso_FixedCutLoose                | lepton passes fixed cut loose isolation WP
  lX_passiso_FixedCutTightTrackOnly       | lepton passes fixed cut tight track only isolation WP
  lX_passiso_Gradient                     | lepton passes gradient isolation WP
  lX_passiso_GradientLoose                | lepton passes gradient loose isolation WP
  lX_passiso_Loose                        | lepton passes loose isolation WP
  ph_pt, ph_eta, ph_phi                   | photon 4-momentum (which pt, eta are used?)
  ph_ptcone20, ph_ptcone30, ph_ptcone40   | track isolation in fixed R cones
  ph_istight                              | photon passes tight isEM ID
  ph_isEM_tight                           | photon isEM word
  ph_istruth                              |
  ph_passiso_FixedCutLoose                | photon passes fixed cut loose isolation WP
  ph_passiso_FixedCutTight                | photon passes fixed cut tight isolation WP
  ph_passiso_FixedCutTightCaloOnly        | photon passes fixed cut tight calo only isolation WP
  ll_pt, ll_eta, ll_phi                   | dilepton reconstructed momentum
  ll_pt_Zmassconstraint, ll_eta_Zmassconstraint, ll_phi_Zmassconstraint | dilepton reconstructed momentum after Z mass constraint
  ll_m                                    | dilepton reconstructed mass
  ll_m_Zmassconstraint                    | dilepton reconstructed mass after Z mass constraint
  ll_hasFSR                               | true if there's a recovered collinear FSR photon
  llg_angles_costheta_ginH, costheta_linZ, phi_linZ     | angular variables discriminating H->Zy from continuum Zy (reco-level)
  llg_angles_costheta_ginH_Zmassconstraint, costheta_linZ_Zmassconstraint, phi_linZ_Zmassconstraint | same as above, reco-level, after Z mass constraint
  llg_deta_Zy                              | deltaEta between Z and gamma (reco)
  llg_deta_Zy_Zmassconstraint              | deltaEta between Z and gamma (truth)
  llg_m                                    | llg invariant mass (reco)
  llg_m_Zmassconstraint                    | llg_invariant mass (reco, after Z mass constraint)
  llg_pTt                                  | llg pTt (reco)
  llg_pTt_Zmassconstraint                  | llg_pTt (reco, after Z mass constraint)

See Twiki https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HGam_run2_Zgamma
