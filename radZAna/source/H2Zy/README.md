# The H2Zy package

Higgs or X decaying into Zgamma analysis package, working with [HGamCore](https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/).

## First time setup

checkout the H2Zy package (for development: master. for production: tagged version)

check what is the latest H2Zy tag at https://gitlab.cern.ch/atlas-hgam-sw/h-Zy/H2Zy/

### Example of checking out and using the master brach (Cmake, Release 21):

```
mkdir MyAnalysis
cd MyAnalysis
mkdir source run build
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/h-Zy/H2Zy.git
source H2Zy/scripts/setupRel21.sh
```
If necessary, after `git clone`, command `git checkout tags/H2Zy-TagName` will check out a tag version

if you have logged out, then you need to setup the environment again:

```
source H2Zy/scripts/continueR21.sh
```

For compiling the package: 

```
cd build
cmake ../source
cmake --build .
```

For processing DATA/MC events: locally, we need the file lists like `infile_Rel21.txt` mentioned below, which contains exact DxAOD or xAOD files.
e.g. `PATH/DAOD_HIGG1D2.12598956._000001.pool.root.1`

```
cd $TestArea/../run
cp -r ../source/H2Zy/Run .
cd Run
source runMC16.sh infile_Rel21.txt
```

### Running grid job

  solution1: see this [GridJob script](https://gitlab.cern.ch/atlas-hgam-sw/h-Zy/H2Zy/blob/master/Run/rungrid.sh)

  solution2: (slow with many datasets)

  ```
  runH2ZyAnalysis H2Zy/H2ZyAnalysisDATA_Rel21.cfg GridDS: DATASETNAME  OutputDS: user.username.OUTPUT NumEvents: 50
  ```

### Output information

- histo_cutflow, cutflow_xxxxxx     :  the cut flow (number of unweighted events after each selection step)
- histo_cutflow_wt, cutflow_xxxxxx_w  :  the weighted cut flow (number of weighted events after each selection step)
- HZG_Tree : output tree for the final selected kinematic variables (see below for a description of its content)
- truthTree: a tree with the true momenta of the Higgs, Z, photon and leptons (only filled if EventHandler.FillTruthTree is true)

### For developers

- 1.Making new Branch

```
git checkout -b my-feature origin/master
```

- 2.Making changes.

```
git commit -am "new feature"
git push origin my-feature
```

- 3.Submitting "Merge Requests" in GitLab.


### Special Options

- For fast-simulation: `IsAFII: YES`

- For Systematic Loop: `EventHandler.SystematicLoop: YES`

- For Theoritical Un. Branches: `H2ZyAnalysis.SaveALLMCWeights: YES`, `H2ZyAnalysis.SaveALLPDFWeights: YES` and `H2ZyAnalysis.AddHTXSVars: YES`

- Save all di-lepton events: `H2ZyAnalysis.TwoLeptonSelection: YES`

- For TruthTree: `EventHandler.FillTruthTree: YES` and `H2ZyAnalysis.TruthSample: YES`

- Add lepton flavor truth cuts: `H2ZyAnalysis.electrononly:YES` or `H2ZyAnalysis.muononly: YES` 

###  Meaning of Cutflow

  Cutflow | Meaning
  --- | ---
  1  | events/sumweight in AOD (book keeper)
  2  | events/sumweight in DAOD (book keeper)
  3  | events/sumweight in DAOD
  4  | events/sumweight in DAOD (no pileup weight)
  5  | pass GRL
  6  | pass Primary-Vertex
  7  | pass Event-Quality
  8  | pass Triggers
  9  | Initial cuts (no cut here)
  10 | pass 2-lepton selection
  11 | pass loose di-lepton mass cut
  12 | pass 2-lepton + 1 gamma selection
  13 | Pre-cuts (no cut here)
  14 | lepton opposite charge
  15 | Trigger Match
  16 | di-lepton mass final cut
  17 | photon pT low-mass cut
  18 | photon ID final cut
  19 | photon isolation final cut
  20 | high-mass cut
  21 | photon pT high-mass cut

###  Important branches in HZG_Tree
  
  Name | Meaning
  --- | ---
  mc_weight_xs                            | cross section weight (the xs in fb)
  mc_weight_final                         | final event weight (mc weight, vertex weight, pileup weight, scale factors)
  mc_weight_scalefactor                   | the product of all scale factors (including the trigger)
  mc_weight_pileup                        | pileup weight
  mc_weight_vertex                        | vertex weight  
  mc_weight_mc                        | mc weight
  mc_type                                 | 0=ggF, 1=VBF, 2=ttH, 3=WH, 4=ZH, 11=Z(ee)gamma Sherpa, 12=Z(mumu)gamma Sherpa, 13=Z(ee) Sherpa, 14=Z(mumu)Sherpa (see HZgammaHelper.cxx)
  m_hasPromptphoton                       | true if there is a true photon (pdgId==22), status=22, barcode < 200000 (this should be revisited because this number is different in different MC  samples), not from hadrons
  mc_channelNumber                        | mc channel number
  mc_Z_decay_topo                         | 1111 = eegamma , 1313 = mumugamma, 1515 = tautaugammma
  EventInfo.runNumber                     | run number
  EventInfo.eventNumber                   | event number
  EventInfo.averageIntPerXing             | average interactions per bunch crossing
  EventInfo.actualIntPerCrossing          | actual interactions per bunch crossing
  EventInfo.channel                       | 1 = eegamma, 2 = mumugamma
  EventInfo.cutflow                       | which cuts are passed by this event
  lX_charge                               | lepton charge (X=1,2)
  lX_pt, lX_eta, lX_phi                   | lepton reconstructed momentum
  lX_pt_Truth, lX_eta_Truth, lX_phi_Truth | lepton generated momentum
  lX_pt_Zmassconstraint, lX_eta_Zmassconst|aint, lX_phi_Zmassconstraint : lepton reconstructed momentum after Z mass constraint
  lX_passiso_FixedCutLoose                | lepton passes fixed cut loose isolation WP
  ph_pt, ph_eta, ph_phi                   | photon 4-momentum (which pt, eta are used?)
  ph_ptcone20, ph_ptcone30, ph_ptcone40   | track isolation in fixed R cones
  ph_istight                              | photon passes tight isEM ID
  ph_isEM_tight                           | photon isEM word
  ph_passiso_FixedCutLoose                | photon passes fixed cut loose isolation WP
  ll_pt, ll_eta, ll_phi                   | dilepton reconstructed momentum
  ll_pt_Zmassconstraint, ll_eta_Zmassconstraint, ll_phi_Zmassconstraint | dilepton reconstructed momentum after Z mass constraint
  ll_m                                    | dilepton reconstructed mass
  ll_m_Zmassconstraint                    | dilepton reconstructed mass after Z mass constraint
  ll_hasFSR                               | true if there's a recovered collinear FSR photon
  llg_pt, llg_eta, llg_phi                | 3-body reconstructed momentum
  llg_pt_Zmassconstraint, llg_eta_Zmassconstraint, llg_phi_Zmassconstraint | 3-body reconstructed momentum after Z mass constraint
  llg_angles_costheta_ginH, costheta_linZ, phi_linZ     | angular variables discriminating H->Zy from continuum Zy (reco-level)
  llg_angles_costheta_ginH_Zmassconstraint, costheta_linZ_Zmassconstraint, phi_linZ_Zmassconstraint | same as above, reco-level, after Z mass constraint
  llg_deta_Zy_Zmassconstraint              | deltaEta between Z and gamma
  llg_dphi_Zy_Zmassconstraint              | deltaPhi between Z and gamma 
  llg_m                                    | llg invariant mass (reco)
  llg_m_Zmassconstraint                    | llg_invariant mass (reco, after Z mass constraint)
  llg_pTt                                  | llg pTt (reco)
  llg_pTt_Zmassconstraint                  | llg_pTt (reco, after Z mass constraint)
  VBF_BDTG                                 | BDT response of Signal (VBF) and Background (Other signal + background)
  VBF_XXXX                                 | Jet kinematics and inputs of VBF_BDTG
  MET_XXXX                                 | MET attributes and variables for Zy+MET search
  Btag_N_j                                 | Number of B-tag jets (MV2c10_FixedCutBEff_70)
  Central_N_j                              | Number of Central (fabs(eta)<2.5) jets
  VBF_N_j                                  | Number of nominal selected (pT>25GeV, fabs(eta)<4.4, pass JVT and cleaning) jets
  EventInfo.category_Run2_13TeV_Zmassconstraint_index | categorization number according to Root/category.cxx
  logMatrixElement_KDvalue                 | 2 [log (ME_ggH) - log(ME_bkg)]

### Important Links 

  - [Zmass constraint](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsZZllllPreparationRunII2014#Z_mass_constraint)
  - [FSR Correction](https://twiki.cern.ch/twiki/bin/view/Atlas/FSRToolForxAOD)
  - [Signal Parameterisation](https://indico.cern.ch/event/435660/contribution/2/attachments/1133076/1620259/HyySigParam_AHard.pdf)
  - [Derivaiton HIGG1D2, rel21.2](https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/DerivationFramework/DerivationFrameworkHiggs/share/HIGG1D2.py)
  - [Muon Cleaning](https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections/Root/MuonCalibrationAndSmearingTool.cxx#L2777)
  - [MET Jet selection](https://gitlab.cern.ch/atlas/athena/blob/21.2/Reconstruction/MET/METUtilities/Root/METMaker.cxx#L140)
  - [Zgamma twiki (old)](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HGam_run2_Zgamma)
