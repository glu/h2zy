#include "H2Zy/Variables.h"
#include "HGamAnalysisFramework/HgammaUtils.h"

#include "TSystem.h"

//! Constructor

namespace HZG {

  Variables::Variables()  {
    Map_int.clear();
    Map_uint.clear();
    Map_float.clear();
    Map_bool.clear();

    Map_TLVV.clear();
    Map_FV.clear();
  }


  void Variables::initialize( xAOD::PhotonContainer &photons, xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::JetContainer &jets)
  {

  }



}
