#include "H2Zy/Object_llg.h"
#include "HGamAnalysisFramework/HgammaUtils.h"

#include "TSystem.h"

namespace HZG {

  int Object_llg::getcategory(TString _catename, TString postfix){
    int _category  = -1;
    if(_catename=="Run1_8TeV"){
      //------------------------------
      //--  pTT>30  ee  --> 1
      //--  pTT<30 && |delta_eta|>2 ee --> 2
      //--  pTT<30 && |delta_eta|<2 ee -->3
      //--  pTT>30  mm  --> 4
      //--  pTT<30 && |delta_eta|>2 mm --> 5
      //--  pTT<30 && |delta_eta|<2 mm -->6
      //------------------------------

      if(Map_float["llg_pTt"+postfix]>30) _category=1; 
      else {
        if(Map_float["llg_deta_Zy"+postfix]>2) _category=2;
        else _category=3;
      }

      if(m_channel==2 && _category>=0) _category=_category+3;

    }

    else if(_catename=="Run2_13TeV"){
	    SetMVAVars(postfix);
	    double BDTOUT = GetMVAResponse();
	    if(BDTOUT>0.82 )  _category=1;
//	    if(BDTOUT>0.82 && Map_float["VBF_Dy_j_j"]>2 )  _category=1;
	    else if(Map_float["ph_pt"]/Map_float["llg_m"+postfix]>0.4) _category = 2;
	    else if(Map_float["llg_pTt"+postfix]>40) _category=3;
	    else _category = 4;

	    if(m_channel==2 && _category>=3) _category=_category+2;
    }

    return _category;
  }

}
