#include "H2Zy/Container_llg.h"
#include "HGamAnalysisFramework/HgammaUtils.h"

#include "TSystem.h"

//! Constructor

namespace HZG {

  Container_llg::Container_llg()  {
    m_container_llg.clear();
    i_object_llg=0;
  }

  void Container_llg::setConfig(const HG::Config &config, bool _isMC)
  {
	  m_config = config;
	  sort_type = m_config.getStr("LLGHandler.Object_llg_sort", "m_d_mll_mZ");
	  ZMassConstraint::ConstraintFit* massConstraint = new ZMassConstraint::ConstraintFit("ZMassConstraint");
	  m_egammaCalibrationTool = ToolHandle<CP::IEgammaCalibrationAndSmearingTool>("EgammaCalibrationAndSmearingTool");
	  if (!massConstraint->setProperty( "EgammaCalibAndSmearingTool", m_egammaCalibrationTool)) {
	          Error("setConfig()","initFirstEvent - cannot setProperty EgammaCalibAndSmearingTool");
		  //ATH_MSG_ERROR("initFirstEvent - cannot setProperty EgammaCalibAndSmearingTool ");
		  //return ealse;
	  }

	  m_muonCalibrationTool = ToolHandle<CP::IMuonCalibrationAndSmearingTool>("MuonCalibrationAndSmearingTool2016" );

	  if (!massConstraint->setProperty( "MuonCalibrationAndSmearingTool", m_muonCalibrationTool)) {
	          Error("setConfig()","initFirstEvent - cannot setProperty MuonCalibrationAndSmearingTool");
		  //ATH_MSG_ERROR("initFirstEvent - cannot setProperty EgammaCalibAndSmearingTool ");
		  //return false;
	  }
	  if (massConstraint->initialize()  != StatusCode::SUCCESS) {
		  cout<<"initFirstEvent - cannot initialize ConstraintFit"<<endl;
	  }

	  //massConstraint->setMuonResSFTool(mu_resolSF);
	  m_massConstraint = massConstraint;

	  m_fsrTool = new FSR::FsrPhotonTool("m_fsrTool");
 
  }

  void Container_llg::initialize(xAOD::PhotonContainer &photons, xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons,  xAOD::JetContainer &jets, xAOD::JetContainer &loose_jets, xAOD::MissingETContainer &met, xAOD::PhotonContainer &all_photons, xAOD::ElectronContainer &all_electrons)
  {
	  selected_photons = &photons;
	  selected_electrons = &electrons;
	  selected_muons = &muons;
	  selected_jets = &jets;
	  selected_loose_jets = &loose_jets;
	  selected_met = &met;

	  photon_container = &all_photons;
	  electron_container = &all_electrons;

	  m_container_llg.clear();
	  i_object_llg=0;
	  initialize(photons, electrons);
	  initialize(photons, muons);
	  sort_dMll();
  }

  double Container_llg::get_maxmll()
  {
	  double mll=-999;
	  TLorentzVector p_l1, p_l2;

	  if(selected_electrons->size()>=2){
		  for(int ilepton1=0; ilepton1<((int)selected_electrons->size()-1); ilepton1++)
		  {
			  for(int ilepton2=ilepton1+1; ilepton2< (int)selected_electrons->size(); ilepton2++)
			  {
				  if((*selected_electrons)[ilepton1]->charge() + (*selected_electrons)[ilepton2]->charge() == 0){
					  p_l1.SetPtEtaPhiM((*selected_electrons)[ilepton1]->pt(), (*selected_electrons)[ilepton1]->eta(), (*selected_electrons)[ilepton1]->phi(), (*selected_electrons)[ilepton1]->m());
					  p_l2.SetPtEtaPhiM((*selected_electrons)[ilepton2]->pt(), (*selected_electrons)[ilepton2]->eta(), (*selected_electrons)[ilepton2]->phi(), (*selected_electrons)[ilepton2]->m());
					  if(mll<(p_l1+p_l2).M()/1000.) mll = (p_l1+p_l2).M()/1000.;
				  }
			  }
		  }

	  }

	  if((*selected_muons).size()>=2){
		  for(int ilepton1=0; ilepton1<((int)selected_muons->size()-1); ilepton1++)
		  {
			  for(int ilepton2=ilepton1+1; ilepton2< (int)selected_muons->size(); ilepton2++)
			  {
				  if((*selected_muons)[ilepton1]->charge() + (*selected_muons)[ilepton2]->charge() == 0){
					  p_l1.SetPtEtaPhiM((*selected_muons)[ilepton1]->pt(), (*selected_muons)[ilepton1]->eta(), (*selected_muons)[ilepton1]->phi(), (*selected_muons)[ilepton1]->m());
					  p_l2.SetPtEtaPhiM((*selected_muons)[ilepton2]->pt(), (*selected_muons)[ilepton2]->eta(), (*selected_muons)[ilepton2]->phi(), (*selected_muons)[ilepton2]->m());
					  if(mll<(p_l1+p_l2).M()/1000.) mll = (p_l1+p_l2).M()/1000.;
				  }
			  }
		  }
	  }

	  return mll;
  }


  bool Container_llg::passcut(Object_llg &_object_llg) {

    bool ispass= true;
    ispass *= ( (_object_llg.charge_lepton1 * _object_llg.charge_lepton2) == -1);
    
    // comment and turn to EventHandler.TriggerThresholds   
    //if(_object_llg.m_channel == 2){
    //  ispass *= (_object_llg.Map_float["l1_pt"]>24); 
    //}

    return ispass;

  }

//  bool Container_llg::operator < (const myclass &m)const {
//    return first < m.first;
//  }

  bool Container_llg::compare_dMll(const Object_llg &object1, const Object_llg &object2){
    return (object1.m_d_mll_mZ < object2.m_d_mll_mZ);
    //return ((object1.Map_float)[sort_type] < (object2.Map_float)[sort_type]);
  }

bool Container_llg::compare_dMll_Zmassconstraint(const Object_llg &object1, const Object_llg &object2){
    return (object1.m_d_mll_mZ_Zmassconstraint < object2.m_d_mll_mZ_Zmassconstraint);
    //return ((object1.Map_float)[sort_type] < (object2.Map_float)[sort_type]);
  }


  void Container_llg::sort_dMll(){
	  if (sort_type=="m_d_mll_mZ") {
		  std::sort(m_container_llg.begin(), m_container_llg.end(), compare_dMll);
	  } 
	  else  if (sort_type=="m_d_mll_mZ_Zmassconstraint") {
		  std::sort(m_container_llg.begin(), m_container_llg.end(), compare_dMll_Zmassconstraint);
	  } 
	  else {
		  HG::fatal("Unknown sort_type :"+sort_type+" Exiting..");
	  }
  }

}
