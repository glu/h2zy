#include "H2Zy/EventFill.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/PDF.h"
#include "LHAPDF/PDFSet.h"
#include "LHAPDF/Reweighting.h"

namespace HZG {

	EventFill::EventFill() {
		//cout<<" setup EventFill"<<endl;
	}
	//---------------- initialize ----------------------------------
	void EventFill::initialize(const HG::Config &config,  const xAOD::EventInfo *_eventinfo, xAOD::TEvent *_event,  Bmap *_beventmap, Imap *_ieventmap, UImap *_uieventmap, ULLmap *_ulleventmap, Fmap *_feventmap, IVmap *_iveventmap, FVmap *_fveventmap,  TLVVmap *_tlvveventmap){
		m_config = config;
		m_eventinfo = _eventinfo;
		m_event = _event;
		m_beventmap = _beventmap;
		m_ieventmap = _ieventmap;
		m_uieventmap = _uieventmap;
		m_ulleventmap = _ulleventmap;
		m_feventmap = _feventmap;
		m_iveventmap = _iveventmap;
		m_fveventmap = _fveventmap;
		m_tlvveventmap = _tlvveventmap;
		
		
		all_correctedphotons.clear();
		all_correctedelectrons.clear();
		all_correctedmuons.clear();
		photons.clear(); //!
		electrons.clear(); //!
		muons.clear(); //!
		all_met.clear();
		met.clear();
		all_truth_met.clear(); //!
		truth_met.clear(); //!
	}

        //-----------------------------
	
	void EventFill::SaveMCWeights(vector<LHAPDF::PDF*> m_pdfs, vector<LHAPDF::PDF*> m_pdfs_mstw, vector<LHAPDF::PDF*> m_pdfs_mmht) 
	 {
	  if(!(m_config.getStr("H2ZyAnalysis.SaveALLMCWeights","NO")=="NO")){
	    const std::vector<float> mc_weights = m_eventinfo->mcEventWeights();
	    (*m_fveventmap)["mc_weight_ALL"] = mc_weights;
	  }

	  static TString s_truthEvtName = m_config.getStr("TruthEvents.ContainerName","TruthEvents");
	  const xAOD::TruthEventContainer *truthEvts = 0;
	  m_event->retrieve(truthEvts,s_truthEvtName.Data()) ;

	  std::vector<float> weights;
	  if ( truthEvts->size() && m_pdfs.size()>0 ) {
	    // this won't work if processing t-tbar MC 
	    float x1 = truthEvts->at(0)->pdfInfo().x1;
	    float x2 = truthEvts->at(0)->pdfInfo().x2;
	    //float xf1 = truthEvts->at(0)->pdfInfo().xf1;
	    //float xf2 = truthEvts->at(0)->pdfInfo().xf2;
	    float Q = truthEvts->at(0)->pdfInfo().Q;
	    int pdgId1 = truthEvts->at(0)->pdfInfo().pdgId1;
	    int pdgId2 = truthEvts->at(0)->pdfInfo().pdgId2;

	    for (size_t imem = 0; imem <= m_pdfs.size()-1; imem++) {
	      float w = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs[0], m_pdfs[imem] ) ;
	      weights.push_back( w ); // one event weight for each error in the set including the nominal
	    }
	    float w_mstw = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs[0], m_pdfs_mstw[0] ) ;
	    float w_mmht = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs[0], m_pdfs_mmht[0] ) ;
	    //float w_nn = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs[0], m_pdfs_nn[0] ) ;
	    (*m_feventmap)["mc_weight_PDF_mstw"] = w_mstw;
	    (*m_feventmap)["mc_weight_PDF_mmht"] = w_mmht;
	    //m_feventmap["mc_weight_PDF_nn"] = w_nn;
	  }
	  if(!(m_config.getStr("H2ZyAnalysis.SaveALLPDFWeights","NO")=="NO")){
	    (*m_fveventmap)["mc_weight_PDF"] = weights;
	  }
	 }

	void EventFill::seteventInfo(HG::EventHandler *_eventHandler)
	 {

	  if(HG::isMC()){
	    m_pileupweight= _eventHandler->pileupWeight();
	    m_vertexweight= _eventHandler->vertexWeight();
	    m_mcweight = _eventHandler->mcWeight();
	    m_initialWeight= m_mcweight * m_vertexweight * m_pileupweight;
	  }

	  (*m_feventmap)["mc_weight_initial"]=m_initialWeight;
	  (*m_feventmap)["mc_weight_pileup"]=m_pileupweight;
	  (*m_feventmap)["mc_weight"]= m_mcweight;
	  (*m_feventmap)["mc_weight_vertex"]=m_vertexweight;
	  (*m_feventmap)["mc_weight_final_NOPU"]=m_mcweight * m_vertexweight; 
	  (*m_feventmap)["mc_weight_final_NOPU_NOVX"]=m_mcweight;
	  (*m_feventmap)["mc_weight_final"]= m_initialWeight;
	  (*m_ieventmap)["EventInfo.isMC"]=HG::isMC();

	  (*m_uieventmap)["EventInfo.runNumber"] = m_eventinfo->runNumber();
	  (*m_ulleventmap)["EventInfo.eventNumber"] = m_eventinfo->eventNumber();
	  (*m_uieventmap)["EventInfo.lumiBlock"] = m_eventinfo->lumiBlock();
	  (*m_feventmap)["EventInfo.averageIntPerXing"] = m_eventinfo->averageInteractionsPerCrossing();
	  (*m_feventmap)["EventInfo.actualIntPerXing"] = m_eventinfo->actualInteractionsPerCrossing();

	  unsigned int m_mc_channel_number;  
	  if(m_eventinfo->eventType(xAOD::EventInfo::IS_SIMULATION))
	    m_mc_channel_number = m_eventinfo->mcChannelNumber();

	  (*m_uieventmap)["mc_channelNumber"] = m_mc_channel_number;

	  m_mu = _eventHandler->mu();
	  (*m_feventmap)["EventInfo.mu"] = m_mu;

	  //----------------  truth vertex info ----
	  //--- truth vertex (already saved in other way in truthsave_vertex() in TruthSelect.cxx
	  //--- this is the recommended way for the truth vertex extraction, but still need further check 

	   {
	    // not working for Sherpa 1
	    /*
	    float truthVertexZ = -999;
	    if(m_eventinfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
	      static TString s_truthEvtName = m_config.getStr("TruthEvents.ContainerName","TruthEvents");
	      const xAOD::TruthEventContainer *truthEvts = 0;
	      m_event->retrieve(truthEvts,s_truthEvtName.Data()) ;
	      if ( truthEvts->size() ) {
		if ( truthEvts->at(0)->signalProcessVertex() )
		  truthVertexZ = truthEvts->at(0)->signalProcessVertex()->z();
	      } 
	    }
	    (*m_feventmap)["mc_PVz_check"] = truthVertexZ;
	    */


	    //-------- for theory uncertainty variation --------------------------
	    if(!(m_config.getStr("H2ZyAnalysis.AddHTXSVars","NO")=="NO")){
	      (*m_feventmap)["EventInfo.HTXS_Higgs_pt"] = m_eventinfo->auxdata<float>("HTXS_Higgs_pt");
	      (*m_uieventmap)["EventInfo.HTXS_Njets_pTjet30"] = m_eventinfo->auxdata<int>("HTXS_Njets_pTjet30");
	      (*m_uieventmap)["EventInfo.HTXS_Njets_pTjet25"] = m_eventinfo->auxdata<int>("HTXS_Njets_pTjet25");
	    }
	   }
	 }

	int EventFill :: retrievePV()
	{
		//---- check the Primary Vertice information ----
		float PVz=0;
		float PVx=0;
		float PVy=0;
		int _pass_pv=0;
		const xAOD::VertexContainer* pVertices = 0;
		if( !m_event->retrieve(pVertices,"PrimaryVertices") ){
			//fatal("Failed to retrieve PrimaryVertices. Exiting.");
		}

		int m_nPV2 = 0;
		_pass_pv=0;
		for (auto _vertex: *pVertices)
		{
			int ntrkv = _vertex->nTrackParticles();
			if( ntrkv > 1 ) ++m_nPV2;
		}
		_pass_pv = (m_nPV2>0);
		//------  check the primary Vertice information ---
		const xAOD::Vertex* pVertex = 0;
		if( pVertices->size() > 0 ){
			pVertex = (*pVertices)[0];
			PVz = pVertex->z();
			PVx = pVertex->x();
			PVy = pVertex->y();
		}
		(*m_feventmap)["EventInfo.PVz"]=PVz;
		(*m_feventmap)["EventInfo.PVx"]=PVx;
		(*m_feventmap)["EventInfo.PVy"]=PVy;
		return _pass_pv;

	}
	
	void EventFill :: retrieveVtxSumPt(xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons)
	 {
	  //---- check the Primary Vertice information ----
	  const xAOD::VertexContainer* pVertices = 0;
	  if( !m_event->retrieve(pVertices,"PrimaryVertices") ){
	    //fatal("Failed to retrieve PrimaryVertices. Exiting.");
	  }

	  xAOD::VertexContainer *vertices_nc = new xAOD::VertexContainer;
	  xAOD::VertexAuxContainer * store = new xAOD::VertexAuxContainer;
	  vertices_nc->setStore(store);
	  //const xAOD::Vertex * electronVertex = removeElectronsFromVertex(vertices_nc,pVertices,electrons,muons);

	  double Lsumpt2 = 1.0, SLsumpt2 = 1.0;
	  if(vertices_nc->size()>=2) {
	    Lsumpt2 = xAOD::PVHelpers::getVertexSumPt((*vertices_nc)[0], 2);
	    SLsumpt2 = xAOD::PVHelpers::getVertexSumPt((*vertices_nc)[1], 2);
	  }
	  (*m_feventmap)["Vertex_leading_sumpt2"]=Lsumpt2;
	  (*m_feventmap)["Vertex_subleading_sumpt2"]=SLsumpt2;

	  vertices_nc->clear();
	  vertices_nc->shrink_to_fit();
	  delete store;
	  delete vertices_nc;
	 }

	const xAOD::Vertex * EventFill::removeElectronsFromVertex(xAOD::VertexContainer * output, const xAOD::VertexContainer * input, xAOD::ElectronContainer & toRemove, xAOD::MuonContainer &toRemoveMuon){

	  //Will return the index in the input container to the first vertex where electron TrackParticles are found - will take the vertex which the highest pT TrackParticle is associated to if there is more  than one  //and -1 if it doesn't find such a vertex
	  const xAOD::Vertex * zeeVertex = nullptr;
	  int nModifiedVertices = 0;

	  std::vector<const xAOD::TrackParticle *> electronsInDetTrackParticles;

	  //get TrackParticles for electrons
	  for (const xAOD::Electron* e : toRemove) {
	    std::vector<const xAOD::TrackParticle *> singleElectronInDetTrackParticles =  xAOD::EgammaHelpers::getTrackParticlesVec(e);
	    //track particles from both electrons in single vector, since we want to remove them all anyway
	    electronsInDetTrackParticles.insert(electronsInDetTrackParticles.begin(), singleElectronInDetTrackParticles.begin(),singleElectronInDetTrackParticles.end());
	  }
	  for (const xAOD::Muon *m: toRemoveMuon) {
	    const xAOD::TrackParticle* idtp=0;
	    ElementLink<xAOD::TrackParticleContainer> idtpLink = m->inDetTrackParticleLink();
	    if(idtpLink.isValid())
	      idtp = *idtpLink;
	    electronsInDetTrackParticles.push_back(idtp);
	    //      cout<<" debug  this is a muon !!! "<<endl;
	  }

	  //std::sort (electronsInDetTrackParticles.begin(), electronsInDetTrackParticles.end(),sortPtAscending());
	  float highestpT = 0.0;
	  int element_counter=0;
	  //loop over input vertices
	  for (const xAOD::Vertex* v : *input) {
	    double sumpt=0;
	    double sumpt2=0;
	    bool containsElectrons = false;
	    xAOD::Vertex * nv = new xAOD::Vertex;
	    //set output aux info to be same as input (for time being)
	    nv->makePrivateStore(v);

      //cout<<"here"<<endl;
      //getchar();
	    std::vector< ElementLink< xAOD::TrackParticleContainer > >::iterator InDetTrackParticleLinksIter;
	    std::vector< ElementLink< xAOD::TrackParticleContainer > > InDetTrackParticleLinksToKeep =  v->trackParticleLinks(); //for modifying
	    //loop over TrackPArticles associated to vertex
	    for(InDetTrackParticleLinksIter = InDetTrackParticleLinksToKeep.begin();InDetTrackParticleLinksIter != InDetTrackParticleLinksToKeep.end();){
      //cout<<"here3"<<endl;
      //getchar();
	      const xAOD::TrackParticle * TPtoTest = (*(InDetTrackParticleLinksIter->cptr()));
	      //checking if any of the Vertex TrackParticles are the same as the electron TrackParticles
	      if(find(electronsInDetTrackParticles.begin(),electronsInDetTrackParticles.end(),TPtoTest)!=electronsInDetTrackParticles.end()){
		//remove the common ones
		InDetTrackParticleLinksIter = InDetTrackParticleLinksToKeep.erase(InDetTrackParticleLinksIter);//iterator should be moved to next valid one after removal
		containsElectrons = true;
		//cout<<" debug000 "<<TPtoTest->pt()/1000<<" "<<TPtoTest->pt()/1000*TPtoTest->pt()/1000<<endl;
		sumpt+=TPtoTest->pt()/1000;
		sumpt2+=TPtoTest->pt()/1000*TPtoTest->pt()/1000;
		if(TPtoTest->pt() > highestpT){
		  highestpT = TPtoTest->pt();
		  zeeVertex = v;
		}
	      }
	      else ++InDetTrackParticleLinksIter; //move iterator forward 'by hand' if we don't remove link
	    }
	    if(containsElectrons){
	      //if the vertex contains an electron TP, clear its tracks, and replace with the element links from above (excluding electron)
	      nModifiedVertices++;
	      nv->clearTracks();
	      nv->setTrackParticleLinks(InDetTrackParticleLinksToKeep);
	    }
      //cout<<"here1"<<endl;
      //getchar();
	    if(element_counter==0)nv->setVertexType( xAOD::VxType::PriVtx );
      //cout<<"here2"<<endl;
      //getchar();

	    // Decorate vertex with element link to the original one,
	    // links to the electrons and flag that tells if it used them
	    //typedef ElementLink<xAOD::VertexContainer> vxlink_t;
	    //nv->auxdecor<vxlink_t>("originalVertexLink") = vxlink_t(m_primaryVertexSGKey, v->index());
	    //renew sumpt and sumpt2
	    nv->auxdecor<float>("sumPt")= xAOD::PVHelpers::getVertexSumPt(nv, 1,false);
	    nv->auxdecor<float>("sumPt2")= xAOD::PVHelpers::getVertexSumPt(nv, 2,false);
	    //maxsumpt=sumpt;
	    //maxsumpt2=sumpt2;
	    //cout<<" debug 111 "<<nv->auxdecor<float>("sumPt2")<<endl;

	    output->push_back(nv);
	    element_counter++;
	  }
	  //zeeVertex->auxdecor<float>("sumPt")= xAOD::PVHelpers::getVertexSumPt(zeeVertex, 1,true)-maxsumpt;
	  //zeeVertex->auxdecor<float>("sumPt2")= xAOD::PVHelpers::getVertexSumPt(zeeVertex, 2,true)-maxsumpt2;
	  return zeeVertex;
	}


	void EventFill::RecSave(HG::EventHandler *_eventHandler, Object_llg *&_llg, HG::PhotonHandler *_photonHandler, HG::ElectronHandler *_electronHandler, HG::MuonHandler *_muonHandler, xAOD::PhotonContainer &_photons,  xAOD::ElectronContainer &_electrons, xAOD::MuonContainer &_muons)
	{

		double m_photonscalefactor=1.0;
		double m_leptonscalefactor=1.0;
		double m_scalefactor = 1.0;

		static SG::AuxElement::Accessor<float> scaleFactor("scaleFactor");
		//----- photon
		int ph_index = _llg->Map_int["index_photon"];
		if (m_config.getBool("H2ZyAnalysis.TwoLeptonSelection",false)) ph_index = -1 ; 
		if(ph_index==-1)
		{
			// this should never happen...
			(*m_beventmap)["ph_istight"]=0;
			(*m_uieventmap)["ph_isEM_tight"]= 0;
			(*m_beventmap)["ph_passiso_FixedCutTightCaloOnly"] = 0;
			(*m_beventmap)["ph_passiso_FixedCutTight"]         = 0;
			(*m_beventmap)["ph_passiso_FixedCutLoose"]         = 0;
		} 
		else 
		{
			(*m_beventmap)["ph_istight"]=_photonHandler->passPIDCut( _photons[ph_index], egammaPID::PhotonIDTight);
			(*m_uieventmap)["ph_isEM_tight"]= _photons[ph_index]->auxdata<unsigned int>("isEMTight");
			(*m_beventmap)["ph_passiso_FixedCutTightCaloOnly"]=_photonHandler->passIsoCut( _photons[ph_index], HG::Iso::FixedCutTightCaloOnly);
			(*m_beventmap)["ph_passiso_FixedCutTight"]        =_photonHandler->passIsoCut( _photons[ph_index], HG::Iso::FixedCutTight);
			(*m_beventmap)["ph_passiso_FixedCutLoose"]        =_photonHandler->passIsoCut( _photons[ph_index], HG::Iso::FixedCutLoose);
			m_photonscalefactor *= scaleFactor(*_photons[ph_index]);
		}
		//----- lepton  //index_lepton1  index_lepton2
		for(int i=1; i<=2; i++)
		{
			int index = (i==1)? _llg->index_lepton1 : _llg->index_lepton2;
			if(_llg->m_channel == 1)
			{
				(*m_beventmap)[Form("l%d_passid_Medium", i)]         = _electronHandler->passPIDCut( _electrons[index], "Medium"); 
				(*m_beventmap)[Form("l%d_passiso_LooseTrackOnly", i)]= _electronHandler->passIsoCut( _electrons[index], HG::Iso::LooseTrackOnly); 
				(*m_beventmap)[Form("l%d_passiso_Loose", i)]         = _electronHandler->passIsoCut( _electrons[index], HG::Iso::Loose); 
				(*m_beventmap)[Form("l%d_passiso_Gradient", i)]      = _electronHandler->passIsoCut( _electrons[index], HG::Iso::Gradient); 
				(*m_beventmap)[Form("l%d_passiso_GradientLoose", i)] = _electronHandler->passIsoCut( _electrons[index], HG::Iso::GradientLoose); 
				(*m_beventmap)[Form("l%d_passiso_FixedCutTight", i)] = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FixedCutTight); 
				(*m_beventmap)[Form("l%d_passiso_FixedCutTightTrackOnly", i)] = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FixedCutTightTrackOnly); 
				(*m_beventmap)[Form("l%d_passiso_FixedCutLoose", i)] = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FixedCutLoose); 
				(*m_beventmap)[Form("l%d_passiso_FCLoose", i)]       = false; // will use the following line after the FCLoose is avaliable for electrons in HGamCore tags
				//(*m_beventmap)[Form("l%d_passiso_FCLoose", i)]       = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FCLoose); 
				//(*m_beventmap)[Form("l%d_passiso_FCLoose_FixedRad,", i)]       = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FCLoose_FixedRad);  
				//(*m_beventmap)[Form("l%d_passiso_FCHighPtCaloOnly", i)] = _electronHandler->passIsoCut( _electrons[index], HG::Iso::FCHighPtCaloOnly); 
				//(*m_beventmap)[Form("l%d_passiso_FixedCutHighPtTrackOnly", i)] = false;
				m_leptonscalefactor *= scaleFactor(*_electrons[index]);
			} 
			else 
			{
				(*m_beventmap)[Form("l%d_passid_Medium", i)]         = 1; 
				(*m_beventmap)[Form("l%d_passiso_Loose", i)]         = _muonHandler->passIsoCut( _muons[index], HG::Iso::Loose); 
				(*m_beventmap)[Form("l%d_passiso_LooseTrackOnly", i)]= _muonHandler->passIsoCut( _muons[index], HG::Iso::LooseTrackOnly); 
				(*m_beventmap)[Form("l%d_passiso_Gradient", i)]      = _muonHandler->passIsoCut( _muons[index], HG::Iso::Gradient); 
				(*m_beventmap)[Form("l%d_passiso_GradientLoose", i)] = _muonHandler->passIsoCut( _muons[index], HG::Iso::GradientLoose); 
				(*m_beventmap)[Form("l%d_passiso_FixedCutTight", i)] = false; // no FixedCutTight for muons, but we want to keep the same tree content for both electrons and photons
				(*m_beventmap)[Form("l%d_passiso_FixedCutTightTrackOnly", i)] = _muonHandler->passIsoCut( _muons[index], HG::Iso::FixedCutTightTrackOnly); 
				(*m_beventmap)[Form("l%d_passiso_FixedCutLoose", i)] = _muonHandler->passIsoCut( _muons[index], HG::Iso::FixedCutLoose); 
				(*m_beventmap)[Form("l%d_passiso_FCLoose", i)]       = _muonHandler->passIsoCut( _muons[index], HG::Iso::FCLoose); 
				//(*m_beventmap)[Form("l%d_passiso_FCLoose_FixedRad,", i)] = _muonHandler->passIsoCut( _muons[index], HG::Iso::FCLoose_FixedRad); 
				//(*m_beventmap)[Form("l%d_passiso_FCHighPtCaloOnly", i)] = false; 
				//(*m_beventmap)[Form("l%d_passiso_FixedCutHighPtTrackOnly", i)] = _muonHandler->passIsoCut( _muons[index], HG::Iso::FixedCutHighPtTrackOnly);
				m_leptonscalefactor *= scaleFactor(*_muons[index]);
			}
		}

		m_scalefactor = m_photonscalefactor * m_leptonscalefactor;
		(*m_feventmap)["mc_weight_scalefactor"] = m_scalefactor;
		(*m_feventmap)["mc_weight_leptonscalefactor"] = m_leptonscalefactor;
		(*m_feventmap)["mc_weight_photonscalefactor"] = m_photonscalefactor;

                //--------- check the qulity of high pt muon ---------
		highmuoncheck(_muons);

		//trigger SF
		double m_triggerscalefactor = 1.0;
		if (m_config.getBool("H2ZyAnalysis.TriggerSF",true)) 
		  m_triggerscalefactor = _eventHandler->triggerScaleFactor(_llg->m_electron_viewcontainer, _llg->m_muon_viewcontainer);
		(*m_feventmap)["mc_weight_triggerscalefactor"] = m_triggerscalefactor;
		(*m_feventmap)["mc_weight_scalefactor"]  = (*m_feventmap)["mc_weight_scalefactor"]  *m_triggerscalefactor;
		(*m_feventmap)["mc_weight_final"] = (*m_feventmap)["mc_weight_initial"] * (*m_feventmap)["mc_weight_scalefactor"] ;
		(*m_feventmap)["mc_weight_final_NOPU"]= (*m_feventmap)["mc_weight"] * (*m_feventmap)["mc_weight_vertex"] * (*m_feventmap)["mc_weight_scalefactor"] ;
		(*m_feventmap)["mc_weight_final_NOPU_NOVX"]= (*m_feventmap)["mc_weight"] * (*m_feventmap)["mc_weight_scalefactor"];
	} 

	void EventFill::highmuoncheck(xAOD::MuonContainer &_muons)
	{
		if(_muons.size()>=2){
			xAOD::Muon *muontest0 = _muons[0];
			xAOD::Muon *muontest1 = _muons[1];
			(*m_beventmap)["l1_BadCSC"] = !isOkMuon(*muontest0);
			(*m_beventmap)["l2_BadCSC"] = !isOkMuon(*muontest1);
		}
		else{
			(*m_beventmap)["l1_BadCSC"] = 0;
			(*m_beventmap)["l2_BadCSC"] = 0;
		}
	}

	bool EventFill::isOkMuon(xAOD::Muon& muon)
	 {

	  if(muon.pt()  < 300e3)
	    return true;

	  const xAOD::TrackParticle* metrack = muon.trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
	  if( metrack ) {
	    double qOverP_ME = metrack->qOverP();
	    double qOverPerr_ME = sqrt( metrack->definingParametersCovMatrix()(4,4) );
	    if( fabs(qOverP_ME) * 0.5 >  qOverPerr_ME ) {
	      return true;
	    } else {
	      return false;
	    }
	  }

	  return false;

	 }

	// retrieve containers
	xAOD::PhotonContainer EventFill::all_photon_container (HG::PhotonHandler *_photonHandler)
	 {
	  all_correctedphotons = _photonHandler->getCorrectedContainer();

	  return all_correctedphotons;
	 }
	xAOD::ElectronContainer EventFill::all_electron_container (HG::ElectronHandler *_electronHandler)
	 {
	  all_correctedelectrons = _electronHandler->getCorrectedContainer();

	  return all_correctedelectrons;
	 }

	xAOD::PhotonContainer EventFill::photon_container (HG::PhotonHandler *_photonHandler)
	 {
	  all_correctedphotons = _photonHandler->getCorrectedContainer();
	  photons = _photonHandler->applySelection( all_correctedphotons );

	  return photons;
	 }
	xAOD::ElectronContainer EventFill::electron_container (HG::ElectronHandler *_electronHandler)
	 {
	  all_correctedelectrons = _electronHandler->getCorrectedContainer();
	  electrons = _electronHandler->applySelection(all_correctedelectrons);

	  return electrons;
	 }
	xAOD::MuonContainer EventFill::muon_container (HG::MuonHandler *_muonHandler)
	 {
	  all_correctedmuons = _muonHandler->getCorrectedContainer();
	  muons = _muonHandler->applySelection( all_correctedmuons );

	  return muons;
	 }
	xAOD::MuonContainer EventFill::muon_clean_container (HG::MuonHandler *_muonHandler, xAOD::MuonContainer muons0)
	 {
	  muons.clear();
	  muons = _muonHandler->applyCleaningSelection(muons0);

	  return muons;	  
	 }
	xAOD::JetContainer EventFill::loose_jet_container(HG::JetHandler *_jetHandler)
	 {
	  xAOD::JetContainer all_correctedjets = _jetHandler->getCorrectedContainer();
	  xAOD::JetContainer loose_jets     = _jetHandler->applySelection(all_correctedjets);

	  return loose_jets;
	 }
	
	xAOD::JetContainer EventFill::jet_container(xAOD::JetContainer _loose_jets)
	 {
	  xAOD::JetContainer jets(SG::VIEW_ELEMENTS);
	  float jet_final_pt = m_config.getNum("H2ZyAnalysis.JetPtFinalCutGeV",25.0);
	  for (auto jet : _loose_jets) {
	    if ( jet->pt()/1000 > jet_final_pt ) { jets.push_back(jet); }
	  }
	  return jets;
	 }

	xAOD::MissingETContainer EventFill::met_container(HG::ETmissHandler *_etmissHandler, HG::JetHandler *_jetHandler, xAOD::PhotonContainer _photons, xAOD::ElectronContainer _electrons, xAOD::MuonContainer _muons)
	 {	  
	  //xAOD::JetContainer all_correctedjets = _jetHandler->getCorrectedContainer();
	  xAOD::JetContainer all_correctedjets;
	  all_met      = _etmissHandler->getCorrectedContainer(&_photons  ,&all_correctedjets ,& _electrons, & _muons    );	  
	  met          = _etmissHandler->applySelection(all_met);

	  return met;
	 }

	void EventFill::JetCleaningSave(HG::JetHandler *_jetHandler)
	 {
	  xAOD::JetContainer all_correctedjets = _jetHandler->getCorrectedContainer();
	  xAOD::JetContainer noclean_loose_jets = _jetHandler->applySelectionNoCleaning(all_correctedjets);
	  xAOD::JetContainer noclean_jets (SG::VIEW_ELEMENTS);

	  float jet_final_pt = m_config.getNum("H2ZyAnalysis.JetPtFinalCutGeV",25.0);
	  (*m_beventmap)["EventInfo.isClean"] = true;
	  (*m_beventmap)["EventInfo.isClean_pre"] = true;
	  for (auto jet : noclean_loose_jets) {
	    if ( jet->pt()/1000 > jet_final_pt ) { noclean_jets.push_back(jet); }
	    if (jet->isAvailable<char>("isClean") && ! jet->auxdata<char>("isClean")) 
	      (*m_beventmap)["EventInfo.isClean_pre"] = false; 
	  }
	  for (auto jet : noclean_jets) {
	    if (jet->isAvailable<char>("isClean") && ! jet->auxdata<char>("isClean")) 
	      (*m_beventmap)["EventInfo.isClean"] = false; 
	  }
	 }
	void EventFill::TruthMETSave(HG::TruthHandler *_truthHandler, TString sysname)
	 {
	  (*m_feventmap)["MET_met_Truth"] = -999;
	  (*m_feventmap)["MET_sumet_Truth"] = -999;
	  (*m_feventmap)["MET_sig_Truth"] = -999;
	  if(HG::isMC()&&sysname==""){
	    all_truth_met   = _truthHandler->getMissingET();
	    truth_met       = _truthHandler->applyMissingETSelection(all_truth_met);
	    if(truth_met["NonInt"]!=nullptr){
	      (*m_feventmap)["MET_met_Truth"]          = truth_met["NonInt"]        ->met()*HG::invGeV;
	      (*m_feventmap)["MET_sumet_Truth"]        = truth_met["NonInt"]        ->sumet()*HG::invGeV;
	      (*m_feventmap)["MET_sig_Truth"]       = truth_met["NonInt"]->met()*HG::invGeV/sqrt(truth_met["NonInt"]->sumet()*HG::invGeV);
	    }
	  }
	 }
}

