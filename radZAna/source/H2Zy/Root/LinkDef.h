#include <H2Zy/ZyTruthAnalysis.h>

#include <H2Zy/H2ZyAnalysis.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class H2ZyAnalysis+;
#endif

#ifdef __CINT__
#pragma link C++ class runZyTruthAnalysis+;
#endif

#ifdef __CINT__
#pragma link C++ class ZyTruthAnalysis+;
#endif
