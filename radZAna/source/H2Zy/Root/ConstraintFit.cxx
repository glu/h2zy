///////////////////////////////////////////////////////////
// A C++ implementation of Mass constraint fitting
// 23/09/2006
// K. Nikolopoulos
// https://svnweb.cern.ch/trac/atlasphys/browser/Physics/Higgs/HSG2/Code/ZMassConstraint/trunk/RootCore/ConstraintFit.C
//
// adapted for ROOT standalone (no CLHEP), simplified and bug-fixed by GM 02/10/2012
// --- * --- * --- * --- * ---* --- * --- * --- * ---
//
//
//
//
#include "H2Zy/ConstraintFit.h"
#include <TMath.h>
#include <TLorentzVector.h>
#include <iostream>
using namespace std;

// this is needed to distribute the algorithm to the workers
//ClassImp(ConstraintFit)

// compute logL(TrueMass|RecoMass) = log G(RecoMass|TrueMass,MassResol)*BW(TrueMass|M0,Gamma)
//
// G is a gaussian resolution model (mean = bias, sigma = MassResol) and
// BW is the relativistic Breit-Wigner = 1/( (TrueMass^2-M0^2)^2 + M0^2 Gamma^2) .
//
// logL = log G + log BW = const - 1/2 * (RecoMass - TrueMass - bias)^2/MassResol^2 + log BW
//                       = const - 1/2 * (RecoMass - TrueMass - bias)^2/MassResol^2 - log ( (TrueMass^2 - M0^2)^2 + M0^2 Gamma^2 )
//
// if the non-relativistic BW is used instead, then 
//
// logL = log G + log BW = const - 1/2 * (RecoMass - TrueMass - bias)^2/MassResol^2 + log BW
//                       = const - 1/2 * (RecoMass - TrueMass - bias)^2/MassResol^2 - log ( (TrueMass - M0)^2 + Gamma^2/4 )
//
// In the code: m_conMass = M0, m_conWidth = Gamma
//
// GM: added factor 1/4 in Gamma^2 for non-relativistic BW
double ConstraintFit::EvaluateLogL(double TrueMass, double RecoMass, double MassResolL, double MassResolR)
{
    double x = RecoMass-TrueMass-m_bias;
    if (m_useRelativisticBWFormula)
    {
	if (x>0)
	    return ( - 0.5*x*x/(MassResolR*MassResolR)
		     - TMath::Log( (TrueMass*TrueMass - m_conMass*m_conMass)*(TrueMass*TrueMass - m_conMass*m_conMass) + m_conMass*m_conMass*m_conWidth*m_conWidth) );
	else
	    return ( - 0.5*x*x/(MassResolL*MassResolL)
		     - TMath::Log( (TrueMass*TrueMass - m_conMass*m_conMass)*(TrueMass*TrueMass - m_conMass*m_conMass) + m_conMass*m_conMass*m_conWidth*m_conWidth) );
	    
    }
    else
    {
	if (x>0)
	    return ( -0.5*x*x/(MassResolR*MassResolR)
		     - TMath::Log( (TrueMass-m_conMass)*(TrueMass-m_conMass) + m_conWidth*m_conWidth/4.0) ) ;
	else
	    return ( -0.5*x*x/(MassResolL*MassResolL)
		     - TMath::Log( (TrueMass-m_conMass)*(TrueMass-m_conMass) + m_conWidth*m_conWidth/4.0) ) ;
    }
}

// compute logL(TrueMass|RecoMass) = log G(RecoMass|TrueMass,MassResol)*BW(TrueMass|M0,Gamma) in the case of a symmetric resolution fcn
double ConstraintFit::EvaluateLogL(double TrueMass, double RecoMass, double MassResol)
{
    return EvaluateLogL(TrueMass, RecoMass, MassResol, MassResol);
}

// compute dlogL(TrueMass|RecoMass)/dTrueMass
// if the relativistic BW is used:
// dLogL / dTrueMass = (RecoMass - TrueMass - bias)/MassResol^2 - 4*TrueMass*(TrueMass^2 - M0^2)/((TrueMass^2 - M0^2)^2 + M0^2 Gamma^2)
//

// if the non-relativistic BW is used instead, then 
// dLogL / d TrueMass = (RecoMass - TrueMass - bias)/MassResol^2 - 2*(TrueMass - M0)/((TrueMass - M0)^2 + Gamma^2/4)
//
// In the code: m_conMass = M0, m_conWidth = Gamma
//
// GM: added factor 1/4 in Gamma^2 for non-relativistic BW
double ConstraintFit::EvaluatedLogL(double TrueMass, double RecoMass, double MassResolL, double MassResolR)
{
    double x = RecoMass-TrueMass-m_bias;
    if (m_useRelativisticBWFormula)
    {
	if (x>0)
	    return ( x/(MassResolR*MassResolR)
		     - 4.*TrueMass*(TrueMass*TrueMass - m_conMass*m_conMass) / ( (TrueMass*TrueMass - m_conMass*m_conMass)*(TrueMass*TrueMass - m_conMass*m_conMass) + m_conMass*m_conMass * m_conWidth*m_conWidth ) );
	else
	    return ( x/(MassResolL*MassResolL)
		     - 4.*TrueMass*(TrueMass*TrueMass - m_conMass*m_conMass) / ( (TrueMass*TrueMass - m_conMass*m_conMass)*(TrueMass*TrueMass - m_conMass*m_conMass) + m_conMass*m_conMass * m_conWidth*m_conWidth ) );
    }
    else
    {
	if (x>0)
	    return ( x/(MassResolR*MassResolR) 
		     - 2.*(TrueMass - m_conMass) / ( (TrueMass - m_conMass)*(TrueMass - m_conMass) + m_conWidth*m_conWidth/4.0) );
	else
	    return ( x/(MassResolL*MassResolL) 
		     - 2.*(TrueMass - m_conMass) / ( (TrueMass - m_conMass)*(TrueMass - m_conMass) + m_conWidth*m_conWidth/4.0) );
    }
}
    
// compute dlogL(TrueMass|RecoMass)/dTrueMass in the case of a symmetric resolution fcn
double ConstraintFit::EvaluatedLogL(double TrueMass, double RecoMass, double MassResol)
{
    return EvaluatedLogL(TrueMass, RecoMass, MassResol, MassResol);
}

// compute invariant mass of the particles whose momenta are stored in p0
double ConstraintFit::CalculateMass(const TMatrixD* p0)
{
   TLorentzVector v1, v2;
   // cartesian coordinates
   v1.SetXYZM((*p0)(0, 0), (*p0)(1, 0), (*p0)(2, 0), m_objmass[0]);
   v2.SetXYZM((*p0)(3, 0), (*p0)(4, 0), (*p0)(5, 0), m_objmass[1]);
   // spherical coordinates
/*
   v1.SetXYZM((*p0)(0,0) * cos((*p0)(1,0)) * sin((*p0)(2,0)),
	      (*p0)(0,0) * sin((*p0)(1,0)) * sin((*p0)(2,0)), 
	      (*p0)(0,0)                   * cos((*p0)(2,0)),
	      m_objmass[0]);
   v2.SetXYZM((*p0)(3,0) * cos((*p0)(4,0)) * sin((*p0)(5,0)),
	      (*p0)(3,0) * sin((*p0)(4,0)) * sin((*p0)(5,0)), 
	      (*p0)(3,0)                   * cos((*p0)(5,0)),
	      m_objmass[1]);
*/
   TLorentzVector vtot = v1+v2;
   double RecoMass = vtot.M();
   return RecoMass;
}

// compute invariant mass of the particles whose momenta are stored in m_parametersInit
double ConstraintFit::CalculateMass()
{
    return CalculateMass(m_parametersInit);
}

// compute vector D of derivatives dd/dalpha_i of constraint d(alpha1..alpha6) = 0 where 
// alpha1..alpha6 = px1, py1, pz1, px2, py2, pz2
// 0 = d(...) = Etot^2 - PXtot^2 - PYtot^2 - PZtot^2 - M^2 = (E1(px1,py1,pz1)+E2(px2,py2,pz2))^2 - (px1+px2)^2 - (py1+py2)^2 - (pz1+pz2)^2 - M^2
// need Etot, Ptot, E1, E2, P1, P2
void ConstraintFit::CalculateM2Jacobian(const TMatrixD* p0, TMatrixD& JacobianM2)
{
   TLorentzVector v1, v2;
   // cartesian coordinates
   v1.SetXYZM((*p0)(0, 0), (*p0)(1, 0), (*p0)(2, 0), m_objmass[0]);
   v2.SetXYZM((*p0)(3, 0), (*p0)(4, 0), (*p0)(5, 0), m_objmass[1]);
   TLorentzVector vtot = v1+v2;

   double PXtot = vtot.Px();
   double PYtot = vtot.Py();
   double PZtot = vtot.Pz();
   double Etot = vtot.E();

   // J = derivatives of M^2 wrt the 6 track parameters
   JacobianM2(0, 0) = (2.*(Etot) * v1.Px() / v1.E() - 2.* PXtot);
   JacobianM2(1, 0) = (2.*(Etot) * v1.Py() / v1.E() - 2.* PYtot);
   JacobianM2(2, 0) = (2.*(Etot) * v1.Pz() / v1.E() - 2.* PZtot);
   JacobianM2(3, 0) = (2.*(Etot) * v2.Px() / v2.E() - 2.* PXtot);
   JacobianM2(4, 0) = (2.*(Etot) * v2.Py() / v2.E() - 2.* PYtot);
   JacobianM2(5, 0) = (2.*(Etot) * v2.Pz() / v2.E() - 2.* PZtot);
}

// compute reco mass resolution, propagating uncertainties on track momenta.
// use momenta stored in p0 and their covariance stored in covariance
double ConstraintFit::CalculateMassResol(const TMatrixD* p0, const TMatrixD* covariance)
{
   double RecoMass = CalculateMass(p0);

   // J = derivatives of M^2 wrt the 6 track parameters
   TMatrixD JacobianMass(6, 1);
   CalculateM2Jacobian(p0, JacobianMass);
   if (m_verbose) {
       cout << "Jacobian dM^2/dxi: " << endl;
       JacobianMass.Print();
   }

   // compute uncertainty on mass^2 = sqrt(Jt Var J)
   TMatrixD tmp1 = (*covariance) * JacobianMass;
   TMatrixD tmp2 = JacobianMass; tmp2.T();
   TMatrixD tmp3 = tmp2*tmp1;
   double sig = tmp3(0,0);
   //double sig = (JacobianMass.T() * (*covariance) * JacobianMass)(0, 0);
   sig = sqrt(sig);
   // use d(M^2) = 2MdM to obtain uncertainty on the mass, dM = d(M^2)/2M
   double MassResol = sig/(2 * RecoMass);
   return MassResol;
}

// compute reco mass resolution, propagating uncertainties on track momenta.
// use momenta stored in m_parametersInit and their covariance stored in m_covarianceInit
double ConstraintFit::CalculateMassResol()
{
    return CalculateMassResol(m_parametersInit, m_covarianceInit);
}

// Return invariant mass that maximises the likelihood L(mass) = G(obsmass | mass)*BW(mass), 
// The idea here is to maximize log L
double ConstraintFit::LikelihoodMass2(double MassResolL, double MassResolR)
{
   // compute reco mass
   double RecoMass = CalculateMass(m_parametersInit);
   if (m_verbose) cout << RecoMass << "+" << MassResolR << "-" << MassResolL << endl; 
   
   // find (numerically) value maxmass that maximises the likelihood (log L(maxmass) = max), scanning 400 points between the reconstructed mass and the resonance pole
   // why not searching also beyond the peak???
   double xLeft, xRight;
   int nsteps = 400;
   if (m_conMass < RecoMass) {
      xLeft = m_conMass;
      xRight = RecoMass;
   }
   else {
      xLeft = RecoMass;
      xRight = m_conMass;
   }
   // GM: explore also regions +-5 sigma outside of mreco..m0 range
   if (!m_useHSG2Settings)
   {
       xLeft -= 5*MassResolL;
       xRight += 5*MassResolR;
       nsteps = 800;
   }
  
   double step = (xRight-xLeft)/nsteps;

   double maxmass = xLeft;
   double max = EvaluateLogL(maxmass,RecoMass,MassResolL,MassResolR);
   if (m_verbose) std::cout << "** "<<maxmass << " "<< max<<std::endl;
   for(int i=1; i<=nsteps; i++) {
       double mass = xLeft + i*step;
       double val = EvaluateLogL(mass,RecoMass,MassResolL,MassResolR);
       if (m_verbose) std::cout << i << " "<<mass << " " << val << std::endl;
       if (val>max) {
	   max=val;
	   maxmass=mass;
       }
   }
   return maxmass;
}

// return invariant mass that maximises the likelihood L(TrueMass) = G(MassReco|TrueMass)*BW(TrueMass), 
// where the mass resolution is a fixed value passed as argument
// the idea is to maximise logL by finding the 0 of its derivative dlogL/dTrueMass using the bisection method.
double ConstraintFit::LikelihoodMass(double MassResolL, double MassResolR)
{
   // compute reco mass
   double RecoMass = CalculateMass(m_parametersInit);
   if (m_verbose) cout << RecoMass << "+" << MassResolR << "-" << MassResolL << endl; 

   // find 0 of derivative of logL with the bisection method, in the range between the nominal Z mass and the reconstructed mass 
   double xLeft, xRight;
   if (m_conMass < RecoMass) {
      xLeft = m_conMass;
      xRight = RecoMass;
   }
   else {
      xLeft = RecoMass;
      xRight = m_conMass;
   }
   // GM: explore also regions +-4 sigma outside of mreco..m0 range
   if (!m_useHSG2Settings)
   {
       xLeft -= 5*MassResolL;
       xRight += 5*MassResolR;
   }
   //
   double dLinitL = EvaluatedLogL(xLeft,RecoMass,MassResolL,MassResolR);
   double dLinitR = EvaluatedLogL(xRight,RecoMass,MassResolL,MassResolR);
   if (m_verbose) std::cout << xLeft << " " << xRight << " " << dLinitL << " "<< dLinitR<<std::endl;
   if (dLinitL * dLinitR < 0.) {
      while (xRight - xLeft > 1.) { //1 MeV
	 double xM = (xRight + xLeft) / 2.; // mass value to be tested (average of xLeft and xRight)
	 double dL = EvaluatedLogL(xM,RecoMass,MassResolL,MassResolR); // value of the derivative of logL for x=xM
         if (dL * dLinitL < 0.) {
            xRight = xM;
            dLinitR = dL;
         } else {
            xLeft = xM;
            dLinitL = dL;
         }
	 if (m_verbose) std::cout << xLeft << " " << xRight << " " << dLinitL << " "<< dLinitR<<std::endl;
      }
      return (xLeft + xRight) / 2.;
   } else {
      if (dLinitL > dLinitR)
         return xLeft;
      else
         return xRight;
   }
}


// return invariant mass that maximises the likelihood L(mass) = G(obsmass | mass)*BW(mass), 
// where the mass resolution is computed from the momentum uncertainties of the 2 daughters
// the maximum is found by searching numerically for the maximum of logL(mass),
// in LikelihoodMass2(MassResol)
double ConstraintFit::LikelihoodMass2(void)
{
   // compute reco mass resolution, propagating uncertainties on track momenta
   double MassResol = CalculateMassResol(m_parametersInit, m_covarianceInit);

   return LikelihoodMass2(MassResol,MassResol);
}

// return invariant mass that maximises the likelihood L(mass) = G(obsmass | mass)*BW(mass), 
// where the mass resolution is computed from the momentum uncertainties of the 2 daughters
// the maximum is found by searching numerically for the root of the derivative of logL,
// in LikelihoodMass(MassResol)
double ConstraintFit::LikelihoodMass(void)
{
   // compute reco mass resolution, propagating uncertainties on track momenta
   double MassResol = CalculateMassResol(m_parametersInit, m_covarianceInit);

   return LikelihoodMass(MassResol,MassResol);
}

// default constructor, sets mass and width to zero
ConstraintFit::ConstraintFit(void):
   m_conHasWidth(false),
   m_conMass(0.),
   m_conWidth(0.),
   m_bias(0.),
   m_useRelativisticBWFormula(true),
   m_verbose(false),
   m_parameters(3),
   m_parametersInit(NULL),
   m_covarianceInit(NULL),
   m_parametersFinal(NULL),
   m_covarianceFinal(NULL),
   m_chi2(NULL)
{}

// constructor with mass and width passed as argument
ConstraintFit::ConstraintFit(double mass, bool haswidth, double width):
   m_conHasWidth(haswidth),
   m_conMass(mass),
   m_conWidth(width),
   m_bias(0.),
   m_useRelativisticBWFormula(true),
   m_verbose(false),
   m_parameters(3),
   m_parametersInit(NULL),
   m_covarianceInit(NULL),
   m_parametersFinal(NULL),
   m_covarianceFinal(NULL),
   m_chi2(NULL)
{}

// destructor
ConstraintFit::~ConstraintFit(void)
{
   delete m_parametersInit;
   delete m_covarianceInit;
   delete m_parametersFinal;
   delete m_covarianceFinal;
   delete m_objmass;
   delete m_chi2;
}

// store in the constraint fit object the initial values of measured momenta and their covariance matrices
//double ConstraintFit::MassFitInterface(double (*pin)[4],double (*sigmain)[3][3], int iobj)
void ConstraintFit::MassFitInterface(double pin[2][4], double sigmain[6][6], int iobj)
{
   // the m_parameters first parameters of pin[i] are the measured momentum parameters (px,py,pz) for particle i (i=0..iobj-1)
   // the next parameter of pin[i] is the mass of the particle i
   // sigmain is the cov matrix between the m_parameters*m_obj parameters
   m_obj            = iobj;
   int dimension    = m_parameters * m_obj;
   m_parametersInit = new TMatrixD(dimension, 1);
   m_covarianceInit = new TMatrixD(dimension, dimension);
   m_parametersFinal = new TMatrixD(dimension, 1);
   m_covarianceFinal = new TMatrixD(dimension, dimension);
   m_chi2           = new TMatrixD(1, 1);
   m_objmass        = new double [m_obj];

   for (int i = 0; i < m_obj; i++)
   {
      m_objmass[i]  = pin[i][m_parameters];
      for (int j = 0; j < m_parameters; j++)
	  (*m_parametersInit)(j + m_parameters*i, 0) = pin[i][j];
   }
   for (int i = 0; i < 2 * m_parameters; i++)
      for (int j = 0; j < 2 * m_parameters; j++)
         (*m_covarianceInit)(i, j) = sigmain[i][j];


   //transform the covariance matrices for px,py,pz to p,phi,theta
   //going from d0,z0,phi,theta,P --> d0,z0,px,py,pz

   // 1 p
   // 2 phi
   // 3 theta

   // TMatrixD Jacobian(2*m_parameters,2*m_parameters,0);
   // for(int i=0;i<2;i++)
   //   {
   //     double p = sqrt(pin[i][0]*pin[i][0]+pin[i][1]*pin[i][1]+pin[i][2]*pin[i][2]);
   //     double phi = atan2(pin[i][1],pin[i][0]);
   //     double theta = acos(pin[i][2]/p);
   //     (*m_parametersInit)(0+3*i,0) = p;
   //     (*m_parametersInit)(1+3*i,0) = phi;
   //     (*m_parametersInit)(2+3*i,0) = theta;

   //     Jacobian(0+3*i,0+3*i)= pin[i][0]/p;
   //     Jacobian(0+3*i,1+3*i)= 1./(p*cos(theta)*cos(phi));
   //     Jacobian(0+3*i,2+3*i)= 1./(-p*sin(theta)*sin(phi));
   //     Jacobian(1+3*i,0+3*i)= pin[i][1]/p;
   //     Jacobian(1+3*i,1+3*i)= 1./(p*cos(theta)*sin(phi));
   //     Jacobian(1+3*i,2+3*i)= 1./(p*sin(theta)*cos(phi));
   //     Jacobian(2+3*i,0+3*i)= pin[i][2]/p;
   //     Jacobian(2+3*i,1+3*i)= 1./(-p*sin(phi));
   //     Jacobian(2+3*i,2+3*i)= 0.;

   //   }
   //(*m_covarianceInit)=Jacobian.T()*(*m_covarianceInit)*Jacobian;

}

// perform the mass constraint fit - will use a fixed a value of the resolution
// if passed as argument, otherwise will evaluate the mass resolution from the
// momenta covariances
double ConstraintFit::MassFitRun(double pin[2][4], double sigmain[6][6], double zresolL, double zresolR)
{
   double chi2;
   if (!m_conHasWidth) {
       // if the resonance has no width, then the mass constrain is m = m_conMass
       *m_parametersFinal = *m_parametersInit;
       *m_covarianceFinal = *m_covarianceInit;
       chi2 = MassFit(m_parametersInit, m_covarianceInit, m_conMass, m_parametersFinal, m_covarianceFinal);
   }
   else
   {
       // otherwise, first compute best mass (Mass), and then the mass constrain is m = Mass
       TMatrixD m_parametersFit = *m_parametersInit;
       if (m_verbose) {
	   cout << "Initial values of the parameters:" << endl;
	   m_parametersFit.Print();
       }
       double Mass;
       if (zresolL>0 && zresolR>0)
	   Mass=LikelihoodMass2(zresolL,zresolR);
       else
	   Mass=LikelihoodMass2();
       chi2 = MassFit(m_parametersInit, m_covarianceInit, Mass, &m_parametersFit, m_covarianceFinal);
       *m_parametersFinal = m_parametersFit;
       *m_covarianceFinal = *m_covarianceInit;
   }
   // double chi2prob = TMath::Prob(chi2, 1);
   // double chi22 = CalculateChi2(m_parametersInit, m_covarianceInit, m_conMass);
   for (int i = 0; i < m_parameters; i++) {
       pin[0][i] = (*m_parametersFinal)(i + 0, 0);
       pin[1][i] = (*m_parametersFinal)(i + 3, 0);
       //(*m_covarianceInit)(i+0+1,i+0+1)= sigmain[0][i][i]*sigmain[0][i][i];
       //(*m_covarianceInit)(i+3+1,i+3+1)= sigmain[1][i][i]*sigmain[1][i][i];
   }
   return chi2;
}

// evaluate the constrain (old function from code where spherical coordinates where used?)
void ConstraintFit::ConstraintCalculation(const TMatrixD *p0, const double Mass, TMatrixD *D, TMatrixD *d)
{
   // the constraint is (E1+E2)^2-(P1x+P2x)^2-(P1y+P2y)^2-(P1z-P2z)^2-m^2 = 0.
   // the partial derivatives of the constraint with respect to px,py,pz are
   // i->particle
   // j->x,y,z
   // dF/dPij = 2 (E1+E2)*Pij/Ei - 2(P1j+P2j)
   //
   // partial derivatives of px,py,pz with respect to the fit parameters
   // dpx/dp = cosPhi *sinTheta
   // dpy/dp = sinPhi *sinTheta
   // dpz/dp = cosTheta
   // --
   // dpx/dtheta = px/tanTheta
   // dpy/dtheta = py/tanTheta
   // dpz/dtheta =-pz*tanTheta
   // --
   // dpx/dphi = -px*tanTheta
   // dpy/dphi =  px/tanTheta
   // dpz/dphi =  0.
   //
   //calculating the constraint linear expansion at the current point
   //
   double p[4][2] = { {0., 0.}, {0., 0.}, {0., 0.}, {0., 0.} };
   p[0][0] = (*p0)(0, 0) * cos((*p0)(1, 0)) * sin((*p0)(2, 0));
   p[1][0] = (*p0)(0, 0) * sin((*p0)(1, 0)) * sin((*p0)(2, 0));
   p[2][0] = (*p0)(0, 0)                    * cos((*p0)(2, 0));
   for (int i = 0; i < 3; i++)
      p[3][0] += p[i][0] * p[i][0];
   p[3][0] = sqrt(p[3][0]);

   p[0][1] = (*p0)(3, 0) * cos((*p0)(4, 0)) * sin((*p0)(5, 0));
   p[1][1] = (*p0)(3, 0) * sin((*p0)(4, 0)) * sin((*p0)(5, 0));
   p[2][1] = (*p0)(3, 0)                    * cos((*p0)(5, 0));
   for (int i = 0; i < 3; i++)
      p[3][1] += p[i][1] * p[i][1];
   p[3][1] = sqrt(p[3][1]);

   double Etot = p[3][0] + p[3][1];

   double constraintD1 = (2.*(Etot) * p[0][0] / p[3][0] - 2.*(p[0][0] + p[0][1])) * (cos((*p0)(2, 1)) * sin((*p0)(3, 1)))
                         + (2.*(Etot) * p[1][0] / p[3][0] - 2.*(p[1][0] + p[1][1])) * (sin((*p0)(2, 1)) * sin((*p0)(3, 1)))
                         + (2.*(Etot) * p[2][0] / p[3][0] - 2.*(p[2][0] + p[2][1])) * (cos((*p0)(3, 1)));
   double constraintD2 = (2.*(Etot) * p[0][0] / p[3][0] - 2.*(p[0][0] + p[0][1])) * (-p[0][0] * tan((*p0)(2, 1)))
                         + (2.*(Etot) * p[1][0] / p[3][0] - 2.*(p[1][0] + p[1][1])) * (p[1][0] / tan((*p0)(2, 1)))
                         + (2.*(Etot) * p[2][0] / p[3][0] - 2.*(p[2][0] + p[2][1])) *  0.;
   double constraintD3 = (2.*(Etot) * p[0][0] / p[3][0] - 2.*(p[0][0] + p[0][1])) * (p[0][0] / tan((*p0)(3, 1)))
                         + (2.*(Etot) * p[1][0] / p[3][0] - 2.*(p[1][0] + p[1][1])) * (p[1][0] / tan((*p0)(3, 1)))
                         + (2.*(Etot) * p[2][0] / p[3][0] - 2.*(p[2][0] + p[2][1])) * (-p[2][0] * tan((*p0)(3, 1)));

   double constraintD4 = (2.*(Etot) * p[0][1] / p[3][1] - 2.*(p[0][0] + p[0][1])) * (cos((*p0)(5, 1)) * sin((*p0)(6, 1)))
                         + (2.*(Etot) * p[1][1] / p[3][1] - 2.*(p[1][0] + p[1][1])) * (sin((*p0)(5, 1)) * sin((*p0)(6, 1)))
                         + (2.*(Etot) * p[2][1] / p[3][1] - 2.*(p[2][0] + p[2][1])) * (cos((*p0)(6, 1)));
   double constraintD5 = (2.*(Etot) * p[0][1] / p[3][1] - 2.*(p[0][0] + p[0][1])) * (-p[0][1] * tan((*p0)(5, 1)))
                         + (2.*(Etot) * p[1][1] / p[3][1] - 2.*(p[1][0] + p[1][1])) * (p[1][1] / tan((*p0)(5, 1)))
                         + (2.*(Etot) * p[2][1] / p[3][1] - 2.*(p[2][0] + p[2][1])) *  0.;
   double constraintD6 = (2.*(Etot) * p[0][1] / p[3][1] - 2.*(p[0][0] + p[0][1])) * (p[0][1] / tan((*p0)(6, 1)))
                         + (2.*(Etot) * p[1][1] / p[3][1] - 2.*(p[1][0] + p[1][1])) * (p[1][1] / tan((*p0)(6, 1)))
                         + (2.*(Etot) * p[2][1] / p[3][1] - 2.*(p[2][0] + p[2][1])) * (-p[2][1] * tan((*p0)(6, 1)));


   double constraintd =  Etot * Etot;
   for (int i = 0; i < 3; i++) {
      constraintd = constraintd - (p[i][0] + p[i][1]) * (p[i][0] + p[i][1]);
   }
   constraintd = constraintd - Mass * Mass;
   if (m_verbose) std::cout << "constraint = " <<constraintd << " mass value " <<Mass<<std::endl;
   //D = new TMatrixD(1, 6);
   (*D)(0, 0) = constraintD1;
   (*D)(0, 1) = constraintD2;
   (*D)(0, 2) = constraintD3;
   (*D)(0, 3) = constraintD4;
   (*D)(0, 4) = constraintD5;
   (*D)(0, 5) = constraintD6;
   //d = new TMatrixD(1, 1);
   (*d)(0, 0) = constraintd;
}

// compute chi2 including constraint = lambda.T() * V_D^-1 * lambda
double ConstraintFit::CalculateChi2(const TMatrixD* p0, TMatrixD* var, const double Mass)
{
   // compute row vector D of derivatives of constrain vs track parameters, for current solution p0
   TMatrixD JacobianMass(6, 1);
   CalculateM2Jacobian(p0, JacobianMass);
   TMatrixD D = JacobianMass; D.T();

   // compute value of constraint d(alpha) = Mass(alpha)^2 - Mass^2 
   double CurrentMass = CalculateMass(p0);
   double constraintd =  CurrentMass*CurrentMass - Mass*Mass;
   TMatrixD d(1, 1);
   d(0, 0) = constraintd;

   // compute alpha0 - alpha
   TMatrixD dalpha = (*m_parametersInit) - (*p0);
   // compute D*(alpha0-alpha + d)
   TMatrixD Ddalpha = D*dalpha + d;

   // compute V_D matrix = (D*Valpha0*DT)^-1, Valpha0 = initial covariance matrix
   TMatrixD DVar = D * (*var);
   TMatrixD Dt = D; Dt.T();
   TMatrixD DVDt = DVar*Dt;
   double det;
   TMatrixD V_D = DVDt; V_D.Invert(&det);
   //if(det==0.0)
   //std::cout << "matrix inversion failed " <<std::endl;

   // compute lambda: Avery: V_D*(d+D*(parametersInit-p0)); Kostas: V_D*d (a0 = a*, expand around current solution?)
   // Avery:
   TMatrixD lambda = V_D*Ddalpha;
   // Kostas
   // TMatrixD lambda = V_D * d;
   // invert DVDt again
   //DVDt.Invert(&det);
   //double chi2 = (lambda.T() * DVDt * lambda)(0, 0);
   TMatrixD c(1,1);
   TMatrixD lambdat = lambda; lambdat.T();
   c = lambdat * Ddalpha;
   double chi2=c(0,0); 
   //std::cout <<"chi2 "<<chi2<<std::endl;
   return chi2;
}

// http://www.phys.ufl.edu/~avery/fitting/kinematic.pdf, eq. 6
// compute the solution alpha_n+1 = alpha_0 - var_alpha0 DT (D var_alpha0 DT)^-1 (D delta alpha0 + d)
// where d = d(alpna_n) is the value of the constraint for the current values of the parameters
// D = D(alpha_n) = dd/dalpha = the derivatives of the constraint vs the parameters, evaluated in alpha_n
// var = the initial (i.e. not-refitted) covariance matrix of the track parameters
double ConstraintFit::MassFitCalculation(TMatrixD* p0, TMatrixD* var, const double Mass)
{
   // compute row vector D of derivatives of constrain vs track parameters, (current solution p0)
   TMatrixD JacobianMass(6, 1);
   CalculateM2Jacobian(p0, JacobianMass);
   TMatrixD Dt = JacobianMass;
   TMatrixD D = JacobianMass; D.T();
   if (m_verbose) {
       cout << "D:" << endl;
       D.Print();
   }

   // compute value of constraint d(alpha) = Mass(alpha)^2 - Mass^2 at expansion point (current solution p0)
   double CurrentMass = CalculateMass(p0);
   double constraintd =  CurrentMass*CurrentMass - Mass*Mass;
   TMatrixD d(1, 1);
   d(0, 0) = constraintd;	 
   if (m_verbose) {
       cout << "d:" << endl;
       d.Print();
   }

   // compute alpha0 - alpha
   TMatrixD dalpha = (*m_parametersInit) - (*p0);
   if (m_verbose) {
       cout << "alpha0-alpha:" << endl;
       dalpha.Print();
   }

   // compute V_D matrix = (D*Valpha0*DT)^-1, Valpha0 = initial covariance matrix
   TMatrixD DVar = D * (*var);
   if (m_verbose) {
       cout << "DV" << endl;
       DVar.Print();
   }
   TMatrixD DVDt = DVar*Dt;
   if (m_verbose) {
       cout << "DVDt" << endl;
       DVDt.Print();
   }
   double det;
   TMatrixD V_D = DVDt; V_D.Invert(&det);
   //if(det==0.0)
   //std::cout << "matrix inversion failed " <<std::endl;
   if (m_verbose) {
       cout << "V_D:" << endl;
       V_D.Print();
   }

   // compute lambda: 
   //Avery: V_D*(d+D*(parametersInit-p0)); Kostas: V_D*d (a0 = a*, expand around current solution?)
   TMatrixD lambda(1,1);
   if (!m_useHSG2Settings) {
       // compute D*(alpha0-alpha + d)
       TMatrixD Ddalpha = D*dalpha;
       Ddalpha += d;
       if (m_verbose) {
	   cout << "D*dalpha0 + d:" << endl;
	   Ddalpha.Print();
       }
       lambda = V_D*Ddalpha;
   }
   else {
       lambda = V_D*d;
   }
   if (m_verbose) {
       cout << "lambda:" << endl;
       lambda.Print();
   }
   TMatrixD Dtlambda = Dt*lambda;
   TMatrixD VarDtlambda = (*var)*Dtlambda;
   TMatrixD test(6,1);
   if (!m_useHSG2Settings)
   {
       // Avery:
       test = (*m_parametersInit) - VarDtlambda;
   }
   else
   {
       // Kostas:
       test = (*p0) - VarDtlambda;
   }
   if (m_verbose) {
       cout << "New parameters:" << endl;
       test.Print();
   }
   (*p0)(0, 0) = test(0, 0);
   (*p0)(1, 0) = test(1, 0);
   (*p0)(2, 0) = test(2, 0);
   (*p0)(3, 0) = test(3, 0);
   (*p0)(4, 0) = test(4, 0);
   (*p0)(5, 0) = test(5, 0);

   return constraintd;
}

// do the iterative kinematic fit:
// at each iteration, compute the new solution using the current solution pOut, 
// the initial parameters p0, the initial covariance matrix var, and the value of Mass to evaluate the constraint
double ConstraintFit::MassFit(const TMatrixD* p0, TMatrixD* var, const double Mass, TMatrixD* pOut, TMatrixD* varout)
{
   bool doIter = true;
   int  maxIterations = 20;
   int  iIter  = 0;
   double constraintValue = -1.e10;

   while (doIter) {
      TMatrixD p_old(*pOut); // the previous solution, alpha_n
      constraintValue = MassFitCalculation(pOut, var, Mass); //call the fit: pOut will contain the new solution alpha_n+1, and the returned value is the value of Mass^2-M(pOut)^2, i.e. d(alpha_n+1) (which should be close to zero to satisfty the constraint...)

      // Convergence criteria
      // 1. the parameters should not change too much (<1.e-6 relative change)
      // 2. the constraint should be satisfied very well (<1.e-6 absolute)
      double maxDiff = 0.0;
      TMatrixD diff = ((*pOut) - p_old); // vector of the differences between the new and the old parameters
      for (int i = 0; i < m_parameters * m_obj; i++) {
         diff(i, 0) = diff(i, 0) / p_old(i, 0);
         if (maxDiff < TMath::Abs(diff(i, 0)))
	     maxDiff = TMath::Abs(diff(i, 0));
      }
      if ((maxDiff < 1.e-6 && TMath::Abs(constraintValue) < 1.e-12) || maxIterations <= iIter)
         doIter = false;
      if (m_verbose) std::cout << "iteration " << iIter << " " <<constraintValue <<std::endl;
      iIter++;
   }
   if (m_verbose)
       std::cout << "Iterations " << iIter << ", constraint = " << constraintValue << std::endl;

   double chi2 = CalculateChi2(pOut, var, Mass);
   return chi2;
}

// dump the initial values of the momenta and of their covariances
void ConstraintFit::PrintInitialParameters()
{
    cout << "Initial parameters:" << endl;
    m_parametersInit->Print();
    cout << endl;
    cout << "Covariance matrix of initial parameters:" << endl;
    m_covarianceInit->Print();
    cout << endl;
}
