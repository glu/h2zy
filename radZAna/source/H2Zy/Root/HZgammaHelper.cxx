#include "H2Zy/HZgammaHelper.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include <xAODEgamma/EgammaxAODHelpers.h>
#include "HGamAnalysisFramework/Config.h"


int getmctype(int mc_channel_number ){
  int _mctype = -1;
  if (mc_channel_number==167481 || 
      mc_channel_number==341445 ||
      (mc_channel_number>=342198 && mc_channel_number<=342211) ||
      (mc_channel_number>=343577 && mc_channel_number<=343594) ||
      (mc_channel_number>=344744 && mc_channel_number<=344750) ||
      mc_channel_number==341936 ||
      mc_channel_number==345316
     ) _mctype=0;  //ggH
  else if (mc_channel_number==167491 || 
      mc_channel_number==341446 ||
      (mc_channel_number>=342212 && mc_channel_number<=342225) ||
      mc_channel_number==345042 || 
      mc_channel_number==344751 ||
      mc_channel_number==345160
      ) _mctype=1; //VBF
  else if (mc_channel_number==167551 ||
      mc_channel_number==341448 ||
      (mc_channel_number>=342288 && mc_channel_number<=342298) || 
      (mc_channel_number>=344758 && mc_channel_number<=344760)
      ) _mctype=2; //ttH
  else if (mc_channel_number==167501 || 
      mc_channel_number==341447 ||
      (mc_channel_number>=342226 && mc_channel_number<=342235) ||
      mc_channel_number==345320 ||
      mc_channel_number==345321 ||
      mc_channel_number==344752 ||
      mc_channel_number==344753 ||
      mc_channel_number==344754 ||
      mc_channel_number==345161
      ) _mctype=3; //WH 
  else if (mc_channel_number==167509 ||
      mc_channel_number==341463 ||
      (mc_channel_number>=342236 && mc_channel_number<=342245) ||
      mc_channel_number==345322 ||
      mc_channel_number==344755 ||
      mc_channel_number==344756 ||
      mc_channel_number==344757 ||
      mc_channel_number==345162
      ) _mctype=4; //ZH 
  else if (mc_channel_number==147449 ||
	   mc_channel_number==301536 ||
	   mc_channel_number==301902 ||
	   mc_channel_number==301903 || 
	   mc_channel_number==301904 ||
	   (mc_channel_number>=343834 && mc_channel_number<=343842)
	   ) _mctype=11;//eegamma Sherpa
  else if (mc_channel_number==147450 ||
	   mc_channel_number==301535 ||
	   mc_channel_number==301899 ||
	   mc_channel_number==301900 ||
	   mc_channel_number==301901 ||
	   (mc_channel_number>=343843 && mc_channel_number<=343851)
	   ) _mctype=12;//mumugamma Sherpa
  else if (mc_channel_number==147406 ||
	   (mc_channel_number>=361372 && mc_channel_number<=361395)
	   ) _mctype=13;//Zee Sherpa
  else if (mc_channel_number==147407 ||
	   (mc_channel_number>=361396&&mc_channel_number<=361419)
	   ) _mctype=14;//Zmumu Sherpa

  return _mctype;
}

float getHiggsResMass(int mc_channel_number){
  float _ResMass=0.;
  if     (mc_channel_number==342198 || 
	  mc_channel_number==342212 || 
	  mc_channel_number==342226 || 
	  mc_channel_number==342236 || 
	  mc_channel_number==342236 || 
	  mc_channel_number==342289
	  ) _ResMass = 100.;
  else if (mc_channel_number==342199 ||
	   mc_channel_number==342213 ||
	   mc_channel_number==342227 ||
	   mc_channel_number==342237 ||
	   mc_channel_number==342290
	   ) _ResMass = 105.;
  else if (mc_channel_number==342200 ||
	   mc_channel_number==342214 ||
	   mc_channel_number==342228 ||
	   mc_channel_number==342238 ||
	   mc_channel_number==342291
	   ) _ResMass = 110.;
  else if (mc_channel_number==342201 ||
	   mc_channel_number==342215 ||
	   mc_channel_number==342229 ||
	   mc_channel_number==342239 ||
	   mc_channel_number==342292
	   ) _ResMass = 115.;
  else if (mc_channel_number==342202 ||
	   mc_channel_number==342216 ||
	   mc_channel_number==342230 ||
	   mc_channel_number==342240 || 
	   mc_channel_number==342293
	   ) _ResMass = 120.;
  else if (mc_channel_number==342288 ||
	   mc_channel_number==341463 ||
	   mc_channel_number==341445 ||
	   mc_channel_number==341446 ||
	   mc_channel_number==341447 ||
	   mc_channel_number==341448 ||
	   mc_channel_number==342288 ||
	   mc_channel_number==345316 ||
	   mc_channel_number==345042 ||
	   mc_channel_number==345320 ||
	   mc_channel_number==345321 ||
	   mc_channel_number==345322
	   ) _ResMass = 125.;
  else if (mc_channel_number==342203 ||
	   mc_channel_number==342217 ||
	   mc_channel_number==342231 ||
	   mc_channel_number==342241 ||
	   mc_channel_number==342294
	   ) _ResMass = 130.;
  else if (mc_channel_number==342204 ||
	   mc_channel_number==342218 ||
	   mc_channel_number==342232 ||
	   mc_channel_number==342242 ||
	   mc_channel_number==342295
	   ) _ResMass = 135.;
  else if (mc_channel_number==342205 ||
	   mc_channel_number==342219 ||
	   mc_channel_number==342233 ||
	   mc_channel_number==342243 ||
	   mc_channel_number==342296
	   ) _ResMass = 140.;
  else if (mc_channel_number==342206 ||
	   mc_channel_number==342220 ||
	   mc_channel_number==342234 ||
	   mc_channel_number==342244 ||
	   mc_channel_number==342297
	   ) _ResMass = 145.;
  else if (mc_channel_number==342207 ||
	   mc_channel_number==342221 ||
	   mc_channel_number==342235 ||
	   mc_channel_number==342245 ||
	   mc_channel_number==342298
	   ) _ResMass = 150.;
  else if (mc_channel_number==342208 ||
	   mc_channel_number==342222 ||
	   mc_channel_number==306220 ||
	   mc_channel_number==306262 ||
	   mc_channel_number==344752 ||
	   mc_channel_number==344755 ||
	   mc_channel_number==344758 
      ) _ResMass = 200.;
  else if (
      mc_channel_number==306221 ||
      mc_channel_number==306263 
      ) _ResMass = 250.;
  else if (mc_channel_number==342209 ||
      mc_channel_number==342223 ||
      mc_channel_number==306222 ||
      mc_channel_number==306264 
      ) _ResMass = 300.;
  else if (mc_channel_number==342210 ||
      mc_channel_number==342224 ||
      mc_channel_number==306226 ||
      mc_channel_number==306268 
	   ) _ResMass = 500.;
  else if (mc_channel_number==343577) _ResMass = 700.;
  else if (mc_channel_number==343578 
      || mc_channel_number==343592 
      || mc_channel_number==343593 
      || mc_channel_number==343594
      || mc_channel_number==306231
      || mc_channel_number==306273
      ) _ResMass = 750.;
  else if (mc_channel_number==343579) _ResMass = 800.;
  else if (mc_channel_number==342211 ||
      mc_channel_number==342225 ||
      mc_channel_number==306234 ||
      mc_channel_number==306276 ||
      mc_channel_number==344753 ||
      mc_channel_number==344756 ||
      mc_channel_number==344759 
      ) _ResMass = 1000.;
  else if (mc_channel_number==343580 
      || mc_channel_number==306235
      || mc_channel_number==306277
      ) _ResMass = 1500.;
  else if (mc_channel_number==341936 
      || mc_channel_number==306236
      || mc_channel_number==306278
      || mc_channel_number==344744
      ) _ResMass = 2000.;
  else if (mc_channel_number==343586 
      || mc_channel_number==306237
      || mc_channel_number==306279
      || mc_channel_number==344745
      || mc_channel_number==345160
      || mc_channel_number==345161
      || mc_channel_number==345162
      ) _ResMass = 2500.;
  else if (mc_channel_number==343587 
      || mc_channel_number==306238
      || mc_channel_number==306280
      || mc_channel_number==344746
      ) _ResMass = 3000.;
  else if (mc_channel_number==343588 
      || mc_channel_number==306239
      || mc_channel_number==306281
      || mc_channel_number==344747
      ) _ResMass = 3500.;
  else if (mc_channel_number==343589 
      || mc_channel_number==306240
      || mc_channel_number==306282
      || mc_channel_number==344748
      ) _ResMass = 4000.;
  else if (mc_channel_number==343590 
      || mc_channel_number==306241
      || mc_channel_number==306283
      || mc_channel_number==344749
      ) _ResMass = 4500.;
  else if (mc_channel_number==343591 
      || mc_channel_number==306242
      || mc_channel_number==306284
      || mc_channel_number==344750 ||
      mc_channel_number==344754 ||
      mc_channel_number==344757 ||
      mc_channel_number==344760 
      ) _ResMass = 5000.;
  return _ResMass;
}


vector<double> thrust_coord( TLorentzVector vector1, TLorentzVector vector2 )
{
  double pT_t = -1000, pT_l = -1000;
  double dx = vector1.Px() - vector2.Px();
  double dy = vector1.Py() - vector2.Py();
  TVector2 thrust(dx, dy);
  thrust = thrust / thrust.Mod();
  pT_t = 2.0*fabs(vector1.Px()*thrust.Py() - vector1.Py()*thrust.Px());
  pT_l = ((vector1.Px()+vector2.Px())*thrust.Px()+(vector1.Py()+vector2.Py())*thrust.Py());

  vector<double> result;
  result.push_back(pT_t);
  result.push_back(pT_l);
  return result;
}

void Photon_Pointing(xAOD::PhotonContainer &photons,   HG::PhotonHandler *_photonHandler, double PVz)
{
	for (auto photon: photons) {
		//_photonHandler->correctPrimaryVertex(photon, PVz);
		//xAOD::PVHelpers::correctPrimaryVertex(*photon, PVz);
	}
}


  
void ZGam_Overlapremoval(HG::OverlapRemovalHandler *_overlapremovalHandler, HG::Config &config, xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::PhotonContainer &photons, xAOD::JetContainer &jets)
{
  // Split Muons into Staco and Calo
  xAOD::MuonContainer calo(SG::VIEW_ELEMENTS);
  xAOD::MuonContainer staco(SG::VIEW_ELEMENTS);
  for (auto muon: muons)
  {
    if (muon->muonType() == xAOD::Muon::MuonType::CaloTagged)
      calo.push_back(muon);
    else
      staco.push_back(muon);
  }
  
  // Staco staco overlap removal
  _overlapremovalHandler->removeOverlap(staco, staco, config.getNum("OverlapRemoval.HZG.StacoMu_StacoMu_DR"));
  
  // Electron/Electron track matching OR
  for (auto elec1=electrons.rbegin(); elec1!=electrons.rend(); ++elec1)
  {
    double d0_1     = (*elec1)->trackParticle()->d0();
    double z0_1     = (*elec1)->trackParticle()->z0();
    double phi_1    = (*elec1)->trackParticle()->phi();
    double qoverp_1 = (*elec1)->trackParticle()->qOverP();
    double Eta_1    = (*elec1)->trackParticle()->eta();
    double Et_1     = (*elec1)->caloCluster()->e()/cosh(Eta_1);
    
    for (auto elec2: electrons)
    {
      if ((*elec1) == elec2) break;
      double d0_2     = elec2->trackParticle()->d0();
      double z0_2     = elec2->trackParticle()->z0();
      double phi_2    = elec2->trackParticle()->phi();
      double qoverp_2 = elec2->trackParticle()->qOverP();
      double Eta_2    = elec2->trackParticle()->eta();
      double Et_2     = elec2->caloCluster()->e()/cosh(Eta_2);
      
      if (d0_1 == d0_2 && z0_1 == z0_2 && phi_1 == phi_2 && qoverp_1 == qoverp_2 &&  Et_1 < Et_2)
      {
	// Particles Identical, so remove ET1
	// std::cout << "NPR: OR: pt elec1, elec2 = " << Et_1 << ", " << Et_2 << std::endl;
	electrons.erase(elec1.base() - 1);
	break;
      }
    }
  }

  // Electron/Electron calo direction OR
  _overlapremovalHandler->removeOverlap(electrons, config.getNum("OverlapRemoval.HZG.Electron_Electron_DEta"), config.getNum("OverlapRemoval.HZG.Electron_Electron_DPhi"));
  

  // Electron/Staco Muon overlap removal
  for (auto elec=electrons.rbegin(); elec!=electrons.rend(); ++elec)
  {
    double el_eta = (*elec)->trackParticle()->eta();
    double el_phi = (*elec)->trackParticle()->phi();

    for (auto muon : muons)
    {
      const xAOD::TrackParticle* checkMuTrk =  muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);

      // if track doesn't exist, use the combined one - this is what happened in the D3PD
      double mu_eta = -1.0;
      double mu_phi = -1.0;

      if(checkMuTrk)
      {
	mu_eta = -TMath::Log(TMath::Tan(checkMuTrk->theta()*0.5));
	mu_phi = checkMuTrk->phi();
      }
      else
      {
	mu_eta = muon->eta();
	mu_phi = muon->phi();
      }

      // Now evaluate DR (HGamAnalysisFramework takes two xAOD particles, but I need to use these parameters only)
      double eta2     = (mu_eta-el_eta)*(mu_eta-el_eta);
      double tmp_dphi = (fabs(mu_phi-el_phi) > TMath::Pi()) ? 2*TMath::Pi()-fabs(mu_phi-el_phi) : fabs(mu_phi-el_phi);
      double_t phi2   = tmp_dphi*tmp_dphi;
      if ( sqrt( eta2 + phi2 ) < config.getNum("OverlapRemoval.HZG.Electron_StacoMu_DR"))
      {
	// Overlap, erase electron
	//std::cout << "NPR: OR: Pt elec, muon = " << (*elec)->pt() << ", " << muon->pt() << std::endl;
	electrons.erase(elec.base() - 1);
	break;
      }
    }
  }
  
  _overlapremovalHandler->removeOverlap(calo, staco, config.getNum("OverlapRemoval.HZG.CaloMu_StacoMu_DR"));
  
  _overlapremovalHandler->removeOverlap(calo, electrons, config.getNum("OverlapRemoval.HZG.CaloMu_Electron_DR"));
  
  _overlapremovalHandler->removeOverlap(photons, electrons, config.getNum("OverlapRemoval.HZG.Photon_Electron_DR"));
  _overlapremovalHandler->removeOverlap(photons, calo     , config.getNum("OverlapRemoval.HZG.Photon_CaloMu_DR"));
  _overlapremovalHandler->removeOverlap(photons, staco    , config.getNum("OverlapRemoval.HZG.Photon_StacoMu_DR"));
  
  //_overlapremovalHandler->removeOverlap(jets, electrons, config.getNum("OverlapRemoval.HZG.Jet_Electron_DR"));
  //_overlapremovalHandler->removeOverlap(jets, photons,   config.getNum("OverlapRemoval.HZG.Jet_Photon_DR"));
  //_overlapremovalHandler->removeOverlap(jets, calo,      config.getNum("OverlapRemoval.HZG.Jet_CaloMu_DR"));
  //_overlapremovalHandler->removeOverlap(jets, staco,     config.getNum("OverlapRemoval.HZG.Jet_StacoMu_DR"));
  
  muons.clear();
  for (auto muon : calo ) muons.push_back(muon);
  for (auto muon : staco) muons.push_back(muon);
  muons.sort(HG::MuonHandler::comparePt);
  
  //if (muons.size()     != nMuons)    std::cout << "deleted muons:     Now " << muons.size() << std::endl;
  //if (electrons.size() != nElectrons)std::cout << "deleted electrons: Now " << electrons.size() << std::endl;
  //if (photons.size()   != nPhotons)  std::cout << "deleted photons:   Now " << photons.size() << std::endl;
  //std::cout << "Done" << std::endl;
  //std::cout << "Final  : " << muons.size() << ", " << electrons.size() << ", " << photons.size() << std::endl;
}


bool isTruthPhoton(const xAOD::Photon *ph)
{
  const xAOD::TruthParticle* truthph = xAOD::TruthHelpers::getTruthParticle(*ph);
  if(!truthph||abs(truthph->pdgId())!=22 ||  (truthph->barcode() > 1000000)) return false;
  else return true;
}

bool isTruthElectron(const xAOD::Electron *el)
{
  const xAOD::TruthParticle* truthelectron = xAOD::TruthHelpers::getTruthParticle(*el);
  if(!truthelectron||abs(truthelectron->pdgId())!=11) return false;
  else return true;
}
bool isTruthMuon(const xAOD::Muon *mu) 
 {
  const xAOD::TrackParticle* trackParticle = mu->primaryTrackParticle();
  const xAOD::TruthParticle* truthmuon = xAOD::TruthHelpers::getTruthParticle(*trackParticle);
  if(!truthmuon||abs(truthmuon->pdgId())!=13) return false;
  else return true;  
 }

// output = boost v1 to v2 rest frame
void lorentzboost(TLorentzVector vector1, TLorentzVector vector2, TLorentzVector &output)
{

  Double_t bx=-vector2.Px()/vector2.E();
  Double_t by=-vector2.Py()/vector2.E();
  Double_t bz=-vector2.Pz()/vector2.E();
  Double_t bp2=bx*bx+by*by+bz*bz;
  Double_t gamma=(bp2>=1)?0.0:1.0 / sqrt(1.0 - bp2);
  Double_t bgamma=gamma * gamma / (1.0 + gamma);

  Double_t px = (1+bgamma*bx*bx) *vector1.Px()+(bgamma*bx*by) *vector1.Py()+(bgamma*bx*bz) *vector1.Pz()+(gamma*bx) *vector1.E();
  Double_t py =(bgamma*bx*by) *vector1.Px()+(1+bgamma*by*by) *vector1.Py()+(bgamma*by*bz) *vector1.Pz()+(gamma*by) *vector1.E();
  Double_t pz = (bgamma*bx*bz) *vector1.Px()+(bgamma*by*bz) *vector1.Py()+(1+bgamma*bz*bz)*vector1.Pz()+(gamma*bz) *vector1.E();
  Double_t e = (gamma*bx) *vector1.Px()+(gamma*by) *vector1.Py()+(gamma*bz) *vector1.Pz()+gamma *vector1.E();
  output.SetPxPyPzE(px,py,pz,e);
}

// output = v1 rotate the coordinates to v2 (in new frame, the eula angle of v2' = (0,0,0)) 
void lorentzrotation(TLorentzVector vector1, TLorentzVector vector2, TLorentzVector &output)
{
  Double_t pt=sqrt(vector2.Px()*vector2.Px()+vector2.Py()*vector2.Py());
  Double_t pp=sqrt(vector2.Px()*vector2.Px()+vector2.Py()*vector2.Py()+vector2.Pz()*vector2.Pz());

  Double_t sp;
  Double_t cp;
  Double_t st;
  Double_t ct;

  if(pt==0.0){
    sp = 0.0;
    cp = 1.0;
  } else {
    sp = vector2.Py()/pt;
    cp = vector2.Px()/pt;
  }

  if(pp==0.0){
    st = 0.0;
    ct = 1.0;
  } else {
    st = pt/pp;
    ct = vector2.Pz()/pp;
  }

  Double_t px = cp*ct *vector1.Px()+sp*ct *vector1.Py()-st *vector1.Pz();
  Double_t py = -sp *vector1.Px()+cp *vector1.Py();
  Double_t pz = cp*st *vector1.Px()+sp*st*vector1.Py()+ct *vector1.Pz();
  Double_t e = vector1.E();
  output.SetPxPyPzE(px,py,pz,e);

 }
