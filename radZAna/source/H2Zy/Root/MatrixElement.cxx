#include "H2Zy/MatrixElement.h"
#include "H2Zy/Object_llg.h"
#include "HGamAnalysisFramework/HgammaUtils.h"
#include "H2Zy/CPPProcess_P1_Sigma_heft_with_hza_gg_epema_ggH.h"
#include "H2Zy/CPPProcess_P1_Sigma_sm_ddx_epema_Bkg.h"
#include "H2Zy/CPPProcess_P1_Sigma_sm_uux_epema_Bkg.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TH2.h"
#include "TFile.h"
#include "TChain.h"
#include <fstream>
#include <iostream>
#include <memory>

namespace HZG {

  void Object_llg::setMatrixElement( TString postfix ) {

    Map_float["logMatrixElement_ggH"] = -999.;
    Map_float["logMatrixElement_bkg_dd"] = -999.;
    Map_float["logMatrixElement_bkg_ss"] = -999.;
    Map_float["logMatrixElement_bkg_uu"] = -999.;
    Map_float["logMatrixElement_bkg_cc"] = -999.;
    Map_float["logMatrixElement_bkg_Mix"] = -999.;
    Map_float["logMatrixElement_KDvalue"] = -999.;

    if(Map_float["ph_pt"]!=0){
      std::vector<std::shared_ptr<ZyMGMEHelperBase>> ZyMGMEHelpers;
      std::string param_card = (std::string)PathResolverFindCalibFile("H2Zy/param_card_PROC_SA_CPP_heft_H_Zy_lly.dat");

      std::map<int, double> obj_4m;
      obj_4m[0] = Map_float["l1_pt"+postfix];
      obj_4m[1] = Map_float["l1_eta"+postfix];
      obj_4m[2] = Map_float["l1_phi"+postfix];
      obj_4m[4] = Map_float["l2_pt"+postfix];
      obj_4m[5] = Map_float["l2_eta"+postfix];
      obj_4m[6] = Map_float["l2_phi"+postfix];
      obj_4m[8] = Map_float["ph_pt"];
      obj_4m[9] =  Map_float["ph_eta"];
      obj_4m[10] = Map_float["ph_phi"];
      obj_4m[11] = 0.;
      if(Map_int["EventInfo.channel"]==1) { obj_4m[3] = 0.00051; obj_4m[7] = 0.00051;}
      else if(Map_int["EventInfo.channel"]==2) { obj_4m[3] = 0.106; obj_4m[7] = 0.106;}

      try {
	ZyMGMEHelpers.clear();
	param_card = (std::string)PathResolverFindCalibFile("H2Zy/param_card_PROC_SA_CPP_heft_H_Zy_lly.dat");
	ZyMGMEHelpers.push_back( ZyMGMEHelperBase::build<CPPProcess_P1_Sigma_heft_with_hza_gg_epema_ggH>(param_card, obj_4m) );
	param_card = (std::string)PathResolverFindCalibFile("H2Zy/param_card_PROC_SA_CPP_sm_Zy_lly.dat");
	ZyMGMEHelpers.push_back( ZyMGMEHelperBase::build<CPPProcess_P1_Sigma_sm_ddx_epema_Bkg>(param_card, obj_4m) );
	ZyMGMEHelpers.push_back( ZyMGMEHelperBase::build<CPPProcess_P1_Sigma_sm_uux_epema_Bkg>(param_card, obj_4m) );
      } catch (const char* e) {
	std::cout << "Exception: " << e << ". Parameter card: " << param_card << std::endl;
      }

      vector<double> ME_temp ;
      vector<double> logME_temp;

      int Ncount = 0 ;
      double ME_square = 0;
      for(auto tH : ZyMGMEHelpers) {

	tH->setMomenta(obj_4m);
	ME_temp.clear();
	logME_temp.clear();
	tH->testMe(ME_temp, logME_temp);

	for(int iproc = 0; iproc < (int) logME_temp.size(); iproc++){

	  if(Ncount==0) Map_float["logMatrixElement_ggH"] = logME_temp[iproc];
	  else if (Ncount==1) Map_float["logMatrixElement_bkg_dd"] = logME_temp[iproc];
	  else if (Ncount==2) Map_float["logMatrixElement_bkg_ss"] = logME_temp[iproc];
	  else if (Ncount==3) Map_float["logMatrixElement_bkg_uu"] = logME_temp[iproc];
	  else if (Ncount==4) Map_float["logMatrixElement_bkg_cc"] = logME_temp[iproc];	  

	  //std::cout<<" ME = "<<ME_temp[iproc]<<" ; logME = "<<logME_temp[iproc]<<std::endl;
	  //std::cout<<" Filling 2D = "<<Map_float["llg_m"+postfix]<<" " <<logME_temp[iproc]<<" "<<Map_float["mc_weight_final"]<<" "<<Map_float["l1_pt"+postfix]<<" "
	  //  <<Map_float["l2_pt"+postfix]<<" "<<Map_float["ph_pt"]<<" "<<Map_int["EventInfo.channel"]<<std::endl;

	  ME_square += ME_temp[iproc]*ME_temp[iproc];
	  Ncount++;
	}
      }
      Map_float["logMatrixElement_bkg_Mix"] =  log(ME_square) / 2;
      Map_float["logMatrixElement_KDvalue"] = 2 * ( Map_float["logMatrixElement_ggH"] - Map_float["logMatrixElement_bkg_Mix"]) ;
    }
  }
}
