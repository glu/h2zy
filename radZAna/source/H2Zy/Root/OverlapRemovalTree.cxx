
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/OverlapRemovalTree.h"
#include "TString.h"
#include "TTree.h"

namespace HZG
{
  OverlapRemovalTree::OverlapRemovalTree(TString TreeName)
  {
    m_OverlapRemoval = new TTree(TreeName, TreeName);
    m_OverlapRemoval->Branch("m_electron",         &m_electron);
    m_OverlapRemoval->Branch("m_electron_z0",      &m_electron_z0);
    m_OverlapRemoval->Branch("m_electron_isTruth", &m_electron_isTruth);
    
    m_OverlapRemoval->Branch("m_muon",         &m_muon);
    m_OverlapRemoval->Branch("m_muon_d0",      &m_muon_d0);
    m_OverlapRemoval->Branch("m_muon_z0",      &m_muon_z0);
    m_OverlapRemoval->Branch("m_muon_isTruth", &m_muon_isTruth);
    m_OverlapRemoval->Branch("m_muon_isStaco", &m_muon_isStaco);

    m_OverlapRemoval->Branch("m_photon",         &m_photon);
    m_OverlapRemoval->Branch("m_photon_isTruth", &m_photon_isTruth);
    
    m_OverlapRemoval->Branch("m_jet", &m_jet);
  }
  
  OverlapRemovalTree::~OverlapRemovalTree()
  {
    delete m_OverlapRemoval;
  }
  
  void OverlapRemovalTree::Fill(xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::PhotonContainer &photons, xAOD::JetContainer &jets)
  {
    // Clear all vectors
    m_electron.clear();
    m_electron_z0.clear();
    m_electron_isTruth.clear();
    
    m_muon.clear();
    m_muon_d0.clear();
    m_muon_z0.clear();
    m_muon_isTruth.clear();
    m_muon_isStaco.clear(); 

    m_photon.clear();
    m_photon_isTruth.clear();
    
    m_jet.clear();

    // Now populate the vectors
    for (auto elec : electrons)
    {
      m_electron.push_back (elec->p4() );
      m_electron_z0.push_back (elec->trackParticle()->z0());
      m_electron_isTruth.push_back(isTruthElectron(elec));
    }

    for (auto muon : muons)
    {
      m_muon.push_back (muon->p4() );
      m_muon_d0.push_back(muon->primaryTrackParticle()->d0());
      m_muon_z0.push_back(muon->primaryTrackParticle()->z0());
      m_muon_isTruth.push_back(isTruthMuon(muon));
      m_muon_isStaco.push_back( (muon->muonType() != xAOD::Muon::MuonType::CaloTagged));
    }

    for (auto gam : photons)
    {
      m_photon.push_back (gam->p4() );
      m_photon_isTruth.push_back(isTruthPhoton(gam));
    }
    
    for (auto jet : jets)
    {
      m_jet.push_back (jet->p4() );
    }

    m_OverlapRemoval->Fill();
  }
}
