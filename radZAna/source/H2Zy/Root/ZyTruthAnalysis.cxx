#include "H2Zy/ZyTruthAnalysis.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/PDF.h"
#include "LHAPDF/PDFSet.h"
#include "LHAPDF/Reweighting.h"

// this is needed to distribute the algorithm to the workers
ClassImp(ZyTruthAnalysis)



ZyTruthAnalysis::ZyTruthAnalysis(const char *name)
: HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_mclumi                             = 0;
}

EL::StatusCode ZyTruthAnalysis::initialize ()
{
	HgammaAnalysis::initialize();
	std::string upmenu = "/../";
	m_mclumi = new MCLumi((std::string)PathResolverFindCalibFile("H2Zy/MC16_lumi.txt"));

	m_pdfs0.clear();
	m_pdfs1.clear();
	const LHAPDF::PDFSet pdfset0("CT10");
	const LHAPDF::PDFSet pdfset1("MSTW2008lo68cl");


	m_pdfs0 = pdfset0.mkPDFs();
	m_pdfs1 = pdfset1.mkPDFs();


	return EL::StatusCode::SUCCESS;
}

ZyTruthAnalysis::~ZyTruthAnalysis()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode ZyTruthAnalysis::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  //histoStore()->createTH1F("m_yy", 60, 110, 140);
  m_outputTree = new TTree("Truthtree","Truthtree");
  m_outputTree->Branch("m_llg", &m_llg);
  m_outputTree->Branch("m_ll", &m_ll);
  m_outputTree->Branch("weight", &weight);
  m_outputTree->Branch("m_mc_channel_number", &m_mc_channel_number);
  m_outputTree->Branch("mc_genxsec", &mc_genxsec);
  m_outputTree->Branch("mc_genfeff", &mc_genfeff);
  m_outputTree->Branch("m_mc_weight_xs", &m_mc_weight_xs);
  m_outputTree->Branch("m_mc_totevent", &m_mc_totevent);
  m_outputTree->Branch("mc_weight_PDF0", &mc_weight_PDF0);
  m_outputTree->Branch("mc_weight_PDF1", &mc_weight_PDF1);
  m_outputTree->Branch("channel", &channel);

  wk()->addOutput (m_outputTree);


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZyTruthAnalysis::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  //HgammaAnalysis::execute();

	const xAOD::TruthEventContainer* truthE=0;
	if (!event()->retrieve(truthE,"TruthEvents")) {
		Error("execute()","Failed to retrieve truthevent container.");
		return EL::StatusCode::FAILURE;
	}

	const xAOD::TruthEvent* mcEvent=*(truthE->begin());
	weight=mcEvent->weights().at(0);
	m_mc_channel_number = eventInfo()->runNumber();
	mc_genxsec  = m_mclumi->getXsec(m_mc_channel_number)*1.e6; // in fb
	mc_genfeff  = m_mclumi->getFEff(m_mc_channel_number);
	m_mc_weight_xs = mc_genxsec*mc_genfeff;
	m_mc_totevent = m_mclumi->getEvts(m_mc_channel_number);

        //------ cut flow ----
        hist_cutflow_name = Form("cutflow_%d",(int) m_mc_channel_number);
	if(m_cutflowhistoTH1F.count(hist_cutflow_name)==0){
		m_cutflowhistoTH1F[Form("%s", hist_cutflow_name.Data())] = new TH1F(Form("%s", hist_cutflow_name.Data()), Form("%s", hist_cutflow_name.Data()), 10, 0, 10.);
		wk()->addOutput(m_cutflowhistoTH1F[Form("%s", hist_cutflow_name.Data())]);
		m_cutflowhistoTH1F[Form("%s_w", hist_cutflow_name.Data())] = new TH1F(Form("%s_w", hist_cutflow_name.Data()), Form("%s_w", hist_cutflow_name.Data()), 10, 0, 10.);
		wk()->addOutput(m_cutflowhistoTH1F[Form("%s_w", hist_cutflow_name.Data())]);
		m_cutflowhistoTH1F[Form("%s_w2", hist_cutflow_name.Data())] = new TH1F(Form("%s_w2", hist_cutflow_name.Data()), Form("%s_w2", hist_cutflow_name.Data()), 10, 0, 10.);
		wk()->addOutput(m_cutflowhistoTH1F[Form("%s_w2", hist_cutflow_name.Data())]);
	}

        //----- pdf set ----
	if(m_pdfs0.size()>0){
		std::vector<float> weights0;
		std::vector<float> weights1;

		float x1 = truthE->at(0)->pdfInfo().x1;
		float x2 = truthE->at(0)->pdfInfo().x2;
		float Q = truthE->at(0)->pdfInfo().Q;
		int pdgId1 = truthE->at(0)->pdfInfo().pdgId1;
		int pdgId2 = truthE->at(0)->pdfInfo().pdgId2;

		for (size_t imem = 0; imem <= m_pdfs0.size()-1; imem++) {
			float w = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs0[0], m_pdfs0[imem] ) ;
			weights0.push_back( w );
		}

		//for (size_t imem = 0; imem <1; imem++) {
		//	float w = (float) LHAPDF::weightxxQ2( pdgId1, pdgId2, x1, x2, Q*Q, m_pdfs0[0], m_pdfs1[imem] ) ;
		//	weights1.push_back( w );
		//}

		mc_weight_PDF0 = weights0;
		mc_weight_PDF1 = weights1;
	}

	//------

	const xAOD::TruthParticleContainer* mc_truth_electrons = 0;
	if(!event()->retrieve(mc_truth_electrons, "TruthElectrons" ).isSuccess()){
		Error("execute()", "Failed to retrieve TruthElectrons container." );
	}

	const xAOD::TruthParticleContainer* mc_truth_muons = 0;
	if(!event()->retrieve(mc_truth_muons, "TruthMuons" ).isSuccess()){
		Error("execute()", "Failed to retrieve TruthMuons container." );
	}


	const xAOD::TruthParticleContainer* mc_truth_photons = 0;
	if(!event()->retrieve(mc_truth_photons, "TruthPhotons" ).isSuccess()){
		Error("execute()", "Failed to retrieve TruthPhotons container." );
	}

	TLorentzVector p_l1, p_l2, p_gam;
	channel=0;
	if(mc_truth_electrons->size()>=2){
		p_l1 = mc_truth_electrons->at(0)->p4();
		p_l2 = mc_truth_electrons->at(1)->p4();
		channel=1;
	}
	else if(mc_truth_muons->size()>=2){
		p_l1 = mc_truth_muons->at(0)->p4();
		p_l2 = mc_truth_muons->at(1)->p4();
		channel=2;
	}
	else  {return EL::StatusCode::SUCCESS; }

	p_gam = mc_truth_photons->at(0)->p4();

	fillcutflow(hist_cutflow_name, 0, weight);
        //------ truth selection----
        if(p_l1.Pt()/1000<10)  return EL::StatusCode::SUCCESS;
        if(p_l2.Pt()/1000<10)  return EL::StatusCode::SUCCESS;
        if(p_gam.Pt()/1000<10)  return EL::StatusCode::SUCCESS;
	fillcutflow(hist_cutflow_name, 1, weight);

	if(channel==1){
		if(fabs(p_l1.Eta())>2.47 || ( fabs(p_l1.Eta())>1.37  && fabs(p_l1.Eta())<1.52) )  return EL::StatusCode::SUCCESS;
		if(fabs(p_l2.Eta())>2.47 || ( fabs(p_l2.Eta())>1.37  && fabs(p_l2.Eta())<1.52) )  return EL::StatusCode::SUCCESS;
	}
	else if(channel==2){
		if(fabs(p_l1.Eta())>2.7 )  return EL::StatusCode::SUCCESS;
		if(fabs(p_l2.Eta())>2.7 )  return EL::StatusCode::SUCCESS;
	}
	else return EL::StatusCode::SUCCESS;

	if(fabs(p_gam.Eta())>2.37 || ( fabs(p_gam.Eta())>1.37  && fabs(p_gam.Eta())<1.52) )  return EL::StatusCode::SUCCESS;
	fillcutflow(hist_cutflow_name, 2, weight);

	if((p_l1+p_l2).M()/1000 < 76.18)  return EL::StatusCode::SUCCESS;
	if((p_l1+p_l2).M()/1000 > 106.18)  return EL::StatusCode::SUCCESS;
	if(p_gam.Pt()/(p_l1+p_l2+p_gam).M() < 0.3)  return EL::StatusCode::SUCCESS;
	fillcutflow(hist_cutflow_name, 3, weight);

	//--------
	
	m_ll=((p_l1+p_l2).M())/1000.;
	m_llg=((p_l1+p_l2+p_gam).M())/1000.;
	m_outputTree->Fill();

	//std::cout<<config()->getStr("TruthEvents.ContainerName","TruthEvents")<<std::endl;

  //xAOD::PhotonContainer photons = photonHandler()->getCorrectedContainer();
  //if (photons.size() < 2) return EL::StatusCode::SUCCESS;
  //TLorentzVector h = photons[0]->p4() + photons[1]->p4();
  //histoStore()->fillTH1F("m_yy", h.M()/HG::GeV);

  return EL::StatusCode::SUCCESS;
}

void ZyTruthAnalysis::fillcutflow(TString histname, int icut, double _weight)
{
	m_cutflowhistoTH1F[Form("%s", histname.Data())]->Fill(icut); 
	m_cutflowhistoTH1F[Form("%s_w", histname.Data())] ->Fill(icut, _weight); 
	m_cutflowhistoTH1F[Form("%s_w2", histname.Data())] ->Fill(icut, _weight*_weight) ;
}
