#ifndef Container_llg_H
#define Container_llg_H

/********************
 * Container_llg: 
 *   class to read settings from text files
 *   relies on root's TEnv
 *
 * Usage:
 *   Container_llg settings("Hgamma.config");
 *   TString gamContainerName = settings.getStr("PhotonContainer");
 *   TString elContainerName  = settings.getStr("ElectronContainer");
 *   vector<TString> systShifts = settings.getStrV("Systematics");
 *
 */

#include <vector>
#include <algorithm>
#include <functional>
#include <TString.h>
#include <TEnv.h>
#include <TLorentzVector.h>

#include "H2Zy/Object_llg.h"


// \brief HZgamma namespace
namespace HZG {

  //! \brief typedef for a vector of strings (to save some typing)
  typedef std::vector<TString> StrV;
  
  class Container_llg {
  public:
    
    //! \brief Container_llg constructor
    //! \param fileName name of text file with user-specified settings
    Container_llg();
    HG::Config m_config;

    //----- Z mass correction
    MuonResMomSF* mu_resolSF ; //! 
    FSR::FsrPhotonTool *m_fsrTool;//!
    ToolHandle<ZMassConstraint::IConstraintFit> m_massConstraint; //!
    ToolHandle<CP::IEgammaCalibrationAndSmearingTool>  m_egammaCalibrationTool;//!
    ToolHandle<CP::IMuonCalibrationAndSmearingTool>    m_muonCalibrationTool; //!

    xAOD::PhotonContainer  *selected_photons;
    xAOD::ElectronContainer *selected_electrons;
    xAOD::MuonContainer *selected_muons;
    xAOD::JetContainer *selected_jets;
    xAOD::JetContainer *selected_loose_jets;
    xAOD::MissingETContainer *selected_met;

    xAOD::PhotonContainer  *photon_container;
    xAOD::ElectronContainer *electron_container;

#ifndef __CINT__
    //void initialize( std::vector< xAOD::Photon* > &photons, std::vector< xAOD::Electron* > &electrons, std::vector< xAOD::Muon* > &muons);
    void initialize(xAOD::PhotonContainer &photons, xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons,  xAOD::JetContainer &jets, xAOD::JetContainer &loose_jets, xAOD::MissingETContainer &met, xAOD::PhotonContainer &all_photons, xAOD::ElectronContainer &all_electrons);
//    template <class partType_ph, class partType_lepton> void initialize( std::vector< partType_ph* > &photons, std::vector< partType_lepton* > &leptons);
    template <class partType_ph, class partType_lepton> void initialize( partType_ph &photons, partType_lepton &leptons);
#endif 
    void setConfig(const HG::Config &config, bool _isMC);

    //! \brief checks the charge of the leptons in llg
    bool passcut(Object_llg &_object_llg);
    double get_maxmll();

    //! \brief  Compare Z Boson masses in the container
    static bool compare_dMll(const Object_llg &object1, const Object_llg &object2);
    static bool compare_dMll_Zmassconstraint(const Object_llg &object1, const Object_llg &object2);

    
    //! \brief sorting llg object algorithm
    void sort_dMll();

    //! \brief Container for the llg objects
    std::vector<Object_llg> m_container_llg;

    //! \brief Number of llg objects in the container
    int i_object_llg;


    TString sort_type; 

  private:
    
    // //! \brief method to abort program with error message
    //    void fatal(TString msg);

    //! \brief string defines the sorting method to be used
    //TString sort_type; 
    
  };


  //template <class partType_ph, class partType_lepton> void Container_llg::initialize(std::vector< partType_ph* > &photons, std::vector< partType_lepton* > &leptons)
  template <class partType_ph, class partType_lepton> void Container_llg::initialize( partType_ph &photons, partType_lepton &leptons)
   {

    int iph=0;
    if(photons.size()<1) iph=-1;
    for(int ilepton1=0; ilepton1<((int)leptons.size()-1); ilepton1++)
     {
      for(int ilepton2=ilepton1+1; ilepton2< (int)leptons.size(); ilepton2++)
       {
	Object_llg _object_llg;
	_object_llg.setConfig(m_config, m_egammaCalibrationTool, m_fsrTool, m_massConstraint, selected_photons, selected_electrons, selected_muons, selected_jets, selected_loose_jets, selected_met, photon_container, electron_container);
	if(iph==-1) _object_llg.initialize_ll(ilepton1, ilepton2, leptons[ilepton1], leptons[ilepton2]);
	else _object_llg.initialize_llg(iph, ilepton1, ilepton2, photons[iph], leptons[ilepton1], leptons[ilepton2]);

	if( passcut(_object_llg) ) m_container_llg.push_back(_object_llg);

	i_object_llg++;
       }
     }


   }


}

#endif
