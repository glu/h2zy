#ifndef H2Zy_EVENTFILL_H
#define H2Zy_EVENTFILL_H

#include <vector>
#include <set>
#include <TString.h>
#include <TEnv.h>
#include <TLorentzVector.h>

#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/TruthUtils.h"
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/Object_llg.h" 
#include "LHAPDF/PDF.h"

#include "HGamAnalysisFramework/PhotonHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"
#include "HGamAnalysisFramework/ETmissHandler.h"

namespace HZG {

  class EventFill{

    public:
      EventFill();
      void initialize(const HG::Config &config, const xAOD::EventInfo *_eventinfo, xAOD::TEvent *_event, Bmap *_beventmap, Imap *_ieventmap, UImap *_uieventmap, ULLmap *_ulleventmap, Fmap *_feventmap, IVmap *_iveventmap, FVmap *_fveventmap,  TLVVmap *_tlvveventmap);
      void SaveMCWeights(vector<LHAPDF::PDF*> m_pdfs, vector<LHAPDF::PDF*> m_pdfs_mstw, vector<LHAPDF::PDF*> m_pdfs_mmht);
      void seteventInfo(HG::EventHandler *_eventHandler);
      int retrievePV();
      void retrieveVtxSumPt(xAOD::ElectronContainer &electrons, xAOD::MuonContainer  &muons);
      const xAOD::Vertex * removeElectronsFromVertex(xAOD::VertexContainer * output, const xAOD::VertexContainer * input, xAOD::ElectronContainer & toRemove, xAOD::MuonContainer  &toRemoveMuon);
      void RecSave(HG::EventHandler *_eventHandler, Object_llg *&_llg, HG::PhotonHandler *_photonHandler, HG::ElectronHandler *_electronHandler, HG::MuonHandler *_muonHandler, xAOD::PhotonContainer &_photons,  xAOD::ElectronContainer &_electrons, xAOD::MuonContainer &_muons);
      void highmuoncheck(xAOD::MuonContainer &_muons);
      bool isOkMuon(xAOD::Muon& muon);

      //retrieve containers
      xAOD::PhotonContainer all_photon_container (HG::PhotonHandler *_photonHandler) ; //!
      xAOD::ElectronContainer all_electron_container (HG::ElectronHandler *_electronHandler); //!
      xAOD::PhotonContainer photon_container (HG::PhotonHandler *_photonHandler) ; //!
      xAOD::ElectronContainer electron_container (HG::ElectronHandler *_electronHandler); //!
      xAOD::MuonContainer muon_container (HG::MuonHandler *_muonHandler); //!
      xAOD::MuonContainer muon_clean_container (HG::MuonHandler *_muonHandler, xAOD::MuonContainer muons0); //!
      xAOD::JetContainer loose_jet_container(HG::JetHandler *_jetHandler)   ; //!
      xAOD::JetContainer jet_container(xAOD::JetContainer _loose_jets); //!      
      xAOD::MissingETContainer met_container(HG::ETmissHandler *_etmissHandler, HG::JetHandler *_jetHandler, xAOD::PhotonContainer _photons, xAOD::ElectronContainer _electrons, xAOD::MuonContainer _muons); //!

      void JetCleaningSave(HG::JetHandler *_jetHandler);  //!
      void TruthMETSave(HG::TruthHandler *_truthHandler, TString sysname); //!

    private:  
      const xAOD::EventInfo *m_eventinfo; //!
      HG::EventHandler *m_eventHandler; //!
      xAOD::TEvent *m_event; //!
      HG::Config m_config; //!

      // something wrong if define jets here.. 
      /*
      xAOD::JetContainer all_correctedjets; //!
      xAOD::JetContainer loose_jets; //!
      xAOD::JetContainer jets; //!
      xAOD::JetContainer noclean_loose_jets; //!
      xAOD::JetContainer noclean_jets; //!
      */

      xAOD::PhotonContainer all_correctedphotons; //!
      xAOD::ElectronContainer all_correctedelectrons; //!
      xAOD::PhotonContainer photons; //!
      xAOD::ElectronContainer electrons; //!
      xAOD::MuonContainer all_correctedmuons; //!
      xAOD::MuonContainer muons; //!
      xAOD::MissingETContainer all_met; //!
      xAOD::MissingETContainer met; //!
      xAOD::MissingETContainer all_truth_met ; //!
      xAOD::MissingETContainer truth_met; //!

      float mass_scale;  //!

      Fmap *m_feventmap; //!
      Imap *m_ieventmap; //!
      UImap *m_uieventmap;  //!
      ULLmap *m_ulleventmap;  //!
      Bmap *m_beventmap; //!
      IVmap *m_iveventmap; //!
      FVmap *m_fveventmap; //!
      TLVVmap *m_tlvveventmap; //!

      //----------
      bool isMC; //!
      //----------

      double m_pileupweight;        //!
      double m_vertexweight;        //!
      double m_mcweight; //!
      double m_initialWeight; //!
      float m_mu; //!

  };

}

#endif
