#ifndef H2Zy_H2ZyAnalysis_H
#define H2Zy_H2ZyAnalysis_H

#include <EventLoop/Worker.h>
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"

#include "H2Zy/TruthSelect.h"
#include "H2Zy/EventFill.h"
#include "H2Zy/Variables.h"
#include "H2Zy/Object_llg.h"
#include "H2Zy/Container_llg.h"
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/MCLumi.h"
#include "H2Zy/OverlapRemovalTree.h"

#include "xAODEgamma/PhotonFwd.h"
#include "xAODEgamma/ElectronFwd.h"

#include "LHAPDF/PDF.h"

#ifndef __CINT__
#include "xAODMuon/Muon.h"
#endif 

#include <vector>
#include <set>
#include <utility>

class H2ZyAnalysis : public HgammaAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  enum CutEnum { xAOD=0, DxAOD=1,ALLEVTS=2,
		 ALLEVTS_NOPU=3, GRL=4, PV=5, EVENT_QUALITY=6, Triggers=7,  Initial_sel=8, 
		 twolepton=9, mll_threshold=10, twolepton_onephoton=11, pre_sel=12, llgcut=13, TRIGGER_MATCH=14,
		 mllcut=15, Ph_ptcut1=16, Ph_ID=17, Ph_ISO=18, MASSCUT=19, Ph_ptcut2=20, PASSALL=21 };
  
  CutEnum m_cutFlow; //!
  int istep_event ; //!
  int istep_weight ; //!
  xAOD::PhotonContainer photons; //!
  xAOD::ElectronContainer electrons; //!
  xAOD::MuonContainer muons0 ; //!
  xAOD::MuonContainer muons ; //!
  xAOD::JetContainer jets   ; //!
  xAOD::JetContainer loose_jets   ; //!
  xAOD::MissingETContainer met;//!
  xAOD::PhotonContainer all_correctedphotons; //!
  xAOD::ElectronContainer all_correctedelectrons; //!

  // output trees
  TTree *m_outputTree;      //!
  TTree *m_summaryTree;     //!
  TTree *m_truthTree;       //!
  
  // output histo with sum of weight info
  TH1D *m_sumW;            //!

  std::map<TString, TTree*> map_outputTree;          //!

  HZG::OverlapRemovalTree *m_ParticlesTreeBeforeOR; //!
  HZG::OverlapRemovalTree *m_ParticlesTreeAfterOR;  //!

#ifndef __CINT__
  std::vector< xAOD::Muon* > m_selMuons;  //!
#endif // __CINT__    
  std::vector< xAOD::Electron* > m_selElectrons;  //!
  std::vector< xAOD::Photon* > m_selPhotons;      //!

  HZG::TruthSelect m_truthselector; //!
  HZG::EventFill m_eventfill; //!

  // bool to keep track of correct initialization order
  bool m_initialized;          //!

  HG::StrV m_list_of_requiredTriggers; //!

  // information on initial and final events and sum of weights for DAODs
  double m_finalSumOfWeights;   //!
  double m_initialSumOfWeights; //!
  double m_finalSumOfWeightsSquared;   //!
  double m_initialSumOfWeightsSquared; //!
  double m_finalEvents;         //!
  double m_initialEvents;       //!
  double m_initialWeight;       //!
  double m_mcweight;            //!

  bool m_newFile; //!
  unsigned int nEventsProcessed ;  //!
  double sumOfWeights           ;  //!
  double sumOfWeightsSquared    ;  //!

  unsigned int nEventsDxAOD       ;  //!
  double sumOfWeightsDxAOD        ;  //!
  double sumOfWeightsSquaredDxAOD ;  //!

  TH1F* m_histo_cutflow;                        //!  
  TH1F* m_histo_cutflow_wt;                     //!  
  //std::map<TString,TH1F*> m_histoTH1F;          //!
  std::map<TString,TH1F*> m_cutflowhistoTH1F;          //!
  std::map<int, TString> m_event_cutflow_name;  //!
  std::map<int, TString> m_event_weight_name;   //!
  std::map<int, int> m_mcchannel_state;  //!
  TString histweight_name; //!
  TString hist_cutflow_name; //!

  // Trigger pass
  TH1F* m_histo_trigger_pass;                     //!
  TH1F* m_histo_trigger_pass_wt;                  //!
  std::map<int, TString> m_event_trigger_name;   //!
  TH1F* m_histo_trigger_denom;                   //!
  TH1F* m_histo_trigger_denom_wt;                //!

  // Trigger match
  TH1F* m_histo_trigger_pass_match;                     //!
  TH1F* m_histo_trigger_pass_match_wt;                  //!
  TH1F* m_histo_trigSF;                                 //!

  int m_set_outputtree; //!
  Dmap m_deventmap; //!
  Fmap m_feventmap; //!
  Imap m_ieventmap; //!
  UImap m_uieventmap; //!
  ULLmap m_ulleventmap; //!
  Bmap m_beventmap; //!
  Bmap m_btreemap; //!

  IVmap m_iveventmap;
  FVmap m_fveventmap;
  TLVVmap m_tlvveventmap;

  vector<TString> m_vbn;

  HZG::Container_llg V_llg; //!
  HZG::Object_llg *cand_llg; //!

  CP::IsolationSelectionTool           *m_isoTool_loose;  //!
  CP::IsolationSelectionTool           *m_isoTool_medium; //!
  CP::IsolationSelectionTool           *m_isoTool_tight;  //!
  CP::IsolationHelper* m_isoHelper; //!
  vector<xAOD::Iso::IsolationType> IsoORtypes; //!

  vector<LHAPDF::PDF*> m_pdfs; //!
  vector<LHAPDF::PDF*> m_pdfs_mstw;
  vector<LHAPDF::PDF*> m_pdfs_mmht;
  vector<LHAPDF::PDF*> m_pdfs_nn;

  //--- event info
  bool m_isDerivation;
  int m_channel;                         //! 1: eegamma; 2: mumugamma
  int m_hasDphoton;
  int m_hasttyphoton;
  unsigned int m_runNumber;              //! run number
  unsigned int m_eventNumber;            //! event number
  unsigned int m_lbn;                    //! lumi block number
  unsigned int m_nGoodCands;             //! the number of good candidates (llg triplets, photons, ...)

  //--- event selection flag --
  bool m_pass_grl;
  bool m_pass_pv;
  bool m_pass_quality;
  int m_nMuons;
  int m_nElectrons;
  int m_nPhotons;

  // - mc info
  unsigned int m_mc_channel_number;      //! mc channel number
  int m_mc_Year;                         //! mc Year
  float m_mc_weight_xs;                  //! cross-section*Br
  float m_mc_totevent;                   //! totevent
  float m_mc_weight_gen;                 //! generator weight
  float m_mc_weight_ph;                  //! photon scale factor
  float m_mc_weight_l1;                  //! lepton scale factors
  float m_mc_weight_l2;                  //! lepton scale factors

  // Apply GRL
  bool m_checkGRL;

  // trigger info
  bool m_trigger_passed;			//!
  unsigned int m_trigger_passed_items;		//!
  unsigned int m_trigger_matched_items;		//!
  bool m_checkTrig;
  bool m_checkTrigMatch;

  // Overlap Removal
  bool m_doOverlapRemoval;

  bool m_MxAODinput;//!
  TString m_photonContainerName;//!
  TString m_jetContainerName;//! 
  TString m_elecContainerName; //!
  TString m_muonContainerName; //!
  TString m_evtInfoName; //!
  bool first; //! 
  
  // events, filter efficiency and luminosity of MC samples
  MCLumi* m_mclumi; //!

  bool m_theorySyst; //!
  HG::StrV m_expSyst; //!

public:
  // standard constructors and destructor
  H2ZyAnalysis() { }
  H2ZyAnalysis(const char *name);
  virtual ~H2ZyAnalysis();

  // functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode fileExecute();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode histFinalize ();

  CutEnum cutflow(TString sysname);
  CutEnum initialcutflow();
  CutEnum precutflow();
  CutEnum finalcutflow();
  void fillCutFlow(CutEnum cut, TString sysname, double w);


  void SetupOutputTree( HZG::Container_llg &_V_llg, TString sysname="");
  void SetupSummaryTree();
  void FillVars(TString varname, HZG::Object_llg &_llg);
  void FillVars(vector<TString> varname, HZG::Object_llg &_llg);
  void ResetVars();

  void SetEventInfo();
  void RecSave(HZG::Object_llg *&_llg);
  void setcutflowname();

  //Trigger related variables & functions
  bool m_applyTrig;
  bool m_applyTrigMatch;

  void addBookKeeping();
  void createTriggerHists();
  void setupTrigger();
  bool fillTriggerInfo(int channel, float weight);
  void finalizeTrigger();
  void triggerEfficiencyPerItem();
  // VBF truth match


  // this is needed to distribute the algorithm to the workers
  bool m_applySystematicLoop;

  ClassDef(H2ZyAnalysis, 1);
};

#endif // Zy_H2ZyAnalysis_H
