#include "H2Zy/ComputeMGME.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TH2.h"
#include "TFile.h"
#include "TChain.h"
#include <fstream>
#include <iostream>
#include <memory>

namespace HZG {

class ZyMGMEHelperBase {
public:
  virtual void testMe(std::vector<double> &ME_temp, std::vector<double> &logME_temp) = 0;
  virtual void setMomenta(std::map<int, double> obj_4m) = 0;

  template <typename T>
    static std::shared_ptr<ZyMGMEHelperBase> build(std::string pCard,std::map<int, double> obj_4m);
};

template <typename T>
class ZyMGMEHelper: public ZyMGMEHelperBase {
  public:
    ZyMGMEHelper(T&& s, std::string pCard, std::map<int, double> obj_4m);
    void testMe(std::vector<double> &ME_temp, std::vector<double> &logME_temp);
    void setMomenta(std::map<int, double> obj_4m);
  private:
    T m_S;
    std::string m_paramCard;

    TLorentzVector m_cms, m_cmslly;
    std::vector<TLorentzVector> m_fermions;
};

template <typename T>
ZyMGMEHelper<T>::ZyMGMEHelper(T&& s, std::string pCard, std::map<int, double> obj_4m):
  m_S(s), m_paramCard(pCard) {
    m_S.initProc(m_paramCard.c_str()); // This can throw an exception
    setMomenta(obj_4m);
  }

template <typename T>
void ZyMGMEHelper<T>::setMomenta(std::map<int, double> obj_4m) {


  if( (m_S.nexternal - m_S.ninitial) < 3 )
    std::cerr << "ERROR: the process is not compatible with our test!" << std::endl;
  m_fermions.clear();
  m_fermions.resize(m_S.nexternal - m_S.ninitial);

  // Input should be in GeV !!!!
  // Define MGME_USE_MEV if you need MeV!
  // In particular, the ordering of m_fermions must correspond to the process:
  // e.g. in MadGraphME/inc/CPPProcess_P0_Sigma_sm_hVBF_ZZ_4l_sm_ddx_epemepemuux.h
  // which is produced after building the MadGraph MEs.


  //#define USE_RANDOM_VALUES
#ifdef USE_RANDOM_VALUES
  TRandom2 rnd;
  for(int i = 0 ; i < 3 ; ++i)
    m_fermions[i].SetPtEtaPhiM( rnd.Uniform(10.,200.), rnd.Uniform(-2.5,2.5), rnd.Uniform(0, 6.2831853072), 106. );
#else
  //m_fermions[0].SetPtEtaPhiM( 35., 1.10, 1.234, 106. );
  //m_fermions[1].SetPtEtaPhiM( 38., -1.0, 1.500, 106. );
  //m_fermions[2].SetPtEtaPhiM( 10., 1.10, 1.234, 106. );

  m_fermions[0].SetPtEtaPhiM(obj_4m[0],obj_4m[1],obj_4m[2],obj_4m[3]);
  m_fermions[1].SetPtEtaPhiM(obj_4m[4],obj_4m[5],obj_4m[6],obj_4m[7]);
  m_fermions[2].SetPtEtaPhiM(obj_4m[8],obj_4m[9],obj_4m[10],obj_4m[11]);

  //for(int i = 0; i<obj_4m.size(); i++) std::cout<< " obj_4m = " << obj_4m[i] <<" "<< i <<std::endl;

#endif
  m_cms.SetPtEtaPhiM(0.,0.,0.,0.);
  m_cmslly.SetPtEtaPhiM(0.,0.,0.,0.);

  for(unsigned i = 0 ; i < m_fermions.size() ; ++i) {
    m_cms += m_fermions[i];
    m_cmslly += m_fermions[i];
  }
  for(unsigned i = 0 ; i < m_fermions.size() ; ++i)
    m_fermions[i].Boost( -( m_cms.BoostVector() ) );

  //std::cout << " cms = " << m_cms.Pt()<< " " <<m_cms.Eta()<< " " <<m_cms.Phi() <<" "<<m_cms.M()<<std::endl;
  //std::cout << std::string(80,'-') << std::endl;
  //std::cout << " Momenta:\t" << "Pt\tEta\tPhi\tMass" << std::endl;

  //double (TLorentzVector::*fPtr[])() const =  { &TLorentzVector::E, &TLorentzVector::Px, &TLorentzVector::Py, &TLorentzVector::Pz };
  //double (TLorentzVector::*fPtr[])() const =  { &TLorentzVector::Pt, &TLorentzVector::Eta, &TLorentzVector::Phi, &TLorentzVector::M };

#if 0 
  for(unsigned i = 0 ; i < m_fermions.size() ; ++i) {
    for(int j = 0 ; j < 4 ; ++j)
      std::cout << "\t" << (m_fermions[i].*fPtr[j])();
    std::cout << std::endl;
  }
  std::cout << std::string(80,'-') << std::endl;
#endif

}

template <typename T>
void ZyMGMEHelper<T>::testMe(std::vector<double> &ME_temp, std::vector<double> &logME_temp) {

  //std::cout << "Param card: " << m_paramCard << std::endl;
  //std::cout << std::string(80,'-') << std::endl;

  // Will use the event's CMS as MH value
  //std::vector<double> mtxElem = ComputeMGME(m_S, m_fermions, m_cms);
  std::vector<double> mtxElem = ComputeMGME_cmsM4l(m_S, m_fermions, m_cms, m_cmslly);

  ME_temp.clear();
  logME_temp.clear();

  // Display matrix elements
  for(int i=0; i<m_S.nprocesses;i++) {
    //std::cout << " Matrix element 2 = "<< setiosflags(ios::fixed) << setprecision(17)<< mtxElem[i]<< " GeV^" << -(2*m_S.nexternal-8) << endl;
    //std::cout << " Log = " << log( mtxElem[i] ) << std::endl;    

    ME_temp.push_back(mtxElem[i]);
    logME_temp.push_back(log(mtxElem[i]));
  }

  //std::cout << std::string(80,'-') << std::endl;
}


template <typename T>
std::shared_ptr<ZyMGMEHelperBase> ZyMGMEHelperBase::build(std::string pCard, std::map<int, double> obj_4m) {
  std::shared_ptr<ZyMGMEHelperBase> sharedPtr = std::make_shared<ZyMGMEHelper<T>>(T(), pCard, obj_4m);
  return sharedPtr;
}

}
