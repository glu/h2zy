#ifndef H2Zy_ZyTruthAnalysis_H
#define H2Zy_ZyTruthAnalysis_H
#include <EventLoop/Worker.h>
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "H2Zy/MCLumi.h"
#include "LHAPDF/PDF.h"

class ZyTruthAnalysis : public HgammaAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!



public:
  // this is a standard constructor
  ZyTruthAnalysis() { }
  ZyTruthAnalysis(const char *name);
  virtual ~ZyTruthAnalysis();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode initialize ();
  void fillcutflow(TString histname, int icut, double _weight);

  TTree *m_outputTree;      //!
  float m_llg; //!
  float m_ll; //!
  float weight; //!
  float mc_genxsec; //!
  float mc_genfeff; //!
  float m_mc_weight_xs; //!
  float m_mc_totevent; //!
  unsigned int m_mc_channel_number; //!
  int channel;

  std::map<TString,TH1F*> m_cutflowhistoTH1F;          //!
  TString hist_cutflow_name;


  MCLumi* m_mclumi; //!
  vector<LHAPDF::PDF*> m_pdfs0; //!
  vector<LHAPDF::PDF*> m_pdfs1; //!

  std::vector<float> mc_weight_PDF0; //!
  std::vector<float> mc_weight_PDF1; //!




  // this is needed to distribute the algorithm to the workers
  ClassDef(ZyTruthAnalysis, 1);
};

#endif // H2Zy_ZyTruthAnalysis_H
