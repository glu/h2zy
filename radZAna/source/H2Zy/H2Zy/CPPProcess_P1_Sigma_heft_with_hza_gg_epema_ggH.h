//==========================================================================
// This file has been automatically generated for C++ Standalone by
// MadGraph5_aMC@NLO v. 2.6.1, 2017-12-12
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef MG5_Sigma_heft_with_hza_gg_epema_H
#define MG5_Sigma_heft_with_hza_gg_epema_H

#include <complex> 
#include <vector> 

#include "H2Zy/Parameters_heft_with_hza.h"

using namespace std; 

//==========================================================================
// A class for calculating the matrix elements for
// Process: g g > h HIG<=1 HIW<=1 WEIGHTED<=2 @1
// *   Decay: h > z a HIG<=1 HIW<=1 WEIGHTED<=2
// *     Decay: z > e+ e- HIG<=1 HIW<=1 WEIGHTED<=2
// Process: g g > h HIG<=1 HIW<=1 WEIGHTED<=2 @1
// *   Decay: h > z a HIG<=1 HIW<=1 WEIGHTED<=2
// *     Decay: z > mu+ mu- HIG<=1 HIW<=1 WEIGHTED<=2
//--------------------------------------------------------------------------

class CPPProcess_P1_Sigma_heft_with_hza_gg_epema_ggH
{
  public:

    // Constructor.
    CPPProcess_P1_Sigma_heft_with_hza_gg_epema_ggH() {}

    // Initialize process.
    virtual void initProc(string param_card_name); 

    // Calculate flavour-independent parts of cross section.
    virtual void sigmaKin(); 

    // Evaluate sigmaHat(sHat).
    virtual double sigmaHat(); 

    // Info on the subprocess.
    virtual string name() const {return "g g > e+ e- a (heft_with_hza)";}

    virtual int code() const {return 1;}

    const vector<double> & getMasses() const {return mME;}

    // Get and set momenta for matrix element evaluation
    vector < double * > getMomenta(){return p;}
    void setMomenta(vector < double * > & momenta){p = momenta;}
    void setInitial(int inid1, int inid2){id1 = inid1; id2 = inid2;}

    // Get matrix element vector
    const double * getMatrixElements() const {return matrix_element;}

    // Constants for array limits
    static const int ninitial = 2; 
    static const int nexternal = 5; 
    static const int nprocesses = 1;

    void setM4l(double m_m4l);

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int perm[], const int hel[]); 
    static const int nwavefuncs = 7; 
    std::complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 1; 
    std::complex<double> amp[namplitudes]; 
    double matrix_1_gg_h_h_za_z_epem(); 

    // Store the matrix element value from sigmaKin
    double matrix_element[nprocesses]; 

    // Color flows, used when selecting color
    double * jamp2[nprocesses]; 

    // Pointer to the model parameters
    Parameters_heft_with_hza * pars; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed each event)
    vector < double * > p; 
    // Initial particle ids
    int id1, id2; 

}; 


#endif  // MG5_Sigma_heft_with_hza_gg_epema_H
