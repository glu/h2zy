#ifndef H2Zy_TRUTHSELECT_H
#define H2Zy_TRUTHSELECT_H

#include <vector>
#include <set>
#include <TString.h>
#include <TEnv.h>
#include <TLorentzVector.h>

#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/TruthUtils.h"
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/Object_llg.h" 

namespace HZG {

  class TruthSelect{

    public:
      TruthSelect();
      void setMVA();
      void initialize(const HG::Config &config, xAOD::TEvent *_event, Bmap *_beventmap, Imap *_ieventmap, Fmap *_feventmap, IVmap *_iveventmap, FVmap *_fveventmap, TLVVmap *_tlvveventmap);
      void dumptruth();
      bool notFromHadronic(const xAOD::TruthParticle *ptcl);
      void truthsave(unsigned int _eventNumber, unsigned int _mc_channel_number);
      void truthsave_vertex();
      void retrieve_Zdecay_topo(const xAOD::TruthParticle *ptcl);
      void retrieve_Higgsdecay_topo(const xAOD::TruthParticle *ptcl);
      void retrieve_truthjets(TLorentzVector p1, TLorentzVector p2, TLorentzVector ph);
      void useGeV(bool _useGeV);
      bool ZIsFromHiggs(const xAOD::TruthParticle *ptcl);

      void print_child(const xAOD::TruthParticle *ptcl);
      HG::TruthPtcls findHiggs();  
      void settree_branch();
      TTree * settruthtree(const HG::Config &config){ 
              HG::Config _config = config;
	      m_truthTree = new TTree("truthTree","truthTree"); 
	      if (_config.getBool("EventHandler.FillTruthTree", false)) settree_branch();  
	      return m_truthTree;
      }; 

      void Reco_Truth_Matching(Object_llg &_llg);
      void searchDphoton();
      int hasDphoton;
      int hasttyphoton;

      const xAOD::TruthVertex* getMotherVert(const xAOD::TruthParticle* p);
      const xAOD::TruthParticle* getMother(const xAOD::TruthParticle* p);
      int AncestorID(const xAOD::TruthParticle *ptcl);


    private:  
      xAOD::TEvent *m_event;
      HG::Config m_config;

    

      const xAOD::TruthParticleContainer *m_truthParticles;
      const xAOD::JetContainer *m_antiKt4TruthJets;

      const xAOD::TruthVertexContainer * m_Vertices;

      xAOD::TruthParticleContainer *m_truthPhotons;
      xAOD::TruthParticleContainer *m_truthElectrons;
      xAOD::TruthParticleContainer *m_truthMuons;
      xAOD::JetContainer *m_truthJets;

      float mass_scale;

      Fmap *m_feventmap;
      Imap *m_ieventmap;
      Bmap *m_beventmap;
      IVmap *m_iveventmap;
      FVmap *m_fveventmap;
      TLVVmap *m_tlvveventmap;



      TTree *m_truthTree;
      float higgs_truth_pt;
      float higgs_truth_eta;
      float higgs_truth_phi;
      float higgs_truth_mass;
      float Dphi_Zy_jj;
      float pTt ;
      float dphi_Zy;
      float eta_Zepp ;
      float m_jj;
      float DRmin_Z_j ;
      float Dy_j_j ;
      float mvaout;
      TMVA::Reader* reader;

      float Z_truth_pt;
      float Z_truth_eta;
      float Z_truth_phi;
      float Z_truth_mass;
      float l1_truth_pt;
      float l1_truth_eta;
      float l1_truth_phi;
      float l1_truth_mass;
      float l2_truth_pt;
      float l2_truth_eta;
      float l2_truth_phi;
      float l2_truth_mass;
      float ph_truth_pt;
      float ph_truth_eta;
      float ph_truth_phi;
      float ph_truth_mass;
      int l1_truth_pdgId;
      int l2_truth_pdgId;
      float j1_truth_pt;
      float j2_truth_pt;
      int N_jets;
      int Central_N_jets;

      bool doDphi;
      unsigned int m_eventNumber;
      unsigned int m_mc_channel_number;
      TLorentzVector p1, p2, p_ph, j1, j2;
  };

}


#endif
