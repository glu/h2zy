#ifndef HZgammaHelper_h
#define HZgammaHelper_h

#include "TROOT.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include "TTree.h"
#include "TH1F.h"
#include <typeinfo> 
#include <iostream> 
#include <TLorentzVector.h>

#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"

#ifndef __CINT__
#include "HGamAnalysisFramework/PhotonHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/OverlapRemovalHandler.h"
#endif

//#include "H2Zy/H2ZyAnalysis.h"
#include "AsgTools/ToolHandle.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/Config.h"

//-----
#include "PathResolver/PathResolver.h"
#include "FsrUtils/FsrPhotonTool.h"
#include "ZMassConstraint/ConstraintFit.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h"
#include "IsolationTool/IsolationHelper.h"

using namespace std;

typedef TString Str;
typedef map<Str,double> Dmap;
typedef map<Str,float> Fmap;
typedef map<Str,int> Imap;
typedef map<Str,unsigned int> UImap;
typedef map<Str,unsigned long long> ULLmap;
typedef map<Str,bool> Bmap;

typedef map<Str, vector<double>* > DPVmap;
typedef map<Str, vector<float>* > FPVmap;
typedef map<Str, vector<int>* > IPVmap;
typedef map<Str, vector<bool>* > BPVmap;

typedef map<Str, vector<double> > DVmap;
typedef map<Str, vector<float> > FVmap;
typedef map<Str, vector<int> > IVmap;
typedef map<Str, vector<bool> > BVmap;
typedef map<Str, vector<TLorentzVector> > TLVVmap;

typedef Analysis::MuonResolutionAndMomentumScaleFactors MuonResMomSF;

template <typename T> void append_map(TString bn, map<Str,T> &in_map, map<Str,T> &out_map)
{
  TString _typeidname = typeid(in_map[bn]).name();
  out_map[bn] = in_map[bn];
}


template <typename T> void MakeSingleBranch(TString bn, map<Str,T> &_map, TTree *_ttree, vector<TString> &V_bn)
{
  TString _typeidname = typeid(_map[bn]).name();
  if(_typeidname.BeginsWith("f")||_typeidname.Contains("float") || _typeidname.Contains("Float_t"))  _ttree->Branch(bn, &_map[bn], bn+"/F");
  else if((_typeidname.Contains("unsigned") && _typeidname.Contains("long")) || (_typeidname.Contains("ULong64_t")))  _ttree->Branch(bn, &_map[bn], bn+"/l");
  else if(_typeidname.Contains("long") || _typeidname.Contains("Long64_t"))  _ttree->Branch(bn, &_map[bn], bn+"/L");
  else if((_typeidname.Contains("unsigned") && _typeidname.Contains("int")) || (_typeidname.Contains("UInt_t")))  _ttree->Branch(bn, &_map[bn], bn+"/i");
  else if(_typeidname.BeginsWith("i")||_typeidname.Contains("int") || _typeidname.Contains("Int_t"))  _ttree->Branch(bn, &_map[bn], bn+"/I");
  else if(_typeidname.BeginsWith("b")||_typeidname.Contains("bool") || _typeidname.Contains("Bool_t"))  _ttree->Branch(bn, &_map[bn], bn+"/O");
  else _ttree->Branch(bn, &_map[bn]);
  V_bn.push_back(bn);
}


template <typename T> void MakeSingleBranch(TString bn, T &_var, TTree *_ttree, vector<TString> &V_bn)
{
  TString _typeidname = typeid(_var).name();
  if(_typeidname.BeginsWith("f")||_typeidname.Contains("float") || _typeidname.Contains("Float_t"))  _ttree->Branch(bn, &_var, bn+"/F");
  else if((_typeidname.Contains("unsigned") && _typeidname.Contains("long")) || (_typeidname.Contains("ULong64_t")))  _ttree->Branch(bn, &_var, bn+"/l");
  else if(_typeidname.Contains("long") || _typeidname.Contains("Long64_t"))  _ttree->Branch(bn, &_var, bn+"/L");
  else if((_typeidname.Contains("unsigned") && _typeidname.Contains("int")) || (_typeidname.Contains("UInt_t")))  _ttree->Branch(bn, &_var, bn+"/i");
  else if(_typeidname.BeginsWith("i")||_typeidname.Contains("int") || _typeidname.Contains("Int_t"))  _ttree->Branch(bn, &_var, bn+"/I");
  else if(_typeidname.BeginsWith("b")||_typeidname.Contains("bool") || _typeidname.Contains("Bool_t"))  _ttree->Branch(bn, &_var, bn+"/O");
  else _ttree->Branch(bn, &_var);
  V_bn.push_back(bn);
}


template <typename T> void MakeVectorBranch(TString bn, map<Str,T> &_map, TTree *_ttree, vector<TString> &V_bn)
{
  TString _typeidname = typeid(_map[bn]).name();
  _ttree->Branch(bn,&_map[bn]);
  V_bn.push_back(bn);
}


template <typename T> void AddToCounters( map<int, TString> &_cutname, TString name, T *_h, int &value, double weight, bool addvalue=0)
{
   _cutname[value]=name;
  _h->Fill(value, (float)weight);
  if(addvalue) { value=value+1; }
}

template <typename T> void AddToCounters( T *_h, int &value, double weight)
{
  _h->Fill(value, (float)weight);
}


template <typename T> void ResetCounterName(map<int, TString> &_cutname, T *_h)
{
   int Ncut = _cutname.size();
   map<int,  TString >::iterator map_iter_i;
   _h->SetBins(Ncut,0,Ncut);
   int ibin=1;
   for(map_iter_i = _cutname.begin(); map_iter_i != _cutname.end(); map_iter_i++)
   {
     _h->GetXaxis()->SetBinLabel(ibin , (map_iter_i->second).Data());
     ibin++;
   }
   _h->SetMinimum(0);
}

vector<double> thrust_coord( TLorentzVector vector1, TLorentzVector vector2 );

int getmctype(int mc_channel_number );
float getHiggsResMass(int mc_channel_number );

//---------------- Photon  further selection and correction  ------
void Photon_Pointing(xAOD::PhotonContainer &photons,  HG::PhotonHandler *_photonHandler, double PVz=0);

void ZGam_Overlapremoval(HG::OverlapRemovalHandler *_overlapremovalHandler,HG::Config &config,  xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::PhotonContainer &photons, xAOD::JetContainer &jets);



//---------------  Electron further selection and correction  -----

//---------------  Muon further selection and correction    -------

//---------------  Truth level definitions  -----------------------

bool isTruthPhoton(const xAOD::Photon *ph);

bool isTruthElectron(const xAOD::Electron *el);

bool isTruthMuon(const xAOD::Muon *mu);


//---- Kinematic calculations ---
void lorentzboost(TLorentzVector vector1, TLorentzVector vector2, TLorentzVector &output);
void lorentzrotation(TLorentzVector vector1, TLorentzVector vector2, TLorentzVector &output);
#endif
