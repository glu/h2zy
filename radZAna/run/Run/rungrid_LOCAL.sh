## merge datasets together

#inlist=$1
#str1=""
#while read line
#do
#str1="${str1},$line"
#done < ${inlist}
#str2=${str1#*,}
#echo $str2

#prun --nFilesPerJob 20 --useAthenaPackages --exec="sed -i 's/,/\n/g' input.txt; cat input.txt; ls -ltr; source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh; asetup AnalysisBase,21.2.25,here; source build/x86_64-slc6-gcc62-opt/setup.sh;pwd;ls -ltr; ./build/x86_64-slc6-gcc62-opt/bin/runH2ZyAnalysis source/H2Zy/data/H2ZyAnalysisDATA_Rel21.cfg InputFileList: input.txt OutputDir: output;cp output/hist-sample.root ./; ls -ltr" --inDS=$str2 --outDS=user.shhan.Zgam_h022_v0 --writeInputToTxt=IN:input.txt --outputs=hist-sample.root --match="*.root*" --inTarBall=jobcontents.tar.gz


## run seperately
inlist=$1
while read line
do
str1=$line
if [[ $str1 = *"Powheg"* ]]; then
str2=${line/.Powheg*}
fi
if [[ $str1 = *"Py8"* ]]; then
str2=${line/.Py8*}
fi
if [[ $str1 = *"Sherpa"* ]]; then
str2=${line/.Sherpa*}
fi
if [[ $str1 = *"Madgraph"* ]]; then
str2=${line/.Madgraph*}
fi
if [[ $str1 = *"r10201"* ]]; then
str3=${line#*r10201}
fi
if [[ $str1 = *"r9364"* ]]; then
str3=${line#*r9364}
fi
str5="$str2$str3"
echo $str1
echo $str5
prun --nFilesPerJob 20 --useAthenaPackages --exec="sed -i 's/,/\n/g' input.txt; cat input.txt; ls -ltr; source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh; asetup AnalysisBase,21.2.25,here; source build/x86_64-slc6-gcc62-opt/setup.sh;pwd;ls -ltr; ./build/x86_64-slc6-gcc62-opt/bin/runH2ZyAnalysis source/H2Zy/data/H2ZyAnalysisMC16_Rel21.cfg InputFileList: input.txt OutputDir: output;cp output/hist-sample.root ./; ls -ltr" --inDS=$str1 --outDS=user.shhan.$str5 --writeInputToTxt=IN:input.txt --outputs=hist-sample.root --match="*.root*" --inTarBall=jobcontents.tar.gz
done < ${inlist}
