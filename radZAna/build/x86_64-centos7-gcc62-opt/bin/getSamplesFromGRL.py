import xml.etree.ElementTree as ET
import rucio.client.didclient as didclient
from urllib2 import urlopen
from urlparse import urljoin
import re
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

def get_nevents_sample(client, scope, sample_name):
    """ get number of events with rucio """
    list_files = client.list_files(scope, sample_name)
    events = sum(f['events'] for f in list_files)
    return events


import argparse

parser = argparse.ArgumentParser(description='List all the available data derivation samples',
                                 epilog="example: ./getSamplesFromGRL.py HIGG1D1")
parser.add_argument('derivation', choices=('HIGG1D1', 'HIGG1D2'))
args = parser.parse_args()


BASE_URL = "https://atlas-groupdata.web.cern.ch/atlas-groupdata/"

grls = {'data15': "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
        'data16': "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
#        'data16': "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
        'data17': "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
        'data18': "GoodRunsLists/data18_13TeV/20181111/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
        }

# if multiple ptags are specified all the dataset matching at least one ptag are kept
ALL_PTAGS = {'HIGG1D1': {'data15': ['p3704'], 'data16': ['p3704'], 'data17': ['p3704'], 'data18': ['p3704']},
             'HIGG1D2': {'data15': ['p3402'], 'data16': ['p3372'], 'data17': ['p3402', 'p3372']}}   # TODO: update HIGG1D2

logging.info("using these GRLs %s", grls)

out = open("data_13TeV_%s.txt" % args.derivation, "w")

report = []
rucio_client = didclient.DIDClient()
infos = []

for grl_name, grl in grls.iteritems():
  nruns = 0
  nmissing = 0
  nfound = 0
  
  grl_path = urljoin(BASE_URL, grl)
  logging.info("reading GRL %s from %s", grl_name, grl_path)
  out.write("# GRL: "+ grl +"\n")
  grl_data = urlopen(grl_path).read()

  root = ET.fromstring(grl_data)

  runlist = []
  for md in root.iter('Metadata'):
    if (md.get('Name') == "RunList"):
      runlist = md.text.split(",")
  nruns = len(runlist)
  logging.info("found %s runs", len(runlist))
  logging.info("run lists: %s", runlist)


  for run in runlist:
    scope = re.search("data.+?TeV", grl).group()
    type = "container"

    filters = {}
    padded_run = "%08d" % int(run)
    filters['name'] = scope + "." + padded_run + ".physics_Main.*.DAOD_%s.*" % args.derivation

    ptags = ALL_PTAGS[args.derivation][grl_name]
    cont = list(rucio_client.list_dids(scope, filters=filters, type=type, long=True))
    if len(cont) == 0:
      logging.error("no samples for run %s, rucio query: %s", run, filters)

    samples = []
    for f in cont:
      for ptag in ptags:
        if ptag in f['name']:
          samples.append(f['name'])

    year = scope[4:6]

    if len(samples) == 0:
      logging.warning("no samples for run %s matching p-tag %s", run, ptags)
    elif len(samples) > 1:
      logging.error("multiple samples for run %s: %s", run, samples)

    if len(samples) != 1:
      out.write("# Missing run " + run + "\n")
      nmissing += 1
    else:
      out.write("period20" + year + " " + scope  + ":" + samples[0] + "/\n")
      nevents = get_nevents_sample(rucio_client, scope, samples[0])
      infos.append({'run': int(run), 'name': samples[0], 'nevents': nevents})
      nfound += 1

  out.write("\n")

  report.append("{}: {} runs, {} found, {} missing".format(grl_name, nruns, nfound, nmissing))
out.close()

for info in sorted(infos, key=lambda x: x['run']):
  print "%d: %s %d" % (info['run'], info['name'], info['nevents'])

print '\n'.join(sorted(report))
