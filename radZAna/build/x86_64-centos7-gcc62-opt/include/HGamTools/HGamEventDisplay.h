#ifndef HGamTools_HGamEventDisplay_H
#define HGamTools_HGamEventDisplay_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "BasicEventDisplayTool.h"

typedef std::vector<TLorentzVector> TLVs;
typedef std::vector<TString> StrV;

class HGamEventDisplay : public HgammaAnalysis {
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  BasicEventDisplayTool m_eventDisplay;


public:
  // this is a standard constructor
  HGamEventDisplay() { }
  HGamEventDisplay(const char *name);
  virtual ~HGamEventDisplay();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();


private:
  StrV createEventInfos(const Bool_t particleLevel);
  StrV createLines(xAOD::PhotonContainer photons, xAOD::JetContainer jets, xAOD::ElectronContainer electrons, xAOD::MuonContainer muons);

  // this is needed to distribute the algorithm to the workers
  ClassDef(HGamEventDisplay, 1);
};

#endif // HGamTools_HGamEventDisplay_H
