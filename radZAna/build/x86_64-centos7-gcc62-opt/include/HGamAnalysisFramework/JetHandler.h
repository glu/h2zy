#pragma once

// STL include(s):
#include <memory>

// EDM include(s):
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetModifier.h"
#include "JetInterface/IJetSelector.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetContainer.h"

// ROOT include(s):
#include "TH2F.h"

// Local include(s):
#include "HGamAnalysisFramework/HgammaHandler.h"

class BTaggingEfficiencyTool;
class BTaggingSelectionTool;


namespace HG {
  class JetHandler : public HgammaHandler<xAOD::Jet, xAOD::JetContainer, xAOD::JetAuxContainer> {

  private:
    // Tool handles for the various calibration and uncertainty tools
    asg::AnaToolHandle<IJetCalibrationTool>             m_jetCalibTool; //!
    asg::AnaToolHandle<IJetSelector>                    m_jetCleaningTool; //!
    asg::AnaToolHandle<ICPJetUncertaintiesTool>         m_jetUncTool;
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trackTool; //!
    asg::AnaToolHandle<IJetUpdateJvt>                   m_jvtTool; //!
    asg::AnaToolHandle<IJetModifier>                    m_fjvtTool; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency>           m_jvtSFTool; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency>           m_fjvtSFTool; //!
    std::unique_ptr<TH2F>                               m_jvtLikelihoodHist; //!

    // B-tagging information
    TString                                                                        m_bTagEVReduction; //!
    StrV                                                                           m_bTagNames; //!
    StrV                                                                           m_bTagDiscriminants; //!
    std::map<TString, StrV>                                                        m_bTagEffs; //!
    std::map<TString, StrV>                                                        m_bTagOPs; //!
    std::map<TString, NumV>                                                        m_bTagCuts; //!
    std::map<TString, std::vector< asg::AnaToolHandle<IBTaggingEfficiencyTool> > > m_bTagEffTools; //!
    std::map<TString, std::vector< asg::AnaToolHandle<IBTaggingSelectionTool> > >  m_bTagSelTools; //!

    double              m_rapidityCut;
    double              m_ptCut;
    double              m_jvf;
    double              m_jvt;
    bool                m_doFJVT;
    bool                m_applyFJVT;
    TString             m_defaultBJetWP;
    double              m_bJetEtaCut;
    double              m_bJetJvfCut;
    bool                m_enableBTagging;
    bool                m_correctVertex;
    bool                m_doCleaning;
    int                 m_mcIndex;
    TString             m_jetAlg;
    std::vector<double> m_jvtEff;

  public:
    JetHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~JetHandler();

    virtual EL::StatusCode initialize(Config &config);

    static SG::AuxElement::Accessor<std::vector<float> > JVF;
    static SG::AuxElement::Accessor<float>               Jvt;
    static SG::AuxElement::Accessor<float>               hardVertexJvt;
    static SG::AuxElement::Accessor<float>               Jvf;
    static SG::AuxElement::Accessor<float>               CorrJvf;
    static SG::AuxElement::Accessor<float>               Rpt;
    static SG::AuxElement::Accessor<float>               scaleFactor;
    static SG::AuxElement::Accessor<char>                isClean;
    static SG::AuxElement::Accessor<float>               DetectorEta;

    virtual xAOD::JetContainer getCorrectedContainer();
    virtual xAOD::JetContainer applySelection(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applySelectionNoJvt(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applySelectionNoCleaning(xAOD::JetContainer &container);
    virtual xAOD::JetContainer applyBJetSelection(xAOD::JetContainer &container, TString wp = "");
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys);

    /// applies kinematic preselection cuts
    bool passPtEtaCuts(const xAOD::Jet *jet);

    /// corrects JVT value
    void decorateRescaledJVT(xAOD::Jet &jet);
    void recalculateJVT(xAOD::Jet &jet);
    void decorateJVTExtras(xAOD::JetContainer &jets);

    /// return JVT event weight
    float weightJvt(const xAOD::JetContainer &jets);

    /// return JVT event weight
    float weightFJvt(const xAOD::JetContainer &jets);

    /// applies kinematic preselection cuts
    bool passJVTCut(const xAOD::Jet *jet);

    /// applies kinematic preselection cuts
    bool passJVFCut(const xAOD::Jet *jet, bool useBTagCut = false);

    /// applies fJVT
    bool passFJVTCut(const xAOD::Jet *jet);

    /// calibrates and smears a jet
    void calibrateJet(xAOD::Jet *jet);

    /// returns flavor label
    TString getFlavorLabel(xAOD::Jet *jet);

    /// returns MC generator index
    void setMCGen(TString sampleName);

    /// apply btags to jets
    void decorateBJetReco(xAOD::Jet *jet);

    /// apply truth btags to jets
    void decorateBJetTruth(xAOD::Jet *jet);

    /// apply btags to jets
    void decorateJVF(xAOD::Jet *jet);

    /// print details about photon to the screen
    static void printJet(const xAOD::Jet *gam, TString comment = "");

    /// obtain combined JVT weights
    static double multiplyJvtWeights(const xAOD::JetContainer *jets_noJvtCut);

    /// obtain combined fJVT weights
    static double multiplyFJvtWeights(const xAOD::JetContainer *jets_noJvtCut);
  };
}
