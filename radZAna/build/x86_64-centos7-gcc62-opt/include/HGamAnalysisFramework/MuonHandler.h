#pragma once

// EDM include(s):
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODMuon/MuonContainer.h"

// Local include(s):
#include "HGamAnalysisFramework/HgammaHandler.h"

namespace CP {
  class IsolationSelectionTool;
}

namespace HG {
  class MuonHandler : public HgammaHandler<xAOD::Muon, xAOD::MuonContainer, xAOD::MuonAuxContainer> {
  private:
    CP::MuonEfficiencyScaleFactors     *m_muonEffScaleFactors;
    CP::MuonEfficiencyScaleFactors     *m_muonEffScaleFactorsIso;   // 2nd instance needed for Iso SF
    CP::MuonEfficiencyScaleFactors     *m_muonEffScaleFactorsTTVA;  // 3rd instance needed for TTVA
    CP::MuonCalibrationAndSmearingTool *m_muonCalibTool_2016; // tool version for 2015 and 2016 data
    CP::MuonCalibrationAndSmearingTool *m_muonCalibTool_2017; // tool version for 2017 data
    CP::MuonCalibrationAndSmearingTool *m_muonCalibTool_2018; // tool version for 2018 data
    std::map<TString, CP::MuonSelectionTool *> m_muonSelectTool;
    std::map<TString, SG::AuxElement::Accessor<char>* > m_pidAcc;
    std::map<TString, SG::AuxElement::Accessor<char>* > m_isBad;
    std::map<HG::Iso::IsolationType, CP::IsolationSelectionTool *> m_isoTools;
    std::map<HG::Iso::IsolationType, SG::AuxElement::Accessor<char>* > m_isoDecorators;

    bool    m_doPidCut;
    StrV    m_pidCuts;
    TString m_defaultPid;

    bool   m_ApplyPtCut;
    double m_PtCut;

    bool   m_ApplyIPCuts;
    double m_d0BySigd0Cut;
    double m_z0Cut;

    bool m_doIsoCut;
    StrV m_isoCuts;
    HG::Iso::IsolationType m_defaultIso;

    double m_MaxEta;
    unsigned int runNumber;

  public:
    static SG::AuxElement::Accessor<float> effSF;
    static SG::AuxElement::Accessor<float> effSFIso;
    static SG::AuxElement::Accessor<float> effSFTTVA;
    static SG::AuxElement::Accessor<float> scaleFactor;
    static SG::AuxElement::Accessor<char> passIPCut;
    static SG::AuxElement::Accessor<char> isAccepted;
    static SG::AuxElement::Accessor<char> isBad;

  public:
    MuonHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~MuonHandler();

    virtual EL::StatusCode initialize(Config &config);

    virtual xAOD::MuonContainer getCorrectedContainer();
    virtual xAOD::MuonContainer applySelection(xAOD::MuonContainer &container);
    virtual xAOD::MuonContainer applyCleaningSelection(xAOD::MuonContainer &container);
    virtual CP::SystematicCode  applySystematicVariation(const CP::SystematicSet &sys);

    /// applies kinematic preselection cuts: not-in-crack + pT cut
    bool passPtCuts(const xAOD::Muon *muon);

    /// applies PID cut specified in config file
    bool passIsoCut(const xAOD::Muon *muon, HG::Iso::IsolationType iso = HG::Iso::Undefined);
    void decorateIso(xAOD::Muon &muon);

    /// Decorates IP cuts result to muon
    bool passIPCuts(const xAOD::Muon *muon);
    void decorateIPCut(xAOD::Muon &muon);

    /// applies PID cut
    bool passPIDCut(const xAOD::Muon *muon, TString pid = "Default");
    void decoratePID(xAOD::Muon &muon);

    /// Decorates isBad
    void decorateIsBad(xAOD::Muon *muon);

    /// calibrates and smears a photon
    static void calibrateAndSmearMuon(xAOD::Muon *muon, CP::MuonCalibrationAndSmearingTool *muonCalibTool);

    /// decorate photon with efficiency scale factor and uncertainty
    void applyScaleFactor(xAOD::Muon *muon);

    /// print details about photon to the screen
    static void printMuon(const xAOD::Muon *muon, TString comment = "");

    HG::Iso::IsolationType getIsoType(TString isoName);

    /// For Experts: get a pointer to the isolation tool (to feed into e.g. CloseByCorrection tool)
    CP::IsolationSelectionTool *getIsoTool(HG::Iso::IsolationType iso);

  };
}
