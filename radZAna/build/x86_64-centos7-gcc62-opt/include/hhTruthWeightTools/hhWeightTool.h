#ifndef hhTruthWeightTools_hhWeightTool_H
#define hhTruthWeightTools_hhWeightTool_H

#include <vector>

#include "AsgTools/AsgTool.h"

namespace xAOD {

  class hhWeightTool : public asg::AsgTool {
      
  public:
    
    //****************************************************************************** 
    hhWeightTool(const std::string& name);
    ~hhWeightTool() {};
    
    StatusCode initialize();
    StatusCode finalize();
    
    float       getWeight(float mhh);

  private:
    
    void addMHHBin(float upperMhhBinEdge, float kFactor, float kFactorError);
    void loadFile(std::string fileName, std::string histName);
    
  private:

    int m_verbosity;
    std::string m_reweightFile;

    // In GeV 
    std::vector<float> m_mHHUpperBinEdge;
    std::vector<float> m_kFactor;
    std::vector<float> m_kFactorErr;
    unsigned int       m_nMHHBins;

  };

}//namespace

#endif
