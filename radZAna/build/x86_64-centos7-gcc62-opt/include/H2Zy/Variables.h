#ifndef Variables_H
#define Variables_H

/********************
 *
 */

#include <vector>
#include <set>
#include <TString.h>
#include <TEnv.h>
#include <TLorentzVector.h>

#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/Config.h"
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/ConstraintFit.h"

#define GEV 1000.
#define MEV 1.
//const float mZ = 91.1876*1000;
//const float Zmass = 91.1876e3; // GeV
//const float Zwidth = 2.4952e3; // GeV
//const bool hasWidth = true;

// \brief HZgamma namespace
namespace HZG {

  //! \brief typedef for a vector of strings (to save some typing)
  typedef std::vector<TString> StrV;
  
  class Variables {
  public:
    
    //! \brief Object_llg constructor
    //! \param fileName name of text file with user-specified settings
    Variables();
    //HG::Config m_config;

    void initialize(xAOD::PhotonContainer &photons, xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::JetContainer &jets);



};

}

//#include "H2Zy/Object_llg.hpp"
#endif
