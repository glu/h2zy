#ifndef Object_llg_H
#define Object_llg_H

#include <vector>
#include <set>
#include <TString.h>
#include <TEnv.h>
#include <TLorentzVector.h>
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/Config.h"
#include "H2Zy/HZgammaHelper.h"
#include "H2Zy/ConstraintFit.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

#define GEV 1000.
#define MEV 1.
const float mZ = 91.1876*1000;
const float Zmass = 91.1876e3; // GeV
const float Zwidth = 2.4952e3; // GeV
const bool hasWidth = true;

// \brief HZgamma namespace
namespace HZG {

  //! \brief typedef for a vector of strings (to save some typing)
  typedef std::vector<TString> StrV;
  
  class Object_llg {
  public:
    
    //! \brief Object_llg constructor
    //! \param fileName name of text file with user-specified settings
    Object_llg();
    HG::Config m_config;

    void initialize_llg( int iph, int ilepton1, int ilepton2, xAOD::IParticle *photon, xAOD::IParticle *lepton1, xAOD::IParticle *lepton2);
    void initialize_ll(int ilepton1, int ilepton2, xAOD::IParticle *lepton1, xAOD::IParticle *lepton2);

    void initialize( xAOD::IParticle *lepton1, xAOD::IParticle *lepton2);

    xAOD::PhotonContainer  *m_selected_photons;
    xAOD::ElectronContainer *m_selected_electrons;
    xAOD::MuonContainer *m_selected_muons;
    xAOD::JetContainer *m_selected_jets;
    xAOD::JetContainer *m_selected_loose_jets;
    xAOD::MissingETContainer *m_selected_met;

    const xAOD::PhotonContainer  *m_all_photons;
    const xAOD::ElectronContainer *m_all_electrons;
    //xAOD::MuonContainer *selected_muons_CB_ST;
    TLorentzVector m_FSRp4;
    FSR::FsrCandidate m_fsrcand;
    ZMassConstraint::ConstraintFitInput input;
    ZMassConstraint::ConstraintFitInput input_ph;
    
    FSR::FsrPhotonTool *m_fsrTool;     //!
    ToolHandle<ZMassConstraint::IConstraintFit> m_massConstraint; //!
    ToolHandle<ZMassConstraint::IConstraintFit> ph_massConstraint; //!

    ToolHandle<CP::IEgammaCalibrationAndSmearingTool>  *energyRescaler;//!

    void setConfig(const HG::Config &config, ToolHandle<CP::IEgammaCalibrationAndSmearingTool>  &_energyRescaler, FSR::FsrPhotonTool *_fsrTool, ToolHandle<ZMassConstraint::IConstraintFit> &_massConstraint,  xAOD::PhotonContainer  *&_photons, xAOD::ElectronContainer *&_electrons, xAOD::MuonContainer *&_muons, xAOD::JetContainer *&_jets, xAOD::JetContainer *&_loose_jets, xAOD::MissingETContainer *&_met, xAOD::PhotonContainer  *&_photon_cont, xAOD::ElectronContainer *&_electron_cont ); 



   //--- kinematic correction ----
    void FSR_correction();
    void Zmass_constraint();
    void transfer_Covariance_2D(const vector<float> *Covariance_1D, TMatrixD *&Covariance_2D);

    void Save_LLGQuantities();
    void Save_VBFVars();
    void Save_METVars();
    void ComputeAdditionalQuantities(TLorentzVector &p_ph, TLorentzVector &p_lepton1, TLorentzVector &p_lepton2, TString postfix="");
    static void CalculateCMSKinematic(TLorentzVector &p_ph, TLorentzVector &p_lepton1, TLorentzVector &p_lepton2, std::map<TString, float> &_Map_float,TString postfix="");
    static void CalculateCommonKinematic(TLorentzVector &p_ph, TLorentzVector &p_lepton1, TLorentzVector &p_lepton2, std::map<TString, float> &_Map_float,TString postfix="");


    int getcategory(TString _catename="", TString postfix="");
    void setcategory(TString _catename="", TString postfix="");
    void setMatrixElement( TString postfix = "");

    void Reset_variablename();

    // xAOD objects:
    xAOD::Photon *m_photon;
    xAOD::Electron *m_electron[2];
    xAOD::Muon *m_muon[2];
    xAOD::IParticle *m_lepton[2];

    xAOD::MuonContainer *m_muon_viewcontainer;
    xAOD::ElectronContainer *m_electron_viewcontainer;

    //--public variables:
    float m_ll;
    float m_ll_fsr;
    float m_llg;
    float m_llg_fsr;
    float m_d_mll_mZ;
    float m_d_mll_mZ_Zmassconstraint;

    int m_channel;

    int charge_lepton1;
    int charge_lepton2;

    int index_photon;
    int index_lepton1;
    int index_lepton2;
 
    bool m_hasFSR;
    int m_index_lepton_fsr;
    float m_dR_lepton_fsr;

    FSR::FsrCandidate candidate_fsr;

    std::map<TString, bool> Map_bool; 
    std::map<TString, int> Map_int; 
    std::map<TString, unsigned int> Map_uint;
    std::map<TString, unsigned long long> Map_ull;
    std::map<TString, float> Map_float; 

    std::map<TString, std::vector<int> > Map_IV; 
    std::map<TString, std::vector<float> > Map_FV; 
    std::map<TString, std::vector<TLorentzVector> > Map_TLVV; 

    std::set <TString> Set_variablename;

    static TString comparename; 

    TLorentzVector TLV_photon;
    TLorentzVector TLV_lepton[2];
    TLorentzVector TLV_FSR;
    TLorentzVector TLV_lepton_Zmassconstraint[2];


    TLorentzVector TLV_photon_original;
    TLorentzVector TLV_photon_PVcorrect;
    TLorentzVector TLV_photon_zero;

    TMVA::Reader *reader;
    std::map<TString, float> ST_Map_float;
    static Object_llg *_ptr;
    void SetMVA();
    void SetMVAVars(TString postfix);
    double GetMVAResponse();

    //static float var1;
    //static float var2;
    static inline Object_llg* ptr()   { return _ptr; }
    
  public:
    void mapping_int(TString _name, int _val, TString postfix="") {_name=_name+postfix; Map_int[_name]=_val; Set_variablename.insert(_name);};
    void mapping_uint(TString _name, unsigned int _val, TString postfix="") {_name=_name+postfix; Map_uint[_name]=_val; Set_variablename.insert(_name);};
    void mapping_float(TString _name, float _val, TString postfix="") {_name=_name+postfix; Map_float[_name]=_val; Set_variablename.insert(_name);}; 
    void mapping_bool(TString _name, bool _val, TString postfix="")  {_name=_name+postfix; Map_bool[_name]=_val; Set_variablename.insert(_name);};
    void mapping_ull(TString _name, unsigned long long _val, TString postfix="") {_name=_name+postfix; Map_ull[_name]=_val; Set_variablename.insert(_name);};
    void mapping_TLVV(TString _name, TLorentzVector _val, TString postfix="") {_name=_name+postfix; Map_TLVV[_name].push_back(_val); Set_variablename.insert(_name);};
    void mapping_FV(TString _name, float _val, TString postfix="") { _name=_name+postfix; Map_FV[_name].push_back(_val); Set_variablename.insert(_name);};
    void RecoQuantities();
   
  };




}

#endif
