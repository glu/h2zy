#include "HGamAnalysisFramework/HgammaUtils.h"
#include "H2Zy/HZgammaHelper.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TTree.h"

#ifndef OVERLAP_REMOVAL_TREE_H
#define OVERLAP_REMOVAL_TREE_H

namespace HZG
{
    class OverlapRemovalTree
    {
    private:
        TTree *m_OverlapRemoval;
        
        std::vector<TLorentzVector> m_electron;
        std::vector<double> m_electron_z0;
        std::vector<bool>   m_electron_isTruth;
        
        std::vector<TLorentzVector> m_muon;
	std::vector<double> m_muon_d0;
        std::vector<double> m_muon_z0;
        std::vector<bool>   m_muon_isTruth;
	std::vector<bool>   m_muon_isStaco; 

        std::vector<TLorentzVector> m_photon;
        std::vector<double> m_photon_z0;
        std::vector<bool>   m_photon_isTruth;
        
        std::vector<TLorentzVector> m_jet;
    public:
        OverlapRemovalTree(TString TreeName);
        ~OverlapRemovalTree();
        void Fill(xAOD::ElectronContainer &electrons, xAOD::MuonContainer &muons, xAOD::PhotonContainer &photons, xAOD::JetContainer &jets);

	TTree* GetTree() {return m_OverlapRemoval;}
    };
}


#endif


