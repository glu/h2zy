/////////////////////////////////////////
//
// Z mass constraint fit
//
// original version by:
// Konstantinos Nikolopoulos
// Univ. of Athens
// https://svnweb.cern.ch/trac/atlasphys/browser/Physics/Higgs/HSG2/Code/ZMassConstraint/trunk/ZMassConstraint/ConstraintFit.h
//
// adapted for ROOT standalone (no CLHEP) by GM 02/10/2012
// added option for relativistic vs non-relativistic BW by GM (28/10/2012)
//
////////////////////////////////////////
#ifndef CONSTRAINTFIT_H
#define CONSTRAINTFIT_H

#include "TMatrixD.h"

class ConstraintFit {
public:
   ConstraintFit(void);                   // default constructor
   ConstraintFit(double, bool, double);   // constructor setting the constraint characteristics
   virtual ~ConstraintFit(void); // default destructor

   double EvaluateLogL(double TrueMass, double RecoMass, double MassResol);  // compute log L(TrueMass|RecoMass), symmetric resolution
   double EvaluatedLogL(double TrueMass, double RecoMass, double MassResol); // compute dlogL(TrueMass|RecoMass)/dTrueMass, symmetric resolution
   double EvaluateLogL(double TrueMass, double RecoMass, double MassResolL, double MassResolR);  // compute log L(TrueMass|RecoMass), asymmetric resolution
   double EvaluatedLogL(double TrueMass, double RecoMass, double MassResolL, double MassResolR); // compute dlogL(TrueMass|RecoMass)/dTrueMass, asymmetric resolution
   double CalculateMass(const TMatrixD* p0);  // calculate invariant mass using momenta passed as argument
   double CalculateMass();                    // calculate invariant mass using initial values of momenta
   void CalculateM2Jacobian(const TMatrixD* p0, TMatrixD& JacobianM2); // calculate derivatives of M^2 vs track momenta
   double CalculateMassResol(const TMatrixD* p0, const TMatrixD* covariance); // calculate resolution using momenta and covariances passed as argument
   double CalculateMassResol();                                               // calculate resolution using initial momenta and covariances

   void  MassFitInterface(double pin[2][4], double sigmain[6][6], int iobj);
   double MassFitRun(double pin[2][4], double sigmain[6][6], double zresolL=0.0, double zresolR=0.0);
   //double  MassFitInterface(double* pin[4],double*[3][3],int);

   void SetConstraintMass(double mass) {
      m_conMass = mass;
   };
   void SetConstraintWidth(double width) {
      m_conWidth = width;
   };
   void SetConstraintHasWidth(bool haswidth) {
      m_conHasWidth = haswidth;
   };
   void SetBias(double bias) {
       m_bias = bias;
   };
   void UseRelativisticBWFormula(bool useRelBW) {
       m_useRelativisticBWFormula = useRelBW;
   };
   void UseHSG2Settings(bool useHSG2Settings) {
       m_useHSG2Settings = useHSG2Settings;
   };
   void SetVerbose(bool verbose) {
       m_verbose = verbose;
   };
   double LikelihoodMass(void);
   double LikelihoodMass2(void);
   double LikelihoodMass(double, double);
   double LikelihoodMass2(double, double);

   void PrintInitialParameters();

private:
   // data members
   bool     m_conHasWidth;
   double   m_conMass;                   // resonance pole
   double   m_conWidth;                  // resonance width
   double   m_bias;                      // measurement bias
   bool     m_useHSG2Settings;           //
   bool     m_useRelativisticBWFormula;  // use either non-relativistic or relativistic BW formula (still missing: energy-dependent width)
   bool     m_verbose;

   int      m_parameters;
   int      m_obj;
   double * m_objmass;
   TMatrixD * m_parametersInit;
   TMatrixD * m_covarianceInit;
   TMatrixD * m_parametersFinal;
   TMatrixD * m_covarianceFinal;
   TMatrixD * m_chi2;

   double CalculateChi2(const TMatrixD* p0, TMatrixD* var, const double Mass);
   double MassFitCalculation(TMatrixD*, TMatrixD*, const double);
   double MassFit(const TMatrixD*, TMatrixD*, const double, TMatrixD*, TMatrixD*);
   void ConstraintCalculation(const TMatrixD*, const double, TMatrixD*, TMatrixD*);

  // this is needed to distribute the algorithm to the workers
//  ClassDef(ConstraintFit, 1);
};
#endif // CONSTRAINTFIT_H
