# Level of verbosity of the output, can be 0 (not output), 1 (minimal), 2 (verbose), >2 (debug)
Verbosity:                                   1
# The observable variable (m_gamgam)
Observable:                                  m_yy[200,1000]

# The block below defines the points at which to perform the S+B fits
#----
# Name of the variable giving the mass peak position
Scan.Var:                                    mResonance
# Lower end of the scan range
Scan.Min:                                    300
# Upper end of the scan range
Scan.Max:                                    800
# Interval between scan points
Scan.Step:                                   10

# The block below defines the parameters of the spurious signal computation and tests
# ---
# Lower range of the interval over which to report spurious signal values
Selection.Scan.Min:                          300
# Upper range of the interval over which to report spurious signal values
Selection.Scan.Max:                          800
# Lower end of the window to maximize over to compute the spurious signal: a number or a formula involving Scan.Var
Selection.Range.Min:                         mResonance - 20
# Upper end of the window to maximize over to compute the spurious signal: a number or a formula involving Scan.Var
Selection.Range.Max:                         mResonance + 20
# Maximum allowed value for S/delta_S
Selection.MaxSignalOverError:                0.20
# Maximum allowed value for S/S_ref
Selection.MaxSignalOverRef:                  0.10
# Integrated luminosity for which to perform the test
Selection.IntegratedLuminosity:              3.2 

# The block below defines the reference signal yield to use for the S/S_ref test. 
# One can specify either a) a reference yield, b) a reference cross-section, or c) the name of a variable giving the yield
# A definition needs to be provided if Selection.MaxSignalOverRef is defined above
#---
# Option a), Signal yield: specified either as a number or a function of Scan.Var (See above)
Selection.RefSignalYield: 
# Options b) Signal cross-section: specified either as a number or a function of Scan.Var (See above). 
# Will get multiplied by Selection.IntegratedLuminosity (see above) to obtain the yield.
Selection.RefSignalCrossSection:
# Options c) variable in a  workspace, defined in the sub-block below:
# name of the variable
Selection.RefSignalVar.Name:                 sigYield_c0
#sigYield_c0
# name of the workspace where it it stored
Selection.RefSignalVar.Workspace:            signalWS
# name of the file in which the workspace is stored
Selection.RefSignalVar.File:                 $ROOTCOREBIN/data/H2Zy/bkg/res_yield_workspace.root 
# integrated luminosity for which the variable value provides the event yield (1 by default)
Selection.RefSignalVar.IntegratedLuminosity: 1.0

# the block below specifies the dataset on which the test is performed
# ---
# file containing the histogram template
Dataset.File:                                $ROOTCOREBIN/data/H2Zy/bkg/hist.root 
# Name of histogram inside the file
Dataset.HistogramName:                       hist_inclusive_Zg_mllg3
# Integrated luminosity for which the histogram is normalized
Dataset.IntegratedLuminosity:                3.2

# the lines below specifies the signal and background normalization variables, in RooWorkspace::factory syntax (name[initial_value,min,max])
Signal.Norm:                                 nSignal[0,-100,1000]
Background.Norm:                             nBkg[100,0,1E6]

# the block below specifies the parameterization to use for the signal component, either as a) an expression or b) a PDF object in a workspace
#---
# Option a) Expression: an expression for the signal PDF either in RooWorkspace::factory syntax or as a a formula expression to use with RooGenericPdf
#Signal.PDF.Expression:
# For the case of a RooGenericPdf expression above, provides the comma-separated list of PDF parameters to use
#Signal.PDF.Parameters:
# Option b) PDF object inside a workspace, defined in the sub-block below:
# name of PDF object
Signal.PDF.Name:                             sigPdf_c0
# name of workspace where it is stored
Signal.PDF.Workspace:                        signalWS
# name of file containing the workspace
Signal.PDF.File:                             $ROOTCOREBIN/data/H2Zy/bkg/res_sigpdf_workspace.root

# list of names of the background PDFs to test. Each should correspond to a definition block below
Background.PDFs:                             Dijet  Dijet0
#ExpPoly2 Dijet Bern3 Bern4 Bern5

# Expressiion for the PDF form, either as in RooWorkspace::factory syntax or as a a formula expression to use with RooGenericPdf
ExpPoly2.Expression:                         exp((m_yy - 100)/100*(a1 + a2*(m_yy - 100)/100))
# For the case of a RooGenericPdf expression above, provides the comma-separated list of PDF parameters to use
ExpPoly2.Parameters:                         a1[0,-10,10] a2[0,-10,10]
# If set to True, the initial values of the parameters will be set from a B-only fit to the dataset above (recommended)
ExpPoly2.SetInitialValuesFromDataset:        True
# If set to True, plots will be generated for fits at each mass point. Useful for debugging but not recommended for large scans.
ExpPoly2.PerPointPlots:                      True

Dijet.Expression:                            pow(m_yy/13E3, p1)*pow(1 - m_yy/13E3, p2)
Dijet.Parameters:                            p1[0,-20,20] p2[0,-20,20]
Dijet.SetInitialValuesFromDataset:           True
Dijet.PerPointPlots:                         True


Dijet0.Expression:                            pow((1-pow(m_yy/13E3,1/3.)),p1)*pow(m_yy/13E3,p2)
Dijet0.Parameters:                            p1[-1,-500,500] p2[-4.5,-50,50]
Dijet0.SetInitialValuesFromDataset:           True
Dijet0.PerPointPlots:                         True


Bern3.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], 1 })
Bern3.SetInitialValuesFromDataset:           True
Bern3.PerPointPlots:                         True

Bern4.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], 1 })
Bern4.SetInitialValuesFromDataset:           True
Bern4.PerPointPlots:                         True

Bern5.Expression:                            RooBernstein(m_yy, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], a5[0,-10,10], 1 })
Bern5.SetInitialValuesFromDataset:           True
Bern5.PerPointPlots:                         True

Signal.PDF.FixParameters: true
