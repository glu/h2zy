# CMake generated Testfile for 
# Source directory: /afs/cern.ch/work/g/glu/public/radZAna/source
# Build directory: /afs/cern.ch/work/g/glu/public/radZAna/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("H2Zy")
subdirs("HGamCore")
subdirs("HGamCore/HGamAnalysisFramework")
subdirs("HGamCore/HGamTools")
subdirs("HGamCore/TruthWeightTools")
subdirs("HGamCore/VertexPositionReweighting")
subdirs("HGamCore/hhTruthWeightTools")
