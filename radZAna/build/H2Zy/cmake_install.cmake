# Install script for directory: /afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/InstallArea/x86_64-centos7-gcc62-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/H2Zy" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE REGEX "/[^/]*\\~$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libH2ZyLib_rdict.pcm")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
      -E make_directory
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
         -E create_symlink ../src/H2Zy/H2Zy
         $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include/H2Zy )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libH2ZyLib.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libH2ZyLib.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libH2ZyLib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libH2ZyLib.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libH2ZyLib.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "H2ZyAnalysisDATA_Rel21.cfg" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/H2ZyAnalysisDATA_Rel21.cfg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "H2ZyAnalysisMC16_Rel21.cfg" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/H2ZyAnalysisMC16_Rel21.cfg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "MC16_lumi.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/MC16_lumi.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "MC16_samples.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/MC16_samples.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "TMVAClassification_BDTG.weights.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/TMVAClassification_BDTG.weights.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/config_rel20_1" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/config_rel20_7" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data15_13TeV.DAOD_HIGG1D2.p3402.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data15_13TeV.DAOD_HIGG1D2.p3402.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data15_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data15_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data16_13TeV.DAOD_HIGG1D2.p3372.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data16_13TeV.DAOD_HIGG1D2.p3372.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data16_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data16_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data17_13TeV.DAOD_HIGG1D2.p3372_p3402.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data17_13TeV.DAOD_HIGG1D2.p3372_p3402.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data17_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data17_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "data18_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/data18_13TeV.physics_Main.deriv.DAOD_HIGG1D2.p3713.list")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "ilumicalc_histograms_None_276262-284484.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/ilumicalc_histograms_None_276262-284484.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc15c_13TeV.Zgam_25ns.PRW.config.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc15c_13TeV.Zgam_25ns.PRW.config.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16_13TeV.AOD.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16_13TeV.AOD.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16_13TeV.DAOD_HIGG1D2.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16_13TeV.DAOD_HIGG1D2.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16a_13TeV.Zgam.PRW.config.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16a_13TeV.Zgam.PRW.config.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16c_13TeV.Zgam.PRW.config.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16c_13TeV.Zgam.PRW.config.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16d_13TeV.Zgam.PRW.config.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16d_13TeV.Zgam.PRW.config.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "mc16e_13TeV.Zgam.PRW.config.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/mc16e_13TeV.Zgam.PRW.config.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "param_card_PROC_SA_CPP_heft_H_Zy_lly.dat" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/param_card_PROC_SA_CPP_heft_H_Zy_lly.dat")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE FILE RENAME "param_card_PROC_SA_CPP_sm_Zy_lly.dat" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/param_card_PROC_SA_CPP_sm_Zy_lly.dat")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/H2Zy" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/H2Zy/data/sigbkgmodel" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runH2ZyAnalysis.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runH2ZyAnalysis.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runH2ZyAnalysis")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runH2ZyAnalysis" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runH2ZyAnalysis")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runH2ZyAnalysis")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/checkGRIDjob.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/checkGRIDjob.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/checkGRIDjob")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/checkGRIDjob" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/checkGRIDjob")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/checkGRIDjob")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/downloadGRIDjob.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/downloadGRIDjob.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/downloadGRIDjob")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/downloadGRIDjob" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/downloadGRIDjob")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/downloadGRIDjob")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runZyTruthAnalysis.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runZyTruthAnalysis.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/bin/runZyTruthAnalysis")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runZyTruthAnalysis" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runZyTruthAnalysis")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/runZyTruthAnalysis")
    endif()
  endif()
endif()

