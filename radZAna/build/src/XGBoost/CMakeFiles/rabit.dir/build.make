# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.11

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost

# Include any dependencies generated for this target.
include CMakeFiles/rabit.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/rabit.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/rabit.dir/flags.make

CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o: CMakeFiles/rabit.dir/flags.make
CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o: rabit/src/allreduce_base.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o -c /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_base.cc

CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_base.cc > CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.i

CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_base.cc -o CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.s

CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o: CMakeFiles/rabit.dir/flags.make
CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o: rabit/src/allreduce_robust.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o -c /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_robust.cc

CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_robust.cc > CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.i

CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/allreduce_robust.cc -o CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.s

CMakeFiles/rabit.dir/rabit/src/engine.cc.o: CMakeFiles/rabit.dir/flags.make
CMakeFiles/rabit.dir/rabit/src/engine.cc.o: rabit/src/engine.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object CMakeFiles/rabit.dir/rabit/src/engine.cc.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/rabit.dir/rabit/src/engine.cc.o -c /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/engine.cc

CMakeFiles/rabit.dir/rabit/src/engine.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/rabit.dir/rabit/src/engine.cc.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/engine.cc > CMakeFiles/rabit.dir/rabit/src/engine.cc.i

CMakeFiles/rabit.dir/rabit/src/engine.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/rabit.dir/rabit/src/engine.cc.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/engine.cc -o CMakeFiles/rabit.dir/rabit/src/engine.cc.s

CMakeFiles/rabit.dir/rabit/src/c_api.cc.o: CMakeFiles/rabit.dir/flags.make
CMakeFiles/rabit.dir/rabit/src/c_api.cc.o: rabit/src/c_api.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CXX object CMakeFiles/rabit.dir/rabit/src/c_api.cc.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/rabit.dir/rabit/src/c_api.cc.o -c /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/c_api.cc

CMakeFiles/rabit.dir/rabit/src/c_api.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/rabit.dir/rabit/src/c_api.cc.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/c_api.cc > CMakeFiles/rabit.dir/rabit/src/c_api.cc.i

CMakeFiles/rabit.dir/rabit/src/c_api.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/rabit.dir/rabit/src/c_api.cc.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-slc6/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/rabit/src/c_api.cc -o CMakeFiles/rabit.dir/rabit/src/c_api.cc.s

# Object files for target rabit
rabit_OBJECTS = \
"CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o" \
"CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o" \
"CMakeFiles/rabit.dir/rabit/src/engine.cc.o" \
"CMakeFiles/rabit.dir/rabit/src/c_api.cc.o"

# External object files for target rabit
rabit_EXTERNAL_OBJECTS =

librabit.a: CMakeFiles/rabit.dir/rabit/src/allreduce_base.cc.o
librabit.a: CMakeFiles/rabit.dir/rabit/src/allreduce_robust.cc.o
librabit.a: CMakeFiles/rabit.dir/rabit/src/engine.cc.o
librabit.a: CMakeFiles/rabit.dir/rabit/src/c_api.cc.o
librabit.a: CMakeFiles/rabit.dir/build.make
librabit.a: CMakeFiles/rabit.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Linking CXX static library librabit.a"
	$(CMAKE_COMMAND) -P CMakeFiles/rabit.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/rabit.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/rabit.dir/build: librabit.a

.PHONY : CMakeFiles/rabit.dir/build

CMakeFiles/rabit.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/rabit.dir/cmake_clean.cmake
.PHONY : CMakeFiles/rabit.dir/clean

CMakeFiles/rabit.dir/depend:
	cd /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost /afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles/rabit.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/rabit.dir/depend

