# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/src/cli_main.cc" "/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles/runxgboost.dir/src/cli_main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DMLC_LOG_CUSTOMIZE=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "dmlc-core/include"
  "rabit/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/dmlc-core/CMakeFiles/dmlc.dir/DependInfo.cmake"
  "/afs/cern.ch/work/g/glu/public/radZAna/build/src/XGBoost/CMakeFiles/rabit.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
