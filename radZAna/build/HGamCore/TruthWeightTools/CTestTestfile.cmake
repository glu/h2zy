# CMake generated Testfile for 
# Source directory: /afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools
# Build directory: /afs/cern.ch/work/g/glu/public/radZAna/build/HGamCore/TruthWeightTools
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(TruthWeightTools_ut_HiggsWeightTool_test_ctest "/afs/cern.ch/work/g/glu/public/radZAna/build/HGamCore/TruthWeightTools/CMakeFiles/ut_HiggsWeightTool_test.sh")
set_tests_properties(TruthWeightTools_ut_HiggsWeightTool_test_ctest PROPERTIES  LABELS "TruthWeightTools" TIMEOUT "120" WORKING_DIRECTORY "/afs/cern.ch/work/g/glu/public/radZAna/build/HGamCore/TruthWeightTools/unitTestRun")
