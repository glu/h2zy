// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME HGamAnalysisFrameworkLibCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_HGcLcLConfig(void *p = 0);
   static void *newArray_HGcLcLConfig(Long_t size, void *p);
   static void delete_HGcLcLConfig(void *p);
   static void deleteArray_HGcLcLConfig(void *p);
   static void destruct_HGcLcLConfig(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HG::Config*)
   {
      ::HG::Config *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HG::Config >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HG::Config", ::HG::Config::Class_Version(), "HGamAnalysisFramework/Config.h", 35,
                  typeid(::HG::Config), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HG::Config::Dictionary, isa_proxy, 4,
                  sizeof(::HG::Config) );
      instance.SetNew(&new_HGcLcLConfig);
      instance.SetNewArray(&newArray_HGcLcLConfig);
      instance.SetDelete(&delete_HGcLcLConfig);
      instance.SetDeleteArray(&deleteArray_HGcLcLConfig);
      instance.SetDestructor(&destruct_HGcLcLConfig);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HG::Config*)
   {
      return GenerateInitInstanceLocal((::HG::Config*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HG::Config*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HgammaAnalysis(void *p = 0);
   static void *newArray_HgammaAnalysis(Long_t size, void *p);
   static void delete_HgammaAnalysis(void *p);
   static void deleteArray_HgammaAnalysis(void *p);
   static void destruct_HgammaAnalysis(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HgammaAnalysis*)
   {
      ::HgammaAnalysis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HgammaAnalysis >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HgammaAnalysis", ::HgammaAnalysis::Class_Version(), "HGamAnalysisFramework/HgammaAnalysis.h", 34,
                  typeid(::HgammaAnalysis), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HgammaAnalysis::Dictionary, isa_proxy, 4,
                  sizeof(::HgammaAnalysis) );
      instance.SetNew(&new_HgammaAnalysis);
      instance.SetNewArray(&newArray_HgammaAnalysis);
      instance.SetDelete(&delete_HgammaAnalysis);
      instance.SetDeleteArray(&deleteArray_HgammaAnalysis);
      instance.SetDestructor(&destruct_HgammaAnalysis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HgammaAnalysis*)
   {
      return GenerateInitInstanceLocal((::HgammaAnalysis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HgammaAnalysis*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace HG {
//______________________________________________________________________________
atomic_TClass_ptr Config::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Config::Class_Name()
{
   return "HG::Config";
}

//______________________________________________________________________________
const char *Config::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HG::Config*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Config::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HG::Config*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Config::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HG::Config*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Config::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HG::Config*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace HG
//______________________________________________________________________________
atomic_TClass_ptr HgammaAnalysis::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HgammaAnalysis::Class_Name()
{
   return "HgammaAnalysis";
}

//______________________________________________________________________________
const char *HgammaAnalysis::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HgammaAnalysis*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HgammaAnalysis::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HgammaAnalysis*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HgammaAnalysis::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HgammaAnalysis*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HgammaAnalysis::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HgammaAnalysis*)0x0)->GetClass(); }
   return fgIsA;
}

namespace HG {
//______________________________________________________________________________
void Config::Streamer(TBuffer &R__b)
{
   // Stream an object of class HG::Config.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HG::Config::Class(),this);
   } else {
      R__b.WriteClassBuffer(HG::Config::Class(),this);
   }
}

} // namespace HG
namespace ROOT {
   // Wrappers around operator new
   static void *new_HGcLcLConfig(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HG::Config : new ::HG::Config;
   }
   static void *newArray_HGcLcLConfig(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HG::Config[nElements] : new ::HG::Config[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGcLcLConfig(void *p) {
      delete ((::HG::Config*)p);
   }
   static void deleteArray_HGcLcLConfig(void *p) {
      delete [] ((::HG::Config*)p);
   }
   static void destruct_HGcLcLConfig(void *p) {
      typedef ::HG::Config current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HG::Config

//______________________________________________________________________________
void HgammaAnalysis::Streamer(TBuffer &R__b)
{
   // Stream an object of class HgammaAnalysis.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HgammaAnalysis::Class(),this);
   } else {
      R__b.WriteClassBuffer(HgammaAnalysis::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HgammaAnalysis(void *p) {
      return  p ? new(p) ::HgammaAnalysis : new ::HgammaAnalysis;
   }
   static void *newArray_HgammaAnalysis(Long_t nElements, void *p) {
      return p ? new(p) ::HgammaAnalysis[nElements] : new ::HgammaAnalysis[nElements];
   }
   // Wrapper around operator delete
   static void delete_HgammaAnalysis(void *p) {
      delete ((::HgammaAnalysis*)p);
   }
   static void deleteArray_HgammaAnalysis(void *p) {
      delete [] ((::HgammaAnalysis*)p);
   }
   static void destruct_HgammaAnalysis(void *p) {
      typedef ::HgammaAnalysis current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HgammaAnalysis

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *maplEintcOintgR_Dictionary();
   static void maplEintcOintgR_TClassManip(TClass*);
   static void *new_maplEintcOintgR(void *p = 0);
   static void *newArray_maplEintcOintgR(Long_t size, void *p);
   static void delete_maplEintcOintgR(void *p);
   static void deleteArray_maplEintcOintgR(void *p);
   static void destruct_maplEintcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,int>*)
   {
      map<int,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,int>", -2, "map", 96,
                  typeid(map<int,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,int>) );
      instance.SetNew(&new_maplEintcOintgR);
      instance.SetNewArray(&newArray_maplEintcOintgR);
      instance.SetDelete(&delete_maplEintcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOintgR);
      instance.SetDestructor(&destruct_maplEintcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,int>*)0x0)->GetClass();
      maplEintcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int> : new map<int,int>;
   }
   static void *newArray_maplEintcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int>[nElements] : new map<int,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOintgR(void *p) {
      delete ((map<int,int>*)p);
   }
   static void deleteArray_maplEintcOintgR(void *p) {
      delete [] ((map<int,int>*)p);
   }
   static void destruct_maplEintcOintgR(void *p) {
      typedef map<int,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,int>

namespace ROOT {
   static TClass *maplEintcOdoublegR_Dictionary();
   static void maplEintcOdoublegR_TClassManip(TClass*);
   static void *new_maplEintcOdoublegR(void *p = 0);
   static void *newArray_maplEintcOdoublegR(Long_t size, void *p);
   static void delete_maplEintcOdoublegR(void *p);
   static void deleteArray_maplEintcOdoublegR(void *p);
   static void destruct_maplEintcOdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,double>*)
   {
      map<int,double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,double>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,double>", -2, "map", 96,
                  typeid(map<int,double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,double>) );
      instance.SetNew(&new_maplEintcOdoublegR);
      instance.SetNewArray(&newArray_maplEintcOdoublegR);
      instance.SetDelete(&delete_maplEintcOdoublegR);
      instance.SetDeleteArray(&deleteArray_maplEintcOdoublegR);
      instance.SetDestructor(&destruct_maplEintcOdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,double>*)0x0)->GetClass();
      maplEintcOdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,double> : new map<int,double>;
   }
   static void *newArray_maplEintcOdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,double>[nElements] : new map<int,double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOdoublegR(void *p) {
      delete ((map<int,double>*)p);
   }
   static void deleteArray_maplEintcOdoublegR(void *p) {
      delete [] ((map<int,double>*)p);
   }
   static void destruct_maplEintcOdoublegR(void *p) {
      typedef map<int,double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,double>

namespace ROOT {
   static TClass *maplEintcOTStringgR_Dictionary();
   static void maplEintcOTStringgR_TClassManip(TClass*);
   static void *new_maplEintcOTStringgR(void *p = 0);
   static void *newArray_maplEintcOTStringgR(Long_t size, void *p);
   static void delete_maplEintcOTStringgR(void *p);
   static void deleteArray_maplEintcOTStringgR(void *p);
   static void destruct_maplEintcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,TString>*)
   {
      map<int,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,TString>", -2, "map", 96,
                  typeid(map<int,TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOTStringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,TString>) );
      instance.SetNew(&new_maplEintcOTStringgR);
      instance.SetNewArray(&newArray_maplEintcOTStringgR);
      instance.SetDelete(&delete_maplEintcOTStringgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOTStringgR);
      instance.SetDestructor(&destruct_maplEintcOTStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,TString>*)0x0)->GetClass();
      maplEintcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,TString> : new map<int,TString>;
   }
   static void *newArray_maplEintcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,TString>[nElements] : new map<int,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOTStringgR(void *p) {
      delete ((map<int,TString>*)p);
   }
   static void deleteArray_maplEintcOTStringgR(void *p) {
      delete [] ((map<int,TString>*)p);
   }
   static void destruct_maplEintcOTStringgR(void *p) {
      typedef map<int,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,TString>

namespace ROOT {
   static TClass *maplETStringcOintgR_Dictionary();
   static void maplETStringcOintgR_TClassManip(TClass*);
   static void *new_maplETStringcOintgR(void *p = 0);
   static void *newArray_maplETStringcOintgR(Long_t size, void *p);
   static void delete_maplETStringcOintgR(void *p);
   static void deleteArray_maplETStringcOintgR(void *p);
   static void destruct_maplETStringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,int>*)
   {
      map<TString,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,int>", -2, "map", 96,
                  typeid(map<TString,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,int>) );
      instance.SetNew(&new_maplETStringcOintgR);
      instance.SetNewArray(&newArray_maplETStringcOintgR);
      instance.SetDelete(&delete_maplETStringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOintgR);
      instance.SetDestructor(&destruct_maplETStringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,int>*)0x0)->GetClass();
      maplETStringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,int> : new map<TString,int>;
   }
   static void *newArray_maplETStringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,int>[nElements] : new map<TString,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOintgR(void *p) {
      delete ((map<TString,int>*)p);
   }
   static void deleteArray_maplETStringcOintgR(void *p) {
      delete [] ((map<TString,int>*)p);
   }
   static void destruct_maplETStringcOintgR(void *p) {
      typedef map<TString,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,int>

namespace {
  void TriggerDictionaryInitialization_libHGamAnalysisFrameworkLib_Impl() {
    static const char* headers[] = {
"HGamAnalysisFramework/HgammaAnalysis.h",
"HGamAnalysisFramework/Config.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/FourMomUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/TriggerAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/IsolationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/RecoToolInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/VertexPositionReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetReclustering",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/egamma/egammaLayerRecalibTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/egamma/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigBunchCrossingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetReclustering",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/FourMomUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/TriggerAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/build/CMakeFiles/XGBoostBuild/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/egamma/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODHIEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/IsolationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/RecoToolInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigBunchCrossingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigAnalysisInterfaces",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/VertexPositionReweighting",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/VertexPositionReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/egamma/egammaLayerRecalibTool",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/hhTruthWeightTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/hhTruthWeightTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/build/HGamCore/HGamAnalysisFramework/CMakeFiles/makeHGamAnalysisFrameworkLibCintDict.QnPzV0/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libHGamAnalysisFrameworkLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace HG{class __attribute__((annotate("$clingAutoload$HGamAnalysisFramework/Config.h")))  Config;}
class __attribute__((annotate("$clingAutoload$HGamAnalysisFramework/HgammaAnalysis.h")))  HgammaAnalysis;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libHGamAnalysisFrameworkLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef __RELEASE__
  #define __RELEASE__ 1376840
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "HGamAnalysisFramework-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ HGamAnalysisFramework-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"

#ifdef __CINT__

  #pragma link off all globals;
  #pragma link off all classes;
  #pragma link off all functions;
  #pragma link C++ nestedclass;

  #pragma link C++ class HgammaAnalysis+;
  #pragma link C++ class HG::Config+;

#endif


#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"HG::Config", payloadCode, "@",
"HgammaAnalysis", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libHGamAnalysisFrameworkLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libHGamAnalysisFrameworkLib_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libHGamAnalysisFrameworkLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libHGamAnalysisFrameworkLib() {
  TriggerDictionaryInitialization_libHGamAnalysisFrameworkLib_Impl();
}
