# Install script for directory: /afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/InstallArea/x86_64-centos7-gcc62-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/HGamCore/HGamAnalysisFramework" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE REGEX "/[^/]*\\~$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/CMakeFiles/XGBoostBuild/" USE_SOURCE_PERMISSIONS)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libHGamAnalysisFrameworkLib_rdict.pcm")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
      -E make_directory
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
         -E create_symlink ../src/HGamCore/HGamAnalysisFramework/HGamAnalysisFramework
         $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include/HGamAnalysisFramework )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libHGamAnalysisFrameworkLib.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/afs/cern.ch/work/g/glu/public/radZAna/build/x86_64-centos7-gcc62-opt/lib/libHGamAnalysisFrameworkLib.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libHGamAnalysisFrameworkLib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libHGamAnalysisFrameworkLib.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libHGamAnalysisFrameworkLib.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/BDT" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "HGamDC14.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/HGamDC14.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "HGamRel20p1.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/HGamRel20p1.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "HGamRel20p7.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/HGamRel20p7.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "HGamRel21.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/HGamRel21.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "MCBunchStructure.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/MCBunchStructure.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "MCSamples.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/MCSamples.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16a_fastSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16a_fastSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16a_fullSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16a_fullSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16c_fastSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16c_fastSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16c_fullSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16c_fullSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16d_fastSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16d_fastSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16d_fullSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16d_fullSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16e_fastSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16e_fastSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PRW_mc16e_fullSim.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PRW_mc16e_fullSim.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "PhotonFakeRates.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/PhotonFakeRates.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h014.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h014.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h014a.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h014a.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h014b.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h014b.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h015.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h015.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h015b.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h015b.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h015c.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h015c.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h015d.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h015d.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h024a.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h024a.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h024d.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h024d.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "SumOfWeights_h024e.config" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/SumOfWeights_h024e.config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVAClassification_BDT.weights.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVAClassification_BDT.weights.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVA_BDT_HadtH.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVA_BDT_HadtH.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVA_BDT_HadtHfullmyy.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVA_BDT_HadtHfullmyy.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVA_BDT_Hadtt.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVA_BDT_Hadtt.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVA_BDT_Hadttfullmyy.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVA_BDT_Hadttfullmyy.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "TMVA_BDT_LeptH.xml" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/TMVA_BDT_LeptH.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE DIRECTORY FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/lwtnn" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "mc16a_defaults_buggy.NotRecommended.prw.root" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/mc16a_defaults_buggy.NotRecommended.prw.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "model_hadronic_bdt.h5" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/model_hadronic_bdt.h5")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "model_hadronic_bdt_plusTopReco.h5" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/model_hadronic_bdt_plusTopReco.h5")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "model_leptonic_bdt.h5" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/model_leptonic_bdt.h5")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "model_leptonic_bdt_plusTopReco.h5" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/model_leptonic_bdt_plusTopReco.h5")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "model_topRecoBDT.h5" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/model_topRecoBDT.h5")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "packagesRel19.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/packagesRel19.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "packagesRel20p1.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/packagesRel20p1.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "packagesRel20p7.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/packagesRel20p7.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/HGamAnalysisFramework" TYPE FILE RENAME "packagesRel21.txt" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/data/packagesRel21.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "make_hgam_pkg" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/scripts/make_hgam_pkg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "add_hgam_alg_to_pkg" FILES "/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework/scripts/add_hgam_alg_to_pkg")
endif()

