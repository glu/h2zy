// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME HGamToolsLibCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HGamTools/HGamCutflowAndMxAOD.h"
#include "HGamTools/HGamExample.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include "HGamTools/HGamYieldExample.h"
#include "HGamTools/SkimAndSlim.h"
#include "HGamTools/DumpNTUP.h"
#include "HGamTools/BasicEventDisplayTool.h"
#include "HGamTools/HGamEventDisplay.h"
#include "HGamTools/HGamSimpleExample.h"
#include "HGamTools/XSecSkimAndSlim.h"
#include "HGamTools/PDFTool.h"
#include "HGamTools/HGamYieldExample.h"
#include "HGamTools/SkimAndSlim.h"
#include "HGamTools/DumpNTUP.h"
#include "HGamTools/BasicEventDisplayTool.h"
#include "HGamTools/HGamEventDisplay.h"
#include "HGamTools/HGamSimpleExample.h"
#include "HGamTools/XSecSkimAndSlim.h"
#include "HGamTools/HGamCutflowAndMxAOD.h"
#include "HGamTools/HGamExample.h"
#include "HGamTools/HggTwoSidedCBPdf.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_PDFTool(void *p = 0);
   static void *newArray_PDFTool(Long_t size, void *p);
   static void delete_PDFTool(void *p);
   static void deleteArray_PDFTool(void *p);
   static void destruct_PDFTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PDFTool*)
   {
      ::PDFTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PDFTool >(0);
      static ::ROOT::TGenericClassInfo 
         instance("PDFTool", ::PDFTool::Class_Version(), "HGamTools/PDFTool.h", 11,
                  typeid(::PDFTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::PDFTool::Dictionary, isa_proxy, 4,
                  sizeof(::PDFTool) );
      instance.SetNew(&new_PDFTool);
      instance.SetNewArray(&newArray_PDFTool);
      instance.SetDelete(&delete_PDFTool);
      instance.SetDeleteArray(&deleteArray_PDFTool);
      instance.SetDestructor(&destruct_PDFTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PDFTool*)
   {
      return GenerateInitInstanceLocal((::PDFTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::PDFTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HGamCutflowAndMxAOD(void *p = 0);
   static void *newArray_HGamCutflowAndMxAOD(Long_t size, void *p);
   static void delete_HGamCutflowAndMxAOD(void *p);
   static void deleteArray_HGamCutflowAndMxAOD(void *p);
   static void destruct_HGamCutflowAndMxAOD(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HGamCutflowAndMxAOD*)
   {
      ::HGamCutflowAndMxAOD *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HGamCutflowAndMxAOD >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HGamCutflowAndMxAOD", ::HGamCutflowAndMxAOD::Class_Version(), "HGamTools/HGamCutflowAndMxAOD.h", 6,
                  typeid(::HGamCutflowAndMxAOD), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HGamCutflowAndMxAOD::Dictionary, isa_proxy, 4,
                  sizeof(::HGamCutflowAndMxAOD) );
      instance.SetNew(&new_HGamCutflowAndMxAOD);
      instance.SetNewArray(&newArray_HGamCutflowAndMxAOD);
      instance.SetDelete(&delete_HGamCutflowAndMxAOD);
      instance.SetDeleteArray(&deleteArray_HGamCutflowAndMxAOD);
      instance.SetDestructor(&destruct_HGamCutflowAndMxAOD);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HGamCutflowAndMxAOD*)
   {
      return GenerateInitInstanceLocal((::HGamCutflowAndMxAOD*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HGamCutflowAndMxAOD*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HGamExample(void *p = 0);
   static void *newArray_HGamExample(Long_t size, void *p);
   static void delete_HGamExample(void *p);
   static void deleteArray_HGamExample(void *p);
   static void destruct_HGamExample(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HGamExample*)
   {
      ::HGamExample *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HGamExample >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HGamExample", ::HGamExample::Class_Version(), "HGamTools/HGamExample.h", 7,
                  typeid(::HGamExample), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HGamExample::Dictionary, isa_proxy, 4,
                  sizeof(::HGamExample) );
      instance.SetNew(&new_HGamExample);
      instance.SetNewArray(&newArray_HGamExample);
      instance.SetDelete(&delete_HGamExample);
      instance.SetDeleteArray(&deleteArray_HGamExample);
      instance.SetDestructor(&destruct_HGamExample);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HGamExample*)
   {
      return GenerateInitInstanceLocal((::HGamExample*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HGamExample*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HggTwoSidedCBPdf(void *p = 0);
   static void *newArray_HggTwoSidedCBPdf(Long_t size, void *p);
   static void delete_HggTwoSidedCBPdf(void *p);
   static void deleteArray_HggTwoSidedCBPdf(void *p);
   static void destruct_HggTwoSidedCBPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggTwoSidedCBPdf*)
   {
      ::HggTwoSidedCBPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggTwoSidedCBPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggTwoSidedCBPdf", ::HggTwoSidedCBPdf::Class_Version(), "HGamTools/HggTwoSidedCBPdf.h", 17,
                  typeid(::HggTwoSidedCBPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggTwoSidedCBPdf::Dictionary, isa_proxy, 4,
                  sizeof(::HggTwoSidedCBPdf) );
      instance.SetNew(&new_HggTwoSidedCBPdf);
      instance.SetNewArray(&newArray_HggTwoSidedCBPdf);
      instance.SetDelete(&delete_HggTwoSidedCBPdf);
      instance.SetDeleteArray(&deleteArray_HggTwoSidedCBPdf);
      instance.SetDestructor(&destruct_HggTwoSidedCBPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggTwoSidedCBPdf*)
   {
      return GenerateInitInstanceLocal((::HggTwoSidedCBPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HGamYieldExample(void *p = 0);
   static void *newArray_HGamYieldExample(Long_t size, void *p);
   static void delete_HGamYieldExample(void *p);
   static void deleteArray_HGamYieldExample(void *p);
   static void destruct_HGamYieldExample(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HGamYieldExample*)
   {
      ::HGamYieldExample *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HGamYieldExample >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HGamYieldExample", ::HGamYieldExample::Class_Version(), "HGamTools/HGamYieldExample.h", 6,
                  typeid(::HGamYieldExample), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HGamYieldExample::Dictionary, isa_proxy, 4,
                  sizeof(::HGamYieldExample) );
      instance.SetNew(&new_HGamYieldExample);
      instance.SetNewArray(&newArray_HGamYieldExample);
      instance.SetDelete(&delete_HGamYieldExample);
      instance.SetDeleteArray(&deleteArray_HGamYieldExample);
      instance.SetDestructor(&destruct_HGamYieldExample);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HGamYieldExample*)
   {
      return GenerateInitInstanceLocal((::HGamYieldExample*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HGamYieldExample*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_SkimAndSlim(void *p = 0);
   static void *newArray_SkimAndSlim(Long_t size, void *p);
   static void delete_SkimAndSlim(void *p);
   static void deleteArray_SkimAndSlim(void *p);
   static void destruct_SkimAndSlim(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SkimAndSlim*)
   {
      ::SkimAndSlim *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SkimAndSlim >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SkimAndSlim", ::SkimAndSlim::Class_Version(), "HGamTools/SkimAndSlim.h", 14,
                  typeid(::SkimAndSlim), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::SkimAndSlim::Dictionary, isa_proxy, 4,
                  sizeof(::SkimAndSlim) );
      instance.SetNew(&new_SkimAndSlim);
      instance.SetNewArray(&newArray_SkimAndSlim);
      instance.SetDelete(&delete_SkimAndSlim);
      instance.SetDeleteArray(&deleteArray_SkimAndSlim);
      instance.SetDestructor(&destruct_SkimAndSlim);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SkimAndSlim*)
   {
      return GenerateInitInstanceLocal((::SkimAndSlim*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::SkimAndSlim*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_DumpNTUP(void *p = 0);
   static void *newArray_DumpNTUP(Long_t size, void *p);
   static void delete_DumpNTUP(void *p);
   static void deleteArray_DumpNTUP(void *p);
   static void destruct_DumpNTUP(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DumpNTUP*)
   {
      ::DumpNTUP *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::DumpNTUP >(0);
      static ::ROOT::TGenericClassInfo 
         instance("DumpNTUP", ::DumpNTUP::Class_Version(), "HGamTools/DumpNTUP.h", 7,
                  typeid(::DumpNTUP), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::DumpNTUP::Dictionary, isa_proxy, 4,
                  sizeof(::DumpNTUP) );
      instance.SetNew(&new_DumpNTUP);
      instance.SetNewArray(&newArray_DumpNTUP);
      instance.SetDelete(&delete_DumpNTUP);
      instance.SetDeleteArray(&deleteArray_DumpNTUP);
      instance.SetDestructor(&destruct_DumpNTUP);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DumpNTUP*)
   {
      return GenerateInitInstanceLocal((::DumpNTUP*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DumpNTUP*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *BasicEventDisplayTool_Dictionary();
   static void BasicEventDisplayTool_TClassManip(TClass*);
   static void *new_BasicEventDisplayTool(void *p = 0);
   static void *newArray_BasicEventDisplayTool(Long_t size, void *p);
   static void delete_BasicEventDisplayTool(void *p);
   static void deleteArray_BasicEventDisplayTool(void *p);
   static void destruct_BasicEventDisplayTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BasicEventDisplayTool*)
   {
      ::BasicEventDisplayTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::BasicEventDisplayTool));
      static ::ROOT::TGenericClassInfo 
         instance("BasicEventDisplayTool", "HGamTools/BasicEventDisplayTool.h", 60,
                  typeid(::BasicEventDisplayTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &BasicEventDisplayTool_Dictionary, isa_proxy, 4,
                  sizeof(::BasicEventDisplayTool) );
      instance.SetNew(&new_BasicEventDisplayTool);
      instance.SetNewArray(&newArray_BasicEventDisplayTool);
      instance.SetDelete(&delete_BasicEventDisplayTool);
      instance.SetDeleteArray(&deleteArray_BasicEventDisplayTool);
      instance.SetDestructor(&destruct_BasicEventDisplayTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BasicEventDisplayTool*)
   {
      return GenerateInitInstanceLocal((::BasicEventDisplayTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::BasicEventDisplayTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *BasicEventDisplayTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::BasicEventDisplayTool*)0x0)->GetClass();
      BasicEventDisplayTool_TClassManip(theClass);
   return theClass;
   }

   static void BasicEventDisplayTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HGamEventDisplay(void *p = 0);
   static void *newArray_HGamEventDisplay(Long_t size, void *p);
   static void delete_HGamEventDisplay(void *p);
   static void deleteArray_HGamEventDisplay(void *p);
   static void destruct_HGamEventDisplay(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HGamEventDisplay*)
   {
      ::HGamEventDisplay *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HGamEventDisplay >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HGamEventDisplay", ::HGamEventDisplay::Class_Version(), "HGamTools/HGamEventDisplay.h", 10,
                  typeid(::HGamEventDisplay), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HGamEventDisplay::Dictionary, isa_proxy, 4,
                  sizeof(::HGamEventDisplay) );
      instance.SetNew(&new_HGamEventDisplay);
      instance.SetNewArray(&newArray_HGamEventDisplay);
      instance.SetDelete(&delete_HGamEventDisplay);
      instance.SetDeleteArray(&deleteArray_HGamEventDisplay);
      instance.SetDestructor(&destruct_HGamEventDisplay);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HGamEventDisplay*)
   {
      return GenerateInitInstanceLocal((::HGamEventDisplay*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HGamEventDisplay*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HGamSimpleExample(void *p = 0);
   static void *newArray_HGamSimpleExample(Long_t size, void *p);
   static void delete_HGamSimpleExample(void *p);
   static void deleteArray_HGamSimpleExample(void *p);
   static void destruct_HGamSimpleExample(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HGamSimpleExample*)
   {
      ::HGamSimpleExample *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HGamSimpleExample >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HGamSimpleExample", ::HGamSimpleExample::Class_Version(), "HGamTools/HGamSimpleExample.h", 6,
                  typeid(::HGamSimpleExample), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HGamSimpleExample::Dictionary, isa_proxy, 4,
                  sizeof(::HGamSimpleExample) );
      instance.SetNew(&new_HGamSimpleExample);
      instance.SetNewArray(&newArray_HGamSimpleExample);
      instance.SetDelete(&delete_HGamSimpleExample);
      instance.SetDeleteArray(&deleteArray_HGamSimpleExample);
      instance.SetDestructor(&destruct_HGamSimpleExample);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HGamSimpleExample*)
   {
      return GenerateInitInstanceLocal((::HGamSimpleExample*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HGamSimpleExample*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_XSecSkimAndSlim(void *p = 0);
   static void *newArray_XSecSkimAndSlim(Long_t size, void *p);
   static void delete_XSecSkimAndSlim(void *p);
   static void deleteArray_XSecSkimAndSlim(void *p);
   static void destruct_XSecSkimAndSlim(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::XSecSkimAndSlim*)
   {
      ::XSecSkimAndSlim *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::XSecSkimAndSlim >(0);
      static ::ROOT::TGenericClassInfo 
         instance("XSecSkimAndSlim", ::XSecSkimAndSlim::Class_Version(), "HGamTools/XSecSkimAndSlim.h", 8,
                  typeid(::XSecSkimAndSlim), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::XSecSkimAndSlim::Dictionary, isa_proxy, 4,
                  sizeof(::XSecSkimAndSlim) );
      instance.SetNew(&new_XSecSkimAndSlim);
      instance.SetNewArray(&newArray_XSecSkimAndSlim);
      instance.SetDelete(&delete_XSecSkimAndSlim);
      instance.SetDeleteArray(&deleteArray_XSecSkimAndSlim);
      instance.SetDestructor(&destruct_XSecSkimAndSlim);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::XSecSkimAndSlim*)
   {
      return GenerateInitInstanceLocal((::XSecSkimAndSlim*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::XSecSkimAndSlim*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr PDFTool::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PDFTool::Class_Name()
{
   return "PDFTool";
}

//______________________________________________________________________________
const char *PDFTool::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PDFTool*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PDFTool::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PDFTool*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PDFTool::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PDFTool*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PDFTool::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PDFTool*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HGamCutflowAndMxAOD::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HGamCutflowAndMxAOD::Class_Name()
{
   return "HGamCutflowAndMxAOD";
}

//______________________________________________________________________________
const char *HGamCutflowAndMxAOD::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamCutflowAndMxAOD*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HGamCutflowAndMxAOD::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamCutflowAndMxAOD*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HGamCutflowAndMxAOD::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamCutflowAndMxAOD*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HGamCutflowAndMxAOD::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamCutflowAndMxAOD*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HGamExample::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HGamExample::Class_Name()
{
   return "HGamExample";
}

//______________________________________________________________________________
const char *HGamExample::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamExample*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HGamExample::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamExample*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HGamExample::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamExample*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HGamExample::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamExample*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HggTwoSidedCBPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggTwoSidedCBPdf::Class_Name()
{
   return "HggTwoSidedCBPdf";
}

//______________________________________________________________________________
const char *HggTwoSidedCBPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggTwoSidedCBPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggTwoSidedCBPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggTwoSidedCBPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HGamYieldExample::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HGamYieldExample::Class_Name()
{
   return "HGamYieldExample";
}

//______________________________________________________________________________
const char *HGamYieldExample::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamYieldExample*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HGamYieldExample::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamYieldExample*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HGamYieldExample::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamYieldExample*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HGamYieldExample::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamYieldExample*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr SkimAndSlim::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *SkimAndSlim::Class_Name()
{
   return "SkimAndSlim";
}

//______________________________________________________________________________
const char *SkimAndSlim::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SkimAndSlim*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SkimAndSlim::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SkimAndSlim*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *SkimAndSlim::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SkimAndSlim*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *SkimAndSlim::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SkimAndSlim*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr DumpNTUP::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *DumpNTUP::Class_Name()
{
   return "DumpNTUP";
}

//______________________________________________________________________________
const char *DumpNTUP::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DumpNTUP*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int DumpNTUP::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DumpNTUP*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *DumpNTUP::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DumpNTUP*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *DumpNTUP::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DumpNTUP*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HGamEventDisplay::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HGamEventDisplay::Class_Name()
{
   return "HGamEventDisplay";
}

//______________________________________________________________________________
const char *HGamEventDisplay::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamEventDisplay*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HGamEventDisplay::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamEventDisplay*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HGamEventDisplay::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamEventDisplay*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HGamEventDisplay::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamEventDisplay*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HGamSimpleExample::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HGamSimpleExample::Class_Name()
{
   return "HGamSimpleExample";
}

//______________________________________________________________________________
const char *HGamSimpleExample::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamSimpleExample*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HGamSimpleExample::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HGamSimpleExample*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HGamSimpleExample::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamSimpleExample*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HGamSimpleExample::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HGamSimpleExample*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr XSecSkimAndSlim::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *XSecSkimAndSlim::Class_Name()
{
   return "XSecSkimAndSlim";
}

//______________________________________________________________________________
const char *XSecSkimAndSlim::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::XSecSkimAndSlim*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int XSecSkimAndSlim::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::XSecSkimAndSlim*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *XSecSkimAndSlim::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::XSecSkimAndSlim*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *XSecSkimAndSlim::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::XSecSkimAndSlim*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void PDFTool::Streamer(TBuffer &R__b)
{
   // Stream an object of class PDFTool.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(PDFTool::Class(),this);
   } else {
      R__b.WriteClassBuffer(PDFTool::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PDFTool(void *p) {
      return  p ? new(p) ::PDFTool : new ::PDFTool;
   }
   static void *newArray_PDFTool(Long_t nElements, void *p) {
      return p ? new(p) ::PDFTool[nElements] : new ::PDFTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_PDFTool(void *p) {
      delete ((::PDFTool*)p);
   }
   static void deleteArray_PDFTool(void *p) {
      delete [] ((::PDFTool*)p);
   }
   static void destruct_PDFTool(void *p) {
      typedef ::PDFTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PDFTool

//______________________________________________________________________________
void HGamCutflowAndMxAOD::Streamer(TBuffer &R__b)
{
   // Stream an object of class HGamCutflowAndMxAOD.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HGamCutflowAndMxAOD::Class(),this);
   } else {
      R__b.WriteClassBuffer(HGamCutflowAndMxAOD::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HGamCutflowAndMxAOD(void *p) {
      return  p ? new(p) ::HGamCutflowAndMxAOD : new ::HGamCutflowAndMxAOD;
   }
   static void *newArray_HGamCutflowAndMxAOD(Long_t nElements, void *p) {
      return p ? new(p) ::HGamCutflowAndMxAOD[nElements] : new ::HGamCutflowAndMxAOD[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGamCutflowAndMxAOD(void *p) {
      delete ((::HGamCutflowAndMxAOD*)p);
   }
   static void deleteArray_HGamCutflowAndMxAOD(void *p) {
      delete [] ((::HGamCutflowAndMxAOD*)p);
   }
   static void destruct_HGamCutflowAndMxAOD(void *p) {
      typedef ::HGamCutflowAndMxAOD current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HGamCutflowAndMxAOD

//______________________________________________________________________________
void HGamExample::Streamer(TBuffer &R__b)
{
   // Stream an object of class HGamExample.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HGamExample::Class(),this);
   } else {
      R__b.WriteClassBuffer(HGamExample::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HGamExample(void *p) {
      return  p ? new(p) ::HGamExample : new ::HGamExample;
   }
   static void *newArray_HGamExample(Long_t nElements, void *p) {
      return p ? new(p) ::HGamExample[nElements] : new ::HGamExample[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGamExample(void *p) {
      delete ((::HGamExample*)p);
   }
   static void deleteArray_HGamExample(void *p) {
      delete [] ((::HGamExample*)p);
   }
   static void destruct_HGamExample(void *p) {
      typedef ::HGamExample current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HGamExample

//______________________________________________________________________________
void HggTwoSidedCBPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggTwoSidedCBPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HggTwoSidedCBPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(HggTwoSidedCBPdf::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggTwoSidedCBPdf(void *p) {
      return  p ? new(p) ::HggTwoSidedCBPdf : new ::HggTwoSidedCBPdf;
   }
   static void *newArray_HggTwoSidedCBPdf(Long_t nElements, void *p) {
      return p ? new(p) ::HggTwoSidedCBPdf[nElements] : new ::HggTwoSidedCBPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggTwoSidedCBPdf(void *p) {
      delete ((::HggTwoSidedCBPdf*)p);
   }
   static void deleteArray_HggTwoSidedCBPdf(void *p) {
      delete [] ((::HggTwoSidedCBPdf*)p);
   }
   static void destruct_HggTwoSidedCBPdf(void *p) {
      typedef ::HggTwoSidedCBPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HggTwoSidedCBPdf

//______________________________________________________________________________
void HGamYieldExample::Streamer(TBuffer &R__b)
{
   // Stream an object of class HGamYieldExample.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HGamYieldExample::Class(),this);
   } else {
      R__b.WriteClassBuffer(HGamYieldExample::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HGamYieldExample(void *p) {
      return  p ? new(p) ::HGamYieldExample : new ::HGamYieldExample;
   }
   static void *newArray_HGamYieldExample(Long_t nElements, void *p) {
      return p ? new(p) ::HGamYieldExample[nElements] : new ::HGamYieldExample[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGamYieldExample(void *p) {
      delete ((::HGamYieldExample*)p);
   }
   static void deleteArray_HGamYieldExample(void *p) {
      delete [] ((::HGamYieldExample*)p);
   }
   static void destruct_HGamYieldExample(void *p) {
      typedef ::HGamYieldExample current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HGamYieldExample

//______________________________________________________________________________
void SkimAndSlim::Streamer(TBuffer &R__b)
{
   // Stream an object of class SkimAndSlim.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SkimAndSlim::Class(),this);
   } else {
      R__b.WriteClassBuffer(SkimAndSlim::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SkimAndSlim(void *p) {
      return  p ? new(p) ::SkimAndSlim : new ::SkimAndSlim;
   }
   static void *newArray_SkimAndSlim(Long_t nElements, void *p) {
      return p ? new(p) ::SkimAndSlim[nElements] : new ::SkimAndSlim[nElements];
   }
   // Wrapper around operator delete
   static void delete_SkimAndSlim(void *p) {
      delete ((::SkimAndSlim*)p);
   }
   static void deleteArray_SkimAndSlim(void *p) {
      delete [] ((::SkimAndSlim*)p);
   }
   static void destruct_SkimAndSlim(void *p) {
      typedef ::SkimAndSlim current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SkimAndSlim

//______________________________________________________________________________
void DumpNTUP::Streamer(TBuffer &R__b)
{
   // Stream an object of class DumpNTUP.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(DumpNTUP::Class(),this);
   } else {
      R__b.WriteClassBuffer(DumpNTUP::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_DumpNTUP(void *p) {
      return  p ? new(p) ::DumpNTUP : new ::DumpNTUP;
   }
   static void *newArray_DumpNTUP(Long_t nElements, void *p) {
      return p ? new(p) ::DumpNTUP[nElements] : new ::DumpNTUP[nElements];
   }
   // Wrapper around operator delete
   static void delete_DumpNTUP(void *p) {
      delete ((::DumpNTUP*)p);
   }
   static void deleteArray_DumpNTUP(void *p) {
      delete [] ((::DumpNTUP*)p);
   }
   static void destruct_DumpNTUP(void *p) {
      typedef ::DumpNTUP current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DumpNTUP

namespace ROOT {
   // Wrappers around operator new
   static void *new_BasicEventDisplayTool(void *p) {
      return  p ? new(p) ::BasicEventDisplayTool : new ::BasicEventDisplayTool;
   }
   static void *newArray_BasicEventDisplayTool(Long_t nElements, void *p) {
      return p ? new(p) ::BasicEventDisplayTool[nElements] : new ::BasicEventDisplayTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_BasicEventDisplayTool(void *p) {
      delete ((::BasicEventDisplayTool*)p);
   }
   static void deleteArray_BasicEventDisplayTool(void *p) {
      delete [] ((::BasicEventDisplayTool*)p);
   }
   static void destruct_BasicEventDisplayTool(void *p) {
      typedef ::BasicEventDisplayTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BasicEventDisplayTool

//______________________________________________________________________________
void HGamEventDisplay::Streamer(TBuffer &R__b)
{
   // Stream an object of class HGamEventDisplay.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HGamEventDisplay::Class(),this);
   } else {
      R__b.WriteClassBuffer(HGamEventDisplay::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HGamEventDisplay(void *p) {
      return  p ? new(p) ::HGamEventDisplay : new ::HGamEventDisplay;
   }
   static void *newArray_HGamEventDisplay(Long_t nElements, void *p) {
      return p ? new(p) ::HGamEventDisplay[nElements] : new ::HGamEventDisplay[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGamEventDisplay(void *p) {
      delete ((::HGamEventDisplay*)p);
   }
   static void deleteArray_HGamEventDisplay(void *p) {
      delete [] ((::HGamEventDisplay*)p);
   }
   static void destruct_HGamEventDisplay(void *p) {
      typedef ::HGamEventDisplay current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HGamEventDisplay

//______________________________________________________________________________
void HGamSimpleExample::Streamer(TBuffer &R__b)
{
   // Stream an object of class HGamSimpleExample.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HGamSimpleExample::Class(),this);
   } else {
      R__b.WriteClassBuffer(HGamSimpleExample::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HGamSimpleExample(void *p) {
      return  p ? new(p) ::HGamSimpleExample : new ::HGamSimpleExample;
   }
   static void *newArray_HGamSimpleExample(Long_t nElements, void *p) {
      return p ? new(p) ::HGamSimpleExample[nElements] : new ::HGamSimpleExample[nElements];
   }
   // Wrapper around operator delete
   static void delete_HGamSimpleExample(void *p) {
      delete ((::HGamSimpleExample*)p);
   }
   static void deleteArray_HGamSimpleExample(void *p) {
      delete [] ((::HGamSimpleExample*)p);
   }
   static void destruct_HGamSimpleExample(void *p) {
      typedef ::HGamSimpleExample current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HGamSimpleExample

//______________________________________________________________________________
void XSecSkimAndSlim::Streamer(TBuffer &R__b)
{
   // Stream an object of class XSecSkimAndSlim.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(XSecSkimAndSlim::Class(),this);
   } else {
      R__b.WriteClassBuffer(XSecSkimAndSlim::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_XSecSkimAndSlim(void *p) {
      return  p ? new(p) ::XSecSkimAndSlim : new ::XSecSkimAndSlim;
   }
   static void *newArray_XSecSkimAndSlim(Long_t nElements, void *p) {
      return p ? new(p) ::XSecSkimAndSlim[nElements] : new ::XSecSkimAndSlim[nElements];
   }
   // Wrapper around operator delete
   static void delete_XSecSkimAndSlim(void *p) {
      delete ((::XSecSkimAndSlim*)p);
   }
   static void deleteArray_XSecSkimAndSlim(void *p) {
      delete [] ((::XSecSkimAndSlim*)p);
   }
   static void destruct_XSecSkimAndSlim(void *p) {
      typedef ::XSecSkimAndSlim current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::XSecSkimAndSlim

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *maplEintcOintgR_Dictionary();
   static void maplEintcOintgR_TClassManip(TClass*);
   static void *new_maplEintcOintgR(void *p = 0);
   static void *newArray_maplEintcOintgR(Long_t size, void *p);
   static void delete_maplEintcOintgR(void *p);
   static void deleteArray_maplEintcOintgR(void *p);
   static void destruct_maplEintcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,int>*)
   {
      map<int,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,int>", -2, "map", 96,
                  typeid(map<int,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,int>) );
      instance.SetNew(&new_maplEintcOintgR);
      instance.SetNewArray(&newArray_maplEintcOintgR);
      instance.SetDelete(&delete_maplEintcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOintgR);
      instance.SetDestructor(&destruct_maplEintcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,int>*)0x0)->GetClass();
      maplEintcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int> : new map<int,int>;
   }
   static void *newArray_maplEintcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int>[nElements] : new map<int,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOintgR(void *p) {
      delete ((map<int,int>*)p);
   }
   static void deleteArray_maplEintcOintgR(void *p) {
      delete [] ((map<int,int>*)p);
   }
   static void destruct_maplEintcOintgR(void *p) {
      typedef map<int,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,int>

namespace ROOT {
   static TClass *maplEintcOTH1FmUgR_Dictionary();
   static void maplEintcOTH1FmUgR_TClassManip(TClass*);
   static void *new_maplEintcOTH1FmUgR(void *p = 0);
   static void *newArray_maplEintcOTH1FmUgR(Long_t size, void *p);
   static void delete_maplEintcOTH1FmUgR(void *p);
   static void deleteArray_maplEintcOTH1FmUgR(void *p);
   static void destruct_maplEintcOTH1FmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,TH1F*>*)
   {
      map<int,TH1F*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,TH1F*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,TH1F*>", -2, "map", 96,
                  typeid(map<int,TH1F*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOTH1FmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,TH1F*>) );
      instance.SetNew(&new_maplEintcOTH1FmUgR);
      instance.SetNewArray(&newArray_maplEintcOTH1FmUgR);
      instance.SetDelete(&delete_maplEintcOTH1FmUgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOTH1FmUgR);
      instance.SetDestructor(&destruct_maplEintcOTH1FmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,TH1F*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,TH1F*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOTH1FmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,TH1F*>*)0x0)->GetClass();
      maplEintcOTH1FmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOTH1FmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOTH1FmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,TH1F*> : new map<int,TH1F*>;
   }
   static void *newArray_maplEintcOTH1FmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,TH1F*>[nElements] : new map<int,TH1F*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOTH1FmUgR(void *p) {
      delete ((map<int,TH1F*>*)p);
   }
   static void deleteArray_maplEintcOTH1FmUgR(void *p) {
      delete [] ((map<int,TH1F*>*)p);
   }
   static void destruct_maplEintcOTH1FmUgR(void *p) {
      typedef map<int,TH1F*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,TH1F*>

namespace {
  void TriggerDictionaryInitialization_libHGamToolsLib_Impl() {
    static const char* headers[] = {
"HGamTools/HGamCutflowAndMxAOD.h",
"HGamTools/HGamExample.h",
"HGamTools/HggTwoSidedCBPdf.h",
"HGamTools/HGamYieldExample.h",
"HGamTools/SkimAndSlim.h",
"HGamTools/DumpNTUP.h",
"HGamTools/BasicEventDisplayTool.h",
"HGamTools/HGamEventDisplay.h",
"HGamTools/HGamSimpleExample.h",
"HGamTools/XSecSkimAndSlim.h",
"HGamTools/PDFTool.h",
"HGamTools/HGamYieldExample.h",
"HGamTools/SkimAndSlim.h",
"HGamTools/DumpNTUP.h",
"HGamTools/BasicEventDisplayTool.h",
"HGamTools/HGamEventDisplay.h",
"HGamTools/HGamSimpleExample.h",
"HGamTools/XSecSkimAndSlim.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/FourMomUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/TriggerAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/IsolationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/RecoToolInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamTools",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/HGamAnalysisFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/FourMomUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/TriggerAnalysisInterfaces",
"/afs/cern.ch/work/g/glu/public/radZAna/source/HGamCore/TruthWeightTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/build/CMakeFiles/XGBoostBuild/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/egamma/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODHIEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/IsolationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/RecoTools/RecoToolInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.72/InstallArea/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/g/glu/public/radZAna/build/HGamCore/HGamTools/CMakeFiles/makeHGamToolsLibCintDict.ea5quh/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libHGamToolsLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$HGamTools/PDFTool.h")))  PDFTool;
class __attribute__((annotate("$clingAutoload$HGamTools/HGamCutflowAndMxAOD.h")))  HGamCutflowAndMxAOD;
class __attribute__((annotate("$clingAutoload$HGamTools/HGamExample.h")))  HGamExample;
class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGamTools/HggTwoSidedCBPdf.h")))  HggTwoSidedCBPdf;
class __attribute__((annotate("$clingAutoload$HGamTools/HGamYieldExample.h")))  HGamYieldExample;
class __attribute__((annotate("$clingAutoload$HGamTools/SkimAndSlim.h")))  SkimAndSlim;
class __attribute__((annotate("$clingAutoload$HGamTools/DumpNTUP.h")))  DumpNTUP;
class __attribute__((annotate("$clingAutoload$HGamTools/BasicEventDisplayTool.h")))  BasicEventDisplayTool;
class __attribute__((annotate("$clingAutoload$HGamTools/HGamEventDisplay.h")))  HGamEventDisplay;
class __attribute__((annotate("$clingAutoload$HGamTools/HGamSimpleExample.h")))  HGamSimpleExample;
class __attribute__((annotate("$clingAutoload$HGamTools/XSecSkimAndSlim.h")))  XSecSkimAndSlim;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libHGamToolsLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef __RELEASE__
  #define __RELEASE__ 1376840
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "HGamTools-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ HGamTools-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HGamTools/HGamCutflowAndMxAOD.h"
#include "HGamTools/HGamExample.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include "HGamTools/HGamYieldExample.h"
#include "HGamTools/SkimAndSlim.h"
#include "HGamTools/DumpNTUP.h"
#include "HGamTools/BasicEventDisplayTool.h"
#include "HGamTools/HGamEventDisplay.h"
#include "HGamTools/HGamSimpleExample.h"
#include "HGamTools/XSecSkimAndSlim.h"
#include "HGamTools/PDFTool.h"
#include "HGamTools/HGamYieldExample.h"
#include "HGamTools/SkimAndSlim.h"
#include "HGamTools/DumpNTUP.h"
#include "HGamTools/BasicEventDisplayTool.h"
#include "HGamTools/HGamEventDisplay.h"
#include "HGamTools/HGamSimpleExample.h"
#include "HGamTools/XSecSkimAndSlim.h"
#include <HGamTools/PDFTool.h>

#include "HGamTools/HGamCutflowAndMxAOD.h"
#include "HGamTools/HGamExample.h"
#include "HGamTools/HggTwoSidedCBPdf.h"
#include <HGamTools/HGamYieldExample.h>
#include <HGamTools/SkimAndSlim.h>
#include <HGamTools/DumpNTUP.h>
#include <HGamTools/BasicEventDisplayTool.h>
#include <HGamTools/HGamEventDisplay.h>
#include <HGamTools/HGamSimpleExample.h>

#include <HGamTools/XSecSkimAndSlim.h>

#ifdef __CINT__

  #pragma link off all globals;
  #pragma link off all classes;
  #pragma link off all functions;
  #pragma link C++ nestedclass;

  #pragma link C++ class HGamCutflowAndMxAOD+;
  #pragma link C++ class HGamExample+;
  #pragma link C++ class HggTwoSidedCBPdf+;
  #pragma link C++ class HGamYieldExample+;
  #pragma link C++ class SkimAndSlim+;
  #pragma link C++ class DumpNTUP+;
  #pragma link C++ class BasicEventDisplayTool+;
  #pragma link C++ class HGamEventDisplay+;
  #pragma link C++ class HGamSimpleExample+;

  #pragma link C++ class XSecSkimAndSlim+;

#endif

#ifdef __CINT__
  #pragma link C++ class PDFTool+;
#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"BasicEventDisplayTool", payloadCode, "@",
"DumpNTUP", payloadCode, "@",
"HGamCutflowAndMxAOD", payloadCode, "@",
"HGamEventDisplay", payloadCode, "@",
"HGamExample", payloadCode, "@",
"HGamSimpleExample", payloadCode, "@",
"HGamYieldExample", payloadCode, "@",
"HggTwoSidedCBPdf", payloadCode, "@",
"PDFTool", payloadCode, "@",
"SkimAndSlim", payloadCode, "@",
"XSecSkimAndSlim", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libHGamToolsLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libHGamToolsLib_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libHGamToolsLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libHGamToolsLib() {
  TriggerDictionaryInitialization_libHGamToolsLib_Impl();
}
